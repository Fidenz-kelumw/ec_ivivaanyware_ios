//
//  AppDelegate.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import OLCOrm
import Fabric
import Crashlytics
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import Reachability
import GoogleMaps
//import FLEX

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var connectivityAlert: UIAlertController?
    let reachability = Reachability()!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        //Fabric Integration
        Fabric.with([Crashlytics.self])
        //FLEXManager.shared().showExplorer()
        GMSServices.provideAPIKey(GOOGLE_MAP_API_KEY)
        
        UITextField.appearance().keyboardAppearance = UIKeyboardAppearance.dark
        
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = ThemeBackgroundColor
        UINavigationBar.appearance().tintColor = ThemeTextColor
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        let textAttributes = [
            NSForegroundColorAttributeName: ThemeTextColor,
            NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
        ]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UIBarButtonItem.appearance().setTitleTextAttributes(textAttributes, for: UIControlState())
        
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name:Notification.Name.reachabilityChanged , object: reachability)
        do {
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
        ////////////////////////////////////////////////////////////////////////
        // Firebase
        ////////////////////////////////////////////////////////////////////////
        
        FirebaseApp.configure()
        //create the notificationCenter
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            // set the type as sound or badge
            center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
                // Enable or disable features based on authorization
                
            }
            application.registerForRemoteNotifications()
        } else {
            // Fallback on earlier versions
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: NSNotification.Name.kFIRMessagingRegistrationTokenRefreshNotification, object: nil)
        
        //self.checkNotifications(application, launchOptions: launchOptions)
        
        
        
        let orm: OLCOrm = OLCOrm.databaseName("SmartFM.sqlite", version: 105, enableDebug: false)
        
        orm.makeTable(type(of: Defaults()))
        orm.makeTable(type(of: Building()))
        orm.makeTable(type(of: ObjType()))
        orm.makeTable(type(of: Stage()))
        orm.makeTable(type(of: Tab()))
        orm.makeTable(type(of: Role()))
        orm.makeTable(type(of: SyncTime()))
        orm.makeTable(type(of: Filter()))
        orm.makeTable(type(of: Object()))
        orm.makeTable(type(of: ObjectInformation()))
        orm.makeTable(type(of: ObjectInfo()))
        orm.makeTable(type(of: DefaultData()))
        orm.makeTable(type(of: OptionsList()))
        orm.makeTable(type(of: Action()))
        orm.makeTable(type(of: PageLink()))
        orm.makeTable(type(of: FieldData()))
        orm.makeTable(type(of: Member()))
        orm.makeTable(type(of: MemberUser()))
        orm.makeTable(type(of: CheckItem()))
        orm.makeTable(type(of: MessageMain()))
        orm.makeTable(type(of: Message()))
        orm.makeTable(type(of: MessageUser()))
        orm.makeTable(type(of: Layout()))
        orm.makeTable(type(of: LayoutPin()))
        orm.makeTable(type(of: DroppedPin()))
        orm.makeTable(type(of: Attachment()))
        orm.makeTable(type(of: UploadedUser()))
        orm.makeTable(type(of: AttachmentsMain()))
        orm.makeTable(type(of: AttachmentType()))
        orm.makeTable(type(of: PushMessage()))
        orm.makeTable(type(of: HomeDefaultData()))
        orm.makeTable(type(of: DefDataValue()))
        orm.makeTable(type(of: SubObject()))
        orm.makeTable(type(of: MessageType()))
        orm.makeTable(type(of: ObjectURL()))
        orm.makeTable(type(of: ObjectItem()))
        orm.makeTable(type(of: ValueTypeData()))
        orm.makeTable(type(of: SwitchTypeData()))
        orm.makeTable(type(of: ButtonsTypeData()))
        orm.makeTable(type(of: SubObjectsMain()))
        orm.makeTable(type(of: SubObject()))
        orm.makeTable(type(of: SubObjectFilter()))
        orm.makeTable(type(of: SubObjectAction()))
        orm.makeTable(type(of: SubObjectDefaultData()))
        orm.makeTable(type(of: SubObjectOptionsList()))
        orm.makeTable(type(of: PinLabel()))
        orm.makeTable(type(of: Spot()))
        
        //Check SSL
        if let ssl = UserManager().getSSL() {
            
            if ssl == "1" {
                
                Protocol = "https"
                
            } else {
                
                Protocol = "http"
            }
        }
        
        ///Clearing background sync times
        if let times = SyncTime.__where("EntityKey='\(EntityKey.BackgroundEnterTime.rawValue)'", sortBy: "Id", accending: true) as? [SyncTime] {
            
            for time in times {
                time.delete()
            }
        }
        
        Messaging.messaging().delegate = self
        
        // start location tracker
        self.tryBackgroundLocationManagerToContinuousCalls()
        
        return true
    }
    
    internal var shouldRotate = false
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return shouldRotate ? .allButUpsideDown : .portrait
    }
    
    //--------------------------------------
    // MARK: Push Notifications
    //--------------------------------------
    func tokenRefreshNotification(_ notification: Notification) {
        
        self.notifyTokenChange()
    }
    
    func tryBackgroundLocationManagerToContinuousCalls (){
        if let defaults = Defaults.all() as? [Defaults], defaults.count > 0 {
            let defaults1=defaults[0]
            if let status = defaults1.isUserLocationUpdateON, status == "1" {
                BackgroundLocationService.sharedInstance.stopMonitoringSignificantLocationChanges()
                BackgroundLocationService.sharedInstance.startMonitoringSignificantLocationChanges()
            }
        }
    }
    
    func notifyTokenChange() {
        UserManager().setFirebaseTokenStatus(false)
        Messaging.messaging().subscribe(toTopic: "/topics/all")
        if let token = InstanceID.instanceID().token() {
            NSLog("token changed to - \(token)")
            
            APIClient().updateFirebaseToken(token, completion: { (success) in
                
                if success {
                    UserManager().setFirebaseTokenStatus(true)
                    NSLog("Token successfully updated")
                } else {
                    UserManager().setFirebaseTokenStatus(false)
                    NSLog("Token updating failed")
                }
            })
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        //Firebase
        Messaging.messaging().apnsToken = deviceToken
        
    }
    

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        let er = error as NSError
        if er.code == 3010 {
            NSLog("Push notifications are not supported in the iOS Simulator.")
        } else {
            NSLog("application:didFailToRegisterForRemoteNotificationsWithError: \(error.localizedDescription)")
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("\(userInfo)")
        if #available(iOS 10.0, *) {
            
            _ = self.saveNewNotification(userInfo: userInfo)
            self.updateBadge()
            completionHandler(UIBackgroundFetchResult.newData)
            
        } else {

            self.openNotification(application, notificationOptions: userInfo)
            
            completionHandler(UIBackgroundFetchResult.newData)
        }
    }
    
    func checkNotifications(_ application: UIApplication, launchOptions: [AnyHashable: Any]?) {
        
        if #available(iOS 10.0, *) {
            
        } else {
            
            if let options = launchOptions {
                if let notificationOptions = options[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
                    
                    self.openNotification(application, notificationOptions: notificationOptions)
                }
                
            }
        }
    }
    
    fileprivate func getLocation(acquired: @escaping ((Double,Double)?) -> Void) {

        SingleLocationUpdate.shared.start()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 15) {
            SingleLocationUpdate.shared.stop()
            acquired(SingleLocationUpdate.shared.getCurrentLocation())
        }
    }
    
    fileprivate func openNotification(_ application: UIApplication, notificationOptions: [AnyHashable: Any]) {
    
        if let notes = PushMessage.all() as? [PushMessage] {
            let newId = notificationOptions["gcm.message_id"] as? String
            var isAlreadySaved = false
            var savedpush: PushMessage?
            
            for note in notes {
                
                if newId == note.ParsePushId {
                    
                    isAlreadySaved = true
                    savedpush = note
                    break
                }
            }
            
            if !isAlreadySaved {
                self.processPush(application, withOptions:notificationOptions)
            } else {
                if let push = savedpush , application.applicationState == UIApplicationState.inactive {
                    self.openObjectDetails(push)
                }
            }
            
            
        } else {
            self.processPush(application, withOptions:notificationOptions)
        }
    }
    
    fileprivate func userInfoToPushMessage(_ userInfo: [AnyHashable: Any]) -> PushMessage {
        
        let pushMessage = PushMessage()
        if let aps = userInfo["aps"] as? NSDictionary {
            pushMessage.Message = aps.value(forKey: "alert") as? String
        }
       
        pushMessage.GetLocation = userInfo["LK"] as? String
        pushMessage.ParsePushId = userInfo["gcm.message_id"] as? String
        pushMessage.MsgFrom = userInfo["F"] as? String
        pushMessage.ObjectKey = userInfo["OK"] as? String
        pushMessage.ObjectType = userInfo["OT"] as? String
        pushMessage.isRead = "0"
        pushMessage.ReceivedAt = Date()
        let pushId = pushMessage.saveAndGetId()
        pushMessage.Id = pushId
        
        
        return pushMessage
    }
    
    fileprivate func getLocationNotificationContent(_ userInfo: [AnyHashable: Any]) -> LocationNotificationMessage {
        
        let content = LocationNotificationMessage()
        
        content.objectKey = userInfo["objectKey"] as? String
        content.objectType = userInfo["objectType"] as? String
        content.spotsContent = userInfo[NOTIFICATION_DATA_SPOTS] as? String
        
        return content
    }
    
    fileprivate func processPush(_ application: UIApplication, withOptions userInfo: [AnyHashable: Any]) {
        
        let pushMessage = self.userInfoToPushMessage(userInfo)
        
        self.deleteInbox()
        
        self.updateBadge()
        
        
        if application.applicationState == UIApplicationState.inactive {
            self.openObjectDetails(pushMessage)
        }
        
    }
    
    fileprivate func updateBadge() {
        UIApplication.shared.applicationIconBadgeNumber = PushMessage.__where("isRead='0'", sortBy: "Id", accending: true).count
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "messageReceived"), object: nil)
    }
    
    fileprivate func deleteInbox() {
        if let pushmessages = PushMessage.all() as? [PushMessage] {
            
            var sorted = pushmessages.sorted(by: { (first, second) -> Bool in
                
                if let id1=first.Id, let id2=second.Id {
                    
                    return id1.compare(id2) == ComparisonResult.orderedDescending
                    
                } else {
                    return true
                }
            })
            
            if let setting = Defaults.all().first as? Defaults, let inboxSize = setting.InboxSize {
                
                if let max = Int(inboxSize) {
                    
                    while sorted.count > max {
                        let msg = sorted.last
                        msg?.delete()
                        sorted.remove(at: sorted.count-1)
                    }
                }
                
            }
        }
    }
    
    fileprivate func openObjectDetails(_ pushNote: PushMessage) {

        if let isLocationNeeded = pushNote.GetLocation, isLocationNeeded == "1" {
            self.getLocation(acquired: {(location) in
                
                if let latS = location?.0, let lonS = location?.1 {
                    let lat = "\(String(describing: latS))"
                    let lon = "\(String(describing: lonS))"
                    
                    APIClient().updateUserGEOLocation(lat: lat, lon: lon, completion: { (status) in
                        if (status){
                            print("Location update from notification success")
                        } else {
                            print("Location update from notification faled")
                        }
                        
                        self.tryBackgroundLocationManagerToContinuousCalls()
                    })
                }
            })
        }
        
        if let _ = pushNote.ObjectKey, let ot = pushNote.ObjectType {
            
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                
                
                if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Splash") as? SplashViewController {
                    
                    vc.showObjectDetails = pushNote
                    
                    self.window?.rootViewController = vc
                    self.window?.alpha = 0
                    self.window?.makeKeyAndVisible()
                    
                    Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(AppDelegate.animationStopped), userInfo: nil, repeats: false)
                    UIView.setAnimationsEnabled(false)
                    UIView.beginAnimations("fadeIn", context: nil)
                    UIView.setAnimationDuration(0.5)
                    self.window?.alpha = 1
                    UIView.commitAnimations()
                }
            }
        }
    }
    
    fileprivate func openObjectDetails(_ locationMessage: LocationNotificationMessage) {
        
        if let _ = locationMessage.objectKey, let ot = locationMessage.objectType {
            
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                
                
                if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Splash") as? SplashViewController {
                    
                    vc.locationMessage = locationMessage
                    
                    self.window?.rootViewController = vc
                    self.window?.alpha = 0
                    self.window?.makeKeyAndVisible()
                    
                    Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(AppDelegate.animationStopped), userInfo: nil, repeats: false)
                    UIView.setAnimationsEnabled(false)
                    UIView.beginAnimations("fadeIn", context: nil)
                    UIView.setAnimationDuration(0.5)
                    self.window?.alpha = 1
                    UIView.commitAnimations()
                }
            }
        }
    }
    
    func animationStopped() {
        
        UIView.setAnimationsEnabled(true)
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called{} instead of applicationWillTerminate: when the user quits.
        
        let syncTime = SyncTime()
        syncTime.EntityKey = EntityKey.BackgroundEnterTime.rawValue
        syncTime.UpdatedAt = Date()
        syncTime.save()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        if let bgEnteredTime = SyncTime.__where("EntityKey='\(EntityKey.BackgroundEnterTime.rawValue)'", sortBy: "Id", accending: true).first as? SyncTime {
            
            if let bgtime = bgEnteredTime.UpdatedAt {
                let interval = sessionExpTime
                if interval <= Double(-1 * bgtime.timeIntervalSinceNow) {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "sessionExpired"), object: self)
                }
            }
        }
        
        if let times = SyncTime.__where("EntityKey='\(EntityKey.BackgroundEnterTime.rawValue)'", sortBy: "Id", accending: true) as? [SyncTime] {
            
            for time in times {
                time.delete()
            }
        }
        
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if !UserManager().isFirebaseTokenSent() {
            NSLog("Notifying token")
            self.notifyTokenChange()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow) {
        self.connectivityAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let alert = connectivityAlert {
            alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
            UIViewController.topMostViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .cellular:
            print("[REACHABILITY] : Reachable via Cellular")
            
            if let alert = self.connectivityAlert {
                alert.dismiss(animated: true, completion: nil)
            }
        case .wifi:
            print("[REACHABILITY] : Reachable via WiFi")
            
            if let alert = self.connectivityAlert {
                alert.dismiss(animated: true, completion: nil)
            }
        case .none:
            print("[REACHABILITY] : Network not reachable")
            
            showAlertAppDelegate(title: CONNECTIVITY_ALERT_TITLE, message: CONNECTIVITY_ALERT_BODY, buttonTitle: "OK", window: self.window!)
        }
    }

}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // The action identifier that the user chose:
        // * UNNotificationDismissActionIdentifier if the user dismissed the notification
        // * UNNotificationDefaultActionIdentifier if the user opened the application from the notification
        // * the identifier for a registered UNNotificationAction for other actions
        
        let userInfo = response.notification.request.content.userInfo
        
        let savedPush = self.saveNewNotification(userInfo: userInfo)
        
        self.updateBadge()
        
        
        if response.actionIdentifier == UNNotificationDismissActionIdentifier {
            
        } else if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
            
            if let open = savedPush {
                self.openObjectDetails(open)
            } else if userInfo[NOTIFICATION_DATA_SPOTS] != nil {
                self.openObjectDetails(self.getLocationNotificationContent(userInfo))
            }
            
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        let userInfo = notification.request.content.userInfo
        
        if userInfo["gcm.message_id"] != nil {
            _ = self.saveNewNotification(userInfo: userInfo)
            self.updateBadge()
            
            let pushNote = userInfoToPushMessage2(userInfo)
            if let isLocationNeeded = pushNote.GetLocation, isLocationNeeded == "1" {
                self.getLocation(acquired: {(location) in
                    
                    if let latS = location?.0, let lonS = location?.1 {
                        let lat = "\(String(describing: latS))"
                        let lon = "\(String(describing: lonS))"
                        
                        APIClient().updateUserGEOLocation(lat: lat, lon: lon, completion: { (status) in
                            if (status){
                                print("Location update from notification success")
                            } else {
                                print("Location update from notification faled")
                            }
                            
                            self.tryBackgroundLocationManagerToContinuousCalls()
                        })
                    }
                })
            }
            
        } else {
            return completionHandler(UNNotificationPresentationOptions.alert)
        }
        
    }
    
    func saveNewNotification(userInfo: [AnyHashable: Any]) -> PushMessage? {
        
        let pushMessage = userInfoToPushMessage2(userInfo)
        var savedPush: PushMessage? = nil
        if userInfo["gcm.message_id"] != nil {
            if let push = checkPushSaved(newId: userInfo["gcm.message_id"] as! String) {
                savedPush = push
            } else {
                savedPush = self.savePush(push: pushMessage)
            }
        }
        
        if userInfo["LK"] !=  nil {
            savedPush?.GetLocation = userInfo["LK"] as! String
        }
        self.deleteInbox()
        
        return savedPush
        
    }
    
    func checkPushSaved(newId: String) -> PushMessage? {
        
        var savedpush: PushMessage?
        
        if let notes = PushMessage.all() as? [PushMessage] {
            for note in notes {
                
                if newId == note.ParsePushId {
                    
                    savedpush = note
                    break
                }
            }
        }
        
        return savedpush
    }
    
    func savePush(push: PushMessage) -> PushMessage {
        
        let id = push.saveAndGetId()
        push.Id = id
        return push
    }
    
    fileprivate func userInfoToPushMessage2(_ userInfo: [AnyHashable: Any]) -> PushMessage {
        
        let pushMessage = PushMessage()
        if let aps = userInfo["aps"] as? NSDictionary {
            pushMessage.Message = aps.value(forKey: "alert") as? String
        }
        //pushMessage.Message = userInfo["alert"] as? String
        pushMessage.GetLocation = userInfo["LK"] as? String
        pushMessage.ParsePushId = userInfo["gcm.message_id"] as? String
        pushMessage.MsgFrom = userInfo["F"] as? String
        pushMessage.ObjectKey = userInfo["OK"] as? String
        pushMessage.ObjectType = userInfo["OT"] as? String
        pushMessage.isRead = "0"
        pushMessage.ReceivedAt = Date()
        
        return pushMessage
    }
    
    fileprivate func handlePush(_ application: UIApplication, notificationOptions: [AnyHashable: Any]) {
        
        
        if (PushMessage.all() as? [PushMessage]) != nil {
            
            var isAlreadySaved = false
            var savedPush: PushMessage? = nil
            if let newId = notificationOptions["gcm.message_id"] as? String {
                savedPush = self.checkPushSaved(newId: newId)
                isAlreadySaved = savedPush == nil ? false : true
            }
            
            if !isAlreadySaved {
                self.processPush(application, withOptions:notificationOptions)
            } else {
                if let push = savedPush , application.applicationState == UIApplicationState.inactive {
                    self.openObjectDetails(push)
                }
            }
            
            
        } else {
            self.processPush(application, withOptions:notificationOptions)
        }
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        if let token = Messaging.messaging().fcmToken {
            NSLog("2. token changed to - \(token)")
            
        }
        self.notifyTokenChange()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
}

extension UIViewController {
    class func topMostViewController() -> UIViewController? {
        return UIViewController.topViewControllerForRoot(rootViewController: UIApplication.shared.keyWindow?.rootViewController)
    }
    
    class func topViewControllerForRoot(rootViewController:UIViewController?) -> UIViewController? {
        guard let rootViewController = rootViewController else {
            return nil
        }
        
        guard let presented = rootViewController.presentedViewController else {
            return rootViewController
        }
        
        switch presented {
        case is UINavigationController:
            let navigationController:UINavigationController = presented as! UINavigationController
            return UIViewController.topViewControllerForRoot(rootViewController: navigationController.viewControllers.last)
            
        case is UITabBarController:
            let tabBarController:UITabBarController = presented as! UITabBarController
            return UIViewController.topViewControllerForRoot(rootViewController: tabBarController.selectedViewController)
            
        default:
            return UIViewController.topViewControllerForRoot(rootViewController: presented)
        }
    }
}
