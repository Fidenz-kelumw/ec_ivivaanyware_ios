//
//  ActionTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/29/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ActionCellDelegate {
    func showConfirmation(_ result: @escaping (Bool)->Void)
    func showError(_ message: String)
    func showSuccess(_ message: String)
    func showProcessingAlert(_ completion: @escaping ()->Void)
    func hideProcessing()
}

protocol PageActionDelegate {
    
    func performAction(_ pageAction: PageAction)
}

class ActionTableViewCell: UITableViewCell {

    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var frameBottom: UIView!
    @IBOutlet weak var frameMiddle: UIView!
    @IBOutlet weak var frameTop: UIView!
    @IBOutlet weak var frameSingle: UIView!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var delegate: ActionCellDelegate?
    var pageActionDelegate: PageActionDelegate?
    
    fileprivate var action: Action?
    fileprivate var objectType: String?
    fileprivate var objectKey: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.constBottom.priority = UILayoutPriority.init(250)
        self.frameTop.layer.borderWidth = 1
        self.frameTop.layer.borderColor = FrameBorderColor.cgColor
        self.frameTop.layer.cornerRadius = 5
        
        self.frameBottom.layer.borderWidth = 1
        self.frameBottom.layer.borderColor = FrameBorderColor.cgColor
        self.frameBottom.layer.cornerRadius = 5
        
        self.frameMiddle.layer.borderWidth = 1
        self.frameMiddle.layer.borderColor = FrameBorderColor.cgColor
        self.frameMiddle.layer.cornerRadius = 5
        
        self.frameSingle.layer.borderWidth = 1
        self.frameSingle.layer.borderColor = FrameBorderColor.cgColor
        self.frameSingle.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ action: Action, objectType: String, objectKey: String, cellPosition: CellPosition) {
        
        DispatchQueue.main.async {
            self.action = action
            self.objectType = objectType
            self.objectKey = objectKey
            
            if let name = action.ActionName {
                self.btnAction.setTitle(name, for: UIControlState())
                self.setEnable(action.Enabled=="1" ? true : false)
            }
            
            self.frameMiddle.isHidden = true
            self.frameTop.isHidden = true
            self.frameBottom.isHidden = true
            self.frameSingle.isHidden = true
            
            var contentViewHeight: CGFloat = 70.0
            if cellPosition == CellPosition.middle {
                contentViewHeight = 60
            } else if cellPosition == CellPosition.single {
                contentViewHeight = 80
            }
            let autoHeight = contentViewHeight - 10.0 - self.btnAction.frame.size.height
            
            if cellPosition == CellPosition.single {
                self.frameSingle.isHidden = false
                self.constTop.constant = 20.0
                self.constBottom.constant = autoHeight
            } else if cellPosition == CellPosition.bottom {
                self.frameBottom.isHidden = false
                self.constBottom.constant = autoHeight
                self.constTop.constant = 10.0
            } else if cellPosition == CellPosition.top {
                self.frameTop.isHidden = false
                self.constBottom.constant = 10.0
                self.constTop.constant = autoHeight
            } else if cellPosition == CellPosition.middle {
                self.frameMiddle.isHidden = false
                self.constBottom.constant = 10.0
                self.constTop.constant = autoHeight
            }
            self.layoutIfNeeded()
        }
    }
    
    fileprivate func setEnable(_ enabled: Bool) {
        
        self.btnAction.backgroundColor = enabled ? EnabledButtonColor : DisabledButtonColor
        self.btnAction.setTitleColor(enabled ? EnabledButtonTextColor : DisabledButtonTextColor, for: UIControlState())
        self.btnAction.isEnabled = enabled
    }
    
    fileprivate func executeAction(_ action:Action, objectType:String, objectKey:String) {
        
        self.delegate?.showProcessingAlert({
            
            APIClient().executeAction(action, objectType: objectType, objectKey: objectKey, completion: { (success, message, pageAction) in
                
                self.delegate?.hideProcessing()
                
                if success {
                    
                    if let msg = message {
                        self.delegate?.showSuccess(msg)
                    } else {
                        self.delegate?.showSuccess("Success")
                    }
                    
                } else {
                    
                    if let msg = message {
                        self.delegate?.showError(msg)
                    } else {
                        self.delegate?.showError("Failed")
                    }
                }
                
                self.pageActionDelegate?.performAction(pageAction)
            })
        })
    }

    //MARK: - Events
    @IBAction func actionPressed(_ sender: AnyObject) {
        
        if let action = self.action, let ot = self.objectType, let ok = self.objectKey {
            
            if action.Confirmation == "1" {
                self.delegate?.showConfirmation({ (result) in
                    
                    if result {
                        self.executeAction(action, objectType: ot, objectKey: ok)
                    }
                })
            } else {
                self.executeAction(action, objectType: ot, objectKey: ok)
            }
        }
    }
    
}
