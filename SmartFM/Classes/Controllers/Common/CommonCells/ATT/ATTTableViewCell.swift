//
//  ATTTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 12/29/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation

@objc protocol ATTCellDelegate {
    
    @objc optional func selectPhoto()
    @objc optional func takePhoto()
    @objc optional func addSignature()
    @objc optional func presentViewController(_ vc: UIViewController)
    @objc optional func showPopup(_ vc: UIViewController)
    @objc optional func scrollViewOffset(forItem item: AnyObject, offset: CGPoint)
}

protocol ATTCellValuesDelegate {
    
    func setImageValues(_ cellImage: [CellAttachment], imageNames:String, forItem: AnyObject?)
    func addTask()
    func taskFinished(_ success: Bool)
    func removeFailed()
}



class ATTTableViewCell: UITableViewCell, GenericFormDelegate, ImageCellDelegate, FormTypeTableViewCellDelegate {
    
    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet weak var collectionViewButtons: UICollectionView!
    @IBOutlet weak var constButtonsCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet var lblMessage: UILabel!
    
    //public variables
    var delegate: ATTCellDelegate?
    var imagesCellValuesDelegate: ATTCellValuesDelegate?
    var formTypeDelegate: FormTypeTableViewCellDelegate?
    var formDelegate: GenericFormDelegate?
    var attachments: [CellAttachment] = []
    var inputButtons: [String] = []
    var editingEnabled: Bool = true
    var removeEnabled: Bool = true
    var selectedIndexOfATT: Int = -1
    
    fileprivate var item: AnyObject?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionViewButtons.dataSource = self
        self.collectionViewButtons.delegate = self
        
        if self.editingEnabled {
            self.constButtonHeight.constant = 60
        } else {
            self.constButtonHeight.constant = 0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    //MARK: - Private Methods
    
    fileprivate func constructName() -> String {
        var imageName = UUID().uuidString
        if let userKey = UserManager().getUserKey() {
            imageName = "\(userKey)_\(imageName)"
        }
        imageName = "\(imageName)_\(Date().timeIntervalSince1970)"
        
        return imageName
    }
    
    fileprivate func addVideo(path: NSURL) {
        var vidPath = path
        let videoName = self.constructName()
        
        if let newPath = FileHandler.renameFile(atURL: path as URL, to: "\(videoName).mov") {
            vidPath = newPath as NSURL
        }
        
        let cellVideo = CellAttachment()
        cellVideo.name = "\(videoName).mov"
        cellVideo.folder = vidPath.deletingLastPathComponent?.absoluteString
        cellVideo.isLocal = true
        cellVideo.isUploading = true
        cellVideo.mediaType = MediaType.VID
        self.attachments.append(cellVideo)
        
        let ip = IndexPath(item: self.attachments.count-1, section: 0)
        self.collectionView.insertItems(at: [ip])
        self.collectionView.scrollToItem(at: ip, at: UICollectionViewScrollPosition.right, animated: true)
        
        self.imagesCellValuesDelegate?.setImageValues(self.attachments, imageNames: self.cellImagesToImageNames(self.attachments), forItem: self.item)
        
        if let objectInfo = self.item as? ObjectInfo, let uploadFolder = objectInfo.UploadFolder {
            self.imagesCellValuesDelegate?.addTask()
            
            APIClient().uploadVideo(url: vidPath as URL, name: "\(videoName).mov", uploadFolder: uploadFolder, completion: { (success) in
                
                if success {
                    cellVideo.isUploading = false
                    cellVideo.isFailedUploading = false
                    self.collectionView.reloadItems(at: [ip])
                    self.imagesCellValuesDelegate?.taskFinished(true)
                } else {
                    cellVideo.isUploading = false
                    self.removeImage(withCellImage: cellVideo)
                    self.imagesCellValuesDelegate?.taskFinished(false)
                    self.formDelegate?.showError?("Uploading video failed.")
                }
            })
            
        } else {
            cellVideo.isUploading = false
            self.removeImage(withCellImage: cellVideo)
        }
    }
    
    fileprivate func addPhoto(_ image: UIImage) {
        
        let imageName = self.constructName()
        
        
        let imageHandler = ImageHandler()
        
        imageHandler.saveImageToFolder(image, withName: imageName, format: "jpg") { (savedImage, path) in
            
            if let folderPath = path {
                let cellImage = CellAttachment()
                cellImage.name = "\(imageName).jpg"
                cellImage.folder = folderPath
                cellImage.isLocal = true
                cellImage.image = savedImage
                cellImage.isUploading = true
                cellImage.mediaType = MediaType.IMG
                self.attachments.append(cellImage)
                
                let ip = IndexPath(item: self.attachments.count-1, section: 0)
                self.collectionView.insertItems(at: [ip])
                self.collectionView.scrollToItem(at: ip, at: UICollectionViewScrollPosition.right, animated: true)
                
                self.imagesCellValuesDelegate?.setImageValues(self.attachments, imageNames: self.cellImagesToImageNames(self.attachments), forItem: self.item)
                
                
                //Uploading
                if let objectInfo = self.item as? ObjectInfo, let uploadFolder = objectInfo.UploadFolder, let uploadImage = UIImage(contentsOfFile: "\(folderPath)/\(imageName).jpg") {
                    self.imagesCellValuesDelegate?.addTask()
                    
                    APIClient().uploadImage(uploadImage, name: "\(imageName).jpg", uploadFolder: uploadFolder) { (success) in
                        
                        if success {
                            cellImage.isUploading = false
                            cellImage.isFailedUploading = false
                            self.collectionView.reloadItems(at: [ip])
                            self.imagesCellValuesDelegate?.taskFinished(true)
                            
                            if self.selectedIndexOfATT > -1 {
                                let oldCellAttachment = self.attachments[self.selectedIndexOfATT]
                                self.removeImage(withCellImage: oldCellAttachment)
                                self.selectedIndexOfATT = -1
                            }
        
                        } else {
                            cellImage.isUploading = false
                            //                            cellImage.isFailedUploading = true
                            //                            self.collectionView.reloadItemsAtIndexPaths([ip])
                            self.removeImage(withCellImage: cellImage)
                            self.imagesCellValuesDelegate?.taskFinished(false)
                            self.formDelegate?.showError?("Uploading image failed.")
                        }
                    }
                }
            }
        }
        
    }
    
    fileprivate func addAudio(path: URL) {
        
        let cellAudio = CellAttachment()
        cellAudio.name = path.lastPathComponent
        cellAudio.folder = path.deletingLastPathComponent().absoluteString
        cellAudio.isLocal = true
        cellAudio.isUploading = true
        cellAudio.mediaType = MediaType.AUD
        self.attachments.append(cellAudio)
        
        let ip = IndexPath(item: self.attachments.count-1, section: 0)
        self.collectionView.insertItems(at: [ip])
        self.collectionView.scrollToItem(at: ip, at: UICollectionViewScrollPosition.right, animated: true)
        
        self.imagesCellValuesDelegate?.setImageValues(self.attachments, imageNames: self.cellImagesToImageNames(self.attachments), forItem: self.item)
        
        if let objectInfo = self.item as? ObjectInfo, let uploadFolder = objectInfo.UploadFolder, let audioName = cellAudio.name {
            self.imagesCellValuesDelegate?.addTask()
            
            APIClient().uploadAudio(url: path as URL, name: "\(audioName)", uploadFolder: uploadFolder, completion: { (success) in
                
                cellAudio.isUploading = false
                if success {
                    cellAudio.isFailedUploading = false
                    self.collectionView.reloadItems(at: [ip])
                    self.imagesCellValuesDelegate?.taskFinished(true)
                } else {
                    self.removeImage(withCellImage: cellAudio)
                    self.imagesCellValuesDelegate?.taskFinished(false)
                    self.formDelegate?.showError?("Uploading audio failed.")
                }
            })
            
        } else {
            cellAudio.isUploading = false
            self.removeImage(withCellImage: cellAudio)
        }
    }
    
    fileprivate func cellImagesToImageNames(_ cellImages :[CellAttachment]) -> String {
        
        var imageNames = ""
        for image in cellImages {
            if let name = image.name {
                imageNames = "\(imageNames)\(name),"
            }
        }
        
        imageNames = String(imageNames.characters.dropLast())
        return imageNames
    }
    
    
    
    //MARK: - Events
    
    func takePicture() {
        
        self.delegate?.takePhoto?()
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.camera
        self.delegate?.presentViewController?(picker)
    }
    
    func takeVideo() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.camera
        picker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
        self.delegate?.presentViewController?(picker)
        
    }
    
    func selectPicture() {
        
        self.delegate?.selectPhoto?()
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String, kUTTypeVideo as String]
        self.delegate?.presentViewController?(picker)
    }
    
    func signature() {
        
        self.delegate?.addSignature?()
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DrawVc") as? UINavigationController {
            if let signatureVc = vc.viewControllers.first as? DrawSignatureViewController {
                signatureVc.delegate = self
            }
            self.delegate?.presentViewController?(vc)
        }
    }
    
    func recordAudio() {
        
        if let vc = UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecordAudioVC") as? UINavigationController {
            if let vc = vc.viewControllers.first as? RecordAudioViewController {
                vc.delegate = self
            }
            self.delegate?.showPopup?(vc)
        }
    }
    
    func getIndex(_ imageName: String) -> Int? {
        
        var index: Int? = nil
        for n in 0..<self.attachments.count {
            
            if self.attachments[n].name == imageName {
                index = n
            }
        }
        
        return index
    }
    
    func setValue(_ objInfo: ObjectInfo) {
        
        self.item = objInfo
        if let inputs = objInfo.InputModes {
            self.inputButtons = inputs
        }
        
        self.editingEnabled = objInfo.Editable == "1"
        self.removeEnabled = (objInfo.DisableDelete != "1") && self.editingEnabled
        
        if self.editingEnabled {
            self.constButtonHeight.constant = 60
        } else {
            self.constButtonHeight.constant = 0
        }
        
        layoutIfNeeded()
        
        
        if let fName = objInfo.FieldName {
            self.fieldName.text = fName
        }
        
        if let imgs = objInfo.defaultData?.cellImages {
            self.attachments = imgs
        }
        
        
        self.collectionView.reloadData()
    }
    
    func setOffset(_ offset: CGPoint?) {
        
        if let os = offset {
            self.collectionView.contentOffset = os
        }
    }
    
    //MARK: - Delegate Methods
    
    func getValue() -> AnyObject {
        
        return self.cellImagesToImageNames(self.attachments) as AnyObject
    }
    
    func removeImage(withCellImage cellImage: CellAttachment) {
        
        if let name = cellImage.name {
            if let index = self.getIndex(name) {
                
                if cellImage.isFailedUploading {
                    
                    self.imagesCellValuesDelegate?.removeFailed()
                }
                
                self.attachments.remove(at: index)
                self.collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
                
                self.imagesCellValuesDelegate?.setImageValues(self.attachments, imageNames: self.cellImagesToImageNames(self.attachments), forItem: self.item)
            }
        }
    }

    
}


extension ATTTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionViewButtons {
            return self.inputButtons.count
        } else {
            if self.attachments.count > 0 {
                lblMessage.isHidden = true
            } else {
                lblMessage.isHidden = false
            }
            return self.attachments.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            
            let cellImage = self.attachments[(indexPath as NSIndexPath).item]
            
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageColCell", for: indexPath) as! ImageCollectionViewCell
            
            if cellImage.mediaType == MediaType.VID {
                
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoColCell", for: indexPath) as! ImageCollectionViewCell
                
            } else if cellImage.mediaType == MediaType.AUD {
                
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioColCell", for: indexPath) as! ImageCollectionViewCell
            }
            
            cell.delegate = self
            cell.setRemoveEnabled(self.removeEnabled)
            cell.imageView.tag = (indexPath as NSIndexPath).item
            
            cell.cellImage = cellImage
            
            let defaultimage = UIImage(named: "Icon_no_photo")
            cell.imageView.image = defaultimage
            cell.largeImage = nil
            
            if cellImage.mediaType == MediaType.AUD {
            
                cell.imageView.image = nil
                
            }
            
            //check isuploading
            if cellImage.isUploading {
                cell.viewUploading.isHidden = false
            } else {
                cell.viewUploading.isHidden = true
            }
            
            //check uploading failed
            if cellImage.isFailedUploading {
                cell.viewUploadingFailed.isHidden = false
            } else {
                cell.viewUploadingFailed.isHidden = true
            }
            
            
            //set media resources
            if cellImage.mediaType == MediaType.VID, let acc = UserManager().getAccount(), let path = cellImage.folder, let name = cellImage.name, let url = URL(string: "\(Protocol)://\(acc)\(path)/\(name)") {
                
                cell.videoUrl = url
                
            } else if cellImage.mediaType == MediaType.AUD, let acc = UserManager().getAccount(), let path = cellImage.folder, let name = cellImage.name, let url = URL(string: "\(Protocol)://\(acc)\(path)/\(name)") {
                    
                cell.audioUrl = url
            }
            
            
            if let img = cellImage.image {
                cell.imageView.image = img
                
                if cellImage.mediaType == MediaType.IMG {
                    cell.largeImage = img
                }
                
            } else if let path = cellImage.folder, let name = cellImage.name {
                
                if cellImage.isLocal {
                    
                    if cellImage.mediaType == MediaType.IMG {
                        
                        cell.imageView.image = UIImage(contentsOfFile: "\(path)/\(name)")
                        cell.largeImage = UIImage(contentsOfFile: "\(path)/\(name)")
                        
                    } else if cellImage.mediaType == MediaType.VID {
                        
                        if let url = URL(string: "\(path)/\(name)") {
                            
                            cell.videoUrl = url
                            
                            ImageHandler().asyncExtractVideoThumbnail(url: url, name: name, completion: { (image) in
                                
                                if cell.imageView.tag == (indexPath as NSIndexPath).row {
                                    
                                    cell.imageView.image = defaultimage
                                    if let img = image {
                                        cell.imageView.image = img
                                    }
                                }
                            })
                            
                        }
                    } else if cellImage.mediaType == MediaType.AUD {
                        
                        cell.audioUrl = URL(string: "\(path)/\(name)")
                    }
                    
                } else {
                    
                    if let acc = UserManager().getAccount() {
                        
                        if cellImage.mediaType == MediaType.VID, let url = URL(string: "\(Protocol)://\(acc)\(path)/\(name)") {
                            
                            ImageHandler().asyncExtractVideoThumbnail(url: url, name: name, completion: { (image) in
                                
                                if cell.imageView.tag == (indexPath as NSIndexPath).row {
                                    
                                    cell.imageView.image = defaultimage
                                    if let img = image {
                                        cell.imageView.image = img
                                    }
                                    cell.videoUrl = url
                                }
                            })
                            
                        } else if cellImage.mediaType == MediaType.IMG {
                            
                            ImageHandler().asyncImageDownload("\(Protocol)://\(acc)\(path)/\(name)", name: name) { (largeImage,image, path) in
                                
                                if cell.imageView.tag == (indexPath as NSIndexPath).row {
                                    
                                    cell.imageView.image = defaultimage
                                    if let img = image {
                                        cell.imageView.image = img
                                    }
                                    cell.largeImage = largeImage
                                }
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ATTButton", for: indexPath) as! InputButtonCollectionViewCell
            
            if let inputMode = InputMode(rawValue: self.inputButtons[indexPath.item]) {
            
                cell.setValues(inputMode: inputMode)
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collectionView {
            
            let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
            
            if cell.cellImage?.mediaType == MediaType.VID {
                
                if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AVPlayerVC") as? AVPlayerViewController, let url = cell.videoUrl {
//                    let uu = URL(string: "http://unchealthcarevideos.org/News/2009/06/ilura.cochlear.mov")!
                    
                    if let url = URL(string: "\(url)?autoct=1") {
                        let player = AVPlayer(url: url)
                        vc.player = player
                        
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                        () -> Void in
                        self.delegate?.presentViewController?(vc)
                    }
                }
                
            } else if cell.cellImage?.mediaType == MediaType.IMG {
                
                if self.editingEnabled {
                    
                    self.selectedIndexOfATT = indexPath.row
                    
                    if let nvc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditVc") as? UINavigationController, let vc = nvc.viewControllers.first as? EditImageViewController {
                        
                        if let img = cell.largeImage {
                            
                            vc.image = img
                            vc.delegate = self
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                                () -> Void in
                                self.delegate?.presentViewController?(nvc)
                            }
                        }
                    }
                    
                } else {
                    
                    if let nvc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShowImageVc") as? UINavigationController, let vc = nvc.viewControllers.first as? ViewAttachmentImageViewController {
                        
                        if let img = cell.largeImage {
                            
                            vc.image = img
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                                () -> Void in
                                self.delegate?.presentViewController?(nvc)
                            }
                        }
                    }
                }
                
                
            } else if cell.cellImage?.mediaType == MediaType.AUD {
                
                if let nvc = UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "AudioPlaybackVC") as? UINavigationController, let vc = nvc.viewControllers.first as? AudioPlaybackViewController {
                    
                    if let url = cell.audioUrl {
                        
                        vc.url = url
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                            () -> Void in
                           
                            let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                            formSheet.shouldCenterVertically = true
                            formSheet.shouldDismissOnBackgroundViewTap = true
                            let width = UIScreen.main.bounds.width - 40
                            let height: CGFloat = 200
                            formSheet.presentedFormSheetSize = CGSize(width: width, height: height)
                            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                            
                            formSheet.present(animated: true, completionHandler: nil)
                        }
                    }
                }
            }
            
        } else {
            
            let cell = collectionView.cellForItem(at: indexPath) as! InputButtonCollectionViewCell
            
            if cell.inputMode == InputMode.Photo {
                self.takePicture()
            } else if cell.inputMode == InputMode.File {
                self.selectPicture()
            } else if cell.inputMode == InputMode.Signature {
                self.signature()
            } else if cell.inputMode == InputMode.Audio {
                self.recordAudio()
            }  else if cell.inputMode == InputMode.Video {
                self.takeVideo()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionViewButtons {
            let sidebar: CGFloat = 60.0
            let size = UIScreen.main.bounds.width - sidebar - 100
            let singleSize: CGFloat = size / 5
            
            if self.inputButtons.count - 1 == indexPath.item {
                self.constButtonsCollectionViewHeight.constant = singleSize
                self.layoutIfNeeded()
            }
            
            return CGSize(width: singleSize, height: singleSize)
        } else {
            return CGSize(width: 80, height: 80)
        }
        
    }
}


extension ATTTableViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate, DrawSignatureDelegate, RecordAudioDelegate, EditImageDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if info[UIImagePickerControllerMediaType] as! NSString == kUTTypeImage {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.addPhoto(image)
            }
        } else if info[UIImagePickerControllerMediaType] as! NSString == kUTTypeMovie || info[UIImagePickerControllerMediaType] as! NSString == kUTTypeVideo {
           
            if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                
                self.addVideo(path: videoURL)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setSignature(_ image: UIImage?) {
        if let img = image {
            self.addPhoto(img)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let item = self.item {
            self.delegate?.scrollViewOffset?(forItem: item, offset: scrollView.contentOffset)
        }
    }
    
    func audioRecorded(withUrl url: URL) {
        
        self.addAudio(path: url)
    }
    
    func saveEditedImage(image: UIImage?) {
        
        if let img = image {
            self.addPhoto(img)
        }
    }
    
    func editingCancelled() {
        
        
    }
}
