//
//  InputButtonCollectionViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/4/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class InputButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    var inputMode: InputMode?
    
    func setValues(inputMode: InputMode) {
        
        self.inputMode = inputMode
        
        if inputMode == InputMode.Photo {
            self.imageView.image = UIImage(named: "btnCamera_v2")
        }
        if inputMode == InputMode.File {
            self.imageView.image = UIImage(named: "btnSelect_v2")
        }
        if inputMode == InputMode.Signature {
            self.imageView.image = UIImage(named: "btnSig_v2")
        }
        if inputMode == InputMode.Video {
            self.imageView.image = UIImage(named: "btnVideo_v2")
        }
        if inputMode == InputMode.Audio {
            self.imageView.image = UIImage(named: "btnAudio_v2")
        }
    }
}
