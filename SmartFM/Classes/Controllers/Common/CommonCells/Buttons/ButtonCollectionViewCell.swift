//
//  ButtonCollectionViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/27/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit


class ButtonCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblBtntext: UILabel!
    
    var optionsList: OptionsList?
    var objectButton: ButtonsTypeData?
    
    func setValues(optionList: OptionsList) {
        
        self.optionsList = optionList
        
        if let display = optionList.DisplayText, display != "" {
            self.lblBtntext.text = display
        } else {
            self.lblBtntext.text = optionList.Value
        }
    }
    
    func setValues(ItemButton: ButtonsTypeData) {
        
        self.objectButton = ItemButton
        
        if let display = ItemButton.Name, display != "" {
            self.lblBtntext.text = display
        } else {
            self.lblBtntext.text = ItemButton.Value
        }
    }
    
    func setSelected(selected: Bool) {
        
        if selected  {
            self.viewBackground.backgroundColor = ThemeRedColor
        } else {
            self.viewBackground.backgroundColor = ButtonsUnselectedColor
        }
    }
}
