//
//  ButtonsTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/27/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class ButtonsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblFieldName: UILabel!
    
    
    var objectInfo: ObjectInfo?
    var delegate: FormTypeTableViewCellDelegate?
    
    
    fileprivate var buttons: [OptionsList] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setValues(objectInfo: ObjectInfo) {
        
        self.objectInfo = objectInfo
        self.lblFieldName.text = objectInfo.FieldName
        
        if let opt = objectInfo.OptionList {
            self.buttons = opt
            self.collectionView.reloadData()
        }
    }
}


extension ButtonsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.buttons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! ButtonCollectionViewCell
        
        let button = self.buttons[indexPath.row]
        
        if button.Value == self.objectInfo?.defaultData?.Value, button.Value != "", button.Value != nil {
            cell.setSelected(selected: true)
        } else {
            cell.setSelected(selected: false)
        }
        
        cell.setValues(optionList: button)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.objectInfo?.Editable == "1" {
            
            let cell = collectionView.cellForItem(at: indexPath) as! ButtonCollectionViewCell
            
            let dt = cell.optionsList?.DisplayText
            
            if let val = cell.optionsList?.Value {
                self.delegate?.originalValueChanged?()
                self.delegate?.valueChanged?(val, displayText: dt == nil ? "" : dt!, forItem: self.objectInfo)
                collectionView.reloadData()
            }
        }
    }
}
