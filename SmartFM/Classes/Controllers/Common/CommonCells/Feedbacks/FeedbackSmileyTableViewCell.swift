//
//  FeedbackSmileyTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/27/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class FeedbackSmileyTableViewCell: UITableViewCell, GenericFormDelegate {

    @IBOutlet weak var btnSmiley1: UIButton!
    @IBOutlet weak var btnSmiley2: UIButton!
    @IBOutlet weak var btnSmiley3: UIButton!
    @IBOutlet weak var btnSmiley4: UIButton!
    @IBOutlet weak var btnSmiley5: UIButton!
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    fileprivate var value: String = ""
    
    var item: ObjectInfo?
    var delegate: FormTypeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setValues(objectInfo: ObjectInfo) {
        
        self.item = objectInfo
        
        self.setEditable(editable: objectInfo.Editable == "1")
        
        self.lblFieldName.text = objectInfo.FieldName
        
        if let defVal = objectInfo.defaultData?.Value {
            self.value = defVal
            if defVal == "" {
                self.setSelected(value: 0)
            } else if let defInt = Int(defVal) {
                self.setSelected(value: defInt)
            }
            
        } else {
            
            self.setSelected(value: 0)
        }
        
    }
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Private methods
    ////////////////////////////////////////////////////////
    
    fileprivate func setEditable(editable: Bool) {
        
        self.btnClose.isHidden = !editable
    }
    
    fileprivate func deselectAll() {
        
        self.btnSmiley1.setImage(UIImage(named: "Smiley_1"), for: UIControlState.normal)
        self.btnSmiley2.setImage(UIImage(named: "Smiley_2"), for: UIControlState.normal)
        self.btnSmiley3.setImage(UIImage(named: "Smiley_3"), for: UIControlState.normal)
        self.btnSmiley4.setImage(UIImage(named: "Smiley_4"), for: UIControlState.normal)
        self.btnSmiley5.setImage(UIImage(named: "Smiley_5"), for: UIControlState.normal)
    }
    
    fileprivate func setSelected(value: Int) {
        
        self.deselectAll()
        
        self.delegate?.valueChanged?("\(value)", displayText: "", forItem: self.item)
        
        self.value = "\(value)"
        
        if value == 1 {
            self.btnSmiley1.setImage(UIImage(named: "Smiley_1_selected"), for: UIControlState.normal)
        } else if value == 2 {
            self.btnSmiley2.setImage(UIImage(named: "Smiley_2_selected"), for: UIControlState.normal)
        } else if value == 3 {
            self.btnSmiley3.setImage(UIImage(named: "Smiley_3_selected"), for: UIControlState.normal)
        } else if value == 4 {
            self.btnSmiley4.setImage(UIImage(named: "Smiley_4_selected"), for: UIControlState.normal)
        } else if value == 5 {
            self.btnSmiley5.setImage(UIImage(named: "Smiley_5_selected"), for: UIControlState.normal)
        }
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Events
    ////////////////////////////////////////////////////////
    
    @IBAction func smileyPressed(_ sender: UIButton) {
        
        if self.item?.Editable == "1" {
            if sender == self.btnSmiley1 {
                self.setSelected(value: 1)
                
            } else if sender == self.btnSmiley2 {
                self.setSelected(value: 2)
                
            } else if sender == self.btnSmiley3 {
                self.setSelected(value: 3)
                
            } else if sender == self.btnSmiley4 {
                self.setSelected(value: 4)
                
            } else if sender == self.btnSmiley5 {
                self.setSelected(value: 5)
                
            }
            
            self.delegate?.originalValueChanged?()
        }
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        
        self.setSelected(value: 0)
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Delegate Methods
    ////////////////////////////////////////////////////////
    
    func getValue() -> AnyObject {
        
        return self.value as AnyObject
    }

}
