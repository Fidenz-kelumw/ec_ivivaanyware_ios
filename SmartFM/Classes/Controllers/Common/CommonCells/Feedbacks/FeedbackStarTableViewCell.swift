//
//  FeedbackStarTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/27/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class FeedbackStarTableViewCell: UITableViewCell, GenericFormDelegate {

    @IBOutlet weak var btnStar1: UIButton!
    @IBOutlet weak var btnStar2: UIButton!
    @IBOutlet weak var btnStar3: UIButton!
    @IBOutlet weak var btnStar4: UIButton!
    @IBOutlet weak var btnStar5: UIButton!
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    fileprivate var value: String = ""
    
    var item: ObjectInfo?
    var delegate: FormTypeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setValues(objectInfo: ObjectInfo) {
        
        self.item = objectInfo
        
        self.setEditable(editable: objectInfo.Editable == "1")
        
        self.lblFieldName.text = objectInfo.FieldName
        
        if let defVal = objectInfo.defaultData?.Value {
            self.value = defVal
            if defVal == "" {
                self.setSelected(value: 0)
            } else if let defInt = Int(defVal) {
                self.setSelected(value: defInt)
            }
            
        } else {
            
            self.setSelected(value: 0)
        }
        
    }
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Private methods
    ////////////////////////////////////////////////////////
    
    fileprivate func setEditable(editable: Bool) {
        
        self.btnClose.isHidden = !editable
    }
    
    fileprivate func deselectAll() {
        
        self.btnStar1.setImage(UIImage(named: "img_feedback_star_unselected"), for: UIControlState.normal)
        self.btnStar2.setImage(UIImage(named: "img_feedback_star_unselected"), for: UIControlState.normal)
        self.btnStar3.setImage(UIImage(named: "img_feedback_star_unselected"), for: UIControlState.normal)
        self.btnStar4.setImage(UIImage(named: "img_feedback_star_unselected"), for: UIControlState.normal)
        self.btnStar5.setImage(UIImage(named: "img_feedback_star_unselected"), for: UIControlState.normal)
    }
    
    fileprivate func setSelected(value: Int) {
        
        self.deselectAll()
        
        self.delegate?.valueChanged?("\(value)", displayText: "", forItem: self.item)
        
        self.value = "\(value)"
        
        if value >= 1 {
            self.btnStar1.setImage(UIImage(named: "img_feedback_star"), for: UIControlState.normal)
        }
        if value >= 2 {
            self.btnStar2.setImage(UIImage(named: "img_feedback_star"), for: UIControlState.normal)
        }
        if value >= 3 {
            self.btnStar3.setImage(UIImage(named: "img_feedback_star"), for: UIControlState.normal)
        }
        if value >= 4 {
            self.btnStar4.setImage(UIImage(named: "img_feedback_star"), for: UIControlState.normal)
        }
        if value >= 5 {
            self.btnStar5.setImage(UIImage(named: "img_feedback_star"), for: UIControlState.normal)
        }
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Events
    ////////////////////////////////////////////////////////
    
    @IBAction func starPressed(_ sender: UIButton) {
        
        if self.item?.Editable == "1" {
            
            if sender == self.btnStar1 {
                self.setSelected(value: 1)
                
            } else if sender == self.btnStar2 {
                self.setSelected(value: 2)
                
            } else if sender == self.btnStar3 {
                self.setSelected(value: 3)
                
            } else if sender == self.btnStar4 {
                self.setSelected(value: 4)
                
            } else if sender == self.btnStar5 {
                self.setSelected(value: 5)
                
            }
            
            self.delegate?.originalValueChanged?()
        }
        
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        
        self.setSelected(value: 0)
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Delegate Methods
    ////////////////////////////////////////////////////////
    
    func getValue() -> AnyObject {
        
        return self.value as AnyObject
    }
}
