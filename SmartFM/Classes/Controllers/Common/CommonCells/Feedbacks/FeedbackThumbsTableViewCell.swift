//
//  FeedbackThumbsTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/27/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class FeedbackThumbsTableViewCell: UITableViewCell, GenericFormDelegate {

    @IBOutlet weak var btnThumbsUp: UIButton!
    @IBOutlet weak var btnThumbsDown: UIButton!
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    fileprivate var value: String = ""
    
    var item: ObjectInfo?
    var delegate: FormTypeTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setValues(objectInfo: ObjectInfo) {
        
        self.item = objectInfo
        
        self.setEditable(editable: objectInfo.Editable == "1")
        
        self.lblFieldName.text = objectInfo.FieldName
        
        if let defVal = objectInfo.defaultData?.Value {
            self.value = defVal
            if defVal == "" {
                self.setSelected(value: 0)
            } else if let defInt = Int(defVal) {
                self.setSelected(value: defInt)
            }
            
        } else {
            
            self.setSelected(value: 0)
        }
        
    }
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Private methods
    ////////////////////////////////////////////////////////
    
    fileprivate func setEditable(editable: Bool) {
        
        self.btnClose.isHidden = !editable
    }
    
    
    fileprivate func setSelected(value: Int) {
        
        self.delegate?.valueChanged?("\(value)", displayText: "", forItem: self.item)
        
        self.value = "\(value)"
        
        if value == 0 {
            self.btnThumbsUp.setImage(UIImage(named: "img_feedback_thumbsup_unselected"), for: UIControlState.normal)
            self.btnThumbsDown.setImage(UIImage(named: "img_feedback_thumbsdown_unselected"), for: UIControlState.normal)
        } else if value == 1 {
            self.btnThumbsUp.setImage(UIImage(named: "img_feedback_thumbsup_selected"), for: UIControlState.normal)
            self.btnThumbsDown.setImage(UIImage(named: "img_feedback_thumbsdown_unselected"), for: UIControlState.normal)
        } else if value == 2 {
            self.btnThumbsUp.setImage(UIImage(named: "img_feedback_thumbsup_unselected"), for: UIControlState.normal)
            self.btnThumbsDown.setImage(UIImage(named: "img_feedback_thumbsdown_selected"), for: UIControlState.normal)
        }
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Events
    ////////////////////////////////////////////////////////
    
    @IBAction func thumbPressed(_ sender: UIButton) {
        
        if self.item?.Editable == "1" {
            if sender == self.btnThumbsUp {
                self.setSelected(value: 1)
                
            } else if sender == self.btnThumbsDown {
                self.setSelected(value: 2)
                
            }
            
            self.delegate?.originalValueChanged?()
        }
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        
        self.setSelected(value: 0)
    }
    
    
    
    ////////////////////////////////////////////////////////
    // MARK: - Delegate Methods
    ////////////////////////////////////////////////////////
    
    func getValue() -> AnyObject {
        
        return self.value as AnyObject
    }

}
