//
//  ImageCollectionViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 7/15/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ImageCellDelegate {
    
    func removeImage(withCellImage cellImage: CellAttachment)
}

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var viewUploading: UIView!
    @IBOutlet weak var viewUploadingFailed: UIView!
    
    var cellImage: CellAttachment?
    var delegate: ImageCellDelegate?
    var largeImage: UIImage?
    var videoUrl: URL?
    var audioUrl: URL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setRemoveEnabled(_ enabled: Bool) {
        
        self.btnRemove.isHidden = !enabled
    }

    @IBAction func remove(_ sender: AnyObject) {
        
        if let cellImage = self.cellImage {
            self.delegate?.removeImage(withCellImage: cellImage)
        }
    }
}
