//
//  ImagesTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 7/15/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

@objc protocol ImagesCellDelegate {
    
    @objc optional func selectPhoto()
    @objc optional func takePhoto()
    @objc optional func addSignature()
    @objc optional func presentViewController(_ vc: UIViewController)
    @objc optional func scrollViewOffset(forItem item: AnyObject, offset: CGPoint)
}

protocol ImagesCellValuesDelegate {
    
    func setImageValues(_ cellImage: [CellAttachment], imageNames:String, forItem: AnyObject?)
    func addTask()
    func taskFinished(_ success: Bool)
    func removeFailed()
}

class ImagesTableViewCell: UITableViewCell, GenericFormDelegate, ImageCellDelegate, FormTypeTableViewCellDelegate {

    @IBOutlet weak var fieldName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSignature: UIButton!
    @IBOutlet weak var btnSelectPhoto: UIButton!
    @IBOutlet weak var btnTakePicture: UIButton!
    @IBOutlet var lblMessage: UILabel!
    
    //public variables
    var delegate: ImagesCellDelegate?
    var imagesCellValuesDelegate: ImagesCellValuesDelegate?
    var formTypeDelegate: FormTypeTableViewCellDelegate?
    var formDelegate: GenericFormDelegate?
    var images: [CellAttachment] = []
    var editingEnabled: Bool = true
    
    fileprivate var item: AnyObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btnSignature.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.btnSelectPhoto.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.btnTakePicture.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        if self.editingEnabled {
            self.constButtonHeight.constant = 100
        } else {
            self.constButtonHeight.constant = 0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Private Methods
    
    fileprivate func addPhoto(_ image: UIImage) {

        
        var imageName = UUID().uuidString
        if let userKey = UserManager().getUserKey() {
            imageName = "\(userKey)_\(imageName)"
        }
        imageName = "\(imageName)_\(Date().timeIntervalSince1970)"
        
        let imageHandler = ImageHandler()
        
        imageHandler.saveImageToFolder(image, withName: imageName, format: "jpg") { (savedImage, path) in
            
            if let folderPath = path {
                let cellImage = CellAttachment()
                cellImage.name = "\(imageName).jpg"
                cellImage.folder = folderPath
                cellImage.isLocal = true
                cellImage.image = savedImage
                cellImage.isUploading = true
                self.images.append(cellImage)
                
                let ip = IndexPath(item: self.images.count-1, section: 0)
                self.collectionView.insertItems(at: [ip])
                self.collectionView.scrollToItem(at: ip, at: UICollectionViewScrollPosition.right, animated: true)
                
                self.imagesCellValuesDelegate?.setImageValues(self.images, imageNames: self.cellImagesToImageNames(self.images), forItem: self.item)
                
                
                //Uploading
                if let objectInfo = self.item as? ObjectInfo, let uploadFolder = objectInfo.UploadFolder, let uploadImage = UIImage(contentsOfFile: "\(folderPath)/\(imageName).jpg") {
                    self.imagesCellValuesDelegate?.addTask()
                    
                    APIClient().uploadImage(uploadImage, name: "\(imageName).jpg", uploadFolder: uploadFolder) { (success) in
                        
                        if success {
                            cellImage.isUploading = false
                            cellImage.isFailedUploading = false
                            self.collectionView.reloadItems(at: [ip])
                            self.imagesCellValuesDelegate?.taskFinished(true)
                        } else {
                            cellImage.isUploading = false
//                            cellImage.isFailedUploading = true
//                            self.collectionView.reloadItemsAtIndexPaths([ip])
                            self.removeImage(withCellImage: cellImage)
                            self.imagesCellValuesDelegate?.taskFinished(false)
                            self.formDelegate?.showError?("Uploading image failed.")
                        }
                    }
                }
            }
        }
        
        
        
    }
    
    fileprivate func cellImagesToImageNames(_ cellImages :[CellAttachment]) -> String {
        
        var imageNames = ""
        for image in cellImages {
            if let name = image.name {
                imageNames = "\(imageNames)\(name),"
            }
        }
        
        imageNames = String(imageNames.characters.dropLast())
        return imageNames
    }
    

    
    //MARK: - Events
    
    @IBAction func takePicture(_ sender: AnyObject) {
        
        self.delegate?.takePhoto?()
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.camera
        self.delegate?.presentViewController?(picker)
    }
    
    @IBAction func selectPicture(_ sender: AnyObject) {
        
        self.delegate?.selectPhoto?()
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.delegate?.presentViewController?(picker)
    }
    
    @IBAction func signature(_ sender: AnyObject) {
        
        self.delegate?.addSignature?()
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DrawVc") as? UINavigationController {
            if let signatureVc = vc.viewControllers.first as? DrawSignatureViewController {
                signatureVc.delegate = self
            }
            self.delegate?.presentViewController?(vc)
        }
    }
    
    
    func getIndex(_ imageName: String) -> Int? {
        
        var index: Int? = nil
        for n in 0..<self.images.count {
            
            if self.images[n].name == imageName {
                index = n
            }
        }
        
        return index
    }
    
    func setValue(_ objInfo: ObjectInfo) {
        
        self.item = objInfo
        
        if let fName = objInfo.FieldName {
            self.fieldName.text = fName
        }
        
        if let imgs = objInfo.defaultData?.cellImages {
            self.images = imgs
        }
        
//        if let defaultImages = objInfo.defaultData?.Value, downloadFolder = objInfo.DownloadFolder {
//            
//            let imageNames = defaultImages.characters.split(",").map(String.init)
//            
//            self.images.removeAll(keepCapacity: false)
//            for imageName in imageNames {
//                
//                let cellImage = CellImage()
//                cellImage.name = imageName
//                cellImage.folder = downloadFolder
//                cellImage.isLocal = false
//                cellImage.image = nil
//                self.images.append(cellImage)
//            }
//        }
        
        self.collectionView.reloadData()
    }
    
    func setOffset(_ offset: CGPoint?) {
        
        if let os = offset {
            self.collectionView.contentOffset = os
        }
    }
    
    //MARK: - Delegate Methods
    
    func getValue() -> AnyObject {
        
        return self.cellImagesToImageNames(self.images) as AnyObject
    }
    
    func removeImage(withCellImage cellImage: CellAttachment) {
        
        if let name = cellImage.name {
            if let index = self.getIndex(name) {
                
                if cellImage.isFailedUploading {
                    
                    self.imagesCellValuesDelegate?.removeFailed()
                }
                
                self.images.remove(at: index)
                self.collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
                
                self.imagesCellValuesDelegate?.setImageValues(self.images, imageNames: self.cellImagesToImageNames(self.images), forItem: self.item)
            }
        }
    }
}


extension ImagesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.images.count > 0 {
            lblMessage.isHidden = true
        } else {
            lblMessage.isHidden = false
        }
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageColCell", for: indexPath) as! ImageCollectionViewCell
        cell.delegate = self
        cell.setRemoveEnabled(self.editingEnabled)
        cell.imageView.tag = (indexPath as NSIndexPath).item

        let cellImage = self.images[(indexPath as NSIndexPath).item]
        cell.cellImage = cellImage
        
        let defaultimage = UIImage(named: "Icon_no_photo")
        cell.imageView.image = defaultimage
        cell.largeImage = nil
        
        //check isuploading
        if cellImage.isUploading {
            cell.viewUploading.isHidden = false
        } else {
            cell.viewUploading.isHidden = true
        }
        
        //check uploading failed
        if cellImage.isFailedUploading {
            cell.viewUploadingFailed.isHidden = false
        } else {
            cell.viewUploadingFailed.isHidden = true
        }
        
        if let img = cellImage.image {
            cell.imageView.image = img
            cell.largeImage = img
            
        } else if let path = cellImage.folder, let name = cellImage.name {
            
            if cellImage.isLocal {
                cell.imageView.image = UIImage(contentsOfFile: "\(path)/\(name)")
                cell.largeImage = UIImage(contentsOfFile: "\(path)/\(name)")
                
            } else {
                
                if let acc = UserManager().getAccount() {
                    
                    ImageHandler().asyncImageDownload("\(Protocol)://\(acc)\(path)/\(name)", name: name) { (largeImage,image, path) in
                        
                        if cell.imageView.tag == (indexPath as NSIndexPath).row {
                            
                            cell.imageView.image = defaultimage
                            if let img = image {
                                cell.imageView.image = img
                            }
                            cell.largeImage = largeImage
                        }
                    }
                }
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let nvc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShowImageVc") as? UINavigationController, let vc = nvc.viewControllers.first as? ViewAttachmentImageViewController {
            
            let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
            
            if let img = cell.largeImage {
                
                vc.image = img
                
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    () -> Void in
                    self.delegate?.presentViewController?(nvc)
                }
            }
        }
    }
}


extension ImagesTableViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate, DrawSignatureDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.addPhoto(image)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setSignature(_ image: UIImage?) {
        if let img = image {
            self.addPhoto(img)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let item = self.item {
            self.delegate?.scrollViewOffset?(forItem: item, offset: scrollView.contentOffset)
        }
    }
}
