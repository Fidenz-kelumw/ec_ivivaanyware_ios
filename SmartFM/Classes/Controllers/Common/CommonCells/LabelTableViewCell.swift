//
//  LabelTableViewCell.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 4/3/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class LabelTableViewCell: UITableViewCell {
    
    var item: AnyObject?
    
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblFieldName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sizeToFit()
        layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.contentView.endEditing(true)
    }
    
    func setValue(_ item: AnyObject) {
        
        self.item = item

        if let oi = self.item as? ObjectInfo {

            self.contentView.layoutIfNeeded()

            self.lblFieldName.text = oi.FieldName
            if let dd = oi.defaultData {
                self.lblData.text = dd.Value
            }
        }
    }
}
