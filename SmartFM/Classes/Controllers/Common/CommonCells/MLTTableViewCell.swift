//
//  MLTTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MLTTableViewCell: UITableViewCell, UITextViewDelegate, GenericFormDelegate {
    
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var txtMLT: UITextView!
    
    var item: AnyObject?
    var delegate: FormTypeTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.txtMLT.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValue(_ item: AnyObject) {
        
        self.item = item
        
        if let oi = self.item as? ObjectInfo {
            self.setEnable(oi.Editable=="1" ? true : false)
            self.lblFieldName.text = oi.FieldName
            if let dd = oi.defaultData {
                self.txtMLT.text = dd.Value
            }
        }
    }
    
    
    func setEnable(_ enabled: Bool) {
        
        self.txtMLT.backgroundColor = enabled ? textFieldEnabledColor : textFieldDisabledColor
        self.txtMLT.isEditable = enabled
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.delegate?.originalValueChanged?()
        self.delegate?.valueChanged?(textView.text, displayText: textView.text, forItem: self.item)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
    }
    
    //MARK: - Delegate Methods
    func getValue() -> AnyObject {
        
        if let text = self.txtMLT.text {
            return text as AnyObject
        } else {
            return "" as AnyObject
        }
    }
}
