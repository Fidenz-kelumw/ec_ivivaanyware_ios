//
//  SAARItemTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/28/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol SAARItemsDelegate {
    
    func removeItem(item: LData)
}

class SAARItemTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    var delegate: SAARItemsDelegate?
    
    fileprivate var data: LData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues(data: LData, editable: Bool) {
        self.data = data
        
        self.lblTitle.text = data.DisplayText
        
        self.btnClose.isHidden = !editable
    }
    
    
    @IBAction func closePressed(_ sender: UIButton) {
        
        if let item = self.data {
            self.delegate?.removeItem(item: item)
        }
    }
}
