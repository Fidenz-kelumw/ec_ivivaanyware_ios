//
//  SAARTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/28/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol SAARDelegate {
    
    func selectSAARPressed(objectInfo: ObjectInfo)
}


class SAARTableViewCell: UITableViewCell, SAARItemsDelegate, GenericFormDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var btnSelectOverlay: UIButton!
    @IBOutlet weak var btnQr: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    
    var delegate: FormTypeTableViewCellDelegate?
    var saarDelegate: SAARDelegate?
    var formType: FormType = FormType.customSearch
    
    fileprivate var objectInfo: ObjectInfo?
    fileprivate var items: [LData] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(objectInfo: ObjectInfo) {
        self.objectInfo = objectInfo
        self.lblFieldName.text = objectInfo.FieldName
        self.items = []
        
        if objectInfo.Editable == "1" {
            
            if objectInfo.QRCode == "1" {
                self.btnQr.isHidden = false
            } else {
                self.btnQr.isHidden = true
            }
            self.btnSelect.isHidden = false
            self.btnSelectOverlay.isHidden = false
            
        } else {
            
            self.btnQr.isHidden = true
            self.btnSelect.isHidden = true
            self.btnSelectOverlay.isHidden = true
        }
        
        
        if let vals = objectInfo.defaultData?.ValuesArray {
            
            for val in vals {
                
                let data = LData()
                data.Value = val
                data.DisplayText = val
                
                if let opts = objectInfo.OptionList {
                    
                    
                    if let match = opts.filter({$0.Value == val}).first, match.DisplayText != nil, match.DisplayText != "" {
                        data.DisplayText = match.DisplayText
                    } else {
                        data.DisplayText = val
                    }
                    
                }
                
                self.items.append(data)
            }
        }
        
        self.tableView.reloadData()
    }
    
    func removeItem(item: LData) {
        if let arr = self.objectInfo?.defaultData?.ValuesArray, let remVal = item.Value, self.objectInfo?.Editable == "1" {
            let newArray = arr.filter({$0 != remVal})
            self.objectInfo?.defaultData?.ValuesArray = newArray
            
            self.delegate?.defaultDataChanged?(new: self.objectInfo?.defaultData, forItem: self.objectInfo)
        }
        
        //config
    }
    
    func addItem(item: LData) {
        
        if let newValue = item.Value, self.objectInfo?.Editable == "1" {
            if let dd = self.objectInfo?.defaultData {
                
                if dd.ValuesArray != nil {
                    
                    if !dd.ValuesArray!.contains(newValue) {
                        dd.ValuesArray!.append(newValue)
                    }
                    
                } else {
                    dd.ValuesArray = [newValue]
                }
                
            } else {
                
                let dd = DefaultData()
                dd.ValuesArray = [newValue]
                self.objectInfo?.defaultData = dd
            }
            
            self.delegate?.defaultDataChanged?(new: self.objectInfo?.defaultData, forItem: self.objectInfo)
        }
    }
    
    
    //MARK: - Events
    
    @IBAction func qrPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        if let nvc = storyboard.instantiateViewController(withIdentifier: "QRPopupVCNav") as? UINavigationController {
            
            
            if let vc = nvc.viewControllers.first as? QRPopupViewController{
                
                vc.selectedAction = SelectedAction.dismiss
                if let lup = self.objectInfo?.LookUpService {
                    vc.lookUpService = lup
                }
                vc.delegate = self
                
                
                if !(self.formType == FormType.customSearch) {
                    
                    vc.hasClose = true
                    let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                    formSheet.shouldCenterVertically = true
                    formSheet.shouldDismissOnBackgroundViewTap = true
                    formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 500.0)
                    formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                    formSheet.present(animated: true, completionHandler: nil)
                    
                } else {
                    
                    vc.selectedAction = SelectedAction.back
                    self.delegate?.pushVc?(vc)
                }
            }
        }
    }
    
    @IBAction func selectPressed(_ sender: Any) {
        
        if let oi = self.objectInfo {
            self.saarDelegate?.selectSAARPressed(objectInfo: oi)
        }
    }
    
    
    
    func setSelectedItem(_ item: AnyObject) {
        
        if let data = item as? LData {
            self.addItem(item: data)
            
        }
    }
}

extension SAARTableViewCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAARItemCell") as! SAARItemTableViewCell
        
        cell.delegate = self
        cell.setValues(data: self.items[indexPath.row], editable: self.objectInfo?.Editable == "1")
        
        return cell
    }
}


extension Array {
    
    func abcd(condition: @escaping (Element) -> Bool) -> Bool {
        
        for e in self {
            
            if condition(e) {
                return true
            }
        }
        
        return false
    }
}
