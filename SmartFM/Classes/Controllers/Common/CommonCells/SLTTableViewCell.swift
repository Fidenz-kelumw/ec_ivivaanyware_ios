//
//  SLTTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit



class SLTTableViewCell: UITableViewCell, GenericFormDelegate, TextViewPopupDelegate {
    
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var txtSLT: UITextField!
    @IBOutlet weak var constTxtBoxTrailing: NSLayoutConstraint!
    @IBOutlet weak var btnExtend: UIButton!
    @IBOutlet var viewBackground: UIView!
    
    var item: AnyObject?
    var delegate: FormTypeTableViewCellDelegate?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.contentView.endEditing(true)
    }

    func setValue(_ item: AnyObject) {
        
        self.item = item
        
        if let oi = self.item as? ObjectInfo {
            
            self.constTxtBoxTrailing.constant = oi.Extended == "1" ? 30 : 0
            self.contentView.layoutIfNeeded()
            
            self.setEnable(oi.Editable=="1" ? true : false)
            self.lblFieldName.text = oi.FieldName
            if let dd = oi.defaultData {
                self.txtSLT.text = dd.Value
            }
        }
    }
    
    func setEnable(_ enabled: Bool) {
        
        self.txtSLT.backgroundColor = enabled ? textFieldEnabledColor : textFieldDisabledColor
        self.txtSLT.isEnabled = enabled
        self.btnExtend.backgroundColor = enabled ? textFieldEnabledColor : textFieldDisabledColor
        self.viewBackground.backgroundColor = enabled ? textFieldEnabledColor : textFieldDisabledColor
    }
    

    //MARK: - Events
    @IBAction func Changed(_ sender: AnyObject) {
        
        self.delegate?.originalValueChanged?()
        self.delegate?.valueChanged?(self.txtSLT.text != nil ? self.txtSLT.text! : "", displayText: self.txtSLT.text != nil ? self.txtSLT.text! : "" ,forItem: self.item)
    }
    
    @IBAction func openDetails(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        let nvc = storyboard.instantiateViewController(withIdentifier: "TextViewPopup") as! UINavigationController
        
        if let vc = nvc.viewControllers.first as? TextViewPopupViewController {
            vc.delegate = self
            vc.loadType = TextViewLoadType.staticText
            vc.editable = (self.item as? ObjectInfo)?.Editable == "1"
            if let txt = self.txtSLT.text {
                
                vc.defaultText = txt
                
                let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                formSheet.shouldCenterVertically = false
                formSheet.shouldDismissOnBackgroundViewTap = false
                formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                
                formSheet.present(animated: true, completionHandler: nil)
            }
        }
    }
    
    //MARK: - Delegate Methods
    func getValue() -> AnyObject {
        
        if let text = self.txtSLT.text {
            return text as AnyObject
        } else {
            return "" as AnyObject
        }
    }
    
    func updatedText(with text: String) {
        
        self.txtSLT.text = text
    }
}
