//
//  SubmitTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/25/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol FormSubmitDelegate {
    func submitPressed()
}

class SubmitTableViewCell: UITableViewCell, GenericFormDelegate {
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var delegate: FormSubmitDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setButtonText(text: String?) {
        
        if let txt = text {
            self.btnSubmit.setTitle(txt, for: UIControlState.normal)
        } else {
            self.btnSubmit.setTitle("Submit", for: UIControlState.normal)
        }
    }

    @IBAction func submitPressed(_ sender: AnyObject) {
        
        self.delegate?.submitPressed()
    }
    
    
    //MARK: - Delegate Methods
    func setEnable(_ enabled: Bool) {
        
        self.btnSubmit.backgroundColor = enabled ? submitButtonColor : submitButtonDisabledColor
        self.btnSubmit.isEnabled = enabled
    }
}
