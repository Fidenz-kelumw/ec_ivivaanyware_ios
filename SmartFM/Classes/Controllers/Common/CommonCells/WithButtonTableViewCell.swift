//
//  WithButtonTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit



enum OpenType {
    case push, modal
}

@objc protocol FormTypeTableViewCellDelegate {
    
    @objc optional func valueChanged(_ value: String, displayText:String, forItem: AnyObject?)
    @objc optional func defaultDataChanged(new: DefaultData?, forItem: AnyObject?)
    @objc optional func getFieldInfoJSON() -> String?
    @objc optional func pushVc(_ viewController: UIViewController)
    @objc optional func originalValueChanged()
}

class WithButtonTableViewCell: UITableViewCell, GenericFormDelegate {
    
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var qrButton: UIButton!
    @IBOutlet weak var lblText: UILabel!
    
    var item: AnyObject?
    var delegate: FormTypeTableViewCellDelegate?
    var customSearchDelegate: FormTypeTableViewCellDelegate?
    var opentype: OpenType = OpenType.modal
    var objectType: String?
    
    var selectedItemDictionary: [String:String]? {
        didSet {
            var value = ""
            var displayText = ""
            if let sel = self.selectedItemDictionary {
                for (key,val) in sel {
                    value = key
                    displayText = val
                }
            }
            
            self.valueChanged(value, displayText: displayText)
        }
    }
//    var selectedItemString: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - Private methods
    func setValue(_ item: AnyObject) {
        
        self.item = item
        
        
        if let oi = self.item as? ObjectInfo {
            Configurations().configSelectionButton(forButton: self.btnSelect, withDisclosure: oi.Editable=="1" ? true : false)
            
            let valueType = oi.ValueType
            if valueType == ValueType.SAS.rawValue {
                if oi.QRCode == "1", oi.Editable == "1" {
                    self.qrButton.isHidden = false
                } else {
                    self.qrButton.isHidden = true
                }
            } else if valueType == ValueType.DS.rawValue {
                if oi.Editable == "1" {
                    self.qrButton.isHidden = false
                } else {
                    self.qrButton.isHidden = true
                }
            } else {
                self.qrButton.isHidden = true
            }
            
            
            
            self.setEnable(oi.Editable=="1" ? true : false)
            self.lblFieldName.text = oi.FieldName
            
            if let dd = oi.defaultData, let val = dd.Value , dd.DisplayText != "" || val != "" {
                var displayText = dd.DisplayText
                if oi.ValueType == ValueType.DT.rawValue {
                    let dateManager = DateTimeManager()
                    displayText = dateManager.getDateOnlyFromISOString(val)
                } else if oi.ValueType == ValueType.DAT.rawValue {
                    let dateManager = DateTimeManager()
                    displayText = dateManager.getDateTimeFromISOString(val)
                } else if (oi.ValueType == ValueType.DS.rawValue || oi.ValueType == ValueType.SAS.rawValue), (dd.DisplayText == "" || dd.DisplayText == nil) {
                    if let newDT = oi.OptionList?.filter({$0.Value == val}).first?.DisplayText, newDT != "" {
                        displayText = newDT
                    } else {
                        displayText = val
                    }
                }
                
                if let dtext = displayText {
                    self.selectedItemDictionary = [val : dtext]
                } else {
                    self.selectedItemDictionary = [val : val]
                }
                //self.btnSelect.setTitle(displayText, for: UIControlState())
                self.lblText.text = "  \(String(describing: displayText!))"
                
            } else {
                if oi.ValueType == ValueType.DS.rawValue, let ndVal = oi.NoDataValue {
                    //self.btnSelect.setTitle(ndVal, for: UIControlState())
                    self.lblText.text = ndVal
                } else {
                    //self.btnSelect.setTitle("Select aaa aaaa aaaaa aaaaaa asdasdasd adsdasd", for: UIControlState())
                    self.lblText.text = "  Select"
                }
            }
        }
    }
    
    func setEnable(_ enabled: Bool) {
        
        self.btnSelect.backgroundColor = enabled ? SelectionButtonBackgroundColor : SelectionButtonDisabledBackgroundColor
        self.btnSelect.isEnabled = enabled
    }
    
    
    //MARK: - Events
    func valueChanged(_ value: String, displayText:String) {
        
        self.delegate?.valueChanged?(value, displayText: displayText, forItem: self.item)
    }
    
    @IBAction func qrPressed(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        
        if let nvc = storyboard.instantiateViewController(withIdentifier: "QRPopupVCNav") as? UINavigationController {
            
            
            if let vc = nvc.viewControllers.first as? QRPopupViewController {
                
                vc.selectedAction = SelectedAction.dismiss
                if let lup = (self.item as? ObjectInfo)?.LookUpService {
                    vc.lookUpService = lup
                }
                vc.delegate = self
                
                
                if !(self.opentype == OpenType.push) {
                    
                    vc.hasClose = true
                    let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                    formSheet.shouldCenterVertically = true
                    formSheet.shouldDismissOnBackgroundViewTap = true
                    formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 500.0)
                    formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                    formSheet.present(animated: true, completionHandler: nil)
                    
                } else {
                    vc.selectedAction = SelectedAction.back
                    self.delegate?.pushVc?(vc)
                }
            }
        }
    }
    
    @IBAction func selectPressed(_ sender: UIButton) {
        
        if let objInfo = self.item as? ObjectInfo {
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
            
            if objInfo.ValueType == ValueType.DS.rawValue {
                if let nvc = storyboard.instantiateViewController(withIdentifier: "SelectPopupWithQR") as? UINavigationController {
                    
                    nvc.navigationBar.barTintColor = popupHeaderColor
                    nvc.navigationBar.tintColor = UIColor.white
                    let textAttributes = [
                        NSForegroundColorAttributeName: UIColor.white,
                        NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
                    ]
                    nvc.navigationBar.titleTextAttributes = textAttributes
                    
                    if let vc = nvc.viewControllers.first as? SelectBuildingViewController {
                        
                        vc.hasHeader = false
                        vc.hasQRScanner = true
                        vc.delegate = self
                        if let fname = objInfo.FieldName {
                            vc.titleString = "Select \(fname)"
                        } else {
                            vc.titleString = "Select"
                        }
                        vc.selectType = SelectType.formData
                        if let options = objInfo.OptionList {
                            vc.items = options
                        }
                        vc.noDataValue = objInfo.NoDataValue
                        
                        if self.opentype == OpenType.modal {
                            vc.selectedAction = SelectedAction.dismiss
                            let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                            formSheet.shouldCenterVertically = true
                            formSheet.shouldDismissOnBackgroundViewTap = true
                            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                            
                            formSheet.present(animated: true, completionHandler: nil)
                        } else {
                            vc.selectedAction = SelectedAction.back
                            self.customSearchDelegate?.pushVc?(vc)
                        }
                    }
                }
            } else if objInfo.ValueType == ValueType.SAS.rawValue {
                if let nvc = storyboard.instantiateViewController(withIdentifier: "SearchAndSelectWithQR") as? UINavigationController {
                    
                    nvc.navigationBar.barTintColor = popupHeaderColor
                    nvc.navigationBar.tintColor = UIColor.white
                    let textAttributes = [
                        NSForegroundColorAttributeName: UIColor.white,
                        NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
                    ]
                    nvc.navigationBar.titleTextAttributes = textAttributes
                    
                    if self.opentype == OpenType.modal {
                        
                        if let vc = nvc.viewControllers.first as? SearchAndSelectViewController {
                            
                            vc.delegate = self
                            vc.hasHeader = false
                            vc.objectType = self.objectType
                            vc.hasQRScanner = (self.item as? ObjectInfo)?.QRCode == "1"
                            vc.selectedAction = SelectedAction.dismiss
                            vc.lookupService = objInfo.LookUpService
                            vc.fieldInfo = self.delegate?.getFieldInfoJSON?()
                            
                            let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                            formSheet.shouldCenterVertically = true
                            formSheet.shouldDismissOnBackgroundViewTap = true
                            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 500.0)
                            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                            
                            formSheet.present(animated: true, completionHandler: nil)
                        }
                        
                    } else {
                        
                        if let vc = nvc.viewControllers.first as? SearchAndSelectViewController {
                            vc.delegate = self
                            vc.hasHeader = false
                            vc.objectType = self.objectType
                            vc.hasQRScanner = (self.item as? ObjectInfo)?.QRCode == "1"
                            vc.selectedAction = SelectedAction.back
                            vc.lookupService = objInfo.LookUpService
                            vc.fieldInfo = self.delegate?.getFieldInfoJSON?()
                            
                            self.customSearchDelegate?.pushVc?(vc)
                        }
                    }
                }
            } else if objInfo.ValueType == ValueType.DT.rawValue {
                
                if let vc = storyboard.instantiateViewController(withIdentifier: "DateOnly") as? DateSelectViewController {
                    vc.delegate = self
                    
                    if self.opentype == OpenType.modal {
                        
                        let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
                        formSheet.shouldCenterVertically = true
                        formSheet.shouldDismissOnBackgroundViewTap = true
                        formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 400.0)
                        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                        
                        formSheet.present(animated: true, completionHandler: nil)
                        
                    } else {
                        vc.hasHeader = false
                        vc.selectedAction = SelectedAction.back
                        self.customSearchDelegate?.pushVc?(vc)
                    }
                    
                    
                }
                
            } else if objInfo.ValueType == ValueType.DAT.rawValue {
                
                if let vc = storyboard.instantiateViewController(withIdentifier: "DateAndTime") as? DateAndTimeSelectViewController {
                    
                    if self.opentype == OpenType.modal {
                        vc.delegate = self
                        let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
                        formSheet.shouldCenterVertically = true
                        formSheet.shouldDismissOnBackgroundViewTap = true
                        formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 400.0)
                        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                        
                        formSheet.present(animated: true, completionHandler: nil)
                        
                    } else {
                        vc.hasHeader = false
                        vc.selectedAction = SelectedAction.back
                        self.customSearchDelegate?.pushVc?(vc)
                    }
                }
                
            }
        }
    }
    
    
    //MARK: - Delegate Methods
    func getValue() -> AnyObject {
        
        if let sel = self.selectedItemDictionary {
            return sel as AnyObject
        }
        
        return ["" : ""] as AnyObject
    }
    
    func setSelectedItem(_ item: AnyObject) {
        
        //Update the change of the original value
        self.delegate?.originalValueChanged?()
        
        if let obj = item as? OptionsList, let val = obj.Value, let dis = obj.DisplayText {
            //self.btnSelect.setTitle(dis, for: UIControlState())
            self.lblText.text = "  \(dis)"
            self.selectedItemDictionary = [val : dis]
            
        } else if let obj = item as? Date {
            
            if let info = self.item as? ObjectInfo {
                
                if info.ValueType == ValueType.DT.rawValue {
                    let value = DateTimeManager().dateToString(obj, format: ISODateFormat)
                    let display = DateTimeManager().getDateFromDateTime(obj)
                    self.selectedItemDictionary = [value : display]
                    //self.btnSelect.setTitle(DateTimeManager().getDateFromDateTime(obj), for: UIControlState())
                    self.lblText.text = "  \(DateTimeManager().getDateFromDateTime(obj))"
                    
                } else if info.ValueType == ValueType.DAT.rawValue {
                    let dtManager = DateTimeManager()
                    let value = DateTimeManager().dateToString(obj, format: ISODateFormat)
                    let display = "\(dtManager.getDateFromDateTime(obj)) \(dtManager.getTimeFromDateTime(obj))"
                    //self.btnSelect.setTitle(display, for: UIControlState())
                    self.lblText.text = "  \(display)"
                    self.selectedItemDictionary = [value : display]
                }
            }
        } else if let obj = item as? LData, let val = obj.Value, let dis = obj.DisplayText  {
            //self.btnSelect.setTitle(dis, for: UIControlState())
            self.lblText.text = "  \(dis)"
            self.selectedItemDictionary = [val : dis]
        }
        
    }
}
