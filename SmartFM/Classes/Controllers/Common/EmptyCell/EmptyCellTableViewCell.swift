//
//  EmptyCellTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class EmptyCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var bgView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ message: String, bgColor: UIColor) {
        self.bgView.backgroundColor = bgColor
        self.lblMessage.text = message
    }
    
    func setColor(txtColor: UIColor) {
        self.lblMessage.textColor = txtColor
    }
}
