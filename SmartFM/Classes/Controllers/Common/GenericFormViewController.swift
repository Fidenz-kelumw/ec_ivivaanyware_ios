//
//  GenericFormViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum FormType {
    case objectInfo, createPage, customSearch
}

@objc protocol GenericFormDelegate {
    
    @objc optional func getValue()->AnyObject
    @objc optional func setSelectedItem(_ item: AnyObject)
    @objc optional func setEnable(_ enabled: Bool)
    @objc optional func hideProcessing()
    @objc optional func showSuccess(_ message: String)
    @objc optional func showError(_ message: String)
    @objc optional func processing(_ completion: ()->Void)
//    @objc optional func showProcessingAlert(_ completion: ()->Void)
    @objc optional func goToHome()
    @objc optional func goToObjectsList()
    @objc optional func showObjectDetails(_ objectKey:String, objecttype: String)
    @objc optional func customSearch(_ fieldData: String)
    @objc optional func showVc(_ vc: UIViewController)
    @objc optional func showpopup(_ vc: UIViewController)
    @objc optional func showHUD(userInteraction: Bool)
    @objc optional func hideHUD()
}

class GenericFormViewController: UIViewController, FormSubmitDelegate, FormTypeTableViewCellDelegate, PageLinkDelegate, PageActionDelegate, ImagesCellDelegate, ATTCellDelegate, ImagesCellValuesDelegate, ATTCellValuesDelegate, TaskQueueDelegate, ObjectDetailsDelegate, SAARDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorCenter: NSLayoutConstraint!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    //public variables
    var objectKey: String?
    var objectType: String?
    var formType: FormType = FormType.objectInfo
    var delegates: [GenericFormDelegate] = []
    var delegate: GenericFormDelegate?
    var navigatorDelegate: PageLinkNavigatorDelegate?
    var actionCellDelegate: ActionCellDelegate?
    var defaultData: [NSDictionary]?
    var location: Building?
    var customSearchDelegate: FormTypeTableViewCellDelegate?
    var uploadingQueue: TaskQueue = TaskQueue()
    var fieldInfo = ""
    
    //private variables
    fileprivate var items: [AnyObject]?
    fileprivate var pageLinks: [PageLink] = []
    fileprivate var actions: [Action] = []
    fileprivate var submitDelegate: GenericFormDelegate?
    fileprivate var submitCell: SubmitTableViewCell?
    fileprivate var isEditable: Bool = false
    fileprivate var kbHeight: CGFloat! = 0.0
    fileprivate var isKeyboardVisibale: Bool = false
    fileprivate var imageCellScrollIndexes:[String: CGPoint] = [:]
    fileprivate var dataChanged: Bool = false
    fileprivate var refreshControl: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        if self.formType == FormType.createPage || self.formType == FormType.customSearch {
            self.activityIndicatorCenter.constant = 0.0
        } else if self.formType == FormType.objectInfo {
            self.activityIndicatorCenter.constant = -27.0
        }
        
        if self.formType == FormType.objectInfo {
            
            self.refreshControl = UIRefreshControl()
            self.refreshControl!.addTarget(self, action: #selector(GenericFormViewController.fetchData), for: UIControlEvents.valueChanged)
            self.tableView.addSubview(refreshControl!)
        }
        
        self.fetchData()
        
        self.tableView.estimatedRowHeight = 110
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    

    /////////////////////////////
    //MARK: - Private Methods
    /////////////////////////////
    fileprivate func hideLoading() {
        self.loadingView.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    fileprivate func showLoading() {
        self.loadingView.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func fetchData() {
        
        if self.formType == FormType.objectInfo {
            self.showLoading()
            self.fetchObjectInfo {
                DispatchQueue.main.async {
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                    self.hideLoading()
                }
                
            }
        } else if self.formType == FormType.createPage {
            
            self.showLoading()
            self.fetchCreateFields({
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.hideLoading()
                }
            })
        } else if self.formType == FormType.customSearch {
            self.showLoading()
            self.fetchCustomSearchFields({
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.hideLoading()
                }
            })
        }
    }
    
    fileprivate func fetchObjectInfo(_ completion:(()->Void)?) {
        if SyncTimeManager().hasTimerExpired(EntityKey.ObjectInformation) {
        
            if let objectType = self.objectType, let objectKey = self.objectKey {
                
                SyncManager().syncObjectInformation(objectType, objectKey: objectKey) { (success, error) -> Void in
                    
                    if success {
                        
                        let oi = self.getObjectInformation()
                        self.items = oi?.objectInfo
                        
                        
                        if let items = self.items as? [ObjectInfo] {
                            self.fillCellImages(items)
                        }
                        
                        if let objInf = oi {
                            self.actions = objInf.actions != nil ? objInf.actions! : []
                            self.pageLinks = objInf.pageLinks != nil ? objInf.pageLinks! : []
                        }
                        if let editable = oi?.UpdateObjectInfo , editable == "1" {
                            self.isEditable = true
                        } else {
                            self.isEditable = false
                        }
                        
                        completion?()
                        
                    } else {
                        
                        completion?()
                    }
                }
            }
            
        } else {
            
            let oi = self.getObjectInformation()
            self.items = oi?.objectInfo
            
            if let items = self.items as? [ObjectInfo] {
                self.fillCellImages(items)
            }
            
            if let objInf = oi {
                self.actions = objInf.actions != nil ? objInf.actions! : []
                self.pageLinks = objInf.pageLinks != nil ? objInf.pageLinks! : []
            }
            if let editable = oi?.UpdateObjectInfo , editable == "1" {
                self.isEditable = true
            } else {
                self.isEditable = false
            }
            
            completion?()
        }
    }

    fileprivate func fetchCreateFields(_ completion:(()->Void)?) {
        
        if let objectType = self.objectType {
           
            SyncManager().syncCreateFields(objectType) { (success) -> Void in
                
                if success {
                    
                    
                    let oi = self.getObjectInformation()
                    self.items = oi?.objectInfo
                    
                    if let items = self.items as? [ObjectInfo] {
                        self.fillCellImages(items)
                    }
                    
                    if let objInf = oi {
                        self.actions = objInf.actions != nil ? objInf.actions! : []
                        self.pageLinks = objInf.pageLinks != nil ? objInf.pageLinks! : []
                    }
                    if let editable = oi?.UpdateObjectInfo , editable == "1" || editable == "" {
                        self.isEditable = true
                    } else {
                        self.isEditable = false
                    }
                    
                    if let _ = self.defaultData {
                        self.changeDefaultData()
                    }
                    
                    completion?()
                    
                } else {
                    
                    completion?()
                }
            }
            
        } else {
            completion?()
        }
    }
    
    fileprivate func fetchCustomSearchFields(_ completion:(()->Void)?) {
        
        if let objectType = self.objectType {
            
            SyncManager().syncCustomSearchFields(objectType, location: self.location) { (success) -> Void in
                
                if success {
                    
                    let oi = self.getObjectInformation()
                    self.items = oi?.objectInfo
                    
                    if let items = self.items as? [ObjectInfo] {
                        self.fillCellImages(items)
                    }
                    
                    if let objInf = oi {
                        self.actions = objInf.actions != nil ? objInf.actions! : []
                        self.pageLinks = objInf.pageLinks != nil ? objInf.pageLinks! : []
                    }
                    if let editable = oi?.UpdateObjectInfo , editable == "1" || editable == "" {
                        self.isEditable = true
                    } else {
                        self.isEditable = false
                    }
                    
                    if let _ = self.defaultData {
                        self.changeDefaultData()
                    }
                    
                    completion?()
                    
                } else {
                    
                    completion?()
                }
            }
        }
    }
    
    fileprivate func fillCellImages(_ items: [ObjectInfo]) {
        
        for objInfo in items {
            
            if let defaultImages = objInfo.defaultData?.Values, let downloadFolder = objInfo.DownloadFolder {
                
                objInfo.defaultData?.cellImages.removeAll(keepingCapacity: false)
                for defAtt in defaultImages {
                    
                    let cellImage = CellAttachment()
                    cellImage.name = defAtt.ATTName
                    if let mediaType = defAtt.ATTType {
                        cellImage.mediaType = MediaType(rawValue: mediaType)
                    }
                    cellImage.folder = downloadFolder
                    cellImage.isLocal = false
                    cellImage.image = nil
                    objInfo.defaultData?.cellImages.append(cellImage)
                    
                }
                
            } else if let defaultImages = objInfo.defaultData?.Value, let downloadFolder = objInfo.DownloadFolder {
                
                //Old way. Support only images
                    let imageNames = defaultImages.characters.split(separator: ",").map(String.init)
                    objInfo.defaultData?.cellImages.removeAll(keepingCapacity: false)
                    for imageName in imageNames {
                        
                        let cellImage = CellAttachment()
                        cellImage.name = imageName
                        cellImage.folder = downloadFolder
                        cellImage.isLocal = false
                        cellImage.image = nil
                        objInfo.defaultData?.cellImages.append(cellImage)
                    }
            }
        }
    }
    
    fileprivate func changeDefaultData() {
        
        if let fields = self.defaultData, let items = self.items as? [ObjectInfo] , self.formType != FormType.objectInfo {
            
            for defaultValue in fields {
                
                if let fieldId = defaultValue.value(forKey: "FieldID") as? String {
                    
                    if let matchingItem = items.filter({$0.FieldID == fieldId}).first {
                        
                        if let val = defaultValue.value(forKey: "Value") as? String , val != "" {
                            matchingItem.defaultData?.Value = val
                            
                            if let display = defaultValue.value(forKey: "DisplayText") as? String , display != "" {
                                matchingItem.defaultData?.DisplayText = display
                            } else {
                                matchingItem.defaultData?.DisplayText = val
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    
    fileprivate func getObjectInformation() -> ObjectInformation? {
        
        if let objectInformations = ObjectInformation.all() as? [ObjectInformation] {
            
            if let objectInformation = objectInformations.first, let objectInformationId = objectInformation.Id {
                
                objectInformation.objectInfo = ObjectInfo.__where("ObjectInformationId=\(objectInformationId)", sortBy: "Id", accending: true) as? [ObjectInfo]
                
                if let objectsInfos = objectInformation.objectInfo {
                    
                    for objectInfo in objectsInfos {
                        
                        if let oiId = objectInfo.Id {
                            objectInfo.defaultData = (DefaultData.__where("ObjectInfoId=\(oiId)", sortBy: "Id", accending: true) as? [DefaultData])?.first
                            if let defdataid = objectInfo.defaultData?.Id {
                                objectInfo.defaultData?.Values = DefDataValue.__where("defaultDataId=\(defdataid)", sortBy: "Id", accending: true) as? [DefDataValue]
                            }
                           
                            objectInfo.OptionList = OptionsList.__where("ObjectInfoId=\(oiId)", sortBy: "Id", accending: true) as? [OptionsList]
                        }
                    }
                }
                
                
                objectInformation.actions = Action.__where("ObjectInformationId=\(objectInformationId)", sortBy: "Id", accending: true) as? [Action]
                
                objectInformation.pageLinks = PageLink.__where("ObjectInformationId=\(objectInformationId)", sortBy: "Id", accending: true) as? [PageLink]
                if let pls = objectInformation.pageLinks {
                    for pageLink in pls {
                        if let plId = pageLink.Id {
                            pageLink.fieldData = FieldData.__where("PageLinkId=\(plId)", sortBy: "Id", accending: true) as? [FieldData]
                        }
                    }
                    
                }
                
                
                return objectInformation
            }
            
        }
        
        return nil
    }
    
    fileprivate func getFieldDataDictionary() -> [[String : Any]]? {
        
        if let it = self.items as? [ObjectInfo] , it.count > 0 {
            
            var fieldValues = [[String: Any]]()
            
            for n in 0 ..< it.count {
                if let fid = it[n].FieldID {
                    
                    var fieldValue = [String: Any]()
                    fieldValue["FieldID"] = fid
                    if it[n].ValueType == "ATT" {
                        if let vals = it[n].defaultData?.Values {
                            var defValues = [[String: String]]()
                            for val in vals {
                                if let ATTName = val.ATTName, let ATTType = val.ATTType {
                                    var defValue = [String: String]()
                                    defValue["ATTName"] = ATTName
                                    defValue["ATTType"] = ATTType
                                    defValues.append(defValue)
                                }
                            }
                            fieldValue["Value"] = defValues
                        } else {
                            fieldValue["Value"] = []
                        }
                    } else if it[n].ValueType == "SAAR" {
                        
                        if let val = it[n].defaultData?.ValuesArray {
                            fieldValue["Value"] = val
                        } else {
                            fieldValue["Value"] = []
                        }
                    } else {
                        if let val = it[n].defaultData?.Value {
                            fieldValue["Value"] = val
                        }
                    }
                    fieldValues.append(fieldValue)
                }
                
            }
            
            return fieldValues
        }
        
        return nil
    }
    
    fileprivate func mandatoryValuesFilled() -> Bool {
        
        if let fields = self.getFieldDataDictionary() {
            
            for n in 0..<fields.count {
                
                let field = fields[n]
                
                for (key , value) in field {
                    
                    if key == "Value" {
                        
                        if let val = value as? String, val == "" {
                            //Non-ATT & Non-SAAR value type
                            if let objInfos = items as? [ObjectInfo] , objInfos.count > 0 {
                                
                                if objInfos[n].Mandatory == "1" {
                                    return false
                                }
                            }
                        } else if let val = value as? [NSDictionary], val.count == 0 {
                            //ATT value type
                            if let objInfos = items as? [ObjectInfo] , objInfos.count > 0 {
                                
                                if objInfos[n].Mandatory == "1" {
                                    return false
                                }
                            }
                        } else if let val = value as? [String], val.count == 0 {
                            //SAAR value type
                            if let objInfos = items as? [ObjectInfo] , objInfos.count > 0 {
                                
                                if objInfos[n].Mandatory == "1" {
                                    return false
                                }
                            }
                        }
                        
                    }
                }
            }
            return true
        }
        
        return false
    }
    
    fileprivate func anyFieldsEditable() -> Bool {
        
        if self.isEditable {
            if let objectInfos = self.items as? [ObjectInfo] , objectInfos.count > 0 {
                
                let editableFields = objectInfos.filter(){$0.Editable == "1"}
                
                return editableFields.count > 0
            }
        }
        
        return false
    }
    
    fileprivate func getUpdateObjectInfoStatus() -> Bool {
        var buttonStatus: Bool = false
        
        if self.formType == FormType.createPage || self.formType == FormType.customSearch {
            buttonStatus = self.mandatoryValuesFilled()
        } else {
            if let oi = self.getObjectInformation(), let status = oi.UpdateObjectInfo, status == "1"{
                
                if let fields = self.getFieldDataDictionary() {
                    if let objInfos = items as? [ObjectInfo] , objInfos.count > 0 {
                        
                        for n in 0..<fields.count {
                            if objInfos[n].Mandatory == "1" {
                                buttonStatus = self.mandatoryValuesFilled()
                                break
                            } else {
                                buttonStatus = true
                            }
                        }
                    } else {
                        buttonStatus = false
                    }
                } else {
                    buttonStatus = false
                }
            } else {
                buttonStatus = false
            }
        }
        
        return buttonStatus
    }
    
    fileprivate func getPosition(items:[AnyObject], forIndexPath indexPath: IndexPath) -> CellPosition {
        
        var position = CellPosition.middle
        if items.count == 1 {
            position = CellPosition.single
        } else if (indexPath as NSIndexPath).row == 0 {
            position = CellPosition.top
        } else if (indexPath as NSIndexPath).row == items.count - 1 {
            position = CellPosition.bottom
        }
        return position
    }
    
    fileprivate func updateObjectInfo(_ fieldInfo: String) {
        
        if let ot = self.objectType, let ok = self.objectKey {
            
            self.delegate?.processing?({
                
                APIClient().updateObjectInfo(ot, objectKey: ok, fieldData: fieldInfo, completion: { (success, message) in
                    
                    self.delegate?.hideProcessing?()
                    
                    if success {
                        self.delegate?.showSuccess?(message)
                        
                        //Resetting data changed flag
                        self.resetUnsavedFlag()
                        
                        //Expiring the ObjectList timer after successful object update
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectList.rawValue)'")
                        
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectInformation.rawValue)'")
                        self.fetchData()
                        
                    } else {
                        self.delegate?.showError?(message)
                    }
                })
                
            })
            
        }
    }

    fileprivate func showImageUploadAlert() {
        
        let alert = UIAlertController(title: "Please wait...", message: "Files are being uploaded.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /////////////////////////////
    //MARK: - Delegate Methods
    /////////////////////////////
    func submitPressed() {
        
        fieldInfo = ""
        if let info = self.getFieldInfoJSON() {
            
            fieldInfo = info
        }
        
        if self.formType == FormType.objectInfo {
            
            if self.getObjectInformation()?.UpdateConfirmation == "1" {
                
                let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to submit?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (_) in
                    
                    if !self.uploadingQueue.hasProcessingTasks() {
                        
                        self.updateObjectInfo(self.fieldInfo)
                        
                    } else {
                        
                        self.showImageUploadAlert()
                    }
                    
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                if !self.uploadingQueue.hasProcessingTasks() {
                    
                    self.updateObjectInfo(fieldInfo)
                    
                } else {
                    
                    self.showImageUploadAlert()
                }
            }
            
            
            
            
        } else if self.formType == FormType.createPage {
            
            if let objectType = self.objectType {
                
                if !self.uploadingQueue.hasProcessingTasks() {
                    
                    self.delegate?.processing?({
                        
                        APIClient().createObject(objectType, fieldData: fieldInfo, completion: { (success, message, objectKey, showDetails) in
                            
                            self.delegate?.hideProcessing?()
                            
                            if success {
                                self.delegate?.showSuccess?(message)
                                
                                //Expiring the ObjectList timer after successful object creation
                                SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectList.rawValue)'")
                                
                                if let ok = objectKey , showDetails == "1" {
                                    self.delegate?.showObjectDetails?(ok, objecttype: objectType)
                                }
                                
                            } else {
                                self.delegate?.showError?(message)
                                
                            }
                        })
                    })
                    
                    
                } else {
                    
                    self.showImageUploadAlert()
                }
                
            }
        } else if self.formType == FormType.customSearch {
            
            if !self.uploadingQueue.hasProcessingTasks() {
                
                self.delegate?.customSearch?(fieldInfo)
                
            } else {
                self.showImageUploadAlert()
            }
        }
        
        
    }
    
    func getFieldInfoJSON() -> String? {
        
        if let fieldValues = self.getFieldDataDictionary() {
            do {
                if let fInfo = NSString(data: try JSONSerialization.data(withJSONObject: fieldValues, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.ascii.rawValue) as? String {
//                    print(fInfo as String)
                    return "\(fInfo)"
                }
                
            } catch let error as NSError {
                NSLog("Couldn't convert to json \(error)")
            }
        }
        
        return nil
    }
    
    func getFieldInfoWithDisplayText() -> [[String : String]]? {
        
        if let it = self.items as? [ObjectInfo] , it.count > 0 {
            
            var fieldValues = [[String: String]]()
            
            for n in 0 ..< it.count {
                if let fid = it[n].FieldID, let val = it[n].defaultData?.Value {
                    
                    var fieldValue = [String: String]()
                    fieldValue["FieldID"] = fid
                    fieldValue["Value"] = val
                    if let display = it[n].defaultData?.DisplayText {
                        fieldValue["DisplayText"] = display
                    }
                    fieldValues.append(fieldValue)
                }
                
            }
//            print("fields = \(fieldValues)")
            return fieldValues
        }
        
        return nil
    }
    
    func configSubmit() {
        
        if let cell = self.submitCell {
            
// -- Old logic is in here, may be this was right
//            if self.anyFieldsEditable() {
//
//                cell.setEnable(self.mandatoryValuesFilled())
//
//            } else {
//                cell.setEnable(false)
//            }
            
            cell.setEnable(self.getUpdateObjectInfoStatus())
        }
    }
    
    func valueChanged(_ value: String, displayText: String, forItem: AnyObject?) {
        
        if let its = self.items as? [ObjectInfo], let it = forItem as? ObjectInfo  , its.count > 0 {
            
            if let filteredObjInfo = its.filter({$0.FieldID==it.FieldID}).first {
                
                filteredObjInfo.defaultData?.Value = value
                filteredObjInfo.defaultData?.DisplayText = displayText
            }
        }
        
        self.configSubmit()
    }
    
    func defaultDataChanged(new: DefaultData?, forItem: AnyObject?) {
        
        if let items = self.items as? [ObjectInfo], let forOi = forItem as? ObjectInfo {
            
            for n in 0..<items.count {
                let item = items[n]
                
                if item.FieldID == forOi.FieldID {
                    
                    item.defaultData = new
                    
                    let ip = IndexPath(row: n, section: 0)
                    self.tableView.reloadRows(at: [ip], with: UITableViewRowAnimation.fade)
                }
            }
            
        }
        
        self.configSubmit()
    }
    
    func setImageValues(_ cellImages: [CellAttachment], imageNames:String, forItem: AnyObject?) {
        
        self.originalValueChanged()
        
        if let its = self.items as? [ObjectInfo], let it = forItem as? ObjectInfo  , its.count > 0 {
            
            if let filteredObjInfo = its.filter({$0.FieldID==it.FieldID}).first {
                
                filteredObjInfo.defaultData?.cellImages = cellImages
                
                var defDataValues: [DefDataValue] = []
                for cellatt in cellImages {
                    let defDataVal = DefDataValue()
                    defDataVal.ATTName = cellatt.name
                    defDataVal.ATTType = cellatt.mediaType?.rawValue
                    defDataValues.append(defDataVal)
                }
                filteredObjInfo.defaultData?.Values = defDataValues
                
                filteredObjInfo.defaultData?.DisplayText = ""
            }
        }
        
        self.configSubmit()
    }
    
    func performAction(_ pageAction: PageAction) {
        
        if pageAction == PageAction.Refresh {
            
            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectInformation.rawValue)'")
            
            self.fetchData()
            
        } else if pageAction == PageAction.Home {
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.delegate?.goToHome?()
            }
            
        } else if pageAction == PageAction.ObjectList {
            
            //Expiring the ObjectList timer when actions are executed in Object Details page, AND when Page Action (in return data) is 'ObjectList'
            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectList.rawValue)'")
            
            
            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectInformation.rawValue)'")
            
            self.fetchData()
            
            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.delegate?.goToObjectsList?()
            }
        }
    }
    
    func presentViewController(_ vc: UIViewController) {
        
        self.delegate?.showVc?(vc)
    }
    
    func showPopup(_ vc: UIViewController) {
        
        self.delegate?.showpopup?(vc)
    }
    
    func scrollViewOffset(forItem item: AnyObject, offset: CGPoint) {
        
        if let objInfo = item as? ObjectInfo, let fId = objInfo.FieldID {
            
            var hasKey = false
            for key in self.imageCellScrollIndexes.keys {
                if key == fId {
                    hasKey = true
                }
            }
            
            if hasKey {
                self.imageCellScrollIndexes.updateValue(offset, forKey: fId)
            } else {
                self.imageCellScrollIndexes[fId] = offset
            }
        }
    }
    
    func addTask() {
        self.uploadingQueue.addTaskToQueue()
    }
    
    func taskFinished(_ success: Bool) {
        
        self.uploadingQueue.taskFinished(success)
    }
    
    func allTasksFinished() {
        
        
    }
    
    func removeFailed() {
        
        self.uploadingQueue.removeFailedTask()
    }
    
    func updateChanges() {
        
        if let objInfos = self.items as? [ObjectInfo] {
            
            for info in objInfos {
                
                if let updatedDefaultData = info.defaultData {
                    
                    updatedDefaultData.update()
                }
            }
        }
    }
    
    func hasUnsavedData() -> Bool {
        
        return self.dataChanged
    }
    
    func resetUnsavedFlag() {
        
        self.dataChanged = false
    }
    
    func originalValueChanged() {
        
        self.dataChanged = true
    }
    
    func selectSAARPressed(objectInfo: ObjectInfo) {
        
        if let objInfos = self.items! as? [ObjectInfo] , objInfos.count > 0 {
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
            
            if let nvc = storyboard.instantiateViewController(withIdentifier: "SAARWithQR") as? UINavigationController, objectInfo.Editable == "1" {
                
                nvc.navigationBar.barTintColor = popupHeaderColor
                nvc.navigationBar.tintColor = UIColor.white
                let textAttributes = [
                    NSForegroundColorAttributeName: UIColor.white,
                    NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
                ]
                nvc.navigationBar.titleTextAttributes = textAttributes
                
                if !(self.formType == FormType.customSearch) {
                    
                    if let vc = nvc.viewControllers.first as? SAARPopupViewController {
                        
                        //vc.delegate = self
                        vc.formTypeDelegate = self
                        vc.objectType = self.objectType
                        vc.selectedAction = SelectedAction.dismiss
                        vc.lookupService = objectInfo.LookUpService
                        vc.optionsList = objectInfo.OptionList
                        vc.hasQRScanner = objectInfo.QRCode == "1"
                        vc.fieldInfo = self.getFieldInfoJSON()
                        vc.objectInfo = objectInfo
                        let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                        formSheet.shouldCenterVertically = true
                        formSheet.shouldDismissOnBackgroundViewTap = true
                        formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 500.0)
                        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                        
                        formSheet.present(animated: true, completionHandler: nil)
                    }
                    
                } else {
                    
                    if let vc = nvc.viewControllers.first as? SAARPopupViewController {
                        
                        vc.formTypeDelegate = self
                        vc.objectType = self.objectType
                        vc.selectedAction = SelectedAction.back
                        vc.lookupService = objectInfo.LookUpService
                        vc.optionsList = objectInfo.OptionList
                        vc.hasQRScanner = objectInfo.QRCode == "1"
                        vc.objectInfo = objectInfo
                        vc.fieldInfo = self.getFieldInfoJSON()
                        
                        self.customSearchDelegate?.pushVc?(vc)
                    }
                }
            }
        }
        
        self.configSubmit()
    }
    
    func pushVc(_ viewController: UIViewController) {
        
        self.customSearchDelegate?.pushVc?(viewController)
    }
    
    func getCorrectCellForSectionOneTwo(cellType: Int, indexPath: IndexPath) -> UITableViewCell {
        
        if cellType == 1 {
            if self.pageLinks.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
                cell.setValues(" ", bgColor: ThemeBackgroundColor)
                return cell
            } else {
                
                let position = self.getPosition(items: self.pageLinks, forIndexPath: indexPath)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PageLinkCell") as! PageLinkTableViewCell
                
                DispatchQueue.main.async {
                    //                    let pl = self.pageLinks[(indexPath as NSIndexPath).row].
                    cell.setValues(self.pageLinks[(indexPath as NSIndexPath).row], cellPosition: position)
                }
                cell.delegate = self
                cell.navigatorDelegate = self.navigatorDelegate
                return cell
            }
        } else {
            if self.actions.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
                cell.setValues(" ", bgColor: ThemeBackgroundColor)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ActionCell") as! ActionTableViewCell
                if let objectType = self.objectType, let objectKey = self.objectKey {
                    
                    let position = self.getPosition(items: self.actions, forIndexPath: indexPath)
                    
                    cell.setValues(self.actions[(indexPath as NSIndexPath).row],objectType: objectType, objectKey: objectKey, cellPosition: position)
                }
                cell.pageActionDelegate = self
                cell.delegate = self.actionCellDelegate
                return cell
            }
        }
    }
    
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    /////////////////////////////
    //MARK: - Events
    /////////////////////////////
    @IBAction func backToForm(_ sender: UIStoryboardSegue) {
        
    }
    
}


extension GenericFormViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if self.items == nil || self.items?.count == 0 {
                
                return 1
                
            } else {
                
                if self.formType == FormType.objectInfo {
                    if self.anyFieldsEditable() {
                        return self.items!.count + 1
                    } else {
                        if self.getObjectInformation()?.HideUpdateButton == "1" {
                            return self.items!.count
                        } else {
                            return self.items!.count + 1
                        }
                    }
                } else {
                    return self.items!.count + 1
                }
            }
            
        }
        else { // new
            if tableView.numberOfSections == 2 {
                if (self.pageLinks.count > 0) {
                    return self.pageLinks.count
                } else {
                    return self.actions.count
                }
            } else {
                if section == 2 {
                    return self.pageLinks.count
                } else if section == 1 {
                    return self.actions.count
                }
            }
        }
        
//        else if section == 2 {
//
//            return self.pageLinks.count
//
//        } else if section == 1 {
//
//            return self.actions.count
//        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let sectionCount = tableView.numberOfSections
        print("Sections -- \(sectionCount)")
        print("Current section -- \((indexPath as NSIndexPath).section)")
        
        if (indexPath as NSIndexPath).section == 0 {
            
            if self.items == nil || self.items?.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
                cell.setValues("No items found", bgColor: ThemeBackgroundColor)
                return cell
                
            } else {
                
                if let objInfos = self.items! as? [ObjectInfo] , objInfos.count > 0 {
                    
                    if (indexPath as NSIndexPath).row == objInfos.count {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell") as! SubmitTableViewCell
                        self.submitCell = cell
                        cell.delegate = self
                        if self.formType == FormType.objectInfo {
                            if self.getObjectInformation()?.UpdateButtonText == nil || self.getObjectInformation()?.UpdateButtonText == "" {
                                cell.setButtonText(text: "Submit")
                            } else {
                                cell.setButtonText(text: self.getObjectInformation()?.UpdateButtonText)
                            }
                            
                        }
                        self.submitDelegate = cell.self
                        self.configSubmit()
                        return cell
                    }
                    
                    let objectInfo = objInfos[(indexPath as NSIndexPath).row]
                    
                    if objectInfo.ValueType == ValueType.SLT.rawValue {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SLT") as! SLTTableViewCell
                        let delegate: GenericFormDelegate = cell.self
                        self.delegates.append(delegate)
                        cell.setValue(objectInfo)
                        cell.delegate = self
                        return cell
                        
                    } else if objectInfo.ValueType == ValueType.LABEL.rawValue {
                    
                        let cell = tableView.dequeueReusableCell(withIdentifier: "LABEL") as! LabelTableViewCell
                        cell.setValue(objectInfo)
                        return cell
                        
                    } else if objectInfo.ValueType == ValueType.MLT.rawValue {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "MLT") as! MLTTableViewCell
                        let delegate: GenericFormDelegate = cell.self
                        self.delegates.append(delegate)
                        cell.setValue(objectInfo)
                        cell.delegate = self
                        return cell
                        
                    }
                    
                    else if objectInfo.ValueType == "IMG" {
                        print("-------------------------- THIS SHOULD NOT BE EXECUTING --------------------------")
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesCell") as! ImagesTableViewCell
                        
                        cell.delegate = self
                        cell.formTypeDelegate = self
                        cell.imagesCellValuesDelegate = self
                        cell.formDelegate = self.delegate
                        cell.setValue(objectInfo)
                        if let id = objectInfo.FieldID {
                            for (key,val) in self.imageCellScrollIndexes {
                                if key == id {
                                    cell.setOffset(val)
                                    break
                                }
                            }
                        }
                        return cell
                        
                    }
                    
                    else if objectInfo.ValueType == ValueType.ATT.rawValue {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ATTCell") as! ATTTableViewCell
                        
                        cell.delegate = self
                        cell.formTypeDelegate = self
                        cell.imagesCellValuesDelegate = self
                        cell.formDelegate = self.delegate
                        cell.setValue(objectInfo)
                        if let id = objectInfo.FieldID {
                            for (key,val) in self.imageCellScrollIndexes {
                                if key == id {
                                    cell.setOffset(val)
                                    break
                                }
                            }
                        }
                        return cell
                        
                    } else if objectInfo.ValueType == ValueType.FEEDBACK.rawValue {
                        
                        if objectInfo.Style == FeedbackStyle.STARS.rawValue {
                            
                            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackStarsCell") as! FeedbackStarTableViewCell
                            let delegate: GenericFormDelegate = cell.self
                            self.delegates.append(delegate)
                            cell.delegate = self
                            cell.setValues(objectInfo: objectInfo)
                            return cell
                            
                        } else if objectInfo.Style == FeedbackStyle.FACES.rawValue {
                            //'Faces' and 'Smiley' types has been swaped
                            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackSmileyCell") as! FeedbackSmileyTableViewCell
                            let delegate: GenericFormDelegate = cell.self
                            self.delegates.append(delegate)
                            cell.delegate = self
                            cell.setValues(objectInfo: objectInfo)
                            return cell
                            
                        } else if objectInfo.Style == FeedbackStyle.SMILEY.rawValue {
                            //'Faces' and 'Smiley' types has been swaped
                            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackFacesCell") as! FeedbackFacesTableViewCell
                            let delegate: GenericFormDelegate = cell.self
                            self.delegates.append(delegate)
                            cell.delegate = self
                            cell.setValues(objectInfo: objectInfo)
                            return cell
                            
                        } else if objectInfo.Style == FeedbackStyle.THUMBS.rawValue {
                            
                            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackThumbsCell") as! FeedbackThumbsTableViewCell
                            let delegate: GenericFormDelegate = cell.self
                            self.delegates.append(delegate)
                            cell.delegate = self
                            cell.setValues(objectInfo: objectInfo)
                            return cell
                            
                        }
                        
                    } else if objectInfo.ValueType == ValueType.BUTTONS.rawValue {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonsCell") as! ButtonsTableViewCell
                        cell.delegate = self
                        cell.setValues(objectInfo: objectInfo)
                        
                        return cell
                        
                    } else if objectInfo.ValueType == ValueType.SAAR.rawValue {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SAARCell") as! SAARTableViewCell

                        cell.delegate = self
                        cell.saarDelegate = self
                        cell.formType = self.formType
                        cell.setValues(objectInfo: objectInfo)
                        
                        return cell
                        
                    } else {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "WithButton") as! WithButtonTableViewCell
                        let delegate: GenericFormDelegate = cell.self
                        self.delegates.append(delegate)
                        cell.setValue(objectInfo)
                        cell.delegate = self
                        cell.objectType = self.objectType
                        if self.formType == FormType.customSearch {
                            cell.customSearchDelegate = self.customSearchDelegate
                            cell.opentype = OpenType.push
                        }
                        return cell
                        
                        
                    }
                    
                }
            }
            
        } else if (indexPath as NSIndexPath).section == 1 {
            if sectionCount == 2 {
                if self.pageLinks.count > 0 {
                    return self.getCorrectCellForSectionOneTwo(cellType: 1, indexPath: indexPath)
                } else {
                    return self.getCorrectCellForSectionOneTwo(cellType: 2, indexPath: indexPath)
                }
            } else {
                return self.getCorrectCellForSectionOneTwo(cellType: 2, indexPath: indexPath)
            }
        } else if (indexPath as NSIndexPath).section == 2 {
            return self.getCorrectCellForSectionOneTwo(cellType: 1, indexPath: indexPath)
        }
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
        cell.setValues("No items found", bgColor: ThemeBackgroundColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let its = self.items {
            
            if let objInfos = its as? [ObjectInfo] , objInfos.count > 0 {
                
                if (indexPath as NSIndexPath).section == 0 {
                    if (indexPath as NSIndexPath).row == objInfos.count {
                        return 75
                    }
                    let objectInfo = objInfos[(indexPath as NSIndexPath).row]
                    if objectInfo.ValueType == ValueType.SLT.rawValue {
                        
                        return 95
                    
                    } else if objectInfo.ValueType == ValueType.LABEL.rawValue {
                        
                        if let data = objectInfo.defaultData, let val = data.Value, let title = objectInfo.FieldName {
                            let font = UIFont(name: "Melbourne", size: 18.0)
                            let detailHeight = heightForLabel(text: val, font: font!, width: self.tableView.bounds.size.width-10)
                            
                            var addingValue: CGFloat = 0.0
                            
                            if title.count > 0 {
                                addingValue = 65.0
                            } else {
                                addingValue = 45.0
                            }
                    
                            var height = (addingValue + detailHeight)
                            if height < 95 {
                                height = 95
                            }
                            
                            return height
                        } else {
                            return 95
                        }
                        
                    } else if objectInfo.ValueType == ValueType.MLT.rawValue {
                        
                        return 170
                        
                    } else if objectInfo.ValueType == ValueType.IMG.rawValue {
                        
                        return 250
                        
                    } else if objectInfo.ValueType == ValueType.SAAR.rawValue {
                        
                        if let count = objectInfo.defaultData?.ValuesArray?.count {
                            let height: CGFloat = 48 + (CGFloat(count) * CGFloat(43.5))
                            return height
                        }
                        return 48
                        
                    } else if objectInfo.ValueType == "ATT" {
                        
                        if let edit = objectInfo.Editable, edit == "1" {
                            return 210
                        } else {
                            return 150
                        }
                        
                    } else {
                        
                        return 95
                    }
                } else {
                    
                    if (indexPath as NSIndexPath).section == 2 {
                        
                        if self.pageLinks.count == 1 {
                            return 80
                        } else if (indexPath as NSIndexPath).row == 0 {
                            return 70
                        } else if (indexPath as NSIndexPath).row == self.pageLinks.count - 1 {
                            return 70
                        } else {
                            return 60
                        }
                        
                    } else {
                        
                        if self.actions.count == 1 {
                            return 80
                        } else if (indexPath as NSIndexPath).row == 0 {
                            return 70
                        } else if (indexPath as NSIndexPath).row == self.actions.count - 1 {
                            return 70
                        } else {
                            return 60
                        }
                    }
                }
            }
        }
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.formType == FormType.objectInfo {
            if self.items?.count > 0 {
                if self.actions.count == 0 && self.pageLinks.count == 0 {
                    return 1
                } else if (self.actions.count == 0 && self.pageLinks.count >= 0) || (self.actions.count >= 0 && self.pageLinks.count == 0){
                    return 2
                } else {
                    return 3
                }
            }
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UITextField(frame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 30.0))
        
        switch section {
            case 0:
                label.text = "      " // form
                break
            case 1:
                
                if tableView.numberOfSections == 2 {
                    if self.pageLinks.count > 0 {
                        label.text = "      Links:"
                    } else {
                        label.text = "      " // actions
                    }
                } else {
                    label.text = "      " // actions
                }
                break
            case 2:
                label.text = "      Links:"
                break
            default:
                break;
        }
        
        label.font = UIFont(name: "Melbourne", size: 20)!
        
        label.textColor = ThemeTextColor
        label.backgroundColor = ThemeBackgroundColor
        
        label.borderStyle = UITextBorderStyle.none
        label.contentVerticalAlignment = UIControlContentVerticalAlignment.bottom
        label.isUserInteractionEnabled = false
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 0
        } else if section == 1 {
            return 20
        }
        return 40.0
    }
}

