//
//  PageLinkTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/29/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

enum CellPosition {
    case top, bottom, middle, single
}

protocol PageLinkDelegate {
    
    func getFieldInfoJSON() -> String?
    func getFieldInfoWithDisplayText() -> [[String : String]]?
}

@objc protocol PageLinkNavigatorDelegate {
    
    @objc optional func showObjects(_ fieldDataJson: String, objectType: String?)
    @objc optional func showCreatePage(_ fieldDataJson: [NSDictionary], objectType: String?)
    @objc optional func showObjectDetailsPage(_ objectType: String, objectKey: String)
}

class PageLinkTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPLButton: UIButton!
    @IBOutlet weak var frameBottom: UIView!
    @IBOutlet weak var frameMiddle: UIView!
    @IBOutlet weak var frameTop: UIView!
    @IBOutlet weak var frameSingle: UIView!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var delegate: PageLinkDelegate?
    var navigatorDelegate: PageLinkNavigatorDelegate?
    
    fileprivate var pageLink: PageLink?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.constBottom.priority = UILayoutPriority.init(250)
        
        self.frameTop.layer.borderWidth = 1
        self.frameTop.layer.borderColor = FrameBorderColor.cgColor
        self.frameTop.layer.cornerRadius = 5
        
        self.frameBottom.layer.borderWidth = 1
        self.frameBottom.layer.borderColor = FrameBorderColor.cgColor
        self.frameBottom.layer.cornerRadius = 5
        
        self.frameMiddle.layer.borderWidth = 1
        self.frameMiddle.layer.borderColor = FrameBorderColor.cgColor
        self.frameMiddle.layer.cornerRadius = 5
        
        self.frameSingle.layer.borderWidth = 1
        self.frameSingle.layer.borderColor = FrameBorderColor.cgColor
        self.frameSingle.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ pageLink: PageLink, cellPosition: CellPosition) {
        
        DispatchQueue.main.async {
            self.pageLink = pageLink
            if let name = pageLink.LinkName {
                self.btnPLButton.setTitle(name, for: UIControlState())
            }
            
            self.frameMiddle.isHidden = true
            self.frameTop.isHidden = true
            self.frameBottom.isHidden = true
            self.frameSingle.isHidden = true
            
            var contentViewHeight: CGFloat = 70.0
            if cellPosition == CellPosition.middle {
                contentViewHeight = 60
            } else if cellPosition == CellPosition.single {
                contentViewHeight = 80
            }
            let autoHeight = contentViewHeight - 10.0 - self.btnPLButton.frame.size.height
            
            if cellPosition == CellPosition.single {
                self.frameSingle.isHidden = false
                self.constTop.constant = 20.0
                self.constBottom.constant = autoHeight
            } else if cellPosition == CellPosition.bottom {
                self.frameBottom.isHidden = false
                self.constBottom.constant = autoHeight
                self.constTop.constant = 10.0
            } else if cellPosition == CellPosition.top {
                self.frameTop.isHidden = false
                self.constBottom.constant = 10.0
                self.constTop.constant = autoHeight
            } else if cellPosition == CellPosition.middle {
                self.frameMiddle.isHidden = false
                self.constBottom.constant = 10.0
                self.constTop.constant = autoHeight
            }
            self.layoutIfNeeded()
        }
    }
    
    fileprivate func getFieldDataDictionary(_ info: [FieldData]) -> [[String : String]]? {
        
        if info.count > 0 {
            
            var fieldValues = [[String: String]]()
            
            for n in 0 ..< info.count {
                if let fid = info[n].FieldID, let val = info[n].Value {
                    
                    var fieldValue = [String: String]()
                    fieldValue["FieldID"] = fid
                    fieldValue["Value"] = val
                    fieldValues.append(fieldValue)
                }
                
            }
            
            return fieldValues
        }
        
        return nil
    }
    

    func getFieldInfoJSON(_ info: [FieldData]) -> String? {
        
        if let fieldValues = self.getFieldDataDictionary(info) {
            do {
                if let fInfo = NSString(data: try JSONSerialization.data(withJSONObject: fieldValues, options: JSONSerialization.WritingOptions(rawValue: 0)), encoding: String.Encoding.ascii.rawValue) as? String {
                    //print(fInfo as String)
                    return "\(fInfo)"
                }
                
                
            } catch let error as NSError {
                NSLog("Couldn't convert to json \(error)")
            }
        }
        
        return nil
    }
    
    func getFieldInfoWithDisplayText(_ info: [FieldData]) -> [[String : String]]? {
        
        if info.count > 0 {
            
            var fieldValues = [[String: String]]()
            
            for n in 0 ..< info.count {
                if let fid = info[n].FieldID, let val = info[n].Value {
                    
                    var fieldValue = [String: String]()
                    fieldValue["FieldID"] = fid
                    fieldValue["Value"] = val
                    if let display = info[n].DisplayText {
                        fieldValue["DisplayText"] = display
                    }
                    fieldValues.append(fieldValue)
                }
                
            }
            print(fieldValues)
            return fieldValues
        }
        
        return nil
    }

    //MARK: - Events
    @IBAction func buttonClicked(_ sender: UIButton) {
        
        if let pl = self.pageLink, let linkType = pl.LinkType {
            
            if linkType == LinkType.Details.rawValue {
                
                if let objectType = pl.ObjectType, let objectkey = pl.ObjectKey {
                    
                    self.navigatorDelegate?.showObjectDetailsPage?(objectType, objectKey: objectkey)
                }
                
            } else if linkType == LinkType.List.rawValue {

                if let fieldData = pl.fieldData {
                    if let defData = self.getFieldInfoJSON(fieldData) {
                        self.navigatorDelegate?.showObjects?(defData, objectType: self.pageLink?.ObjectType)
                    }
                }
                
            } else if linkType == LinkType.Create.rawValue {
                
                if let fieldData = pl.fieldData {
                    if let defData = self.getFieldInfoWithDisplayText(fieldData) {
                        self.navigatorDelegate?.showCreatePage?(defData as [NSDictionary], objectType: pl.ObjectType)
                    }
                }
            }
        }
    }
}
