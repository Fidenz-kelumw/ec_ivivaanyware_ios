//
//  AddAttachmentViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import AVKit

protocol SetAttachmentDelegate {
    
    func selectPhoto()
    func takePhoto()
    func resetPhoto()
    func resetData()
    func drawSignature()
    func setSelectedAttachmentType(_ attachmentType: AttachmentType?)
    func setAttachmentTitle(_ title: String?)
    func uploadImage(_ image: UIImage, type: AttachmentType, name: String?)
    func uploadVideo(videoUrl: URL, type: AttachmentType, name: String?)
    func uploadAudio(audioUrl: URL, type: AttachmentType, name: String?)
    func editImage(asset: AttachedAsset)
    func takeVideo()
    func recordAudio()
    func showVideo(url: URL)
}

class AddAttachmentViewController: PopupParentViewController, GenericFormDelegate, ShowAttachmentDelegate, AttachmentTypesViewControllerDelegate, RecordAudioDelegate {

    @IBOutlet weak var placeHolderView: UIView!
    @IBOutlet weak var btnAttachmentType: UIButton!
    @IBOutlet weak var txtTile: UITextField!
    @IBOutlet weak var viewTitleContainer: UIView!
    
    var currentView: UIViewController!
    var attachmentTypes: [AttachmentType] = []
    var inputTypes: [String] = []
    var newPhotoDelegate: SetAttachmentDelegate?
    var attachedAsset: AttachedAsset?
    var attachmentTitle: String?
    var selectedAttachmentType: AttachmentType?
    var delegate: AttachmentsDelegate?
    
    fileprivate var alertError: LIHAlert?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewTitleContainer.layer.borderColor = textboxBorderColor.cgColor
        self.viewTitleContainer.layer.borderWidth = 0.5
        
        Configurations().configSelectionButton(forButton: self.btnAttachmentType, withDisclosure: true)
        
        if let attTitle = self.attachmentTitle {
            self.txtTile.text = attTitle
        }
        
        self.initAlerts()
        
        
        if let type = self.selectedAttachmentType {
            self.selectedAttachmentType = type
            self.configAttachmentType()
            
        } else {
            if let type = self.attachmentTypes.first {
                self.selectedAttachmentType = type
                self.configAttachmentType()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowAttachment" {
             
            if let vc = segue.destination as? ShowAttachmentViewController {
                
                vc.image = self.attachedAsset?.attachedImage
                if let mt = self.attachedAsset?.mediaType {
                    vc.mediaType = mt
                }
                vc.videoUrl = self.attachedAsset?.attachedMediaURL
                vc.delegate = self
            }
            
        } else if segue.identifier == "AttachmentPhoto" {
            
            if let vc = segue.destination as? AddPhotoViewController {
                
                vc.delegate = self.newPhotoDelegate
            }
            
        } else if segue.identifier == "AttachmentSignature" {
            
            if let vc = segue.destination as? AddSignatureViewController {
                
                vc.delegate = self.newPhotoDelegate
            }
            
        } else if segue.identifier == "showAttachments" {
            
            if let vc = segue.destination as? AttachmentTypesViewController {
                vc.inputButtons = self.inputTypes
                vc.delegate = self.delegate
                vc.setAttDelegate = self.newPhotoDelegate
                vc.attDelegate = self
            }
        } else if segue.identifier == "showAudio" {
            
            if let vc = segue.destination as? AudioPlaybackViewController {
                
                vc.url = self.attachedAsset?.attachedMediaURL
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    
    //MARK: - Private Methods
    fileprivate func initAlerts() {
        
        self.alertError = LIHAlertManager.getErrorAlert("Please select an attachment to continue")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.initAlert(self.view)
    }
    
    fileprivate func configAttachmentType() {
        if let att = self.selectedAttachmentType?.TypeName {
            self.btnAttachmentType.setTitle(att, for: UIControlState.normal)
            self.configContainer()
        }
    }
    
    fileprivate func configContainer() {
        
        if let asset = self.attachedAsset {
            
            if asset.mediaType == MediaType.IMG {
                self.performSegue(withIdentifier: "ShowAttachment", sender: nil)
            } else if asset.mediaType == MediaType.VID {
                self.performSegue(withIdentifier: "ShowAttachment", sender: nil)
            } else if asset.mediaType == MediaType.AUD {
                self.performSegue(withIdentifier: "showAudio", sender: nil)
            }
        } else {
            self.performSegue(withIdentifier: "showAttachments", sender: nil)
        }
    }
    
    
    //MARK: - Events
    @IBAction func titleChanged(_ sender: AnyObject) {
        
        self.newPhotoDelegate?.setAttachmentTitle(self.txtTile.text)
    }
    
    @IBAction func sendPressed(_ sender: AnyObject) {
        
        if let asset = self.attachedAsset, let type = self.selectedAttachmentType {
            
            self.newPhotoDelegate?.resetData()
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
            
            if asset.mediaType == MediaType.IMG, let image = asset.attachedImage {
                self.newPhotoDelegate?.uploadImage(image, type: type, name: self.txtTile.text)
                
            } else if asset.mediaType == MediaType.VID, let url = asset.attachedMediaURL {
                self.newPhotoDelegate?.uploadVideo(videoUrl: url, type: type, name: self.txtTile.text)
                
            } else if asset.mediaType == MediaType.AUD, let url = asset.attachedMediaURL {
                self.newPhotoDelegate?.uploadAudio(audioUrl: url, type: type, name: self.txtTile.text)
            }
            
        } else {
            
            self.alertError?.show(nil, hidden: nil)
        }
        
    }

    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.newPhotoDelegate?.resetData()
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func selectAttachmentPressed(_ sender: AnyObject) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPopup") as? SelectBuildingViewController {
            
            vc.hasHeader = false
            vc.selectedAction = SelectedAction.back
            vc.delegate = self
            vc.hasQRScanner = false
            vc.items = self.attachmentTypes
            vc.selectType = SelectType.attachmentTypeSelect
            vc.hasHeader = false
            vc.selectedAction = SelectedAction.back
            vc.title = "Attachment Type"
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //show attachment delegate
    func editImagePressed() {
        
        if let asset = self.attachedAsset {
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
            self.newPhotoDelegate?.editImage(asset: asset)
        }
    }
    
    func playVideoPressed(withURL url: URL) {
        
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.newPhotoDelegate?.showVideo(url: url)
        
    }
    
    //MARK: - Delegates
    func setSelectedItem(_ item: AnyObject) {
        
        self.attachedAsset = nil
        self.newPhotoDelegate?.resetPhoto()
        self.selectedAttachmentType = item as? AttachmentType
        self.newPhotoDelegate?.setSelectedAttachmentType(self.selectedAttachmentType)
        self.configAttachmentType()
    }
    
    func recordAudio() {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecordAudioInnerVC") as? RecordAudioViewController {
            vc.delegate = self
            vc.isInner = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func audioRecorded(withUrl url: URL) {
        
        let attachedAsset = AttachedAsset()
        attachedAsset.mediaType = MediaType.AUD
        attachedAsset.attachedMediaURL = url
        
        self.attachedAsset = attachedAsset
        
        self.configContainer()
    }
}

