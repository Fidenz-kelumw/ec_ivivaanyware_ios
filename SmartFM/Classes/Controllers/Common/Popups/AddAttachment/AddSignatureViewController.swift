//
//  AddSignatureViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class AddSignatureViewController: UIViewController {

    var delegate: SetAttachmentDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addAttachmentPressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.delegate?.drawSignature()
    }

}
