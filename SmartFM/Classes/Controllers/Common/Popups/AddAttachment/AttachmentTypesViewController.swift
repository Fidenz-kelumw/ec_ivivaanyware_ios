//
//  AttachmentTypesViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/20/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol AttachmentTypesViewControllerDelegate {
    func recordAudio()
}

class AttachmentTypesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    var inputButtons: [String] = []
    var delegate: AttachmentsDelegate?
    var setAttDelegate: SetAttachmentDelegate?
    var attDelegate: AttachmentTypesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.inputButtons.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ATTButton", for: indexPath) as! InputButtonCollectionViewCell
        
        if let inputMode = InputMode(rawValue: self.inputButtons[indexPath.item]) {
            
            cell.setValues(inputMode: inputMode)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! InputButtonCollectionViewCell
        
        if cell.inputMode == InputMode.Photo {
            self.takePicture()
        } else if cell.inputMode == InputMode.File {
            self.selectPicture()
        } else if cell.inputMode == InputMode.Signature {
            self.signature()
        } else if cell.inputMode == InputMode.Audio {
            self.recordAudio()
        }  else if cell.inputMode == InputMode.Video {
            self.takeVideo()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let vcWidth: CGFloat = 300.0
        let size = vcWidth - 40.0 - 40.0
        let singleSize: CGFloat = size / 5
        
        
        return CGSize(width: singleSize, height: singleSize)
        
    }
    
    
    
    
    //MARK: - Private Functions
    fileprivate func takePicture() {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.setAttDelegate?.takePhoto()
    }
    
    fileprivate func selectPicture() {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.setAttDelegate?.selectPhoto()
    }
    
    fileprivate func signature() {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.setAttDelegate?.drawSignature()
    }
    
    fileprivate func takeVideo() {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.setAttDelegate?.takeVideo()
    }
    
    fileprivate func recordAudio() {
        self.attDelegate?.recordAudio()
    }
}

