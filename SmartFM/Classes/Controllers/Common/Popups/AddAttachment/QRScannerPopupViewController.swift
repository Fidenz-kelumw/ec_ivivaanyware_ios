//
//  QRPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class WRScannerpopupViewController: PopupParentViewController, LIHQRScannerDelegate {
    
    @IBOutlet weak var qrContainer: UIView!
    
    fileprivate var qrScanner: LIHQRScanner?
    fileprivate var lastScannedQr: String?
    
    fileprivate var processingAlert: LIHAlert?
    fileprivate var errorAlert: LIHAlert?
    
    var delegate: GenericFormDelegate?
    var selectedAction: SelectedAction = SelectedAction.dismiss
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initAlerts()
        
        self.qrScanner = LIHQRScanner()
        self.qrScanner?.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        
        self.qrScanner?.frameColor = ThemeRedColor
        self.qrScanner?.initialize(videoContainer: self.qrContainer)
        self.qrScanner?.startSession(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Methods
    
    fileprivate func showError(_ message: String?) {
        
        if let msg = message {
            self.errorAlert?.contentText = msg
        } else {
            self.errorAlert?.contentText = "QR not supported"
        }
        self.errorAlert?.show(nil, hidden: nil)
    }
    
    fileprivate func initAlerts() {
        
        self.errorAlert = LIHAlertManager.getErrorAlert("QR not supported")
        AlertManager().configErrorAlert(self.errorAlert, hasNavBar: true)
        self.errorAlert?.initAlert(self.view)
        
        self.processingAlert = LIHAlertManager.getProcessingAlert("Processing...")
        AlertManager().configProcessing(self.processingAlert, hasNavBar: true)
        self.processingAlert?.initAlert(self.view)
        
    }
    
    fileprivate func processQr(_ QRString: String) {
        
        self.processingAlert?.show({
            
            APIClient().getItemForQr(QRString, lookUpService: "") { (data, message) in
                
                self.processingAlert?.hideAlert(nil)
                if let selected = data, let _ = selected.DisplayText, let _ = selected.Value {
                    
                    self.delegate?.setSelectedItem?(selected)
                    
                    self.qrScanner = nil
                    if self.selectedAction == SelectedAction.dismiss {
                        
                        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
                        
                    } else {
                        
                        self.performSegue(withIdentifier: "backToForm", sender: nil)
                    }
                    
                } else if let msg = message {
                    
                    self.showError(msg)
                    
                } else {
                    
                    self.showError(nil)
                }
            }
            
            }, hidden: nil)
        
    }
    
    //MARK: - LIHQRScannerDelegate
    func qrDetected(_ qrString: String?, error: NSError?) {
        
        if let qr = qrString , error == nil {
            
            self.qrScanner?.stopSession()
            
            self.processQr(qr)
            
        }
    }
    
    
}
