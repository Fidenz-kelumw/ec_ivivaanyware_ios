//
//  ShowAttachmentViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ShowAttachmentDelegate {
    func editImagePressed()
    func playVideoPressed(withURL url: URL)
}

class ShowAttachmentViewController: UIViewController {

    @IBOutlet weak var imgAttachment: UIImageView!
    @IBOutlet weak var imgVideoIndicator: UIImageView!
    
    var image: UIImage?
    var videoUrl: URL?
    var delegate: ShowAttachmentDelegate?
    var mediaType: MediaType = MediaType.IMG
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.mediaType == MediaType.IMG {
            self.imgAttachment.image = self.image
            self.imgVideoIndicator.isHidden = true
            
        } else if self.mediaType == MediaType.VID, let url = self.videoUrl {
            self.imgAttachment.image = Thumbnailer().thumbnailFromVideo(url: url)
            self.imgVideoIndicator.isHidden = false
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func editPressed(_ sender: Any) {
        
        if self.mediaType == MediaType.IMG {
            self.delegate?.editImagePressed()
        } else if self.mediaType == MediaType.VID, let url = self.videoUrl {
            self.delegate?.playVideoPressed(withURL: url)
        }
    }
}
