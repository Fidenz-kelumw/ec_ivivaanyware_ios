//
//  AdvanceMessageViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol AdvanceMessageDelegate {
    func addMessage(text: String, messageType: MessageType)
}

class AdvanceMessageViewController: UIViewController, GenericFormDelegate {

    //IBOutlets
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnMessageType: UIButton!
    
    
    
    //private members
    
    
    
    //public members
    var defaultText: String?
    var messageTypes: [MessageType]?
    var selectedMessageType: MessageType?
    var delegate: AdvanceMessageDelegate?
    
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtMessage.layer.cornerRadius = 3
        self.txtMessage.layer.borderColor = UIColor.gray.cgColor
        self.txtMessage.layer.borderWidth = 1
        
        self.setupUI()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Private Methods
    //////////////////////////////////////////////////////
    
    fileprivate func setupUI() {
        
        self.txtMessage.text = self.defaultText
        self.updateMessageType()
    }
    
    fileprivate func updateMessageType() {
        
        if let mt = self.selectedMessageType?.TypeName {
            self.btnMessageType.setTitle("      \(mt)", for: UIControlState.normal)
        }
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Events
    //////////////////////////////////////////////////////
    
    @IBAction func closePressed(_ sender: Any) {
        
        self.view.endEditing(true)
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func selectPressed(_ sender: Any) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPopup") as? SelectBuildingViewController {
            
            if let types = self.messageTypes {
                vc.items = types
                vc.selectType = SelectType.MessageType
                vc.hasQRScanner = false
                vc.delegate = self
                vc.selectedAction = SelectedAction.back
                vc.hasHeader = false
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func addPressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if let txt = self.txtMessage.text, let mt = self.selectedMessageType {
            self.delegate?.addMessage(text: txt, messageType: mt)
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        }
        
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////

    func setSelectedItem(_ item: AnyObject) {
        
        self.selectedMessageType = item as? MessageType
        self.updateMessageType()
    }
}
