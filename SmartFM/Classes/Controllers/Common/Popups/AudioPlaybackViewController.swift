//
//  AudioPlaybackViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/9/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MZTimerLabel
import AVFoundation

class AudioPlaybackViewController: UIViewController {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var lblTime: MZTimerLabel!
    
    //passed members
    var url: URL?
    
    //private members
    fileprivate var audioPlayer:AVAudioPlayer?
    fileprivate var isPaused: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initializeUI()
    }

    override func viewDidLayoutSubviews() {
        
        self.btnStop.layer.cornerRadius = self.btnStop.frame.size.width / 2
        self.btnPlay.layer.cornerRadius = self.btnPlay.frame.size.width / 2
    }
    
    
    //MARK - Private Methods
    
    fileprivate func initializeUI() {
        
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
    }
    
    fileprivate func pause() {
        
        self.audioPlayer?.pause()
        self.lblTime.pause()
        self.isPaused = true
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
    }
    
    fileprivate func resetLabel() {
        
        self.lblTime.pause()
        self.lblTime.reset()
    }
    
    fileprivate func play() {
        
        if self.isPaused {
            self.audioPlayer?.play()
            self.lblTime.start()
            self.btnPlay.setImage(UIImage(named: "pause"), for: UIControlState.normal)
            self.isPaused = false
            
        } else {
            if let url = self.url {
                
                do {
                    let data = try Data(contentsOf: url)
                    self.audioPlayer = try AVAudioPlayer(data: data)
                    self.audioPlayer?.delegate = self
                    self.audioPlayer?.play()
                    self.lblTime.start()
                    self.btnPlay.setImage(UIImage(named: "pause"), for: UIControlState.normal)
                    
                } catch let e {
                    NSLog("Failed to play audio with url \(url) | \(e)")
                }
                
            } else {
                NSLog("No url found")
            }
        }
        
        
        
    }
    
    fileprivate func stop() {
        
        self.isPaused = false
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
        self.audioPlayer?.stop()
        self.resetLabel()
    }

    //MARK - Events
    
    @IBAction func closePressed(_ sender: Any) {
        
        self.audioPlayer = nil
        mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func playPressed(_ sender: Any) {
        
        if let player = self.audioPlayer {
            
            if player.isPlaying {
                self.pause()
            } else {
                self.play()
            }
        } else {
            self.play()
        }
    }
    
    @IBAction func stopPressed(_ sender: Any) {
        
        self.stop()
    }
    
}


extension AudioPlaybackViewController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        self.resetLabel()
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
    }
}
