
//
//  CustomSearchPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/10/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol CustomSearchDelegate {
    
    func applyFilter(_ fieldData: String)
}

class CustomSearchPopupViewController: PopupParentViewController, GenericFormDelegate, FormTypeTableViewCellDelegate {
    
    @IBOutlet weak var containerView: UIView!
    
    var objectType: String?
    var delegate: CustomSearchDelegate?
    var defaultData: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GenericFormVc") as? GenericFormViewController {
            
            vc.objectType = self.objectType
            vc.formType = FormType.customSearch
            vc.delegate = self
            vc.customSearchDelegate = self
            vc.defaultData = self.convertStringToDictionary(self.defaultData) as [NSDictionary]?
            
            self.addChildViewController(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height);
            self.containerView.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Methods
    func convertStringToDictionary(_ text: String?) -> [[String:String]]? {
        
        if let data = text?.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String:String]]
            } catch let error as NSError {
                NSLog(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: - Delegate Methods
    func customSearch(_ fieldData: String) {
        
        self.delegate?.applyFilter(fieldData)
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    func pushVc(_ viewController: UIViewController) {
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - Events
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }

}
