//
//  DateAndTimeSelectViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/26/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class DateAndTimeSelectViewController: PopupParentViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var constHeaderHeight: NSLayoutConstraint!
    
    var delegate: GenericFormDelegate?
    var hasHeader = true
    var selectedAction = SelectedAction.dismiss
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if hasHeader {
            self.constHeaderHeight.constant = 40.0
        } else {
            self.constHeaderHeight.constant = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func okPressed(_ sender: AnyObject) {
        
        let date = self.datePicker.date
        
        self.delegate?.setSelectedItem?(date as AnyObject)
        
        if self.selectedAction == SelectedAction.dismiss {
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
