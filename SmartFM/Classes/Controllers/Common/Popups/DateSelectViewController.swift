//
//  DateSelectViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/26/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit


class DateSelectViewController: PopupParentViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var constHeaderHeight: NSLayoutConstraint!
    
    var delegate: GenericFormDelegate?
    var hasHeader = true
    var selectedAction: SelectedAction = SelectedAction.dismiss
    
    // custom settings
    var minimumDate: String?  // to keep start date
    var isStartDate: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if hasHeader {
            self.constHeaderHeight.constant = 40.0
        } else {
            self.constHeaderHeight.constant = 0
        }
        
        self.setDateWithMinMax()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setDateWithMinMax() {
        self.datePicker.maximumDate = Date()
        
        if !isStartDate, let sd = minimumDate {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let dateFromString: Date = dateFormatter.date(from: sd)!
            self.datePicker.minimumDate = dateFromString
        }
    }
    
    
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func okPressed(_ sender: AnyObject) {
        
        var date = self.datePicker.date
        if let newDate = DateTimeManager().setTimeForNSDate(date, hours: 0, minutes: 0, seconds: 0) {
            date = newDate
        }
        
        self.delegate?.setSelectedItem?(date as AnyObject)
        
        if self.selectedAction == SelectedAction.dismiss {
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
