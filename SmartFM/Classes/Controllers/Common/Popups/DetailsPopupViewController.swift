//
//  DetailsPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol DetailsPopupDelegate {
    
    func updatePressed(_ text:String, forItem item: CheckItem)
}

class DetailsPopupViewController: PopupParentViewController {

    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var updateButtonHeightConstraint: NSLayoutConstraint!
    
    var item: CheckItem?
    var delegate: DetailsPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtDescription.isEditable = self.item?.DescriptionEdit == "1" ? true : false
        self.txtDescription.text = self.item?.Description
        
        if self.item?.DescriptionEdit == "1" {
            self.updateButtonHeightConstraint.constant = 40.0
        } else {
            self.updateButtonHeightConstraint.constant = 0.0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func updatePressed(_ sender: AnyObject) {
        
        if let itm = self.item {
            self.delegate?.updatePressed(self.txtDescription.text, forItem: itm)
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        }
    }
    
}
