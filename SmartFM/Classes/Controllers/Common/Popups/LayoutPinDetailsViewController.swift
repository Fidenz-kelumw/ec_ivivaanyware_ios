//
//  LayoutPinDetailsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/10/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol LayoutPinDetailsDelegate {
    
    func showObjectDetailsPage(_ objectType: String, objectKey: String)
}

class LayoutPinDetailsViewController: PopupParentViewController {

    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    
    var delegate: LayoutPinDetailsDelegate?
    var objectKey: String?
    var objectType: String?
    var pin: LayoutPin?
    var pinLabel: PinLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var objectAvailable = false
        if let _ = self.objectKey, let ot = self.objectType {
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                objectAvailable = true
            }
        }
        
        if let oKey = self.objectKey, oKey != "", let oType = self.objectType, oType != "" {
            self.btnView.alpha = 1.0
            self.btnView.isEnabled = true
        } else {
            self.btnView.alpha = 0.8
            self.btnView.isEnabled = false
        }

        self.setValues(self.pin?.Name, description: self.pin?.Description, hasObject:  objectAvailable)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues(_ name: String?, description: String?, hasObject: Bool) {
        
        if let obName = name {
            self.lblName.text = obName
        } else {
            self.lblName.text = "N/A"
        }
        
        if let obDesc = description {
            self.lblDescription.text = obDesc
        } else {
            self.lblDescription.text = "N/A"
        }
        
        if !hasObject {
            self.btnViewHeightConstraint.constant = 0
        } else {
            self.btnViewHeightConstraint.constant = 40.0
        }
        
        if let nm = self.pinLabel?.Name {
            self.nameLabel.text = nm
        }
        
        if let desc = self.pinLabel?.Description {
            self.descLabel.text = desc
        }
    }

    @IBAction func btnViewClicked(_ sender: AnyObject) {
        
        if let ok = self.objectKey, let ot = self.objectType {
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
            self.delegate?.showObjectDetailsPage(ot, objectKey: ok)
        }
    }

    @IBAction func closePressed(_ sender: AnyObject) {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
}
