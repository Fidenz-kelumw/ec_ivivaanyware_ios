//
//  MarkerDetailsViewController.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/11/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import Kingfisher

protocol MarkerDetailsDelegate {
    func markerViewDetailsPressed(detailsObject: SpotDetails)
}

class MarkerDetailsViewController: UIViewController {
    
    @IBOutlet var btnViewDetails: UIButton!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var tvTitle: UILabel!
    @IBOutlet var tvDescription: UILabel!
    @IBOutlet var imgHeader: UIImageView!
    
    var detailsObject: SpotDetails?
    var delegate: MarkerDetailsDelegate?
    
    var apiKey: String?
    var account: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.populatePopupData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnClosePressed(_ sender: Any) {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }

    @IBAction func btnViewDetailsPressed(_ sender: Any) {
        self.mz_dismissFormSheetController(animated: true) { (status) in
            self.delegate?.markerViewDetailsPressed(detailsObject: self.detailsObject!)
        }
    }
    
    private func populatePopupData(){
        
        let userManager = UserManager()
        self.apiKey = userManager.getApiKey()
        self.account = userManager.getAccount()
        
        if let ssl = userManager.getSSL() {
            Protocol = ssl
        }
        if let data = detailsObject {
            
            self.tvTitle.text = data.SpotPopupHeading
            self.tvDescription.text = data.SpotPopupDescription
            self.btnViewDetails.setTitle(data.ButtonText, for: .normal)
            
            if let buttonStatus = data.EnableActionButton,  buttonStatus == "0" {
                self.btnViewDetails.isEnabled = false
                self.btnViewDetails.alpha = 0.8
            }
            
            if let imgURL = data.SpotPopupImageURL, let accountThis = account, let apiKeyThis = apiKey {
                if imgURL.isEmpty == false {
                    
                    let imageURL = "\(Protocol)://\(accountThis)/\(imgURL)?apikey=\(apiKeyThis)"
                    let url = URL(string: imageURL)
                    imgHeader.kf.setImage(with: url)
                }
            }
            
        }
    }
}
