//
//  PushMessageViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/17/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol PushMessageDelegate {
    func showObjectDetails(_ objectType: String, objectKey: String)
}

class PushMessageViewController: PopupParentViewController {

    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewButtonHeightConstraint: NSLayoutConstraint!
    
    var pushMessage: PushMessage?
    var delegate: PushMessageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var objectAvailable = false
        if let _ = self.pushMessage?.ObjectKey, let ot = self.pushMessage?.ObjectType {
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                objectAvailable = true
            }
        }
        
        self.setValues(self.pushMessage?.MsgFrom, message: self.pushMessage?.Message, hasObject: objectAvailable)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setValues(_ from: String?, message: String?, hasObject: Bool) {
        
        if let obName = from {
            self.lblFrom.text = obName
        } else {
            self.lblFrom.text = "N/A"
        }
        
        if let msg = message {
            self.lblMessage.text = msg
        } else {
            self.lblMessage.text = "N/A"
        }
        
        if !hasObject {
            self.viewButtonHeightConstraint.constant = 0
        } else {
            self.viewButtonHeightConstraint.constant = 40.0
        }
    }
    
    @IBAction func btnViewPressed(_ sender: UIButton) {
        
        if let ok = self.pushMessage?.ObjectKey, let ot = self.pushMessage?.ObjectType {
            
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                
                self.delegate?.showObjectDetails(ot, objectKey: ok)
            }
        }
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }

    @IBAction func closePressed(_ sender: UIButton) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
}
