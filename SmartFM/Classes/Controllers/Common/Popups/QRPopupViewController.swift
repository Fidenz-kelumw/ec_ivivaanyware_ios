//
//  QRPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import AVFoundation

class QRPopupViewController: PopupParentViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var qrContainer: UIView!
    @IBOutlet var qrBorder: UIView!
    
    fileprivate var lastScannedQr: String?
    fileprivate var processingAlert: LIHAlert?
    fileprivate var errorAlert: LIHAlert?
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var initialPosition: CGRect?
    var isInitialLoad: Bool = false
    var lookUpService = ""
    var hasClose = false
    
    var delegate: GenericFormDelegate?
    var selectedAction: SelectedAction = SelectedAction.dismiss
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !self.hasClose {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        self.isInitialLoad = true
        
        self.initAlerts()
        
        qrBorder?.layer.borderColor = ThemeRedColor.cgColor
        qrBorder?.layer.borderWidth = 2
    }
    
    override func viewDidLayoutSubviews() {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if isInitialLoad {
            self.initialPosition = self.qrBorder?.frame
            self.isInitialLoad = false
        }
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error:NSError?
        let input: AnyObject!
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if (error != nil) {
            
            NSLog("\(error?.localizedDescription)")
            return
        }
        
        captureSession = AVCaptureSession()
        captureSession?.addInput(input as! AVCaptureInput)
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = self.qrContainer.layer.bounds
        if let preLayer = videoPreviewLayer {
            self.qrContainer.layer.addSublayer(preLayer)
        }
        
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async { () -> Void in
            
            self.captureSession?.startRunning()
        }
        
        self.qrContainer.bringSubview(toFront: qrBorder!)
        self.qrBorder.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.captureSession?.stopRunning()
        self.videoPreviewLayer?.removeFromSuperlayer()
        self.videoPreviewLayer = nil
        self.captureSession = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Methods
    
    fileprivate func showError(_ message: String?) {
        
        if let msg = message {
            self.errorAlert?.contentText = msg
        } else {
            self.errorAlert?.contentText = "QR not supported"
        }
        self.errorAlert?.show(nil, hidden: nil)
    }
    
    fileprivate func startCaptureRunning() {
        
        
        self.processingAlert?.hideAlert({ () -> () in
            self.qrBorder?.frame = self.initialPosition!
            self.captureSession?.startRunning()
            //self.view.bringSubviewToFront(self.alertProcessing!.alertView!)
            //self.view.bringSubviewToFront(self.alertError!.alertView!)
        })
    }
    
    fileprivate func initAlerts() {
        
        self.errorAlert = LIHAlertManager.getErrorAlert("QR not supported")
        AlertManager().configErrorAlert(self.errorAlert, hasNavBar: true)
        self.errorAlert?.initAlert(self.view)
        
        self.processingAlert = LIHAlertManager.getProcessingAlert("Processing...")
        AlertManager().configProcessing(self.processingAlert, hasNavBar: true)
        self.processingAlert?.initAlert(self.view)
        
    }
    
    fileprivate func processQr(_ QRString: String) {
        
        self.processingAlert?.show({ 
            
            APIClient().getItemForQr(QRString, lookUpService: self.lookUpService) { (data, message) in
                
                self.processingAlert?.hideAlert(nil)
                if let selected = data, let _ = selected.DisplayText, let _ = selected.Value {
                    
                    self.delegate?.setSelectedItem?(selected)
                    
                    if self.selectedAction == SelectedAction.dismiss {
                        
                        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
                        
                    } else if self.selectedAction == SelectedAction.popVc {
                        
                        _ = self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        self.performSegue(withIdentifier: "backToForm", sender: nil)
                    }
                    
                } else if let msg = message {
                    
                    self.showError(msg)
                    self.startCaptureRunning()
                    
                } else {
                    
                    self.showError(nil)
                    self.startCaptureRunning()
                }
            }
            
            }, hidden: nil)
        
    }
    
    
    //MARK: - Events
    
    @IBAction func closePressed(_ sender: UIBarButtonItem) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    
    //MARK: - LIHQRScannerDelegate
    func qrDetected(_ qrString: String?, error: NSError?) {
        
        
    }
    
    
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            //qrBorder?.frame = CGRectZero
            qrBorder?.frame = self.initialPosition!
            
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrBorder?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                self.captureSession?.stopRunning()
                self.processQr(metadataObj.stringValue)
            }
        }
        
        let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
        
        qrBorder?.frame = barCodeObject.bounds
    }
}
