//
//  RecordAudioViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/4/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import AVFoundation
import FontAwesomeKit
import MZTimerLabel

protocol RecordAudioDelegate {
    
    func audioRecorded(withUrl url: URL)
}

class RecordAudioViewController: UIViewController {

    
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var imageviewRecord: UIImageView!
    @IBOutlet weak var lblTime: MZTimerLabel!
    @IBOutlet weak var btnAttach: UIButton!
    
    //public members
    var delegate: RecordAudioDelegate?
    var isInner = false
    
    //private members
    fileprivate var recordingSession: AVAudioSession?
    fileprivate var audioRecorder: AVAudioRecorder?
    fileprivate var audioPlayer:AVAudioPlayer?
    fileprivate var recordedURL: URL? {
        didSet {
            if let _ = self.recordedURL {
                self.enableAttachButton()
            } else {
                self.disableAttachButton()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isInner {
            self.navigationItem.leftBarButtonItem = nil
//            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        
        self.initializeUI()

        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession?.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession?.setActive(true)
            recordingSession?.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.btnRecord.layer.cornerRadius = self.btnRecord.frame.size.width / 2
        self.btnClear.layer.cornerRadius = self.btnClear.frame.size.width / 2
        self.btnPlay.layer.cornerRadius = self.btnPlay.frame.size.width / 2
    }
    
    
    //MARK - Private Methods
    
    fileprivate func initializeUI() {
        
        self.enableRecordButton()
        self.disablePlayButton()
        self.disableClearButton()
        self.resetTimer()
        self.disableAttachButton()
    }
    
    fileprivate func loadRecordUI() {
        
        self.imageviewRecord.image = UIImage(named: "mic")
    }
    
    fileprivate func loadStopUI() {
        
        self.imageviewRecord.image = UIImage(named: "stop")
    }
    
    fileprivate func enableRecordButton() {
        
        self.btnRecord.isEnabled = true
        self.btnRecord.alpha = 1
        self.imageviewRecord.alpha = 1
    }
    
    fileprivate func disableRecordButton() {
        
        
        self.btnRecord.isEnabled = false
        self.btnRecord.alpha = 0.5
        self.imageviewRecord.alpha = 0.5
    }
    
    fileprivate func enablePlayButton() {
        
        self.btnPlay.isEnabled = true
        self.btnPlay.alpha = 1
    }
    
    fileprivate func disablePlayButton() {
        
        self.btnPlay.isEnabled = false
        self.btnPlay.alpha = 0.5
    }
    
    fileprivate func enableClearButton() {
        
        self.btnClear.isEnabled = true
        self.btnClear.alpha = 1
    }
    
    fileprivate func disableClearButton() {
        
        self.btnClear.isEnabled = false
        self.btnClear.alpha = 0.5
    }
    
    fileprivate func enableAttachButton() {
        
        self.btnAttach.isEnabled = true
        self.btnAttach.alpha = 1
    }
    
    fileprivate func disableAttachButton() {
        
        self.btnAttach.isEnabled = false
        self.btnAttach.alpha = 0.5
    }
    
    fileprivate func clear() {
        
        if let url = FileHandler.getDirectory(forType: MediaType.AUD) {
            FileHandler.deleteContentsAtURL(path: url)
        }
        
        self.disableClearButton()
        self.disablePlayButton()
        self.enableRecordButton()
        
        self.recordedURL = nil
    }

    fileprivate func play() {
        
        if let url = self.recordedURL {
            
            do {
                
                self.audioPlayer = try AVAudioPlayer(contentsOf: url)
                self.audioPlayer?.delegate = self
                self.audioPlayer?.play()
                self.lblTime.start()
                self.btnPlay.setImage(UIImage(named: "stop_player"), for: UIControlState.normal)
                
            } catch let e {
                NSLog("Failed to initialize player with url \(url.absoluteString) | \(e.localizedDescription)")
            }
        }
    }
    
    fileprivate func constructName() -> String {
        var imageName = UUID().uuidString
        if let userKey = UserManager().getUserKey() {
            imageName = "\(userKey)_\(imageName)"
        }
        imageName = "\(imageName)_\(Date().timeIntervalSince1970)"
        
        return imageName
    }
    
    fileprivate func stopPlaying() {
        
        self.audioPlayer?.stop()
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
    }
    
    fileprivate func startRecordingAudio() {
        
        if let audioFilename = FileHandler.getDirectory(forType: MediaType.AUD)?.appendingPathComponent("\(self.constructName()).m4a") {
        
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 12000,
                AVNumberOfChannelsKey: 1,
                AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
            ]
            
            do {
                audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
                audioRecorder?.delegate = self
                audioRecorder?.record()
                self.lblTime.start()
                self.loadStopUI()
                
            } catch {
                self.finishRecording(success: false)
                self.loadRecordUI()
            }
        
        }
        
    }
    
    fileprivate func finishRecording(success: Bool) {
        audioRecorder?.stop()
        audioRecorder = nil
        
        if success {
            self.disableRecordButton()
        } else {
            self.enableRecordButton()
        }
    }
    
    fileprivate func resetTimer() {
        self.lblTime.pause()
        self.lblTime.reset()
    }
    
    //MARK - Events
    
    @IBAction func closePressed(_ sender: Any) {
        
        self.audioRecorder = nil
        self.audioPlayer = nil
        
        if self.isInner {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            mz_dismissFormSheetController(animated: true, completionHandler: nil)
        }
    }
    
    @IBAction func attachPressed(_ sender: Any) {
        
        if let url = self.recordedURL {
            
            self.delegate?.audioRecorded(withUrl: url)
            if self.isInner {
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                mz_dismissFormSheetController(animated: true, completionHandler: nil)
            }
        }
    }
    
    @IBAction func recordPressed(_ sender: Any) {
        
        if self.audioRecorder == nil {
            
            self.startRecordingAudio()
            
        } else {
            
            if self.audioRecorder!.isRecording {
                
                self.audioRecorder?.stop()
                self.loadRecordUI()
                
            } else {
                
                self.startRecordingAudio()
            }
        }
        
        
    }
    
    @IBAction func clearPressed(_ sender: Any) {
        
        self.clear()
    }
    
    @IBAction func playPressed(_ sender: Any) {
        
        if let player = self.audioPlayer {
            if player.isPlaying {
                self.stopPlaying()
            } else {
                self.play()
            }
        } else {
            self.play()
        }
    }
}


extension RecordAudioViewController: AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    //audio recorder
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        if flag {
            self.finishRecording(success: true)
            self.recordedURL = recorder.url
            
            self.enablePlayButton()
            self.enableClearButton()
            self.disableRecordButton()
        }
        
        self.resetTimer()
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        
        NSLog("Error while recording | \(error?.localizedDescription)")
    }
    
    
    
    //audio player
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        self.btnPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
        self.resetTimer()
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        
        NSLog("Error while playing | \(error?.localizedDescription)")
    }
}
