//
//  SAARPopupItemTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/28/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SAARPopupItemTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!
    
    var data: LData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setValues(val: LData) {
        self.data = val
        self.lblTitle.text = val.DisplayText
        
        self.setItemSelected(selected: val.isSelected)
    }
    
    fileprivate func setItemSelected(selected: Bool) {
        
        let imgName = selected ? "img_saar_selected" : "img_saar_unselected"
        self.imgSelected.image = UIImage(named: imgName)
    }
}
