//
//  SAARPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/28/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit



class SAARPopupViewController: PopupParentViewController, GenericFormDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //for search
    fileprivate var isFromClear: Bool = false
    fileprivate var searchActive : Bool = false
    fileprivate var hasResults: Bool = false
    
    
    fileprivate var filteredItems: [LData] = []
    fileprivate var isError: Bool = false
    
    var lookupService: String?
    var optionsList: [OptionsList]?
    var fieldInfo: String?
    var delegate: GenericFormDelegate?
    var formTypeDelegate: FormTypeTableViewCellDelegate?
    var hasQRScanner: Bool = true
    var selectedAction = SelectedAction.dismiss
    var objectInfo: ObjectInfo?
    var objectType: String?
    var titleString: String?
    var searchtype: SearchType = SearchType.fetchData
    var items: [LData] = [] {
        didSet {
            self.filteredItems = self.items
        }
    }
    var errorMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = self.titleString {
            self.title = title
        }
        
        if !self.hasQRScanner {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if self.selectedAction == SelectedAction.back {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchBar.delegate = self
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        if self.searchtype == SearchType.fetchData {
            self.fetchData()
        } else {
            self.hideLoaing()
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "QRFrSAAR" {
            
            if let vc = segue.destination as? QRPopupViewController {
                
                vc.delegate = self
                vc.selectedAction = SelectedAction.popVc
                
                if let lus = self.lookupService {
                    vc.lookUpService = lus
                }
            }
        }
    }
    
    
    //MARK: - Private Methods
    func fetchData() {
        
        self.isError = false
        if let lus = self.lookupService, let ot = self.objectType {
            
            APIClient().fetchLookupServices(lus, fieldInfo: self.fieldInfo, objectType: ot) { (data, error) in
                
                if let items = data {
                    if let er = error {
                        self.isError = true
                        self.errorMessage = er
                    } else{
                        self.items = items
                    }
                    
                } else {
                    if let er = error {
                        self.isError = true
                        self.errorMessage = er
                    } else{
                        self.isError = true
                    }
                }
                self.fillSelected()
                
                self.hideLoaing()
                self.tableView.reloadData()
            }
            
        } else if let options = self.optionsList {
            
            var ldatas: [LData] = []
            for option in options {
                ldatas.append(self.optionToLData(option: option))
            }
            self.fillSelected()
            self.tableView.reloadData()
        }
    }
    
    func hideLoaing() {
        self.loaingView.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    fileprivate func optionToLData(option: OptionsList) -> LData {
        
        let data = LData()
        data.DisplayText = option.DisplayText
        data.Value = option.Value
        
        return data
    }
    
    fileprivate func fillSelected() {
        
        if let selectedVals = self.objectInfo?.defaultData?.ValuesArray {
        
            for item in self.items {
                
                if let val = item.Value {
                    
                    if selectedVals.contains(val) {
                        item.isSelected = true
                    }
                }
            }
        }
    }
    
    fileprivate func updateAndReload(val: LData) {
        
        for n in 0..<self.items.count {
            
            let item = self.items[n]
            
            if item.Value == val.Value {
                item.isSelected = true
                break
            }
            
        }
        
        for n in 0..<self.filteredItems.count {
            
            let item = self.filteredItems[n]
            
            if item.Value == val.Value {
                item.isSelected = true
                let ip = IndexPath(item: n, section: 0)
                self.tableView.reloadRows(at: [ip], with: UITableViewRowAnimation.fade)
                break
            }
            
        }
        
    }
    
    func addItem(item: LData) {
        
        if let newValue = item.Value {
            if let dd = self.objectInfo?.defaultData {
                
                if dd.ValuesArray != nil {
                    dd.ValuesArray!.append(newValue)
                } else {
                    dd.ValuesArray = [newValue]
                }
                
            } else {
                
                let dd = DefaultData()
                dd.ValuesArray = [newValue]
                self.objectInfo?.defaultData = dd
            }
            
            self.formTypeDelegate?.defaultDataChanged?(new: self.objectInfo?.defaultData, forItem: self.objectInfo)
        }
    }
    
    func removeItem(item: LData) {
        
        if let arr = self.objectInfo?.defaultData?.ValuesArray, let remVal = item.Value {
            let newArray = arr.filter({$0 != remVal})
            self.objectInfo?.defaultData?.ValuesArray = newArray
            
            self.formTypeDelegate?.defaultDataChanged?(new: self.objectInfo?.defaultData, forItem: self.objectInfo)
        }
    }
    
    
    //MARK: - Events
    @IBAction func close(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func closePressed(_ sender: AnyObject) {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    
    func setSelectedItem(_ item: AnyObject) {
        
        if let itm = item as? LData {
            self.addItem(item: itm)
             self.updateAndReload(val: itm)
        }
    }
}


extension SAARPopupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.filteredItems.count > 0 {
            return self.filteredItems.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.filteredItems.count > 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SAARTableCell") as! SAARPopupItemTableViewCell
            
            cell.setValues(val: self.filteredItems[(indexPath as NSIndexPath).row])
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                if let msg = self.errorMessage {
                    cell.setValues("\(msg)", bgColor: UIColor.white)
                    cell.setColor(txtColor: UIColor.red)
                } else {
                    cell.setValues("Could not fetch data.", bgColor: UIColor.white)
                    cell.setColor(txtColor: UIColor.red)
                }
                return cell
            } else {
                cell.setValues("No Items Found.", bgColor: UIColor.white)
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.filteredItems[indexPath.row]
        data.isSelected = !data.isSelected
        
        if data.isSelected {
            self.addItem(item: data)
        } else {
            self.removeItem(item: data)
        }
        
        self.formTypeDelegate?.originalValueChanged?()
        
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension SAARPopupViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if !isFromClear {
            searchActive = true;
        } else {
            self.isFromClear = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            searchActive = false;
        } else{
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filteredItems = self.items.filter(){ $0.DisplayText?.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil }
        
        
        if searchText.characters.count <= 0 {
            searchActive = false;
            self.isFromClear = true
            self.filteredItems = self.items
        } else {
            searchActive = true
        }
        
        self.tableView.reloadData()
    }
}
