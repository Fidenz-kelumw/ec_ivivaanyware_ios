//
//  SearchAndSelectViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

enum SearchType {
    case fetchData, cachedData
}

class SearchAndSelectViewController: PopupParentViewController {

    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    //for search
    fileprivate var isFromClear: Bool = false
    fileprivate var searchActive : Bool = false
    fileprivate var hasResults: Bool = false
    fileprivate var filteredItems: [AnyObject] = []
    fileprivate var isError: Bool = false
    
    var lookupService: String?
    var fieldInfo: String?
    var delegate: GenericFormDelegate?
    var hasQRScanner: Bool = true
    var selectedAction = SelectedAction.dismiss
    var hasHeader: Bool = true
    var titleString: String?
    var objectType: String?
    var searchtype: SearchType = SearchType.fetchData
    var items: [AnyObject] = [] {
        didSet {
            self.filteredItems = self.items
        }
    }
    var noDataValue: String?
    var errorMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = self.titleString {
            self.title = title
            self.popupTitle.text = title
        }
        
        if !self.hasHeader {
            self.headerHeightConstraint.constant = 0.0
        } else {
            self.headerHeightConstraint.constant = 40.0
        }
        
        if !self.hasQRScanner {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if self.selectedAction == SelectedAction.back {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchBar.delegate = self
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        if self.searchtype == SearchType.fetchData {
            self.fetchData()
        } else {
            self.hideLoaing()
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "QRForSAS" {
            
            if let vc = segue.destination as? QRPopupViewController {
                
                vc.delegate = self.delegate
                vc.selectedAction = self.selectedAction
                
                if let lus = self.lookupService {
                    vc.lookUpService = lus
                }
            }
        }
    }
    
    
    //MARK: - Private Methods
    func fetchData() {
        
        self.isError = false
        if let lus = self.lookupService, let ot = objectType {
            
            APIClient().fetchLookupServices(lus, fieldInfo: self.fieldInfo, objectType: ot) { (data, error) in
                
                if var items = data {
                    if self.noDataValue != nil {
                        let opt = LData()
                        opt.DisplayText = self.noDataValue
                        opt.Value = ""
                        items.append(opt)
                    }
                    
                    if let er = error {
                        self.isError = true
                        self.errorMessage = er
                    } else {
                        self.items = items
                    }
                    
                } else {
                    if let er = error {
                        self.isError = true
                        self.errorMessage = er
                    } else {
                        self.isError = true
                    }
                }
                self.hideLoaing()
                self.tableView.reloadData()
            }
        }
    }
    
    func hideLoaing() {
        self.loaingView.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    
    //MARK: - Events
    @IBAction func close(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }

    @IBAction func closePressed(_ sender: AnyObject) {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
}


extension SearchAndSelectViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.filteredItems.count > 0 {
            return self.filteredItems.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.filteredItems.count > 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            
            if let data = self.filteredItems as? [LData] {
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                cell.textLabel?.text = data[(indexPath as NSIndexPath).row].DisplayText
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                if let msg = self.errorMessage {
                    cell.setValues("\(msg)", bgColor: UIColor.white)
                    cell.setColor(txtColor: UIColor.red)
                } else {
                    cell.setValues("Could not fetch data.", bgColor: UIColor.white)
                    cell.setColor(txtColor: UIColor.red)
                }
                return cell
            } else {
                cell.setValues("No Items Found.", bgColor: UIColor.white)
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.filteredItems.count >= (indexPath as NSIndexPath).row {
            self.delegate?.setSelectedItem?(self.filteredItems[(indexPath as NSIndexPath).row])
            if self.selectedAction == SelectedAction.back {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
            }
        }
    }
}


extension SearchAndSelectViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if !isFromClear {
            searchActive = true;
        } else {
            self.isFromClear = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            searchActive = false;
        } else{
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let data = self.items as? [LData] {
            self.filteredItems = data.filter(){ ($0.DisplayText?.lowercased().contains(searchText.lowercased()))!}
                //(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil }
        }
        
        
        if searchText.characters.count <= 0 {
            searchActive = false;
            self.isFromClear = true
            self.filteredItems = self.items
        } else {
            searchActive = true
        }
        
        self.tableView.reloadData()
    }
}
