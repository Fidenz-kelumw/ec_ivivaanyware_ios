//
//  SelectBuildingViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit


enum SelectType {
    case location, formData, attachmentTypeSelect, ImageUploadSize, MessageType, SubObjectFilter
}

enum SelectedAction {
    case dismiss, back, popVc
}

class SelectBuildingViewController: PopupParentViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    var items:[Any] = []
    var titleString: String?
    var selectType: SelectType = SelectType.location
    var delegate: GenericFormDelegate?
    var hasHeader: Bool = true
    var hasQRScanner: Bool = true
    var selectedAction = SelectedAction.back
    var noDataValue: String?
    //var isNavigationControlled = false
    
    ///////////////////////////////////////////
    //MARK: - View Controller Methods
    ///////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !self.hasHeader {
            self.headerHeightConstraint.constant = 0.0
        } else {
            self.headerHeightConstraint.constant = 40.0
        }
        
        if self.selectedAction == SelectedAction.back {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        if !self.hasQRScanner {
            self.navigationItem.rightBarButtonItem = nil
        }
        

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        if let ttl = self.titleString {
            self.popupTitle.text = ttl
        }
        
        if let noVal = self.noDataValue {
            
            if self.selectType == SelectType.formData {
                let opt = OptionsList()
                opt.DisplayText = noVal
                opt.Value = ""
                self.items.insert(opt, at: 0)
            } else if self.selectType == SelectType.SubObjectFilter {
                let opt = SubObjectOptionsList()
                opt.DisplayText = noVal
                opt.Value = ""
                self.items.insert(opt, at: 0)
            }
        }
        self.loaingView.isHidden = true
        self.activityIndicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "QRForDS" {
            
            if let vc = segue.destination as? QRPopupViewController {
                
                vc.delegate = self.delegate
                vc.selectedAction = self.selectedAction
            }
        }
    }
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    @IBAction func close(_ sender: UIButton) {
        
        mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }

    @IBAction func closePressed(_ sender: AnyObject) {
        
        mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
}


extension SelectBuildingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        switch self.selectType {
            
        case SelectType.location:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! Building).LocationName
            
        case SelectType.formData:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! OptionsList).DisplayText
            
        case SelectType.attachmentTypeSelect:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! AttachmentType).TypeName
            
        case SelectType.ImageUploadSize:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! String)
            
        case SelectType.MessageType:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! MessageType).TypeName
            
        case SelectType.SubObjectFilter:
            cell.textLabel?.text = (self.items[(indexPath as NSIndexPath).row] as! SubObjectOptionsList).DisplayText
            
        default: break
            
        }
        
        cell.textLabel?.font = UIFont(name: "Melbourne", size: 18)!
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.setSelectedItem?(self.items[(indexPath as NSIndexPath).row] as AnyObject)
        if self.selectedAction == SelectedAction.back {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        }
    }
}
