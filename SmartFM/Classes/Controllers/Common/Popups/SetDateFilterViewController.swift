//
//  SetDateFilterViewController.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 1/10/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import Toast_Swift

protocol SetDateFilterDelegate {
    func filterWithDate(startDate: String, endDate: String)
}

class SetDateFilterViewController: UIViewController, GenericFormDelegate {
    
    @IBOutlet var btnStartDate: UIButton!
    @IBOutlet var btnClearStartDate: UIButton!
    @IBOutlet var btnEndDate: UIButton!
    @IBOutlet var btnClearEndDate: UIButton!
    @IBOutlet var btnSetData: UIButton!
    
    var delegate: SetDateFilterDelegate?
    var startDateFromController: String?
    var endDateFromController: String?
    
    fileprivate var selectedButton:Int = 0
    fileprivate var minDate:Int = 0
    fileprivate var minMonth:Int = 0
    fileprivate var minYear:Int = 0
    
    fileprivate var isStartDateSet:Bool = false
    fileprivate var isEndDateSet:Bool = false
    
    fileprivate let colorRed = UIColor(rgb: 0x00E8041E)
    fileprivate let colorGray = UIColor(rgb: 0x00282F34)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setInitialData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closePressed(_ sender: Any) {
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func startDatePressed(_ sender: Any) {
        self.selectedButton = 1;
        
        self.showDateSelection()
    }
    
    @IBAction func clearStartDatePressed(_ sender: Any) {
        self.btnStartDate.setTitle("Start Date", for: UIControlState.normal)
        self.isStartDateSet = false
        self.btnClearStartDate.isEnabled = false
        self.btnSetData.isEnabled = false
        
        self.buttonState(button: btnClearStartDate, state: false)
        self.buttonState(button: btnSetData, state: false)
    }
    
    @IBAction func endDatePressed(_ sender: Any) {
        self.selectedButton = 2;
        
        if (isStartDateSet){
            self.showDateSelection()
        } else {
            self.view.makeToast("Please set \"Start Date\" first", duration: 2.0, position: .bottom)
        }
    }
    
    @IBAction func clearEndDatePressed(_ sender: Any) {
        self.btnEndDate.setTitle("End Date", for: UIControlState.normal)
        self.isEndDateSet = false
        self.btnClearEndDate.isEnabled = false
        self.btnSetData.isEnabled = false
        
        self.buttonState(button: btnClearEndDate, state: false)
        self.buttonState(button: btnSetData, state: false)
    }
    
    @IBAction func setFilterPressed(_ sender: Any) {
        self.mz_dismissFormSheetController(animated: true) { (status) in
            self.delegate?.filterWithDate(startDate: (self.btnStartDate.titleLabel?.text)!, endDate: (self.btnEndDate.titleLabel?.text)!)
        }
    }
    
    private func setInitialData() {
       
        if let sd = startDateFromController, let ed = endDateFromController {
            self.btnStartDate.setTitle(sd, for: UIControlState.normal)
            self.btnEndDate.setTitle(ed, for: UIControlState.normal)
            
            self.btnClearStartDate.isEnabled = true
            self.btnClearEndDate.isEnabled = true
            self.btnSetData.isEnabled = true
            
            self.isStartDateSet = true
            self.isEndDateSet = true
            
            self.buttonState(button: btnClearStartDate, state: true)
            self.buttonState(button: btnClearEndDate, state: true)
            self.buttonState(button: btnSetData, state: true)
        } else {
            self.btnClearStartDate.isEnabled = false
            self.btnClearEndDate.isEnabled = false
            self.btnSetData.isEnabled = false
            
            self.isStartDateSet = false
            self.isEndDateSet = false
            
            self.buttonState(button: btnClearStartDate, state: false)
            self.buttonState(button: btnClearEndDate, state: false)
            self.buttonState(button: btnSetData, state: false)
        }
    }
    
    private func buttonState(button: UIButton, state: Bool) {
        if (state) {
            button.backgroundColor = colorRed
            button.isEnabled = true
        } else {
            button.backgroundColor = colorGray
            button.isEnabled = false
        }
    }
    
    private func showDateSelection(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        
        if let vc = storyboard.instantiateViewController(withIdentifier: "DateOnly") as? DateSelectViewController {
            vc.delegate = self
            
            if (selectedButton == 2){
                vc.minimumDate = self.btnStartDate.titleLabel?.text
            }
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 400.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
            
        }
    }
    
    func setSelectedItem(_ item: AnyObject) {
        
        if let obj = item as? Date {
        
            if selectedButton == 1 {
                self.btnStartDate.setTitle(DateTimeManager().getDateFromDateTime(obj), for: UIControlState())
                self.buttonState(button: btnClearStartDate, state: true)
                self.isStartDateSet = true
            } else {
                self.btnEndDate.setTitle(DateTimeManager().getDateFromDateTime(obj), for: UIControlState())
                self.buttonState(button: btnClearEndDate, state: true)
                self.isEndDateSet = true
            }
        }
        
        if (isEndDateSet && isStartDateSet){
            self.buttonState(button: btnSetData, state: true)
        }
    }
}
