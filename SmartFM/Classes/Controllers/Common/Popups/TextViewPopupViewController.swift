//
//  TextViewPopupViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol TextViewPopupDelegate {
    
    func updatedText(with text: String)
}

enum TextViewLoadType {
    case staticText, ChecklistInfo
}

class TextViewPopupViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var loadingHolder: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var delegate: TextViewPopupDelegate?
    var defaultText: String! = ""
    var editable: Bool = true
    var ChecklistInfoCode: String?
    var loadType: TextViewLoadType = TextViewLoadType.staticText
    var buttonText = "Update"
    
    var objectType:String?
    var objectKey: String?
    var infoCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.loadType == TextViewLoadType.staticText {
            self.hideLoading()
            self.textView.text = self.defaultText
        } else if self.loadType == TextViewLoadType.ChecklistInfo {
            self.fetchChecklistInfo()
        }
        
        self.textView.isEditable = self.editable
        
        if !editable, self.loadType == TextViewLoadType.staticText {
            self.btnUpdate.isEnabled = self.editable
            self.btnUpdate.alpha = 0.5
        }
        
        
        self.setButtonTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    fileprivate func fetchChecklistInfo() {
        
        if let ok = self.objectKey, let ot = self.objectType, let ic = self.infoCode {
            
            self.showLoading()
            APIClient().getCheckItemInfo(objectType: ot, objectKey: ok, infoCode: ic, completion: { (message) in
                
                self.hideLoading()
                if let msg = message {
                    self.textView.text = msg
                } else {
                    self.textView.text = "Failed. Please try again"
                }
            })
        }
    }
    
    fileprivate func showLoading() {
        
        self.activityIndicator.startAnimating()
        self.loadingHolder.isHidden = false
    }
    
    fileprivate func hideLoading() {
        
        self.loadingHolder.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    fileprivate func setButtonTitle() {
        
        self.btnUpdate.setTitle(self.buttonText, for: UIControlState.normal)
    }

    
    @IBAction func close(_ sender: Any) {
        
        mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    
    @IBAction func update(_ sender: Any) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
        self.delegate?.updatedText(with: self.textView.text)
    }
    
}
