//
//  YesNoDialogViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/3/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol YesNoDelegate {
    
    func ActionChanged(_ action: Bool)
}

class YesNoDialogViewController: PopupParentViewController {

    @IBOutlet weak var lblMessage: UILabel!
    
    var delegate: YesNoDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK: - Public methods
    
    func setValues(_ message: String) {
        
        self.lblMessage.text = message
    }
    
    //MARK: - Events
    
    @IBAction func yesPressed(_ sender: AnyObject) {
        self.delegate?.ActionChanged(true)
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func noPressed(_ sender: AnyObject) {
        self.delegate?.ActionChanged(false)
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
}
