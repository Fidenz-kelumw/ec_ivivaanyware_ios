//
//  SmartFMParentViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/5/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SmartFMParentViewController: UIViewController {

    var hairlineView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(SmartFMParentViewController.performGoToHome), name: NSNotification.Name(rawValue: "sessionExpired"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        if let _ = self.navigationController {
            
            let hairLine: UIView = UIView()
            self.hairlineView = hairLine
            hairLine.backgroundColor = TopHairLineColor
            
            let width = UIScreen.main.bounds.size.width
            
            hairLine.frame = CGRect(x: 20, y: 0, width: width-40, height: 1)
            
            self.view.addSubview(hairLine)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func performGoToHome() {
        
        self.navigationController?.popToRootViewController(animated: true)
//        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
