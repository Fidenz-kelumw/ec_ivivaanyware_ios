//
//  CreatePageViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class CreatePageViewController: SmartFMParentViewController, GenericFormDelegate {

    @IBOutlet weak var formContainer: UIView!
    
    var fieldData: [NSDictionary]?
    var objectType: String?
    
    fileprivate var alertSuccess: LIHAlert?
    fileprivate var alertError: LIHAlert?
    fileprivate var alertProcessing: LIHAlert?
    fileprivate var goToObjectKey: String?
    fileprivate var goToObjectType: String?
    fileprivate var messageToDisplayInWorkOrder: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initAlerts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FormFromCreatePage" {
            
            if let vc = segue.destination as? GenericFormViewController {
                
                vc.objectType = self.objectType
                vc.formType = FormType.createPage
                vc.defaultData = self.fieldData
                vc.delegate = self
            }
            
        } else if segue.identifier == "ObjectDetailsFromCreatePage" {
            
            if let vc = segue.destination as? ObjectDetailsViewController {
                
                vc.objectKey = self.goToObjectKey
                vc.objectType = self.goToObjectType
                vc.isFromCreatePage = true
                vc.messageToDisplay = self.messageToDisplayInWorkOrder
            }
        }
    }
    
    
    
    //MARK: - Private Methods
    fileprivate func initAlerts() {
        
        self.alertSuccess = LIHAlertManager.getSuccessAlert("Successfully created")
        AlertManager().configSuccessAlert(self.alertSuccess, hasNavBar: true)
        self.alertSuccess?.initAlert(self.view)
        
        self.alertError = LIHAlertManager.getErrorAlert("Failed to create")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.initAlert(self.view)
        
        self.alertProcessing = LIHAlertManager.getProcessingAlert("Please wait..")
        AlertManager().configProcessing(self.alertProcessing, hasNavBar: true)
        self.alertProcessing?.initAlert(self.view)
    }

    func addSubview(_ subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
    }
    
    
    //MARK: - Delegate Methods
    func showSuccess(_ message: String) {
        
        self.alertSuccess?.contentText = message
        self.alertSuccess?.show(nil, hidden: nil)
        self.messageToDisplayInWorkOrder = message
        
    }
    
    func showError(_ message: String) {
        
        self.alertError?.contentText = message
        self.alertError?.show(nil, hidden: nil)
    }
    
    func processing(_ completion: @escaping () -> Void) {
        
        self.alertProcessing?.show({ 
            completion()
            }, hidden: nil)
    }
    
    func hideProcessing() {
        
        self.alertProcessing?.hideAlert(nil)
    }
    
    func showObjectDetails(_ objectKey: String, objecttype: String) {
        
        self.goToObjectType = objecttype
        self.goToObjectKey = objectKey
        self.performSegue(withIdentifier: "ObjectDetailsFromCreatePage", sender: nil)
    }
   
    func showVc(_ vc: UIViewController) {
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func showpopup(_ vc: UIViewController) {
        
        //ATTENTION
        // @NOTE: Ask Lasith about ^
        let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
        formSheet.shouldCenterVertically = false
        formSheet.shouldDismissOnBackgroundViewTap = false
        formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 350.0)
        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
        
        formSheet.present(animated: true, completionHandler: nil)
    }
}
