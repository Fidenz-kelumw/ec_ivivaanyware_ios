//
//  CustomSearchViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit



class CustomSearchViewController: UIViewController, GenericFormDelegate {

    @IBOutlet weak var container: UIView!
    
    var location: Building?
    var objectType: String?
    var delegate: CustomSearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowSearchForm" {
            
            if let vc = segue.destination as? GenericFormViewController {
                
                vc.objectType = self.objectType
                vc.formType = FormType.customSearch
                vc.delegate = self
            }
        }
    }

    
    //MARK: - Delegate Methods
    func customSearch(_ fieldData: String) {
        
        self.delegate?.applyFilter(fieldData)
        self.navigationController?.popViewController(animated: true)
    }
}
