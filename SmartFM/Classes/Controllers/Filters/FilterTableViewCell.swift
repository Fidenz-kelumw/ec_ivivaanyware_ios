//
//  FilterTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var lblFilterDescription: UILabel!
    @IBOutlet weak var frameBottom: UIView!
    @IBOutlet weak var frameMiddle: UIView!
    @IBOutlet weak var frameTop: UIView!
    @IBOutlet weak var frameSingle: UIView!
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    
    var filter: Filter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let selectionview: UIView = UIView()
        selectionview.backgroundColor = TableCellSelectionColor
        selectedBackgroundView = selectionview
        
        self.constBottom.priority = UILayoutPriority.init(250)
        
        self.frameTop.layer.borderWidth = 1
        self.frameTop.layer.borderColor = FrameBorderColor.cgColor
        self.frameTop.layer.cornerRadius = 5
        
        self.frameBottom.layer.borderWidth = 1
        self.frameBottom.layer.borderColor = FrameBorderColor.cgColor
        self.frameBottom.layer.cornerRadius = 5
        
        self.frameMiddle.layer.borderWidth = 1
        self.frameMiddle.layer.borderColor = FrameBorderColor.cgColor
        self.frameMiddle.layer.cornerRadius = 5
        
        self.frameSingle.layer.borderWidth = 1
        self.frameSingle.layer.borderColor = FrameBorderColor.cgColor
        self.frameSingle.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func longPressed(sender: UILongPressGestureRecognizer) {
//        
//        if sender.state == UIGestureRecognizerState.Began {
//            print("long pressed \(filter?.FilterName)")
//        }
//    }

    func setValue(_ filter: Filter, cellPosition: CellPosition) {
        
        self.filter = filter
        
        if let filterName = filter.FilterName {
            self.lblFilterName.text = filterName
        } else {
            self.lblFilterName.text = "N/A"
        }
        
        
        if let desc = filter.Description {
            self.lblFilterDescription.text = desc
        } else {
            self.lblFilterDescription.text = "N/A"
        }
        
        self.frameMiddle.isHidden = true
        self.frameTop.isHidden = true
        self.frameBottom.isHidden = true
        self.frameSingle.isHidden = true
        
        var contentViewHeight: CGFloat = 85.0
        if cellPosition == CellPosition.middle {
            contentViewHeight = 75
        } else if cellPosition == CellPosition.single {
            contentViewHeight = 95
        }
        let autoHeight = contentViewHeight - 10.0 - self.container.frame.size.height
        
        if cellPosition == CellPosition.single {
            self.frameSingle.isHidden = false
            self.constTop.constant = 20.0
            self.constBottom.constant = autoHeight
        } else if cellPosition == CellPosition.bottom {
            self.frameBottom.isHidden = false
            self.constBottom.constant = autoHeight
            self.constTop.constant = 10.0
        } else if cellPosition == CellPosition.top {
            self.frameTop.isHidden = false
            self.constBottom.constant = 10.0
            self.constTop.constant = autoHeight
        } else if cellPosition == CellPosition.middle {
            self.frameMiddle.isHidden = false
            self.constBottom.constant = 10.0
            self.constTop.constant = autoHeight
        }
        self.layoutIfNeeded()
        
    }
}
