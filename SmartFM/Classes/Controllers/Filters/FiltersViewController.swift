//
//  FiltersViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit


class FiltersViewController: SmartFMParentViewController, GenericFormDelegate {

    @IBOutlet weak var btnSelectBuilding: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var objectType: String?
    var menuName: String?
    
    fileprivate var filters: [Filter] = []
    
    fileprivate var selectedBuilding: Building? {
        didSet {
            if let building = self.selectedBuilding {
                if let name = building.LocationName {
                    self.btnSelectBuilding.setTitle(name, for: UIControlState())
                } else {
                    self.btnSelectBuilding.setTitle("N/A", for: UIControlState())
                }
            } else {
                 self.btnSelectBuilding.setTitle("Select Building", for: UIControlState())
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mn = self.menuName {
            self.title = "\(mn) - Filters"
        }
        
        Configurations().configSelectionButton(forButton: self.btnSelectBuilding, withDisclosure: false)
        
        if let def = Defaults.all().first as? Defaults, let locKey = def.LocationKey {
            
            if let building = Building.__where("LocationKey='\(locKey)'", sortBy: "Id", accending: true).first as? Building {
                self.selectedBuilding = building
            }
        }
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        
        if let objType = self.objectType {
            
            if let allFilters = Filter.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [Filter] {
                self.filters = allFilters
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData()
                })
            }
            
            if let obj = ObjType.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true).first as? ObjType {
                
                if obj.CreatePage != "1" {
                    self.navigationItem.rightBarButtonItem = nil
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "Objects" {
            if let vc = segue.destination as? ObjectsMainViewController {
                vc.building=self.selectedBuilding
                
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    vc.filter = self.filters[(indexPath as NSIndexPath).row]
                    vc.objectType = self.objectType
                    vc.objectsType = ObjectsType.defaultFilter
                }
            }
        } else if segue.identifier == "ShowCreatePage" {
            
            if let vc = segue.destination as? CreatePageViewController {
                
                vc.objectType = self.objectType
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///////////////////////////////////////////
    //MARK: - Private methods
    ///////////////////////////////////////////
    fileprivate func getPosition(items:[AnyObject], forIndexPath indexPath: IndexPath) -> CellPosition {
        
        var position = CellPosition.middle
        if items.count == 1 {
            position = CellPosition.single
        } else if (indexPath as NSIndexPath).row == 0 {
            position = CellPosition.top
        } else if (indexPath as NSIndexPath).row == items.count - 1 {
            position = CellPosition.bottom
        }
        return position
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    
    @IBAction func selectBulding(_ sender: UIButton) {
       
        if let vc =  UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAndSelect") as? SearchAndSelectViewController  {
            vc.hasHeader = true
            vc.objectType = self.objectType
            vc.titleString = "Select Location"
            vc.selectedAction = SelectedAction.dismiss
            vc.delegate = self
            if let buildings = Building.all() as? [Building] {
                
                var datas: [LData] = []
                for building in buildings {
                    let data = LData()
                    data.Id = building.Id
                    data.Value = building.LocationKey
                    data.DisplayText = building.LocationName
                    datas.append(data)
                }
                
                vc.items = datas
                let opt = LData()
                opt.DisplayText = "Any Location"
                opt.Value = ""
                vc.items.insert(opt, at: 0)
                vc.searchtype = SearchType.cachedData
            }
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
        }
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Delegate Methods
    ///////////////////////////////////////////
    func setSelectedItem(_ item: AnyObject) {
        
        if let data = item as? LData {
            
            let building = Building()
            building.Id = data.Id
            building.LocationName = data.DisplayText
            building.LocationKey = data.Value
            
            self.selectedBuilding = building
        }
    }
    
}


extension FiltersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.filters.count == 0 {
            return 1
        }
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.filters.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            cell.setValues("No filters found", bgColor: ThemeBackgroundColor)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filtercell") as! FilterTableViewCell
        
        DispatchQueue.main.async {
            cell.setValue(self.filters[(indexPath as NSIndexPath).row], cellPosition: self.getPosition(items: self.filters, forIndexPath: indexPath))
        }
        
        
//        let rec: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: cell, action: #selector(FilterTableViewCell.longPressed(_:)))
//        rec.minimumPressDuration = 1.0
//        cell.addGestureRecognizer(rec)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let pos = self.getPosition(items: self.filters, forIndexPath: indexPath)
        
        if pos == CellPosition.top || pos == CellPosition.bottom {
            return 85
        } else if pos == CellPosition.single {
            return 95
        } else {
            return 75
        }
        
    }
}
