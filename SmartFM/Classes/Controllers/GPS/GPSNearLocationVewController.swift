//
//  GPSNearLocationVewController.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/11/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import GoogleMaps
import UserNotifications

class GPSNearLocationVewController: UIViewController, GMSMapViewDelegate, MarkerDetailsDelegate {
    
    //IBOutlets
    @IBOutlet var mapView: GMSMapView!
    
    //private members
    fileprivate var failedAlert: LIHAlert?
    fileprivate var oldNotificationMessage: String = ""
    fileprivate var notificationMessage: String = ""
    fileprivate var goToObjectKey: String?
    fileprivate var goToObjectType: String?
    
    //main data
    var delegate: GenericFormDelegate?
    var locationManager = CLLocationManager()
    var tabName: String?
    var isMyLocationFocused: Bool = false
    var timer: Timer?
    
    //object data
    var objectType: String?
    var objectKey: String?
    var locationSpots: String?
    
    //data holders
    var spotList: [Spot]?
    var currentLocation: CLLocation?
    var markedPotsInMap = [SpotMarkerModel]()
    var notifiedSpots = [SpotResolve]()
    var oldNotifiedSpots = [SpotResolve]()
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initAlert()
        self.configNotifiedSpotsIfAvailable()
        self.setupMapSettings()
        self.initLocationManager()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopTimer()
        isMyLocationFocused = false
        locationManager.stopUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isMyLocationFocused = false
        locationManager.startUpdatingLocation()
    }

    //////////////////////////////////////////////////////
    //MARK: - Init and data retrieve methods
    //////////////////////////////////////////////////////
    private func initAlert(){
        self.failedAlert = LIHAlertManager.getErrorAlert("Failed to download data")
        AlertManager().configErrorAlert(self.failedAlert, hasNavBar: true)
        self.failedAlert?.initAlert(self.view)
    }
    
    private func configNotifiedSpotsIfAvailable() {
        if let dummySpots = self.createDummyNotifiedSpotsWithNotificationData(locationSpots: locationSpots) {
            self.oldNotifiedSpots = dummySpots
            self.notifiedSpots = dummySpots
        }
    }
    
    private func setupMapSettings() {
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.settings.setAllGesturesEnabled(true)
        mapView.isBuildingsEnabled = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        self.tryGetSpotsToPopulat({ (success, error) -> Void in
            
            if success {
                self.drawMarkersWithRadius()
            } else {
                self.failedAlert?.show(nil, hidden: nil)
            }
        })
    }
    
    private func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.distanceFilter = 2
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    private func tryGetSpotsToPopulat(_ success:@escaping (Bool, String?)->Void) {
        
        ProgressHUD.show("Retrieving spots...", interaction: false)
        
        APIClient().getSpots { (spots, error) -> Void in
            
            ProgressHUD.dismiss()
            
            if let foundSpots = spots {
                if Spot.truncateTable() {

                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {

                        for spot in foundSpots {
                            spot.save()
                        }

                        DispatchQueue.main.async {
                            self.spotList = foundSpots
                            success(true, nil)
                        }
                    }
                } else {
                    success(false, error)
                }
            } else {
                success(false, error)
            }
        }
    }
    
    //////////////////////////////////////////////////////
    //MARK: - marker draw methods
    //////////////////////////////////////////////////////
    func drawMarkersWithRadius() {
        let spotsAvailable = Spot.all() as? [Spot]
        
        if let spots = spotsAvailable {
            
            for spot in spots {
                for noti in self.notifiedSpots {
                    if let sp = noti.spot, sp.SpotID == spot.SpotID {
                        spot.isNotified = true
                    }
                }
            }
        }
        
        // clear all data on map
        mapView.clear()
        markedPotsInMap.removeAll()
        
        if let spots = spotsAvailable {
            for spot in spots {
                
                if let la = spot.Lat, let ln = spot.Lan, la != "", ln != "" {
                    
                    let marker = GMSMarker()
                    let circleCenter = CLLocationCoordinate2D(latitude: Double(spot.Lat!)!, longitude: Double(spot.Lan!)!)
                    let circ = GMSCircle(position: circleCenter, radius: Double(spot.Distance!)!)
                    
                    if spot.isNotified {
                        circ.fillColor = UIColor(red: 0.23, green: 0.24, blue: 1.00, alpha: 0.3)
                        circ.strokeColor = UIColor(red: 0.23, green: 0.24, blue: 1.00, alpha: 1.0)
                        marker.icon = GMSMarker.markerImage(with: UIColor.blue)
                    } else {
                        circ.fillColor = UIColor(red: 1.0, green: 0, blue: 0, alpha: 0.3)
                        circ.strokeColor = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0)
                        marker.icon = GMSMarker.markerImage(with: UIColor.red)
                    }
                    
                    circ.strokeWidth = 2
                    circ.map = mapView
                    
                    marker.position = CLLocationCoordinate2D(latitude: Double(spot.Lat!)!, longitude: Double(spot.Lan!)!)
                    marker.map = mapView
                    
                    markedPotsInMap.append(SpotMarkerModel.init(marker: marker, spotItem: spot))
                }
            }
        }
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Notification manager
    //////////////////////////////////////////////////////
    func populateNotification() {
        let resolvedSpots = self.resolveSpots()
        let objectCompareResult = self.isSpotsAlreadyNotified(resolvedSpots: resolvedSpots)
        
        if let spots = resolvedSpots, spots.count > 0, !objectCompareResult {
            
            self.notifiedSpots = spots
            self.oldNotifiedSpots = self.notifiedSpots
            
            var count: Int = 0
            var insideSpotMessage: String = ""
            
            for resolvedSpot in spots {
                
                if let spot = resolvedSpot.spot, let spotId = spot.SpotID {
                    if spots.count == 1 {
                        insideSpotMessage += (spotId + ".")
                    } else {
                        if spots.count == 2 {
                            insideSpotMessage += ((count == 0) ? (spotId + " and "):(spotId + "."))
                        } else {
                            if count == spots.count - 1 {
                                insideSpotMessage += " and " + spotId
                            } else {
                                insideSpotMessage += ((count == 0) ? (spotId):(", " + spotId))
                            }
                        }
                    }
                }
                
                self.notifiedSpots[count].isNotified = true
                count += 1
            }
            
            UIApplication.shared.applicationIconBadgeNumber = 0
            
            if #available(iOS 10.0, *) {
                let center = UNUserNotificationCenter.current()
                center.removeAllDeliveredNotifications()
                center.removeAllPendingNotificationRequests()
            } else {
                UIApplication.shared.cancelAllLocalNotifications()
            }
            
            
            let notificationMessage = "You are near " + insideSpotMessage
            
            self.notificationMessage = notificationMessage
            self.createLocationNotification(message: notificationMessage)
            self.updateNearLocationToAPI()
        }
    }
    
    func resolveSpots() -> [SpotResolve]? {
        let spotsAvailable = Spot.all() as? [Spot]
        var resolvedSpots = [SpotResolve]()
        
        if let spots = spotsAvailable {
            
            for spot in spots {
    
                if spot.Lat != nil, spot.Lan != nil, spot.Lat != "", spot.Lan != "",let notify = spot.NotifyNearestSpot, notify == "1" {
    
                    let distence = self.getDistenceToSpot(spot: spot)
                    let isInside = (((spot.Distance?.toDouble())! - distence) > 0)
                    
                    resolvedSpots.append(SpotResolve.init(spot: spot, isInside: isInside, isNotified: false, distenceToLocation: distence))
                }
            }
            
            let insideSpots = resolvedSpots.filter(){$0.isInside}
            resolvedSpots = insideSpots.sorted(by: { $0.distenceToLocation! > $1.distenceToLocation! })
            
            return resolvedSpots
        } else {
            return nil
        }
    }
    
    func createLocationNotification (message: String) {
        
        // before create notification validate with old data again
        if notifiedSpots.count > 0 && (self.notificationMessage != self.oldNotificationMessage) {
            
            let params = [
                "objectKey" : "",
                "objectType": objectType!,
                NOTIFICATION_DATA_SPOTS:self.createNotifiedSpotsString()
            ]
            
            self.oldNotificationMessage = self.notificationMessage
            
            if #available(iOS 10.0, *) {
                let content = UNMutableNotificationContent()
                content.title = "SmartFM"
                content.subtitle = "Location based"
                content.body = message
                content.badge = 1
                content.userInfo = params
                content.sound = UNNotificationSound.default()
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                
            } else {
                let notification = UILocalNotification()
                notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
                notification.alertTitle = "SmartFM"
                notification.alertBody = message
                notification.userInfo = params
                notification.soundName = UILocalNotificationDefaultSoundName
                UIApplication.shared.scheduleLocalNotification(notification)
            }
        }
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Timer manager
    //////////////////////////////////////////////////////
    func startTimer() {
        self.timer?.invalidate()
        
        if let defaults = Defaults.all() as? [Defaults] {
            
            if defaults.count > 0 {
                
                let defaults1=defaults[0]
                if let transac = defaults1.NearestSpotUpdateSeconds {
                    if #available(iOS 10.0, *) {
                        timer = Timer.scheduledTimer(withTimeInterval: transac.toDouble()!, repeats: true) { (_) in
                            self.populateNotification()
                        }
                    } else {
                        timer = Timer.scheduledTimer(timeInterval: transac.toDouble()!, target: self, selector: #selector(populateNotification), userInfo: nil, repeats: true)
                    }
                }
            }
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Action calls
    //////////////////////////////////////////////////////
    func updateNearLocationToAPI() {
        for spot in self.notifiedSpots {
            if let currentSpot = spot.spot, let spotID = currentSpot.SpotID {
                APIClient().notifyNearestSpot(spotId: spotID, proximity: "Near", distence: (String(describing: spot.distenceToLocation)), completion: {(status) -> Void in
                })
            }
        }
    }
    
    func getDistenceToSpot(spot: Spot) -> Double {
        
        var distenceToObject = 0.0
        if let sLat = spot.Lat?.toDouble(), let sLan = spot.Lan?.toDouble() {
            let zoneLoc = CLLocation(latitude: sLat, longitude: sLan)
            let curLoc  = self.currentLocation
            distenceToObject = zoneLoc.distance(from: curLoc!)
        }
        
        return distenceToObject
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Custom mothods for validation & msg creation
    //////////////////////////////////////////////////////
    func isSpotsAlreadyNotified(resolvedSpots: [SpotResolve]?) -> Bool {
        
        if let spots = resolvedSpots {
            // no spots notified
            if spots.count == 0 && self.oldNotifiedSpots.count == 0 {
                return false
            }
            
            // array size differ
            if spots.count != self.oldNotifiedSpots.count {
                return false
            } else {
                // array size equal
                var count: Int = 0
                for spot in spots {
                    if let s = spot.spot, let sid = s.SpotID, let oldS = self.oldNotifiedSpots[count].spot, let oldSID = oldS.SpotID {
                        if sid != oldSID {
                            return false
                        }
                    }
                    count += 1
                }
                return true
            }
        } else {
            return false
        }
    }
    
    func createNotifiedSpotsString() -> String {
        
        var resultString: String = ""
        var count = 0
        
        for spot in self.notifiedSpots {
            
            if let s = spot.spot, let sID = s.SpotID {
                if self.notifiedSpots.count == 1 {
                    resultString += (sID)
                } else {
                    if count == 0 {
                        resultString += (sID + "_**_")
                    } else {
                        if self.notifiedSpots.count == 2 || count == self.notifiedSpots.count - 1{
                            resultString += (sID)
                        } else {
                            resultString += (sID + "_**_")
                        }
                    }
                }
            }
            
            count += 1
        }
        
        return resultString
    }
    
    func getNotifiedSpotsByNotificationData(data: String) -> [Spot]? {
        let spots = Spot.all() as? [Spot]
        let notifiedSpotIDs: [String] = data.components(separatedBy: "_**_")
        
        var selectedSpots = [Spot]()
        
        if let spots = spots {
            for spot in spots {
                for s in notifiedSpotIDs {
                    if let sID = spot.SpotID, sID == s {
                        selectedSpots.append(spot)
                    }
                }
            }
            
            return selectedSpots
        } else {
            return nil
        }
    }
    
    func createDummyNotifiedSpotsWithNotificationData(locationSpots: String?) -> [SpotResolve]? {
        
        var resolvedSpots = [SpotResolve]()
        
        if let locations = locationSpots, let spotList = self.getNotifiedSpotsByNotificationData(data: locations) {
            for spot in spotList {
                resolvedSpots.append(SpotResolve.init(spot: spot, isInside: true, isNotified: true, distenceToLocation: 0.0))
            }
        }
        return resolvedSpots
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        var selectedItem: SpotMarkerModel?
        for model in  markedPotsInMap {
            if model.marker == marker {
                selectedItem = model
                break
            }
        }
        
        if let selectedSpot = selectedItem, let spot = selectedSpot.spotItem {
            
            ProgressHUD.show("Please wait...", interaction: false)
            APIClient().getSpotDetails(objectType: spot.ObjectType!, objectKey: spot.ObjectKey!, locationKey: spot.SpotID!, distence: "\(self.getDistenceToSpot(spot: spot))", completion: { (spotDetails) -> Void in
                
                ProgressHUD.dismiss()
                if let details = spotDetails {
                    DispatchQueue.main.async {
                        self.populatePopup(spotDetails: details)
                    }
                } else {
                    NSLog("[SyncManager] syncBuildings Failed")
                }
            })
            
        }
        
        return true
    }
    
    func populatePopup(spotDetails: SpotDetails){
        
        let storyBoard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "MarkerDetails") as? MarkerDetailsViewController {
            
            vc.detailsObject = spotDetails
            vc.delegate = self
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 265.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
        }
    }
    
    func markerViewDetailsPressed(detailsObject: SpotDetails) {
        self.goToObjectType = detailsObject.NextpageObjectType
        self.goToObjectKey = detailsObject.NextPageObjectKey
        
        if let objKey = detailsObject.NextPageObjectKey, let objType = detailsObject.NextpageObjectType{
            self.delegate?.showObjectDetails?(objKey, objecttype: objType)
        }
    }
    
}

extension GPSNearLocationVewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        
        if !isMyLocationFocused{
            isMyLocationFocused = true
            
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
            mapView.camera = camera
            
            self.startTimer()
        }

        self.currentLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
