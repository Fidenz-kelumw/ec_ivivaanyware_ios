//
//  HomeObjectsTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/14/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol HomeObjectDelegate {
    
    func createObjPressed(_ forObjectType: ObjType)
}

class HomeObjectsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgObjectIcon: UIImageView!
    @IBOutlet weak var lblObjectType: UILabel!
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var viewCreateObj: UIView!
    @IBOutlet weak var btnCreateObj: UIButton!
    
    fileprivate var objectType: ObjType?
    
    var delegate: HomeObjectDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.frameView.layer.borderColor = FrameBorderColor.cgColor
        self.frameView.layer.borderWidth = 1
        self.frameView.layer.cornerRadius = 5
        
        self.btnCreateObj.setImage(UIImage.fontAwesomeIconWithName(FontAwesome.Plus, textColor: UIColor.white, size: CGSize(width: 30, height: 30)), for: UIControlState.normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ objType: ObjType) {
        
        self.objectType = objType
        
        if let obj = objType.MenuName {
            
            self.lblObjectType.text = obj
            
        }
        
        if objType.CreatePage == "1" {
            self.viewCreateObj.isHidden = false
        } else {
            self.viewCreateObj.isHidden = true
        }
    }
    
    func setValuesWithString(_ objType: String) {
        
        self.lblObjectType.text = objType
        
        if objType == "WorkOrder" {
            
            self.imgObjectIcon.image = UIImage(named: "objTypeIcon_workOrder")
            
        } else if objType == "Asset" {
            
            self.imgObjectIcon.image = UIImage(named: "objTypeIcon_asset")
            
        } else if objType == "Inventory" {
            
            self.imgObjectIcon.image = UIImage(named: "objTypeIcon_inventory")
            
        } else if objType == "Incident" {
            
            self.imgObjectIcon.image = UIImage(named: "objTypeIcon_incident")
            
        }
    }
    
    
    @IBAction func createObj(_ sender: AnyObject) {
        
        if let ot = self.objectType {
            self.delegate?.createObjPressed(ot)
        }
    }
    
}
