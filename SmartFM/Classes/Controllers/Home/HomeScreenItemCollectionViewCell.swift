//
//  HomeScreenItemCollectionViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class HomeScreenItemCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var buttonname: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    var notificationCount: Int = 0
   
    func initUI() {
        
        self.lblBadge.layer.cornerRadius = self.lblBadge.frame.size.width / 2
        self.containerView.layer.cornerRadius = 5

        if self.notificationCount == 0 {
            self.lblBadge.isHidden = true
        } else {
            self.lblBadge.text = "  \(self.notificationCount)  "
            self.lblBadge.isHidden = false
        }
    }
    
    
    func setValues(_ buttonName: String) {
        self.buttonname.text = buttonName
    }
    
}
