//
//  ViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import Darwin
import Kingfisher

class ViewController: UIViewController, HomeObjectDelegate {
    
    //Outlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var badgeHolderBorderView: UIView!
    @IBOutlet weak var badge2HolderBorderView: UIView!
    @IBOutlet weak var lblInboxCount: UILabel!
    @IBOutlet weak var lblMessageCount: UILabel!
    @IBOutlet weak var userBorder: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewProbileBg: UIView!
    @IBOutlet weak var actIndContainer: UIView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    @IBOutlet weak var homeNaviBar: UINavigationItem!
    
    //public variables
    var user: User?
    var showObjectDetails: PushMessage?
    var locationMessage: LocationNotificationMessage?
    
    //Private variables
    fileprivate var objectTypes:[ObjType] = []
    fileprivate var userInfo: String?
    fileprivate var menuName: String?
    fileprivate var objectTypeForCreate: ObjType?
    fileprivate var alertDataDownloading: LIHAlert?
    fileprivate var forceMoveObjectType: String?
    fileprivate var menuNameForNoFilterPage: String?
    fileprivate var alertError: LIHAlert?
    
    ///////////////////////////////////////////
    //MARK: - View Controller Methods
    ///////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initAlerts()
        
        self.loadHomeObjectTypes()
        
        self.updateBadge()
        
        if let obj = self.showObjectDetails {
            self.showObjectDetails(obj)
        }
        
        if let obj = self.locationMessage {
            self.showObjectDetails(obj)
        }
        
        if UserManager().isFirstTime() == true, let uName = UserManager().getUserName(){
            UserManager().setFirstRun(false)
            self.view.makeToast("Welcome \(uName)", duration: 2.0, position: .bottom)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.viewProbileBg.layer.cornerRadius = self.viewProbileBg.frame.size.height / 2
        
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.height / 2
        self.userBorder.layer.cornerRadius = self.userBorder.frame.size.height / 2
        self.userBorder.layer.borderColor = ThemeRedColor.cgColor
        self.userBorder.layer.borderWidth = 2
        
        self.badgeHolderBorderView.layer.cornerRadius = self.badgeHolderBorderView.frame.size.height / 2
        self.badgeHolderBorderView.layer.borderWidth = 1
        self.badgeHolderBorderView.layer.borderColor = UIColor.white.cgColor
        
        self.badge2HolderBorderView.layer.cornerRadius = self.badge2HolderBorderView.frame.size.height / 2
        self.badge2HolderBorderView.layer.borderWidth = 1
        self.badge2HolderBorderView.layer.borderColor = UIColor.white.cgColor
        
        self.adjustTableviewScroll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.newMessageReceived), name: NSNotification.Name(rawValue: "messageReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.loadHomeObjectTypes), name: NSNotification.Name(rawValue: "objectTypesDownloaded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.updateHomepageDefaultData), name: NSNotification.Name(rawValue: "defaultDataDownloaded"), object: nil)
        
        self.updateBadge()
        self.loadHomeObjectTypes()
        self.updateHomepageDefaultData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if UserManager().isLogged() {
            
            SyncManager().syncMasterData(withTimer: true) { (success) in
                
                if success {
                    APIClient().validateUser({ (valid) in
                        
                        if !valid {
                            UserManager().wipeUserData(completion: nil)
                        }
                    })
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowQRScanner" {
            
            if let vc = segue.destination as? QRReaderViewController {
                
                vc.scanType = ScanType.openObject
            }
        } else if segue.identifier == "showFilter" {
            
            if let vc = segue.destination as? FiltersViewController {
                
                vc.objectType = self.userInfo
                vc.menuName = self.menuName
            }
        } else if segue.identifier == "ObjectFromHome" {
            
            if let vc = segue.destination as? ObjectsMainViewController {
                
                vc.objectType = self.userInfo
                vc.menuNameForNoFilterPage = self.menuNameForNoFilterPage
                vc.objectsType = ObjectsType.noFilter
            }
        } else if segue.identifier == "CreateObject" {
            NSLog("prepare for segue")
            if let vc = segue.destination as? CreatePageViewController {
                vc.objectType = self.objectTypeForCreate?.ObjectType
                vc.fieldData = self.getDefaultdataDictionary() as [NSDictionary]?
            }
        } else if segue.identifier == "ForceMoveToDetailsPage" {
            
            if let vc = segue.destination as? ObjectDetailsViewController, let objType = forceMoveObjectType {
                vc.objectType = objType
            }
        } else if segue.identifier == "MyVisits" {
            
            if let vc = segue.destination as? MyVisitsViewController {
            }
        }
    }
    
    func newMessageReceived() {
        
        self.updateBadge()
    }

    
    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    
    fileprivate func adjustTableviewScroll() {
        
        let contentSize = self.tableView.contentSize.height
        if self.tableView.frame.size.height < contentSize {
            self.tableView.isScrollEnabled = true
        } else {
            self.tableView.isScrollEnabled = false
        }
    }
    
    func showObjectDetails(_ object: PushMessage) {
        
        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectDetailsVc") as? ObjectDetailsViewController {
                
                vc.objectType = object.ObjectType
                vc.objectKey = object.ObjectKey
                object.isRead = "1"
                object.update()
                self.updateBadge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func showObjectDetails(_ object: LocationNotificationMessage) {
        
        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectDetailsVc") as? ObjectDetailsViewController {
                
                vc.objectType = object.objectType
                vc.objectKey = object.objectKey
                vc.locationSpots = object.spotsContent
                self.updateBadge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func showError(_ message: String) {
        self.alertError?.contentText = message
        self.alertError?.show(nil, hidden: nil)
    }
    
    fileprivate func initAlerts() {
        self.alertError = LIHAlertManager.getErrorAlert("Error occured")
        AlertManager().configErrorAlert(alertError, hasNavBar: true)
        self.alertError?.initAlert(view)
    }
    
    func updateHomepageDefaultData() {
        
        let userManager = UserManager()
        if let name = userManager.getUserName() {
            self.lblUser.text = name
        }
        
        if let appName = UserManager().getAppName() {
            if !appName.isEmpty{
                self.homeNaviBar.title = appName
            }
        }
        
         let imageExtensions = ["png", "jpg", "gif"]
        
        if let account = userManager.getAccount(), let imagePath = userManager.getAccountImage(), imagePath != ""{
            
            let url = URL(fileURLWithPath: "\(Protocol)://\(account)\(imagePath)")
            let pathExtension: String? = url.pathExtension
            
            if let pathExt = pathExtension, imageExtensions.contains(pathExt){
                self.imgProfile.loadImage(withUrl: "\(Protocol)://\(account)\(imagePath)", key: "userImage")
            }
        }
    }
    
    fileprivate func showLoading() {
        
        self.actInd.startAnimating()
        self.actIndContainer.isHidden = false
    }
    
    fileprivate func hideLoading() {
        
        self.actIndContainer.isHidden = true
        self.actInd.stopAnimating()
    }
    
    fileprivate func hasFilterPage(_ objectType: String) -> Bool {
        
        if let obj = ObjType.__where("ObjectType='\(objectType)'", sortBy: "Id", accending: true).first as? ObjType {
            
            if obj.FilterListPage == "1" {
                return true
            }
        }
        
        return false
    }
    
    fileprivate func forceMoveDetailsPage(_ objectType: String) -> Bool {
        
        if let obj = ObjType.__where("ObjectType='\(objectType)'", sortBy: "Id", accending: true).first as? ObjType {
            
            if obj.MovetoDetailPage == "1" {
                return true
            }
        }
        
        return false
    }
    
    fileprivate func getMenuType(_ objectType: String, _ menuType:String) -> String {
        
        if let obj = ObjType.__where("ObjectType='\(objectType)' and MenuName='\(menuType)'", sortBy: "Id", accending: true).first as? ObjType {
            
            if let menuType = obj.MenuType {
                return menuType
            }
        }
        
        return "NoMenuType"
    }
    
    fileprivate func updateBadge() {
        let count = PushMessage.__where("isRead='0'", sortBy: "Id", accending: true).count
        self.lblMessageCount.text = "\(count)"
        if count == 0 {
            self.badgeHolderBorderView.isHidden = true
        } else {
            self.badgeHolderBorderView.isHidden = false
        }
        
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func loadHomeObjectTypes() {
        
        self.showLoading()
        
        if var objectTypes = ObjType.all() as? [ObjType] {
            self.hideLoading()
            
            for n in (0..<objectTypes.count).reversed() {
                if objectTypes[n].MenuName == nil || objectTypes[n].MenuName == "" {
                    objectTypes.remove(at: n)
                }
            }
            
            for ot in objectTypes {
                
                ot.CreateFromMenu = ot.getCreateFromMenu()
                
            }
            
            self.objectTypes = objectTypes
            self.tableView.reloadData()
            self.adjustTableviewScroll()
            
        } else {
            self.hideLoading()
        }
    }
    
    fileprivate func getDefaultdataDictionary() -> [[String: String]]? {
        
        if let ot = self.objectTypeForCreate?.ObjectType, let objType = self.objectTypes.filter({$0.ObjectType == ot}).first, let defaults = objType.CreateFromMenu {
            
            var defaultData: [[String: String]] = [[String: String]]()
            
            for defaultD in defaults {
                
                if let fid = defaultD.FieldID, let val = defaultD.Value {
                    
                    var fieldValue = [String: String]()
                    fieldValue["FieldID"] = fid
                    fieldValue["Value"] = val
                    if let display = defaultD.DisplayText {
                        fieldValue["DisplayText"] = display
                    }
                    defaultData.append(fieldValue)
                }
            }
            
            return defaultData
        }
        
        return nil
    }
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    @IBAction func goToHome(_ sender: UIStoryboardSegue) {
        
    }
    
    @IBAction func workOrdersPressed(_ sender: AnyObject) {
        
    }
    
    @IBAction func assetsPressed(_ sender: AnyObject) {
        self.userInfo = "Asset"
        if self.hasFilterPage("Asset") {
            performSegue(withIdentifier: "showFilter", sender: nil)
        } else {
            
            performSegue(withIdentifier: "ObjectFromHome", sender: nil)
        }
    }
    
    @IBAction func InventoryPressed(_ sender: AnyObject) {
        self.userInfo = "Inventory"
        if self.hasFilterPage("Inventory") {
            performSegue(withIdentifier: "showFilter", sender: nil)
        } else {
            
            performSegue(withIdentifier: "ObjectFromHome", sender: nil)
        }
    }
    
    @IBAction func incidentsPressed(_ sender: AnyObject) {
        self.userInfo = "Incident"
        if self.hasFilterPage("Incident") {
            performSegue(withIdentifier: "showFilter", sender: nil)
        } else {
            
            performSegue(withIdentifier: "ObjectFromHome", sender: nil)
        }
    }
    
    @IBAction func inboxPressed(_ sender: AnyObject) {
        
    }
    
    @IBAction func qrPressed(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "ShowQRScanner", sender: nil)
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Delegate Methods
    ///////////////////////////////////////////
    
    func createObjPressed(_ forObjectType: ObjType) {
        NSLog("create pressed")
        self.objectTypeForCreate = forObjectType
        
        DispatchQueue.main.async { 
            
            self.performSegue(withIdentifier: "CreateObject", sender: nil)
        }
    }
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.objectTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectTypeCell") as! HomeObjectsTableViewCell
        let objType = self.objectTypes[(indexPath as NSIndexPath).row]
        cell.setValues(objType)
        cell.delegate = self
        
        
        let defaultImage: UIImage? = UIImage(named: "default_objType")
        cell.imgObjectIcon.image = defaultImage
        cell.imgObjectIcon.tag = (indexPath as NSIndexPath).row
        if let imgPath = objType.ObjectIconPath {
            
            if let acc = UserManager().getAccount() {
                
                ImageHandler().asyncImageLoadWithCache("\(Protocol)://\(acc)\(imgPath)", key: objType.ObjectType != nil ? objType.ObjectType! : "ot", location: DefaultImagesLocation) { (image) in
                    
                    if let img = image {
                        if cell.imgObjectIcon.tag == (indexPath as NSIndexPath).row {
                            
                            cell.imgObjectIcon.image = img
                        }
                    } else {
                        cell.imgObjectIcon.image = defaultImage
                    }
                }
            }
            
        } else {
            cell.imgObjectIcon.image = defaultImage
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.userInfo = self.objectTypes[(indexPath as NSIndexPath).row].ObjectType
        self.menuName = self.objectTypes[(indexPath as NSIndexPath).row].MenuName
        self.menuNameForNoFilterPage = self.menuName;
        
        let menuType = getMenuType(self.userInfo!, self.menuName!)
        
        // For Apps
        
        if menuType == "App" {
            
            if let appName = self.objectTypes[(indexPath as NSIndexPath).row].MenuLink{
                
                var schema = ""
                var itunesAppID = ""
                var isValidAppName = false
                
                
                switch appName {
                case "Condeco":
                    schema = "com.condecosoftware.roombooking"
                    itunesAppID = "condeco-room-booking-v2/id1150570522?mt=8"
                    isValidAppName = true
                    
                case "Smart WP":
                    schema = "com.ecyber.iVivaAnywhere.Smart-Office"
                    itunesAppID = "ivivaanywhere-smartworkplace/id1067711840?mt=8"
                    isValidAppName = true
                    
                case "HeadCount":
                    schema = "ivivaheadcount"
                    itunesAppID = "ivivaanywhere-headcount/id1067202294?mt=8"
                    isValidAppName = true
                    
                default:
                    self.showError("Sorry, \"\(appName)\" is not included in supported applications.")
                }
                
                if isValidAppName {
                    let appScheme = schema+"://"
                    let itunesLink = "itms-apps://itunes.apple.com/app/"+itunesAppID
                    
                    if let appUrl = URL(string: appScheme), UIApplication.shared.canOpenURL(appUrl){
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(appUrl, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(appUrl)
                        }
                    } else{
                        // create the alert
                        let alert = UIAlertController(title: "Notice", message: "\"\(appName)\" not installed in this device. Press install to download application from App Store", preferredStyle: UIAlertControllerStyle.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction(title: "Install", style: UIAlertActionStyle.destructive, handler: { (_) in
                            if let iTunesUrl = URL(string: itunesLink), UIApplication.shared.canOpenURL(iTunesUrl){
                                
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(iTunesUrl, options: [:], completionHandler: nil)
                                } else {
                                    UIApplication.shared.openURL(iTunesUrl)
                                }
                            } else{
                                self.showError("Something went wrong.")
                            }
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                        
                        // show the alert
                        DispatchQueue.main.async {
                            self.present(alert, animated: true, completion:nil)
                        }
                    }
                }
            } else{
                self.showError("Invalid application name.")
            }
            
        }
            
        // For Web Links
        
        else if  menuType ==  "Browser" {
            
            //Uncomment the bellow
            
//            if let urlString = self.objectTypes[(indexPath as NSIndexPath).row].URL{
//                if let url = URL(string: urlString){
//                    if #available(iOS 10.0, *) {
//                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                    } else {
//                        UIApplication.shared.openURL(url)
//                    }
//                } else{
//                    self.showError("Cannot open the URL.")
//                }
//
//            } else{
//                self.showError("Cannot open the URL.")
//            }
            
            //Uncomment the above
            
            performSegue(withIdentifier: "MyVisits", sender: nil)
            
        }
        
        // For Objects
        
        else{
            if self.forceMoveDetailsPage(self.userInfo!){
                
                let object = self.objectTypes[(indexPath as NSIndexPath).row]
                self.forceMoveObjectType = object.ObjectType
                
                performSegue(withIdentifier: "ForceMoveToDetailsPage", sender: nil)
            } else {
                if self.hasFilterPage(self.userInfo!) {
                    performSegue(withIdentifier: "showFilter", sender: nil)
                } else {
                    
                    performSegue(withIdentifier: "ObjectFromHome", sender: nil)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
}


