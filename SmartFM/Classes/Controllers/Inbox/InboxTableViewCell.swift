//
//  InboxTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/17/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol InboxTableViewCellDelegate {
    
    func viewDetailsPressed(forIndexPath indexPath: IndexPath)
}

class InboxTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var unreadIndicator: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var imgObjType: UIImageView!
    @IBOutlet weak var btnArrow: UIButton!
    
    var delegate: InboxTableViewCellDelegate?
    
    fileprivate var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let selectionview: UIView = UIView()
        selectionview.backgroundColor = TableCellSelectionColor
        selectedBackgroundView = selectionview
        
        self.lblMessage.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.lblMessage.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ pushItem: PushMessage, isExpanded: Bool, indexPath: IndexPath) {
        
        self.indexPath = indexPath
        
        if let from = pushItem.MsgFrom {
            self.lblFrom.text = from
        } else {
            self.lblFrom.text = "N/A"
        }
        
        if pushItem.ObjectType == "WorkOrder" {
            self.imgObjType.image = UIImage(named: "objTypeIcon_workOrder")
        } else if pushItem.ObjectType == "Asset" {
            self.imgObjType.image = UIImage(named: "objTypeIcon_asset")
        } else if pushItem.ObjectType == "Inventory" {
            self.imgObjType.image = UIImage(named: "objTypeIcon_inventory")
        } else if pushItem.ObjectType == "Incident" {
            self.imgObjType.image = UIImage(named: "objTypeIcon_incident")
        } else {
            self.imgObjType.image = nil
        }
        
        if pushItem.isRead == "1" {
            
            self.unreadIndicator.backgroundColor = inboxReadCellColor
            
            self.unreadIndicator.layer.borderColor = UIColor.white.cgColor
            self.unreadIndicator.layer.borderWidth = 0.5
            
            self.lblMessage.textColor = UIColor.white
            self.lblFrom.textColor = UIColor.white
            
            if isExpanded {
                self.btnArrow.setImage(UIImage(named: "arrow_white"), for: UIControlState())
            } else {
                self.btnArrow.setImage(UIImage(named: "arrow_white_down"), for: UIControlState())
            }
            
        } else {
            
            self.unreadIndicator.layer.borderWidth = 0
            self.unreadIndicator.backgroundColor = UIColor.white
            
            self.lblMessage.textColor = UIColor.black
            self.lblFrom.textColor = UIColor.black
            
            if isExpanded {
                self.btnArrow.setImage(UIImage(named: "arrow_blue"), for: UIControlState())
            } else {
                self.btnArrow.setImage(UIImage(named: "arrow_blue_down"), for: UIControlState())
            }
        }
        
        if let time = pushItem.ReceivedAt {
            
            self.lblTime.text = (time as NSDate).timeAgoSinceNow()
            
        } else {
            
            self.lblTime.text = "N/A"
        }
        
        self.lblMessage.text = pushItem.Message
    }

    @IBAction func viewDetails(_ sender: AnyObject) {
        
        
        if let ip = self.indexPath {
            self.delegate?.viewDetailsPressed(forIndexPath: ip)
        }
    }
}
