//
//  InboxViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/17/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class InboxViewController: SmartFMParentViewController, PushMessageDelegate, InboxTableViewCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var pointerCenterConstraint: NSLayoutConstraint!
    
    var messages: [PushMessage] = []
    fileprivate let cellHeight: CGFloat = 95.0
    fileprivate var indexPathToExpand: IndexPath? = nil
    fileprivate var previousIndexPath: IndexPath? = nil
    fileprivate var MESSAGE_FONT_SIZE: CGFloat = 16
    fileprivate var alertError: LIHAlert?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertError = LIHAlertManager.getErrorAlert("Error occured")
        AlertManager().configErrorAlert(alertError, hasNavBar: true)
        alertError?.initAlert(self.view)
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.segmentControl.selectedSegmentIndex = 0
        
        //self.setIndexPaths()
        self.fetchMessages(false)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(InboxViewController.newMessageReceived), name: NSNotification.Name(rawValue: "messageReceived"), object: nil)
        

    }
    
    override func viewWillLayoutSubviews() {
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            self.pointerCenterConstraint.constant = self.segmentControl.frame.size.width / 4 * -1
        }
    }
    
    //MARK: - Private Methods
    fileprivate func showError(_ message: String) {
        
        self.alertError?.contentText = message
        self.alertError?.show(nil, hidden: nil)
    }
    
    fileprivate func loadAllMessages() {
        
        self.fetchMessages(true)
        self.tableView.reloadData()
    }
    
    fileprivate func loadUnreadMessages() {
        
        self.fetchMessages(false)
        self.tableView.reloadData()
    }
    
    fileprivate func fetchMessages(_ withRead: Bool) {
        
        if withRead {
            if let pushNs = PushMessage.all() as? [PushMessage] {
                self.messages = pushNs.reversed()
            }
        } else {
            if let pushNs = PushMessage.__where("isRead='0'", sortBy: "Id", accending: true) as? [PushMessage] {
                self.messages = pushNs.reversed()
            }

        }
    }
    
    fileprivate func heightForView(_ text:String) -> CGFloat {
        let width = UIScreen.main.bounds.width - 80.0
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: "Melbourne", size: 17.0)
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    fileprivate func viewMessage(_ indexPath: IndexPath) {
        
        let message = self.messages[(indexPath as NSIndexPath).row]
        
        if let ok = message.ObjectKey, let ot = message.ObjectType {
            
            if let _ = ObjType.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true).first as? ObjType {
                
                self.showObjectDetails(ot, objectKey: ok)
                
                if let id = message.Id, let msg = PushMessage.__where("Id=\(id)", sortBy: "Id", accending: true).first as? PushMessage {
                    
                    msg.isRead = "1"
                    msg.update()
                    message.isRead = "1"
                    
                    self.tableView.beginUpdates()

                    self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    self.tableView.endUpdates()
                }
                
                UIApplication.shared.applicationIconBadgeNumber = PushMessage.__where("isRead='0'", sortBy: "Id", accending: true).count
                
            } else {
                self.showError("Object type not available")
            }
            
        } else {
            
            self.showError("Object details not available")
        }
        
    }
    
    fileprivate func animaterPointer(_ selectedSegmentIndex: Int) {
        
        if selectedSegmentIndex == 0 {
            
            self.pointerCenterConstraint.constant = self.segmentControl.frame.size.width / 4 * -1
            
        } else {
            self.pointerCenterConstraint.constant = self.segmentControl.frame.size.width / 4
        
        }
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.view.layoutIfNeeded()
        }) 
    }
    
    func newMessageReceived() {
        
        if self.segmentControl.selectedSegmentIndex == 0 {
            self.fetchMessages(false)
            self.loadUnreadMessages()
        } else {
            self.fetchMessages(true)
            self.loadAllMessages()
        }
    }
    
    
    //MARK: - Events
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        
        self.indexPathToExpand = nil
        self.previousIndexPath = nil
        if sender.selectedSegmentIndex == 0 {
            self.loadUnreadMessages()
        } else {
            self.loadAllMessages()
        }
        
        self.animaterPointer(sender.selectedSegmentIndex)
    }
    
    
    //MARK: - Delegate Methods
    func showObjectDetails(_ objectType: String, objectKey: String) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectDetailsVc") as? ObjectDetailsViewController {
            
            vc.objectType = objectType
            vc.objectKey = objectKey
            DispatchQueue.main.async(execute: {
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
    
    func viewDetailsPressed(forIndexPath indexPath: IndexPath) {
        self.viewMessage(indexPath)
    }
}

extension InboxViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.messages.count == 0 {
            return 1
        } else {
            return self.messages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.messages.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            cell.setValues("No messages to display.", bgColor: ThemeBackgroundColor)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell") as! InboxTableViewCell
        cell.delegate = self
        cell.setValues(self.messages[(indexPath as NSIndexPath).row], isExpanded: self.indexPathToExpand == indexPath ? true : false, indexPath: indexPath)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.indexPathToExpand == indexPath {
            
            if self.messages.count >= (indexPath as NSIndexPath).row {
                
                if let message = self.messages[(indexPath as NSIndexPath).row].Message {
                    let addHeight = self.heightForView(message)
                    let removeHeight = self.heightForView("a")
                    return self.cellHeight - removeHeight + addHeight
                    
                } else {
                    
                    return self.cellHeight
                }
                
            } else {
                
                return self.cellHeight
            }
        }
        return self.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.previousIndexPath = self.indexPathToExpand
        if self.indexPathToExpand == indexPath {
            self.indexPathToExpand = nil
        } else {
            self.indexPathToExpand = indexPath
        }
        
        if let prev = self.previousIndexPath , indexPath != prev {
            tableView.reloadRows(at: [indexPath, prev], with: UITableViewRowAnimation.fade)
        } else {
            tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
        
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
        
    }
}

