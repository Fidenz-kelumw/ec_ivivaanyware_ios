//
//  ItemListTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol ObjectItemDelegate {
     func reloadItem(withItemkey key: String, itemData: String)
}

class ItemListTableViewCell: UITableViewCell {

    //IBOutlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var lblNameOnly: UILabel!
    @IBOutlet weak var valuesContainer: UIView!
    @IBOutlet weak var switchContainer: UIView!
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var itemSwitch: UISwitch!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnUp: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    
    
    //private members
    fileprivate var item: ObjectItem?
    fileprivate var buttons: [ButtonsTypeData] = []
    fileprivate var prevoiusValue: Float?
    
    //public members
    var delegate: GenericFormDelegate?
    var itemDelegate: ObjectItemDelegate?
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues(item: ObjectItem, hasOverlay: Bool) {
        
        self.item = item
        
        
        self.lblName.text = item.ItemName
        self.lblNameOnly.text = item.ItemName
        
        
        if item.TypeID == ObjectItemType.SWITCH.rawValue {
            if item.ItemData == "1" {
                self.lblDescription.text = "ON"
            } else {
                self.lblDescription.text = "OFF"
            }
        } else if item.TypeID == ObjectItemType.BUTTONS.rawValue {
            if let val = item.ItemData {
                if let match = item.buttonsTypeData?.filter({$0.Value == val}).first {
                    self.lblDescription.text = match.Name
                } else {
                    self.lblDescription.text = ""
                }
            } else {
                self.lblDescription.text = ""
            }
        } else {
            self.lblDescription.text = item.ItemData
        }
        
        if let col = item.DataColor {
            self.lblDescription.textColor = UIColor.colorWithHexString(String(col.characters.dropFirst()) as NSString)
        } else {
            self.lblDescription.textColor = UIColor.white
        }
        
        if item.ItemData == nil || item.ItemData == "" {
            self.configNameOnly()
        } else {
            self.configNameAndData()
        }
        
        self.overlayView.isHidden = !hasOverlay
        
        self.setEditData()
        self.configValueButtons()
    }
    
    func showEdit() {
        
        self.hideEdit()
        
        if self.item?.TypeID == ObjectItemType.VALUE.rawValue {
            self.valuesContainer.isHidden = false
        } else if self.item?.TypeID == ObjectItemType.SWITCH.rawValue {
            self.switchContainer.isHidden = false
        } else if self.item?.TypeID == ObjectItemType.BUTTONS.rawValue {
            self.buttonsContainer.isHidden = false
        }
        
        self.setEditData()
    }
    
    func hideEdit() {
        
        self.switchContainer.isHidden = true
        self.valuesContainer.isHidden = true
        self.buttonsContainer.isHidden = true
    }
    
    fileprivate func configNameOnly() {
        
        self.lblNameOnly.isHidden = false
        self.lblName.isHidden = true
    }
    
    fileprivate func configNameAndData() {
        
        self.lblNameOnly.isHidden = true
        self.lblName.isHidden = false
    }
    
    fileprivate func setEditData() {
        
        if self.item?.TypeID == ObjectItemType.VALUE.rawValue {
            self.txtValue.text = self.item?.ItemData
        } else if self.item?.TypeID == ObjectItemType.SWITCH.rawValue {
            self.itemSwitch.setOn(self.item?.ItemData == "1", animated: true)
        } else if self.item?.TypeID == ObjectItemType.BUTTONS.rawValue {
            if let btns = self.item?.buttonsTypeData {
                self.buttons = btns
                self.collectionView.reloadData()
            }
        }
    }
    
    fileprivate func getMaxValue() -> Float? {
        if let maxTxt = self.item?.valueTypeData?.MAX, let max = Float(maxTxt) {
            return max
        }
        return nil
    }
    
    fileprivate func getMinValue() -> Float? {
        if let minTxt = self.item?.valueTypeData?.MIN, let min = Float(minTxt) {
            return min
        }
        return nil
    }
    
    fileprivate func getCurrentValue() -> Float? {
        if let txt = self.txtValue.text, let val = Float(txt) {
            return val
        }
        return nil
    }
    
    fileprivate func getIncValue() -> Float? {
        if let IncrementTxt = self.item?.valueTypeData?.INC, let Increment = Float(IncrementTxt) {
            return Increment
        }
        return nil
    }
    
    fileprivate func configValueButtons() {
        
        if let max = self.getMaxValue(), let val = self.getCurrentValue(), let inc = self.getIncValue() {
            if val >= max - inc {
                self.setEnable(button: self.btnUp, enabled: false)
            } else {
                self.setEnable(button: self.btnUp, enabled: true)
            }
        }
        
        if let min = self.getMinValue(), let val = self.getCurrentValue(), let inc = self.getIncValue() {
            if val <= min + inc {
                self.setEnable(button: self.btnDown, enabled: false)
            } else {
                self.setEnable(button: self.btnDown, enabled: true)
            }
        }
    }
    
    fileprivate func validate(val: Float) -> Bool {
        
        if let min = self.getMinValue() {
            if val < min {
                return false
            }
        }
        
        if let max = self.getMaxValue() {
            if val > max {
                return false
            }
        }
        
        return true
    }
    
    fileprivate func setEnable(button: UIButton, enabled: Bool) {
        
        button.isEnabled = enabled
        button.alpha = enabled ? 1 : 0.5
    }
    
    fileprivate func setDefaultValue() {
        
        if let min = self.getMinValue() {
            self.txtValue.text = "\(min)"
        } else if let max = self.getMaxValue() {
            self.txtValue.text = "\(max)"
        } else {
            self.txtValue.text = "0"
        }
    }
    
    fileprivate func constructErrorMessage() -> String {
        
        var msg = "Value should be "
        var hasMin = false
        if let min = self.getMinValue() {
            msg = msg + "greater than \(min) "
            hasMin = true
        }
        if let max = self.getMaxValue() {
            if hasMin {
                msg = msg + "and "
            }
            msg = msg + "less than \(max)"
        }
        
        return msg
    }
    
    fileprivate func updateItem(itemData: String) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tn = self.tabName, let itemKey = self.item?.ItemKey {
            
            
            self.delegate?.showHUD?(userInteraction: false)
            
            APIClient().getUpdateObjectItem(ot, objectKey: ok, tabName: tn, itemKey: itemKey, itemData: itemData, completion: { (success, error) in
                
                self.delegate?.hideHUD?()
                
                if success {
                    self.delegate?.showSuccess?("\(error)")
                    self.itemDelegate?.reloadItem(withItemkey: itemKey, itemData: itemData)
                } else {
                    self.delegate?.showError?("\(error)")
                }
            })
        }
    }
    
    
    //MARK: - Events
    @IBAction func valueUp(_ sender: Any) {
        
        var inc: Float = 1
        if let Increment = self.getIncValue() {
            inc = Increment
        }
        
        if let val = self.getCurrentValue() {
            let newVal = val + inc
            if self.validate(val: newVal) {
                self.txtValue.text = "\(newVal)"
            } else {
                self.txtValue.text = "\(val)"
            }
        } else {
            if self.validate(val: inc) {
                self.txtValue.text = "\(inc)"
            } else {
                self.setDefaultValue()
            }
        }
        
        self.configValueButtons()
    }
    
    @IBAction func valueDown(_ sender: Any) {
        var inc: Float = 1
        if let IncrementTxt = self.item?.valueTypeData?.INC, let Increment = Float(IncrementTxt) {
            inc = Increment
        }
        
        if let val = self.getCurrentValue() {
            let newVal = val - inc
            if self.validate(val: newVal) {
                self.txtValue.text = "\(newVal)"
            } else {
                self.txtValue.text = "\(val)"
            }
        } else {
            if self.validate(val: inc) {
                self.txtValue.text = "\(inc)"
            } else {
                self.setDefaultValue()
            }
        }
        
        self.configValueButtons()
    }
    
    @IBAction func setValue(_ sender: UIButton) {
        
        if let cur = self.getCurrentValue() {
            if self.validate(val: cur) {
                self.updateItem(itemData: "\(cur)")
            } else {
                
                let alert = UIAlertController(title: "Error", message: self.constructErrorMessage(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.delegate?.showVc?(alert)
            }
        }
    }
    
    @IBAction func switchStateChnaged(_ sender: UISwitch) {
        
        self.updateItem(itemData: sender.isOn ? "1" : "0")
    }
    
}


extension ItemListTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.buttons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! ButtonCollectionViewCell
        
        let button = self.buttons[indexPath.row]
        cell.setValues(ItemButton: button)
        
        if self.item?.ItemData == button.Value,  self.item?.ItemData != "",  self.item?.ItemData != nil {
            cell.setSelected(selected: true)
        } else {
            cell.setSelected(selected: false)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var indexPathsToReload = [indexPath]
        var clickedNewCell = false
        if let currentSelectedValue = self.item?.ItemData {
            for n in 0..<self.buttons.count {
                let btn = self.buttons[n]
                if btn.Value == currentSelectedValue {
                    let ip = IndexPath(item: n, section: 0)
                    indexPathsToReload.append(ip)
                    
                    if indexPath.row != n {
                        clickedNewCell = true
                    } else {
                        clickedNewCell = false
                    }
                    
                    break
                }
            }
        }
        
        let cellButton = (collectionView.cellForItem(at: indexPath) as! ButtonCollectionViewCell).objectButton
        self.item?.ItemData = cellButton?.Value
        if clickedNewCell, let val = cellButton?.Value {
            self.updateItem(itemData: val)
        }
        self.collectionView.reloadItems(at: indexPathsToReload)
    }
}


