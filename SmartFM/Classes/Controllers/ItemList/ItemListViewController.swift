//
//  ItemListViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController, ObjectItemDelegate {

    //IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnReload: RNLoadingButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    
    
    //private members
    fileprivate var editModeEnabled = false {
        didSet {
            self.configEditButton()
            self.tableView.reloadData()
            if !self.editModeEnabled {
                self.selectedItemRow = nil
            }
        }
    }
    fileprivate var items: [ObjectItem] = []
    fileprivate var selectedItemRow: Int?
    
    //public members
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    var delegate: GenericFormDelegate?
    var forceSync:Bool?
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.configEditButton()
        
        self.showLoading()
        fetchData { (success, error) in
            self.hideLoading()
            if success {
                self.tableView.reloadData()
            } else {
                var errorMessage = "Fetching items failed"
                if let er = error {
                    errorMessage = er
                }
                self.delegate?.showError?("\(errorMessage)")
            }
        }
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Private Methods
    //////////////////////////////////////////////////////
    fileprivate func fetchData(completion: ((Bool, String?)->Void)?) {
        
        if self.forceSync == nil {
            self.forceSync = false
        }
        
        if self.forceSync!{
            self.syncItemsList(completion: { (success, error) in
                
                completion?(success, error)
            })
        }else{
            if SyncTimeManager().hasTimerExpired(EntityKey.ItemList) {
                
                self.syncItemsList(completion: { (success, error) in
                    
                    completion?(success, error)
                })
                
            } else {
                
                if self.loadItemsListFromCache() {
                    completion?(true, nil)
                } else {
                    completion?(false, nil)
                }
            }
        }
        
    }
    
    fileprivate func syncItemsList(completion: ((Bool, String?)->Void)?) {
    
        if let ot = self.objectType, let ok = self.objectKey, let tn = self.tabName {
            
            self.btnReload.isLoading = true
            SyncManager().syncObjectItems(ot, objectKey: ok, tabName: tn, success: { (success, error) in
                
                self.btnReload.isLoading = false
                if success {
                    let loaded = self.loadItemsListFromCache()
                    completion?(loaded, error)
                } else {
                    completion?(false, error)
                }
            })
        }
    }
    
    fileprivate func loadItemsListFromCache() -> Bool {
        
        if let objs = ObjectItem.all() as? [ObjectItem] {
            
            for obj in objs {
                
                if obj.TypeID == ObjectItemType.VALUE.rawValue, let objId = obj.Id {
                    obj.valueTypeData = self.getValueTypeData(forItem: objId)
                } else if obj.TypeID == ObjectItemType.SWITCH.rawValue, let objId = obj.Id  {
                    obj.switchtypeData = self.getSwitchTypeData(forItem: objId)
                } else if obj.TypeID == ObjectItemType.BUTTONS.rawValue, let objId = obj.Id  {
                    obj.buttonsTypeData = self.getButtonsTypeData(forItem: objId)
                }
            }
            
            self.items = objs
            
            self.btnEdit.isHidden = self.items.first?.PageEditEnabled == "1" ? false : true
            
            return true
        }
        return false
    }
    
    fileprivate func getValueTypeData(forItem itemId: NSNumber) -> ValueTypeData? {
        
        let val = ValueTypeData.__where("objectItemId=\(itemId)", sortBy: "Id", accending: true).first as? ValueTypeData
        return val
    }
    
    fileprivate func getSwitchTypeData(forItem itemId: NSNumber) -> SwitchTypeData? {
        
        return SwitchTypeData.__where("objectItemId=\(itemId)", sortBy: "Id", accending: true).first as? SwitchTypeData
    }
    
    fileprivate func getButtonsTypeData(forItem itemId: NSNumber) -> [ButtonsTypeData]? {
        
        return ButtonsTypeData.__where("objectItemId=\(itemId)", sortBy: "Id", accending: true) as? [ButtonsTypeData]
    }
    
    fileprivate func configEditButton() {
        
        var imagename = "btn_edit_unselected"
        if self.editModeEnabled {
            imagename = "btn_edit_selected"
        }
        let image = UIImage(named: imagename)
        self.btnEdit.setImage(image, for: UIControlState.normal)
    }
    
    fileprivate func showLoading() {
        
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Events
    //////////////////////////////////////////////////////
    
    @IBAction func reloadPressed(_ sender: UIButton) {
        
        self.editModeEnabled = false
        self.syncItemsList { (success, error) in
            
            if success {
                self.tableView.reloadData()
            } else {
                
                var errorMessage = "Fetching data failed"
                if let er = error {
                    errorMessage = er
                }
                self.delegate?.showError?("\(errorMessage)")
            }
        }
    }
    
    @IBAction func editPressed(_ sender: Any) {
        
        self.editModeEnabled = !self.editModeEnabled
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////
    
    func reloadItem(withItemkey key: String, itemData: String) {
        
        for n in 0..<self.items.count {
            let item = self.items[n]
            if item.ItemKey == key {
                item.ItemData = itemData
                item.update()
                let ip = IndexPath(row: n, section: 0)
                self.tableView.reloadRows(at: [ip], with: UITableViewRowAnimation.fade)
                break
            }
        }
    }
}


extension ItemListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.items.count == 0 {
            return 1
        }
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.items.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            cell.setValues("No items found.", bgColor: ThemeBackgroundColor)
            
            return cell
        }
        
        let item = self.items[indexPath.row]
        var hasOverlay = self.editModeEnabled
        var showEdit = false
        if self.editModeEnabled, indexPath.row == self.selectedItemRow {
            hasOverlay = false
            showEdit = true
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NameDataCell") as! ItemListTableViewCell
        cell.delegate = self.delegate
        cell.itemDelegate = self
        cell.objectType = self.objectType
        cell.objectKey = self.objectKey
        cell.tabName = self.tabName
        cell.setValues(item: item, hasOverlay: hasOverlay)
        showEdit ? cell.showEdit() : cell.hideEdit()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedItemRow != indexPath.row {
            
            let item = self.items[indexPath.row]
            if self.editModeEnabled, item.ItemEdit == "1" {
                var indexPaths = [indexPath]
                if let row = self.selectedItemRow {
                    let ip = IndexPath(row: row, section: 0)
                    indexPaths.append(ip)
                }
                self.selectedItemRow = indexPath.row
                tableView.reloadRows(at: indexPaths, with: UITableViewRowAnimation.fade)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.editModeEnabled, indexPath.row == self.selectedItemRow {
            return 115
        }
        
        return 50
    }
}
