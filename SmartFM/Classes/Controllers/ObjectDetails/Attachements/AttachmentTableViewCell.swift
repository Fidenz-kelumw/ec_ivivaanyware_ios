//
//  AttachmentTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/10/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class AttachmentTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAttachment: UIImageView!
    @IBOutlet weak var attachmentName: UILabel!
    @IBOutlet weak var attachmentType: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var fullImage: UIImage?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues(_ attachment: Attachment) {
        
        if let name = attachment.AttachmentName {
            self.attachmentName.text = name
        } else {
            self.attachmentName.text = ""
        }
        
        if let name = attachment.AttachmentType {
            self.attachmentType.text = name
        } else {
            self.attachmentType.text = ""
        }
        
        
        if let user = attachment.User?.UserName {
            self.lblUser.text = user
        } else {
            self.lblUser.text = ""
        }
        
        if let date = attachment.UploadedAt {
            self.lblDate.text = DateTimeManager().getDateTimeFromISOString(date)
        } else {
            self.lblUser.text = ""
        }
    }
}
