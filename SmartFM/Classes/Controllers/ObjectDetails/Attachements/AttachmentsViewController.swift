//
//  AttachmentsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/10/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit

protocol AttachmentsDelegate {
    
    func showVc(_ vc: UIViewController)
    func showSuccess(_ message: String)
    func showError(_ message: String)
    func showProcessingAlert(_ completion: @escaping ()->Void)
    func hideProcessing()
}

class AttachmentsViewController: UIViewController, SetAttachmentDelegate, DrawSignatureDelegate, EditImageDelegate, VideoPlayerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddAttachment: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    var delegate: AttachmentsDelegate?
    var forceSync:Bool?
    
    fileprivate var attachmentMain: AttachmentsMain?
    fileprivate var isError = false
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var imageForNewAttachment: AttachedAsset?
    fileprivate var titleForNewAttachment: String?
    fileprivate var attachmentTypeForNewAttachment: AttachmentType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(AttachmentsViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl!)
        
        if let sync = self.forceSync{
            if sync{
                self.showLoading()
                self.fetchDataWithoutTimer{
                    self.hideLoading()
                }
            } else{
                self.fetchData(nil)
            }
        }else{
            self.fetchData(nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func refresh(_ sender: UIRefreshControl) {
        
        self.fetchDataWithoutTimer {
            self.refreshControl?.endRefreshing()
            
        }
    }
    
    //Mark: - Private Methods
    
    fileprivate func fetchData(_ completion: (()->Void)?) {
        
        self.showLoading()
        if SyncTimeManager().hasTimerExpired(EntityKey.Attachments) {
            
            self.fetchDataWithoutTimer({
                self.hideLoading()
                completion?()
            })
            
        } else {
            
            self.hideLoading()
            if let attachmentMain = AttachmentsMain.all().first as? AttachmentsMain {
                
                self.attachmentMain = attachmentMain
                self.extractAttachments()
                
            }
            self.tableView.reloadData()
            
            if self.attachmentMain?.AddNewAttachment == "1" {
                self.setButtonStatus(enabled: true)
            } else {
                self.setButtonStatus(enabled: false)
            }
            completion?()
        }
        
    }
    
    fileprivate func fetchDataWithoutTimer(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            SyncManager().syncAttachments(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                
                self.hideLoading()
                if success {
                    if let attachmentMain = AttachmentsMain.all().first as? AttachmentsMain {
                        
                        self.attachmentMain = attachmentMain
                        self.extractAttachments()
                        self.tableView.reloadData()
                        
                        if self.attachmentMain?.AddNewAttachment == "1" {
                            self.setButtonStatus(enabled: true)
                        } else {
                            self.setButtonStatus(enabled: false)
                        }
                        completion?()
                    } else {
                        completion?()
                    }
                } else {
                    completion?()
                }
            }
        }
    }
    
    fileprivate func extractAttachments() {
        
        if let attMainId = self.attachmentMain?.Id {
            
            if let attachments = Attachment.__where("attMainId=\(attMainId)", sortBy: "Id", accending: true) as? [Attachment] {
                
                for attachment in attachments {
                    
                    if let attId = attachment.Id {
                        attachment.User = UploadedUser.__where("AttachmentId=\(attId)", sortBy: "Id", accending: true).first as? UploadedUser
                    }
                }
                
                self.attachmentMain?.attachments = attachments
            }
            
            if let attachmentTypes = AttachmentType.__where("attMainId=\(attMainId)", sortBy: "Id", accending: true) as? [AttachmentType] {
                
                self.attachmentMain?.AttachmentTypes = attachmentTypes
            }
        }
        
    }
    
    fileprivate func showLoading() {
        
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    fileprivate func setButtonStatus(enabled: Bool) {
        
        self.btnAddAttachment.isEnabled = enabled
        if enabled {
            self.btnAddAttachment.backgroundColor = ThemeRedColor
        } else {
            self.btnAddAttachment.backgroundColor = DisabledButtonColor
        }
    }
    
    fileprivate func showNewAttachmentPopup(animated: Bool) {
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        if let vc = storyboard.instantiateViewController(withIdentifier: "AddAttachment") as? UINavigationController {
            
            vc.navigationBar.barTintColor = popupHeaderColor
            vc.navigationBar.tintColor = UIColor.white
            let textAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
            ]
            vc.navigationBar.titleTextAttributes = textAttributes
            
            if vc.viewControllers.count>0 {
                if let fvc = vc.viewControllers[0] as? AddAttachmentViewController {
                    if let main = self.attachmentMain {
                        
                        if let asset = self.imageForNewAttachment {
                            fvc.attachedAsset = asset
                        }
                        
                        fvc.selectedAttachmentType = self.attachmentTypeForNewAttachment
                        fvc.attachmentTitle = self.titleForNewAttachment
                        fvc.newPhotoDelegate = self
                        fvc.attachmentTypes = main.AttachmentTypes
                        if let inputs = main.InputModes {
                            fvc.inputTypes = inputs
                        }
                        fvc.delegate = self.delegate
                    }
                }
            }
            
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = false
            formSheet.shouldDismissOnBackgroundViewTap = false
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 350.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: animated, completionHandler: nil)
        }
    }
    
    fileprivate func constructName() -> String {
        var imageName = UUID().uuidString
        if let userKey = UserManager().getUserKey() {
            imageName = "\(userKey)_\(imageName)"
        }
        imageName = "\(imageName)_\(Date().timeIntervalSince1970)"
        
        return imageName
    }
    
    fileprivate func setAttachment(_ media: AttachedAsset?) {
        
        self.imageForNewAttachment = media
        self.showNewAttachmentPopup(animated: false)
    }
    
    fileprivate func imageToBase64(_ image: UIImage) -> String? {
        
        var compressionRatio: CGFloat = 1.0
        var pass:Int = 1
        
        if var imageData = UIImageJPEGRepresentation(image, compressionRatio) {
            
            if imageData.count > 1 * 1000 * 1000 {
                
                if let imgdata0 = UIImageJPEGRepresentation(image, 0) {
                    
                    return imgdata0.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
                }
                
            } else {
                
                while imageData.count > 100000 && pass < 5 {
                    compressionRatio = compressionRatio * 0.5
                    if let newImageData = UIImageJPEGRepresentation(image, compressionRatio) {
                        imageData = newImageData
                    }
                    pass = pass + 1
                }
                
                return imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
            }
        }
        
        return nil
    }
    
    
    //Mark: - Events
    @IBAction func addAttachment(_ sender: AnyObject) {
        
        self.imageForNewAttachment = nil
        self.showNewAttachmentPopup(animated: true)
        
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
    }
    
    
    //MARK: - Delegate Methods
    func takePhoto() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.camera
//        picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String, kUTTypeVideo as String]
        self.delegate?.showVc(picker)
    }
    
    func selectPhoto() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String, kUTTypeVideo as String]
        self.delegate?.showVc(picker)
    }
    
    func resetPhoto() {
        
        self.imageForNewAttachment = nil
    }
    
    func resetData() {
        
        self.imageForNewAttachment = nil
        self.titleForNewAttachment = nil
        self.attachmentTypeForNewAttachment = nil
    }
    
    func drawSignature() {
        
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DrawVc") as? UINavigationController {
            if let signatureVc = vc.viewControllers.first as? DrawSignatureViewController {
                signatureVc.delegate = self
            }
            self.delegate?.showVc(vc)
        }
    }
    
    func takeVideo() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.camera
        picker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
        self.delegate?.showVc(picker)
    }
    
    func recordAudio() {
        
        if let vc = UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecordAudioVC") as? UINavigationController {
            if let vc = vc.viewControllers.first as? RecordAudioViewController {
                //vc.delegate = self
            }
        }
    }
    
    func setSignature(_ image: UIImage?) {
        
        let asset = AttachedAsset()
        asset.attachedImage = image
        asset.mediaType = MediaType.IMG
        
        self.imageForNewAttachment = asset
        self.showNewAttachmentPopup(animated: false)
    }
    
    func uploadImage(_ image: UIImage, type: AttachmentType, name: String?) {
        
        var compressedImage: UIImage = image
        if let size = UserManager().getUploadSize() {
            
            if size == ImageUploadSize.Small {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 400)
                
            } else if size == ImageUploadSize.Large {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 1200)
                
            } else if size == ImageUploadSize.Original {
                
            } else {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 800)
            }
        }
        
        
        if let ot = self.objectType, let ok = self.objectKey, let tabName = self.tabName, let attType = type.TypeName {
            
            self.delegate?.showProcessingAlert({
                
                let imagefilename = self.constructName()
                
                APIClient().uploadImage(compressedImage, name: "\(imagefilename).jpg", uploadFolder: "/UploadContent/SmartFM", completion: { (success) in
                    
                    if success {
                        
                        APIClient().addNewAttachment(ot, objectKey: ok, tabName: tabName, attachmentType: attType, attachmentName: name, fileName: "\(imagefilename).jpg", mediaType: MediaType.IMG, completion: { (success,attachmentKey,message) in
                            
                            self.delegate?.hideProcessing()
                            if success {
                                
                                if let msg = message {
                                    self.delegate?.showSuccess(msg)
                                } else {
                                    self.delegate?.showSuccess("Successfully uploaded")
                                }
                                self.showLoading()
                                self.fetchDataWithoutTimer({
                                    self.hideLoading()
                                })
                                
                            } else {
                                
                                if let msg = message {
                                    self.delegate?.showError(msg)
                                } else {
                                    self.delegate?.showError("Failed to upload")
                                }
                            }
                        })
                        
                    } else {
                        
                        self.delegate?.showError("Failed to upload")
                    }
                })
                
                
            })
        }
    }
    
    func uploadVideo(videoUrl: URL, type: AttachmentType, name: String?) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tabName = self.tabName, let attType = type.TypeName {
            
            self.delegate?.showProcessingAlert {
                
                let filename = self.constructName()
                
                APIClient().uploadVideo(url: videoUrl, name: "\(filename).mov", uploadFolder: "/UploadContent/SmartFM", completion: { (success) in
                    
                    if success {
                        
                        APIClient().addNewAttachment(ot, objectKey: ok, tabName: tabName, attachmentType: attType, attachmentName: name, fileName: "\(filename).mov", mediaType: MediaType.VID, completion: { (success,attachmentKey,message) in
                            
                            self.delegate?.hideProcessing()
                            if success {
                                
                                if let msg = message {
                                    self.delegate?.showSuccess(msg)
                                } else {
                                    self.delegate?.showSuccess("Successfully uploaded")
                                }
                                self.showLoading()
                                self.fetchDataWithoutTimer({
                                    self.hideLoading()
                                })
                                
                            } else {
                                
                                if let msg = message {
                                    self.delegate?.showError(msg)
                                } else {
                                    self.delegate?.showError("Failed to uploaded")
                                }
                            }
                        })
                        
                    } else {
                        
                        self.delegate?.showError("Failed to uploaded")
                    }
                })
            }
        }
    }
    
    func uploadAudio(audioUrl: URL, type: AttachmentType, name: String?) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tabName = self.tabName, let attType = type.TypeName {
            
            self.delegate?.showProcessingAlert {
                
                let filename = self.constructName()
                
                APIClient().uploadAudio(url: audioUrl, name: "\(filename).m4a", uploadFolder: "/UploadContent/SmartFM", completion: { (success) in
                    
                    if success {
                        
                        APIClient().addNewAttachment(ot, objectKey: ok, tabName: tabName, attachmentType: attType, attachmentName: name, fileName: "\(filename).m4a", mediaType: MediaType.AUD, completion: { (success,attachmentKey,message) in
                            
                            self.delegate?.hideProcessing()
                            if success {
                                
                                if let msg = message {
                                    self.delegate?.showSuccess(msg)
                                } else {
                                    self.delegate?.showSuccess("Successfully uploaded")
                                }
                                self.showLoading()
                                self.fetchDataWithoutTimer({
                                    self.hideLoading()
                                })
                                
                            } else {
                                
                                if let msg = message {
                                    self.delegate?.showError(msg)
                                } else {
                                    self.delegate?.showError("Failed to uploaded")
                                }
                            }
                        })
                        
                    } else {
                        
                        self.delegate?.showError("Failed to uploaded")
                    }
                })
            }
        }
    }
    
    func setSelectedAttachmentType(_ attachmentType: AttachmentType?) {
        
        self.attachmentTypeForNewAttachment = attachmentType
    }
    
    func setAttachmentTitle(_ title: String?) {
        
        self.titleForNewAttachment = title
    }
    
    func editImage(asset: AttachedAsset) {
        
        if let nav = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditVc") as? UINavigationController {
            if let vc = nav.viewControllers.first as? EditImageViewController {
                vc.delegate = self
                vc.image = asset.attachedImage
            }
            self.delegate?.showVc(nav)
        }
    }
    
    func showVideo(url: URL) {
        
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AVPlayerVC") as? VideoPlayerViewController {
            let player = AVPlayer(url: url)
            vc.player = player
            vc.videoPlayerDelegate = self
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                () -> Void in
                self.delegate?.showVc(vc)
            }
        }
    }
    
    //edit image delegate
    func saveEditedImage(image: UIImage?) {
        
        self.imageForNewAttachment?.attachedImage = image
        self.showNewAttachmentPopup(animated: false)
    }
    
    func editingCancelled() {
        
        self.showNewAttachmentPopup(animated: false)
    }
    
    
    //videoplayer delegate
    func donePressed() {
        
        self.showNewAttachmentPopup(animated: false)
    }
}

extension AttachmentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let attMain = self.attachmentMain , attMain.attachments.count > 0 {
            return attMain.attachments.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let attMain = self.attachmentMain , attMain.attachments.count > 0 {
            
            let att = attMain.attachments[(indexPath as NSIndexPath).row]
            
            
            if att.ATTType == MediaType.IMG.rawValue || att.ATTType == "" {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "attachmentCell") as! AttachmentTableViewCell
                
                let defaultImage = UIImage(named: "attachmentImage")!
                cell.imgAttachment.tag = (indexPath as NSIndexPath).row
                cell.imgAttachment.image = defaultImage
                if let image = att.FilePath {
                    
                    if let acc = UserManager().getAccount() {
                        ImageHandler().asyncImageDownloadWithCache("\(Protocol)://\(acc)\(image)", key: att.AttachmentKey != nil ? att.AttachmentKey! : "aa", completion: { (image, thumbnail, path) in
                            
                            if cell.imgAttachment.tag == (indexPath as NSIndexPath).row {
                                
                                cell.fullImage = image
                                
                                if let img = thumbnail {
                                    cell.imgAttachment.image = img
                                } else if let img = image {
                                    cell.imgAttachment.image = img
                                } else {
                                    cell.imgAttachment.image = defaultImage
                                }
                            }
                        })
                    }
                }
                cell.setValues(att)
                return cell
                
            } else if att.ATTType == MediaType.AUD.rawValue {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "audioAttachmentCell") as! AudioAttachmentTableViewCell
                
                if let path = att.FilePath, let acc = UserManager().getAccount(), let url = URL(string: "\(Protocol)://\(acc)\(path)") {
                    cell.audioUrl = url
                }
                
                cell.setValues(att)
                return cell
                
            } else if att.ATTType == MediaType.VID.rawValue {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "videoAttachmentCell") as! VideoAttachmentTableViewCell
                if let path = att.FilePath, let acc = UserManager().getAccount(), let url = URL(string: "\(Protocol)://\(acc)\(path)") {
                    cell.videoUrl = url
                    Thumbnailer().thumbnailFromVideo(url: url, completion: { (thumbnail) in
                        if let thumb = thumbnail {
                            cell.imgAttachment.image = thumb
                        } else {
                            cell.imgAttachment.image = nil
                        }
                    })
                }
                cell.setValues(att)
                return cell
            }
            
            return UITableViewCell()
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues("Failed to fetch data", bgColor: ThemeBackgroundColor)
            } else {
                // @NOTE: 02/10/2017 Changes
                cell.setValues("No attachments found", bgColor: ThemeBackgroundColor)
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? AttachmentTableViewCell {
            
            if let image = cell.fullImage , image != UIImage(named: "attachmentImage") {
                
                if let nvc = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageVc") as? UINavigationController, let vc = nvc.viewControllers.first as? ViewAttachmentImageViewController {
                    
                    vc.image = image
                    
                    if let imageTitle = cell.attachmentName.text {
                        vc.title = imageTitle
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                        () -> Void in
                        self.delegate?.showVc(nvc)
                    }
                }
            }
        } else if let cell = tableView.cellForRow(at: indexPath) as? VideoAttachmentTableViewCell {
            
            if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AVPlayerVC") as? AVPlayerViewController, let url = cell.videoUrl {
//                let uu = URL(string: "http://unchealthcarevideos.org/News/2009/06/ilura.cochlear.mov")!
                
                if let url = URL(string: "\(url)?autoct=1") {
                    let player = AVPlayer(url: url)
                    vc.player = player
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                    () -> Void in
                    self.delegate?.showVc(vc)
                }
            }
            
        } else if let cell = tableView.cellForRow(at: indexPath) as? AudioAttachmentTableViewCell {
            
            if let nvc = UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "AudioPlaybackVC") as? UINavigationController, let vc = nvc.viewControllers.first as? AudioPlaybackViewController {
                
                if let url = cell.audioUrl {
                    
                    vc.url = url
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
                        () -> Void in
                        
                        let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                        formSheet.shouldCenterVertically = true
                        formSheet.shouldDismissOnBackgroundViewTap = true
                        let width = UIScreen.main.bounds.width - 40
                        let height: CGFloat = 200
                        formSheet.presentedFormSheetSize = CGSize(width: width, height: height)
                        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                        
                        formSheet.present(animated: true, completionHandler: nil)
                    }
                }
            }
        }
        
    }
    
}

extension AttachmentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        let asset = AttachedAsset()
        
        if info[UIImagePickerControllerMediaType] as! NSString == kUTTypeImage {
            asset.attachedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            asset.mediaType = MediaType.IMG
        } else if info[UIImagePickerControllerMediaType] as! NSString == kUTTypeMovie || info[UIImagePickerControllerMediaType] as! NSString == kUTTypeVideo {
            
            if let url = info[UIImagePickerControllerMediaURL] as? URL {
                let name = "\(self.constructName()).mov"
                let renamed_url = FileHandler.renameFile(atURL: url, to: name)
                asset.attachedMediaURL = renamed_url
                asset.mediaType = MediaType.VID
            }
                
        }
        
        self.setAttachment(asset)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        self.showNewAttachmentPopup(animated: false)
    }
    
}

