//
//  DrawSignatureViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit


protocol DrawSignatureDelegate {
    
    func setSignature(_ image: UIImage?)
}

class DrawSignatureViewController: UIViewController {

    
    @IBOutlet weak var imgDrawBoard: UIImageView!
    
    var delegate: DrawSignatureDelegate?
    
    fileprivate var swiped = false
    fileprivate var lastPoint = CGPoint.zero
    fileprivate var isEmpty = true
    var brushWidth: CGFloat = 5.0
    var opacity: CGFloat = 1.0
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.swiped = false
        if let touch = touches.first {
            self.lastPoint = touch.location(in: self.view)
        }
    }
    
    
    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
        
        // 1
        UIGraphicsBeginImageContext(view.frame.size)
        let context = UIGraphicsGetCurrentContext()
        imgDrawBoard.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        
        // 2
        context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        // 3
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(brushWidth)
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: 1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        // 4
        context?.strokePath()
        
        // 5
        imgDrawBoard.image = UIGraphicsGetImageFromCurrentImageContext()
        imgDrawBoard.alpha = opacity
        UIGraphicsEndImageContext()
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.isEmpty = false
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            
            // 7
            lastPoint = currentPoint
        }
    }

    @IBAction func cancelPressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePressed(_ sender: AnyObject) {
        
        if !self.isEmpty {
            UIGraphicsBeginImageContext(self.imgDrawBoard.bounds.size)
            UIColor.white.set()
            UIRectFill(CGRect(x: 0, y: 0, width: self.imgDrawBoard.frame.size.width, height: self.imgDrawBoard.frame.size.height))
            self.imgDrawBoard.image?.draw(in: CGRect(x: 0, y: 0, width: self.imgDrawBoard.frame.size.width, height: self.imgDrawBoard.frame.size.height))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            self.delegate?.setSignature(image)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
