//
//  EditImageViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/24/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol EditImageDelegate {
    func saveEditedImage(image: UIImage?)
    func editingCancelled()
}

class EditImageViewController: UIViewController {

    //IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var color1: UIButton!
    @IBOutlet weak var color2: UIButton!
    @IBOutlet weak var color3: UIButton!
    @IBOutlet weak var color4: UIButton!
    @IBOutlet weak var color5: UIButton!
    
    
    //public members
    var image: UIImage?
    var delegate: EditImageDelegate?
    
    //private members
    fileprivate var swiped = false
    fileprivate var lastPoint = CGPoint.zero
    var brushWidth: CGFloat = 5.0
    var opacity: CGFloat = 1.0
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageView.image = self.image
        
        self.color1.backgroundColor = UIColor.black
        self.color2.backgroundColor = UIColor.white
        self.color3.backgroundColor = UIColor.red
        self.color4.backgroundColor = UIColor.yellow
        self.color5.backgroundColor = UIColor.blue
        
        self.setSelected(button: self.color1)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        
        self.color1.layer.cornerRadius = self.color1.frame.size.width / 2
        self.color2.layer.cornerRadius = self.color2.frame.size.width / 2
        self.color3.layer.cornerRadius = self.color3.frame.size.width / 2
        self.color4.layer.cornerRadius = self.color4.frame.size.width / 2
        self.color5.layer.cornerRadius = self.color5.frame.size.width / 2
        
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.swiped = false
        if let touch = touches.first {
            self.lastPoint = touch.location(in: self.view)
        }
    }
    
    
    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
        
        // 1
        UIGraphicsBeginImageContext(view.frame.size)
        let context = UIGraphicsGetCurrentContext()
        imageView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        
        // 2
        context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        // 3
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(brushWidth)
        context?.setStrokeColor(red: red, green: green, blue: blue, alpha: 1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        // 4
        context?.strokePath()
        
        // 5
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        imageView.alpha = opacity
        UIGraphicsEndImageContext()
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            drawLineFrom(lastPoint, toPoint: currentPoint)
            
            // 7
            lastPoint = currentPoint
        }
    }
    
    
    fileprivate func addBorder(toView view: UIView) {
        
        self.color1.layer.borderWidth = 0
        self.color2.layer.borderWidth = 0
        self.color3.layer.borderWidth = 0
        self.color4.layer.borderWidth = 0
        self.color5.layer.borderWidth = 0
        
        
        if view == self.color2 {
            view.layer.borderColor = UIColor.black.cgColor
        } else {
            view.layer.borderColor = UIColor.white.cgColor
        }
        view.layer.borderWidth = 1
    }
    
    fileprivate func setSelected(button: UIButton) {
        
        self.addBorder(toView: button)
        if let color = button.backgroundColor {
            let components = color.components
            self.red = components.red
            self.green = components.green
            self.blue = components.blue
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        self.delegate?.editingCancelled()
    }
    
    @IBAction func savePressed(_ sender: AnyObject) {
        
        UIGraphicsBeginImageContext(self.imageView.bounds.size)
        UIColor.white.set()
        UIRectFill(CGRect(x: 0, y: 0, width: self.imageView.frame.size.width, height: self.imageView.frame.size.height))
        self.imageView.image?.draw(in: CGRect(x: 0, y: 0, width: self.imageView.frame.size.width, height: self.imageView.frame.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.dismiss(animated: false, completion: nil)
        self.delegate?.saveEditedImage(image: image)
        
    }


    @IBAction func colorSelected(_ sender: UIButton) {
        
        self.setSelected(button: sender)
    }
    
    @IBAction func clearPressed(_ sender: Any) {
        
        self.imageView.image = self.image
    }
}
