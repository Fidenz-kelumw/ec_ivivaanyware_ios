//
//  ViewAttachmentImageViewController.swift
//  Smart Office
//
//  Created by Lasith Hettiarachchi on 11/9/15.
//  Copyright © 2015 fidenz. All rights reserved.
//

import UIKit

class ViewAttachmentImageViewController: UIViewController {

    var imageView: UIImageView!
    var image: UIImage?
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewDidLayoutSubviews() {
        
        if let img = self.image {
            
            // 1
            let image = img
            imageView = UIImageView(image: image)
            imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size:image.size)
            scrollView.addSubview(imageView)
            
            // 2
            scrollView.contentSize = CGSize(width: image.size.width, height: image.size.height)
            
            // 3
            let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewAttachmentImageViewController.scrollViewDoubleTapped(_:)))
            doubleTapRecognizer.numberOfTapsRequired = 2
            doubleTapRecognizer.numberOfTouchesRequired = 1
            scrollView.addGestureRecognizer(doubleTapRecognizer)
            
            // 4
            let scrollViewFrame = scrollView.frame
            let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
            let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
            let minScale = min(scaleWidth, scaleHeight);
            scrollView.minimumZoomScale = minScale;
            
            // 5
            scrollView.maximumZoomScale = 2.0
            scrollView.zoomScale = minScale;
            
        }
        
        centerScrollViewContents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func centerScrollViewContents() {
        let boundsSize = scrollView.bounds.size
        var contentsFrame = imageView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        imageView.frame = contentsFrame
    }
    
    //MARK: - Events
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func scrollViewDoubleTapped(_ recognizer: UITapGestureRecognizer) {
        // 1
//        let pointInView = recognizer.locationInView(imageView)
//        
//        // 2
//        var newZoomScale = scrollView.zoomScale * 2.0
//        newZoomScale = min(newZoomScale, scrollView.maximumZoomScale)
//        print("Zoom Scale = \(newZoomScale)")
//        // 3
//        let scrollViewSize = scrollView.bounds.size
//        let w = scrollViewSize.width / newZoomScale
//        let h = scrollViewSize.height / newZoomScale
//        let x = pointInView.x - (w / 2.0)
//        let y = pointInView.y - (h / 2.0)
//        
//        print("x=\(x) y=\(y) w=\(w) h=\(h) ")
//        
//        let rectToZoomTo = CGRectMake(x, y, w, h);
//        
//        // 4
//        scrollView.zoomToRect(rectToZoomTo, animated: true)
    }
}


extension ViewAttachmentImageViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerScrollViewContents()
    }
}
