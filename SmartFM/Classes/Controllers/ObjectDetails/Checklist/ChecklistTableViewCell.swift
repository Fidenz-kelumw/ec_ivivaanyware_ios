//
//  ChecklistTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

@objc protocol ChecklistDelegate {
    
    @objc optional func showProcessing(_ completion:()->Void)
    @objc optional func hideProcessing()
    @objc optional func showSuccess(_ message:String)
    @objc optional func showError(_ message:String)
    @objc optional func statusChanged(_ status:String, forItemKey: String?)
    @objc optional func deadlineChanged(_ deadline: String, forItemKey: String?)
}

class ChecklistTableViewCell: UITableViewCell, YesNoDelegate, GenericFormDelegate {

    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var deadline: UILabel!
    @IBOutlet weak var btnCkeckBox: UIButton!
    @IBOutlet weak var btnDeadline: UIButton!
    
    var delegate: ChecklistDelegate?
    var delegateMain: ChecklistDelegate?
    
    fileprivate var item: CheckItem?
    fileprivate var objectType: String?
    fileprivate var objectKey: String?
    fileprivate var tabName: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func setValues(_ item: CheckItem, objectKey: String?, objectType: String?, tabName: String?) {
        
        self.item = item
        self.objectType = objectType
        self.objectKey = objectKey
        self.tabName = tabName
        
        if let name = item.ItemName {
            self.itemName.text = name
        } else {
            self.itemName.text = "N/A"
        }
        
        
        if let desc = item.Description {
            self.itemDescription.text = desc
        } else {
            self.itemDescription.text = "N/A"
        }
        
        
        if let deadline = item.Deadline {
            let dateManager = DateTimeManager()
            self.deadline.text = dateManager.getDateTimeFromISOString(deadline)
        } else {
            self.deadline.text = "N/A"
        }
        
        
        //Checkbox
        self.setCheckButton(item)
        
        
        
        //Deadline
        if item.CalendarEdit == "0" {
            
            self.btnDeadline.isEnabled = false
            self.btnDeadline.setImage(UIImage(named: "icon_deadline_notset"), for: UIControlState())
            self.btnDeadline.isHidden = false
            
        } else if item.CalendarEdit == "1" {
            
            self.btnDeadline.isEnabled = true
            self.btnDeadline.setImage(UIImage(named: "icon_deadline_set"), for: UIControlState())
            self.btnDeadline.isHidden = false
            
        } else if item.CalendarEdit == "2" {
            
            self.btnDeadline.isEnabled = false
            self.btnDeadline.isHidden = true
        } else {
            
            self.btnDeadline.isEnabled = false
            self.btnDeadline.setImage(UIImage(named: "icon_deadline_notset"), for: UIControlState())
            self.btnDeadline.isHidden = false
        }
    }
    
    //MARK: - Private Methods
    fileprivate func setCheckButton(_ item:CheckItem) {
        if item.StatusEdit == "1" {
            
            self.btnCkeckBox.isEnabled = true
            
            if item.Status == "1" {
                self.btnCkeckBox.setImage(UIImage(named: "checklist_checkbox_selected"), for: UIControlState())
            } else {
                self.btnCkeckBox.setImage(UIImage(named: "checklist_checkbox_unselected"), for: UIControlState())
            }
            
        } else {
            
            self.btnCkeckBox.isEnabled = false
            
            if item.Status == "1" {
                self.btnCkeckBox.setImage(UIImage(named: "checklist_checkbox_disabled_selected"), for: UIControlState())
            } else {
                self.btnCkeckBox.setImage(UIImage(named: "checklist_checkbox_disabled_unselected"), for: UIControlState())
            }
        }
    }

    
    //MARK: - Events
    @IBAction func btnDeadlnePressed(_ sender: AnyObject) {
        
        if let item = self.item , item.CalendarEdit == "1" {
            
            if item.Status == "1" {
                
                self.delegateMain?.showError?("The task is already complete")
            } else {
                
                let storyBoard = UIStoryboard(name: "Popups", bundle: Bundle.main)
                
                if let vc = storyBoard.instantiateViewController(withIdentifier: "DateAndTime") as? DateAndTimeSelectViewController {
                    
                    let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
                    formSheet.shouldCenterVertically = true
                    formSheet.shouldDismissOnBackgroundViewTap = true
                    formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                    formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                    
                    vc.delegate = self
                    formSheet.present(animated: true, completionHandler: nil)
                }
            }
        }
    }
    
    @IBAction func btnCkeckboxPressed(_ sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        
        if let vc = storyBoard.instantiateViewController(withIdentifier: "YesNoVc") as? YesNoDialogViewController {
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 150.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            if self.item?.Status == "1" {
                
                vc.setValues("Do you want to uncheck this item?")
                
            } else {
                
                vc.setValues("Do you want to set this item as completed?")
            }
            vc.delegate = self
            formSheet.present(animated: true, completionHandler: nil)
        }
        
    }
    
    @IBAction func btnInfoPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        let nvc = storyboard.instantiateViewController(withIdentifier: "TextViewPopup") as! UINavigationController
        
        if let vc = nvc.viewControllers.first as? TextViewPopupViewController {
            
            vc.editable = false
            vc.loadType = TextViewLoadType.ChecklistInfo
            vc.objectType = self.objectType
            vc.objectKey = self.objectKey
            vc.infoCode = self.item?.InfoCode
            vc.buttonText = "Dismiss"
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
            formSheet.shouldCenterVertically = false
            formSheet.shouldDismissOnBackgroundViewTap = false
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
        }
    }
    
    //MARK: - Delegate Methods
    func ActionChanged(_ action: Bool) {
        
        if action {
            
            if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName, let itemKey = self.item?.ItemKey {
                
                var status = "0"
                if self.item?.Status == "1" {
                    
                    status = "0"
                    
                } else {
                    
                    status = "1"
                }
                
                self.delegateMain?.showProcessing?({
                    
                    APIClient().setItemStatus(objectType, objectKey: objectKey, tabName: tabName, itemKey: itemKey, status: status, completion: { (success, message) in
                        
                        self.delegateMain?.hideProcessing?()
                        
                        if success {
                            
                            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Checklist.rawValue)'")
                            if let msg = message {
                                self.delegateMain?.showSuccess?(msg)
                            } else {
                                self.delegateMain?.showSuccess?("Successfully updated")
                            }
                            
                            self.delegate?.statusChanged?(status, forItemKey: self.item?.ItemKey)
                            
                        } else {
                            if let msg = message {
                                self.delegateMain?.showError?(msg)
                            } else {
                                self.delegateMain?.showError?("Failed to update")
                            }
                        }
                        
                    })
                })
            }
            
        }
    }
    
    func setSelectedItem(_ item: AnyObject) {
        
        if let selectedDate = item as? Date {
            
            let dateManager = DateTimeManager()
            let dateString = dateManager.dateToString(selectedDate, format: ISODateFormat)
            
            if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName, let itemKey = self.item?.ItemKey {
                
                self.delegateMain?.showProcessing?({
                    
                    APIClient().setItemDeadline(objectType, objectKey: objectKey, tabName: tabName, itemKey: itemKey, deadline: dateString, completion: { (success, message) in
                        
                        self.delegateMain?.hideProcessing?()
                        
                        if success {
                            
                            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Checklist.rawValue)'")
                            if let msg = message {
                                self.delegateMain?.showSuccess?(msg)
                            } else {
                                self.delegateMain?.showSuccess?("Successfully updated")
                            }
                            
                            self.delegate?.deadlineChanged?(dateString, forItemKey: self.item?.ItemKey)
                            
                        } else {
                            if let msg = message {
                                self.delegateMain?.showError?(msg)
                            } else {
                                self.delegateMain?.showError?("Failed to update")
                            }
                        }
                    })
                })
            }
            
            
            
            self.item?.Deadline = dateString
            self.delegate?.deadlineChanged?(dateString, forItemKey: self.item?.ItemKey)
            
        }
    }
}
