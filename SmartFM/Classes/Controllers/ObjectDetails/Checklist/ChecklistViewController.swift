//
//  ChecklistViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class ChecklistViewController: UIViewController, ChecklistDelegate, DetailsPopupDelegate {

    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    var checkCellMainDelegate: ChecklistDelegate?
    var forceSync:Bool?
    
    fileprivate var checkItems: [CheckItem] = []
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var isError = false
    fileprivate var errorMessage = "Failed to fetch data"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        if let sync = self.forceSync{
            if sync{
                self.showLoading()
                self.fetchDataWithoutTimer{
                    self.hideLoading()
                }
            } else{
                self.fetchData(nil)
            }
        }else{
            self.fetchData(nil)
        }
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(MembersViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableView.addSubview(refreshControl!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(_ sender: UIRefreshControl) {
        
        self.fetchDataWithoutTimer {
            self.refreshControl?.endRefreshing()
        }
    }
    
    
    func fetchData(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            self.showLoading()
            if SyncTimeManager().hasTimerExpired(EntityKey.Checklist) {
                
                SyncManager().syncChecklist(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                    
                    self.hideLoading()
                    if success {
                        if let checkItems = CheckItem.where2("1=1", sortBy: "ItemKey", accending: true) as? [CheckItem] {
                            
                            self.checkItems = checkItems
                            
                            self.isError = false
                            self.tableView.reloadData()
                            completion?()
                            
                        } else {
                            self.checkItems = []
                            self.isError = false
                            self.tableView.reloadData()
                            completion?()
                        }
                        
                    } else {
                        self.checkItems = []
                        self.isError = true
                        
                        if let er = error {
                            self.errorMessage = er
                        }
                        
                        self.tableView.reloadData()
                    }
                }
                
            } else {
                
                self.hideLoading()
                if let checkItems = CheckItem.where2("1=1", sortBy: "ItemKey", accending: true) as? [CheckItem] {
                    
                    self.checkItems = checkItems
                    
                    self.isError = false
                    self.tableView.reloadData()
                    completion?()
                    
                } else {
                    self.checkItems = []
                    self.isError = true
                    self.tableView.reloadData()
                    completion?()
                }
            }
            
            
        }
    }
    
    fileprivate func fetchDataWithoutTimer(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            SyncManager().syncChecklist(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                
                self.hideLoading()
                if success {
                    if let checkItems = CheckItem.all() as? [CheckItem] {
                        
                        self.checkItems = checkItems
                        
                        self.isError = false
                        self.tableView.reloadData()
                        completion?()
                        
                    } else {
                        
                        self.checkItems = []
                        self.isError = false
                        self.tableView.reloadData()
                        completion?()
                    }
                } else {
                    
                    self.checkItems = []
                    self.isError = true
                    
                    if let er = error {
                        self.errorMessage = er
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    fileprivate func showLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    //MARK: - Delegate Methods
    func statusChanged(_ status: String, forItemKey: String?) {
        
        if let item = self.checkItems.filter({$0.ItemKey == forItemKey}).first {
            item.Status = status
            item.update()
            self.tableView.reloadData()
        }
    }
    
    func deadlineChanged(_ deadline: String, forItemKey: String?) {
        
        if let item = self.checkItems.filter({$0.ItemKey == forItemKey}).first {
            item.Deadline = deadline
            item.update()
            self.tableView.reloadData()
        }
    }
    
    func updatePressed(_ text: String, forItem item: CheckItem) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tb=self.tabName, let key = item.ItemKey {
            
            self.checkCellMainDelegate?.showProcessing?({
                
                APIClient().setItemDescription(ot, objectKey: ok, tabName: tb, itemKey: key, description: text, completion: { (success, message) in
                    
                    self.checkCellMainDelegate?.hideProcessing?()
                    
                    if success{
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Checklist.rawValue)'")
                        if let msg = message {
                            self.checkCellMainDelegate?.showSuccess?(msg)
                        } else {
                            self.checkCellMainDelegate?.showSuccess?("Successfully updated")
                        }
                        
                        self.fetchDataWithoutTimer(nil)
                        
                    } else {
                        if let msg = message {
                            self.checkCellMainDelegate?.showError?(msg)
                        } else {
                            self.checkCellMainDelegate?.showError?("Failed to update")
                        }
                    }
                })
            })
        }
    }
}

extension ChecklistViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.checkItems.count == 0 {
            return 1
        }
        return self.checkItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.checkItems.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues("\(self.errorMessage)", bgColor: ThemeBackgroundColor)
            } else {
                cell.setValues("No items found", bgColor: ThemeBackgroundColor)
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckItemCell") as! ChecklistTableViewCell
        cell.delegate = self
        cell.delegateMain = self.checkCellMainDelegate
        cell.setValues(self.checkItems[(indexPath as NSIndexPath).row], objectKey: self.objectKey, objectType: self.objectType, tabName: self.tabName)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.checkItems[(indexPath as NSIndexPath).row]
        let storyBoard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        
        if let isDescriptionEdit = item.DescriptionEdit, isDescriptionEdit == "1" {
            if let vc = storyBoard.instantiateViewController(withIdentifier: "DetailsPopup") as? DetailsPopupViewController {
                
                vc.delegate = self
                vc.item = item
                let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
                formSheet.shouldCenterVertically = false
                formSheet.shouldDismissOnBackgroundViewTap = true
                formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 200.0)
                formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                
                formSheet.present(animated: true, completionHandler: nil)
            }
        }
    }
}
