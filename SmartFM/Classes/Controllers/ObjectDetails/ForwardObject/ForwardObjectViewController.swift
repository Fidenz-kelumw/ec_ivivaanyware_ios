//
//  ForwardObjectViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class ForwardObjectViewController: PopupParentViewController, GenericFormDelegate {
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var btnSelectMember: UIButton!
    
    var objectKey: String?
    var objectType: String?
    
    fileprivate var alertSuccess: LIHAlert?
    fileprivate var alertProcessing: LIHAlert?
    fileprivate var alertError: LIHAlert?
    
    fileprivate var selectedMember: LData? {
        didSet {
            if let member = self.selectedMember {
                
                if let displayText = member.DisplayText , displayText != "" {
                    
                    self.btnSelectMember.setTitle(displayText, for: UIControlState())
                    
                } else if let val = member.Value , val != "" {
                    
                    self.btnSelectMember.setTitle(val, for: UIControlState())
                }  else {
                    self.selectedMember = nil
                }
                
            } else {
                
                self.btnSelectMember.setTitle("Select", for: UIControlState())
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Forward"
        self.initAlerts()
        
        Configurations().configSelectionButton(forButton: self.btnSelectMember, withDisclosure: true)
        IHKeyboardAvoiding.setAvoiding(self.view)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private methods
    
    fileprivate func showAlert(_ alert: LIHAlert?, message: String, completion:(()->Void)?) {
        alert?.contentText = message
        alert?.show(nil) {
            completion?()
        }
    }
    
    fileprivate func initAlerts() {
        
        self.alertError = LIHAlertManager.getErrorAlert("Error occured. Please try again")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.alertHeight = 50
        self.alertError?.alertColor = UIColor.colorWithRGB(red: 200, green: 57, blue: 58, alpha: 1)
        self.alertError?.initAlert(self.view)
        
        self.alertSuccess = LIHAlertManager.getSuccessAlert("Successfully sent")
        AlertManager().configSuccessAlert(self.alertSuccess, hasNavBar: true)
        self.alertSuccess?.alertHeight = 50
        self.alertSuccess?.initAlert(self.view)
        
        self.alertProcessing = LIHAlertManager.getProcessingAlert("Please wait..")
        AlertManager().configProcessing(self.alertProcessing, hasNavBar: true)
        self.alertProcessing?.alertHeight = 50
        self.alertProcessing?.initAlert(self.view)
    }
    
    //MARK: - Events
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
    }
    
    @IBAction func selectMemberPressed(_ sender: AnyObject) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAndSelect") as? SearchAndSelectViewController {
            vc.hasHeader = false
            vc.objectType = self.objectType
            vc.hasQRScanner = false
            vc.selectedAction = SelectedAction.back
            vc.delegate = self
            vc.lookupService = "GetUsers"
            vc.title = "Select Member"
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func sendPressed(_ sender: AnyObject) {
        
        if let objKey = self.objectKey, let objType = self.objectType {
            
            if let memberkey = self.selectedMember?.Value {
                
                self.btnSelectMember.isUserInteractionEnabled = false
                
                self.alertProcessing?.show({
                    APIClient().forwardObject(objType, objectKey: objKey, memberKey: memberkey, message: self.txtMessage.text, completion: { (success, messageText) in
                        self.alertProcessing?.hideAlert(nil)
                        if success {
                            self.showAlert(self.alertSuccess, message: messageText, completion: {
                                self.mz_dismissFormSheetController(animated: true, completionHandler: nil)
                                
                                self.btnSelectMember.isUserInteractionEnabled = true
                            })
                        } else {
                            self.showAlert(self.alertError, message: messageText, completion: {
                                
                                
                                self.btnSelectMember.isUserInteractionEnabled = true
                            })
                        }
                    })
                    
                    }, hidden: nil)
                
            } else {
                
                self.showAlert(self.alertError, message: "Select a member to send", completion: nil)
            }
        }
    }
    //MARK: - Delegate Methods
    func setSelectedItem(_ item: AnyObject) {
        
        if let selected = item as? LData {
            
            self.selectedMember = selected
        }
    }
}
