//
//  ObjectDetailsInfoViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ObjectDetailsInfoDelegate {
    func setObjectDetailsDelegate(_ objectDetailsDelegate: ObjectDetailsDelegate?)
}

class ObjectDetailsInfoViewController: UIViewController {
    
    var objectKey: String?
    var objectType: String?
    
    var formDelegate: GenericFormDelegate?
    var navigatorDelegate: PageLinkNavigatorDelegate?
    var actionCellDelegate: ActionCellDelegate?
    
    var delegate: ObjectDetailsInfoDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FormSegue" {
            
            if let vc = segue.destination as? GenericFormViewController {
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.delegate = self.formDelegate
                vc.navigatorDelegate = self.navigatorDelegate
                vc.actionCellDelegate = self.actionCellDelegate
                self.delegate?.setObjectDetailsDelegate(vc.self)
            }
        }
    }
    
}
