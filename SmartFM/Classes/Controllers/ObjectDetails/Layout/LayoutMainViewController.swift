//
//  LayoutMainViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class LayoutMainViewController: UIViewController, UpdatePinDelegate {

    @IBOutlet weak var btnUpdateDroppedPin: UIButton!
    @IBOutlet weak var viewNotice: UIView!
    
    var objectKey: String?
    var objectType: String?
    var tabName: String?
    var delegate: LayoutDelegate?
    
    fileprivate var pinX: String? {
        didSet {
            self.configUpdatePinButton()
        }
    }
    fileprivate var pinY: String? {
        didSet {
            self.configUpdatePinButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configUpdatePinButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func configUpdatePinButton() {
        
        if self.pinY != nil && self.pinX != nil {
            self.enableButton()
        } else {
            self.disableButton()
        }
    }
    
    fileprivate func disableButton() {
        
        self.btnUpdateDroppedPin.isEnabled = false
        self.btnUpdateDroppedPin.backgroundColor = DisabledButtonColor
    }
    
    fileprivate func enableButton() {
        
        self.btnUpdateDroppedPin.isEnabled = true
        self.btnUpdateDroppedPin.backgroundColor = ThemeRedColor
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "LayoutInnerSegue" {
            
            if let vc = segue.destination as? LayoutViewController {
                
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.tabName
                vc.delegate = self.delegate
                vc.updatePinDelegate = self
            }
        }
    }
    
    
    @IBAction func updatePressed(_ sender: AnyObject) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tabName=self.tabName, let x = self.pinX, let y = self.pinY {
            
            APIClient().updateDroppedPin(ot, objectKey: ok, tabName: tabName, X: x, Y: y, completion: { (success, message) in
                
                if success {
                    SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Layout.rawValue)'")
                    self.delegate?.showSuccess(message)
                } else {
                    self.delegate?.showError(message)
                }
                self.pinX = nil
                self.pinY = nil
            })
        } else {
            self.delegate?.showError("Error occured. Please try again")
        }
    }
    
    //delegates
    func droppedPinUpdated(_ X: String, Y: String) {
        
        self.pinX = X
        self.pinY = Y
    }
    
    func configNotice(_ show: Bool) {
        
        self.viewNotice.isHidden = !show
    }
}
