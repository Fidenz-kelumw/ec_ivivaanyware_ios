//
//  LayoutViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MapKit

protocol LayoutDelegate {
    
    func showObjectDetailsPage(_ objectType: String, objectKey: String)
    func showSuccess(_ message: String)
    func showError(_ message: String)
}

protocol UpdatePinDelegate {
    
    func droppedPinUpdated(_ X: String, Y: String)
    
    func configNotice(_ show: Bool)
}

class LayoutViewController: UIViewController,MapLinearDelegate, UIGestureRecognizerDelegate, LayoutPinDetailsDelegate {

    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    
    fileprivate static let DROPPED_PIN_ID = "droppedPin"
    
    var objectKey: String?
    var objectType: String?
    var tabName: String?
    var delegate: LayoutDelegate?
    var updatePinDelegate: UpdatePinDelegate?
    
    fileprivate var layout: Layout?
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var scrollView: ImageScrollView!
    fileprivate var alertSuccess: LIHAlert?
    fileprivate var alertError: LIHAlert?
    
    
    ///////////////////////////////////////////
    //MARK: - View Controller Methods
    ///////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initAlerts()
        self.fetchData { 
            
            let userManager = UserManager()
            if let acc = userManager.getAccount(), let layoutPath = self.layout?.Path, let zoomLevelsString = self.layout?.ZoomLevels, let zoomLevels = Int(zoomLevelsString) {
                
                self.setUpMap(acc, layoutPath: layoutPath, zoomLevels: zoomLevels, completion: {
                    
                    self.addMarkers()
                    self.addDroppedPin()
                    self.configPinNotice()
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    
    fileprivate func initAlerts() {
        
        self.alertError = LIHAlertManager.getErrorAlert("")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.paddingTop = 0
        self.alertError?.initAlert(self.view)
        
        self.alertSuccess = LIHAlertManager.getSuccessAlert("")
        AlertManager().configSuccessAlert(self.alertSuccess, hasNavBar: true)
        self.alertError?.paddingTop = 0
        self.alertSuccess?.initAlert(self.view)
        
        
    }
    
    fileprivate func configPinNotice() {
        
        if self.layout?.droppedPin?.Editable == "1" {
            
            self.updatePinDelegate?.configNotice(true)
        } else {
            
            self.updatePinDelegate?.configNotice(false)
            
        }
    }
    
    fileprivate func showLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    fileprivate func fetchData(_ completion: (()->Void)?) {
        
        self.showLoading()
        if SyncTimeManager().hasTimerExpired(EntityKey.Layout) {
            
            self.fetchDataWithoutTimer({
                completion?()
            })
            
        } else {
            
            self.hideLoading()
            if let layout = Layout.all().first as? Layout {
                
                self.layout = layout
                self.extractLayout()
                
            }
            completion?()
        }
        
    }
    
    fileprivate func fetchDataWithoutTimer(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            SyncManager().syncLayout(objectType, objectKey: objectKey, tabName: tabName) { (success) in
                
                self.hideLoading()
                if success {
                    if let layout = Layout.all().first as? Layout {
                        
                        self.layout = layout
                        
                        self.extractLayout()
                        
                        completion?()
                        
                    } else {
                        completion?()
                    }
                } else {
                    completion?()
                }
            }
        }
    }

    fileprivate func extractLayout() {
        
        if let layoutId = self.layout?.Id {
            self.layout?.droppedPin = DroppedPin.__where("layoutId=\(layoutId)", sortBy: "Id", accending: true).first as? DroppedPin
            self.layout?.pins = LayoutPin.__where("layoutId=\(layoutId)", sortBy: "Id", accending: true) as? [LayoutPin]
            self.layout?.pinLabel = PinLabel.__where("layoutId=\(layoutId)", sortBy: "Id", accending: true).first as? PinLabel
        }
    }
    
    fileprivate func setUpMap(_ account: String,layoutPath: String, zoomLevels: Int, completion: @escaping ()->()){
        
        self.scrollView = ImageScrollView()
        
        self.scrollView.delegatePin = self
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            //self.scrollView.markers = nil
            let layoutPath = layoutPath
            let zoomLevels = zoomLevels - 1
            
            ImageScrollView.setMapURL("\(Protocol)://\(account + layoutPath)")
            ImageScrollView.setScale(Float(zoomLevels))
            
            self.scrollView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            self.scrollView.index = 0
            self.scrollView.tag = 5
            self.scrollView.northAngle = 0
            
            if self.layout?.droppedPin?.Editable == "1" {
                let singleTap = UITapGestureRecognizer(target: self, action: #selector(LayoutViewController.scrollViewTapped(_:)))
                singleTap.cancelsTouchesInView = false
                singleTap.numberOfTapsRequired = 1
                singleTap.delegate = self
                self.scrollView.addGestureRecognizer(singleTap)
            }
            
            DispatchQueue.main.async {
                
                self.view = self.scrollView
                completion()
            }
        }
        
        
    }
    
    fileprivate func addMarkers() {
        
        if let pins = self.layout?.pins {
            
            for pin in pins {
                
                if let xString = pin.X, let yString = pin.Y, let x = Float(xString), let y = Float(yString), let id = pin.Id, let color = pin.Color {
                    
                    var imageName = "pin_red"
                    if color == "Red" {
                        imageName = "pin_red"
                    } else if color == "Blue" {
                        imageName = "pin_blue"
                    } else if color == "Brown" {
                        imageName = "pin_brown"
                    } else if color == "Gray" {
                        imageName = "pin_gray"
                    } else if color == "Green" {
                        imageName = "pin_green"
                    } else if color == "Orange" {
                        imageName = "pin_orange"
                    } else if color == "Purple" {
                        imageName = "pin_purple"
                    } else if color == "Yellow" {
                        imageName = "pin_yellow"
                    } 
                    
                    self.setMarker(x, latitude: y, image: UIImage(named: imageName)!, id: "\(id)")
                }
                
            }
        }
    }
    
    fileprivate func addDroppedPin() {
        
        if let pin = self.layout?.droppedPin {
            
            if let xString = pin.X, let yString = pin.Y, let x = Float(xString), let y = Float(yString) {
                self.setMarker(x, latitude: y, image: UIImage(named: "droppedPin")!, id: "\(LayoutViewController.DROPPED_PIN_ID)")
            }
        }
    }
    
    fileprivate func setMarker(_ longitude: Float, latitude:Float, image:UIImage, id: String){
        let point: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        let ptr: MKMapPoint = MKMapPointForCoordinate(point)
        let x: Float = Float(ptr.x/(4096*16))
        let y: Float = Float(ptr.y/(4096*16))
        
        self.scrollView.setMarkeX(x, setY: y, image: image, forId: id)
    }
    
    fileprivate func zoomToPoint(_ x: CGFloat, y:CGFloat) {
        
        let rectWidth: CGFloat = 600.0
        let rectHeight: CGFloat = 600.0
        
        let zoomPoint = self.getRealCoordinates(CGPoint(x: x, y: y))
        
        var rect = CGRect.null
        if 4096.0 >= zoomPoint.x+(rectWidth/2) && 4096.0 >= zoomPoint.y-(rectHeight/2) {
            
            rect = CGRect(x: zoomPoint.x-(rectWidth/2), y: zoomPoint.y-(rectHeight/2), width: rectWidth, height: rectHeight)
        } else {
            
            if 4096.0 <= zoomPoint.x+(rectWidth/2) && 4096.0 >= zoomPoint.y-(rectHeight/2) {
                
                rect = CGRect(x: 4096.0-(rectWidth/2), y: zoomPoint.y-(rectHeight/2), width: rectWidth, height: rectHeight)
                
            } else if 4096.0 >= zoomPoint.x+(rectWidth/2) && 4096.0 <= zoomPoint.y-(rectHeight/2) {
                
                rect = CGRect(x: zoomPoint.x-(rectWidth/2), y: 4096.0-(rectHeight/2), width: rectWidth, height: rectHeight)
                
            } else if 4096.0 <= zoomPoint.x+(rectWidth/2) && 4096.0 <= zoomPoint.y-(rectHeight/2) {
                
                rect = CGRect(x: 4096.0-(rectWidth/2), y: 4096.0-(rectHeight/2), width: rectWidth, height: rectHeight)
            }
        }
        
        self.scrollView.zoom(to: rect, animated: true)
    }
    
    fileprivate func getRealCoordinates(_ point: CGPoint) -> CGPoint {
        
//        let ctrpoint: CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(point.y), CLLocationDegrees(point.x));
//        let ptr: MKMapPoint = MKMapPointForCoordinate(ctrpoint);
        let x: Float = Float(point.x/(4096*16));
        let y: Float = Float(point.y/(4096*16));
        
        return CGPoint(x: CGFloat(x), y: CGFloat(y))
    }
    
    fileprivate func viewPointToMapPoint(viewPoint point: CGPoint) -> CGPoint {
        
        let ptrX = point.x*4096*16
        let ptrY = point.y*4096*16
        let mapPoint = MKMapPoint(x: Double(ptrX), y: Double(ptrY))
        let loc = MKCoordinateForMapPoint(mapPoint)
        
        return CGPoint(x: CGFloat(loc.longitude), y: CGFloat(loc.latitude))
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Gesture
    ///////////////////////////////////////////
    func scrollViewTapped(_ sender: UITapGestureRecognizer) {
        
        let point = sender.location(in: self.scrollView)
        
        let newPoint = CGPoint(x: point.x/self.scrollView.zoomScale , y: point.y/self.scrollView.zoomScale)
        
        let mappoint = self.viewPointToMapPoint(viewPoint: newPoint)
        self.scrollView.moveMarker(Float(newPoint.x), setY: Float(newPoint.y), image: UIImage(named: "droppedPin")!, forId: "\(LayoutViewController.DROPPED_PIN_ID)")
        
        self.updatePinDelegate?.droppedPinUpdated("\(mappoint.x)", Y: "\(mappoint.y)")
        
//        if let ot = self.objectType, ok = self.objectKey, tabName = self.tabName {
//            APIClient().updateDroppedPin(ot, objectKey: ok, tabName: tabName, X: "\(mappoint.x)", Y: "\(mappoint.y)", completion: { (success, message) in
//                
//                if success {
//                    self.delegate?.showSuccess(message)
//                } else {
//                    self.delegate?.showError(message)
//                }
//            })
//        } else {
//             self.delegate?.showError("Error occured. Please try again")
//        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if self.layout?.droppedPin?.Editable == "1" {
            if let view = touch.view, let markers = self.scrollView.markers as NSArray as? [MapMarker] {
                
                for marker in markers {
                    if view.isDescendant(of: marker.viewImage) {
                        return false
                    }
                }
                
            }
        }
        
        return true
    }
    
    ///////////////////////////////////////////
    //MARK: - Delegate Methods
    ///////////////////////////////////////////
    func pinPressed(_ pinId: String!) {
        
        if pinId != LayoutViewController.DROPPED_PIN_ID, pinId != nil {
            
            if let pin = LayoutPin.__where("Id=\(pinId!)", sortBy: "Id", accending: true).first as? LayoutPin {
                
                let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
                if let vc = storyboard.instantiateViewController(withIdentifier: "PinDetails") as? LayoutPinDetailsViewController {
                    
                    vc.delegate = self
                    vc.objectType = pin.ObjectType
                    vc.objectKey = pin.ObjectKey
                    vc.pin = pin
                    vc.pinLabel = self.layout?.pinLabel
                    
                    let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
                    formSheet.shouldCenterVertically = true
                    formSheet.shouldDismissOnBackgroundViewTap = true
                    formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: pin.ObjectKey != nil && pin.ObjectType != nil ? 230.0 : 190.0)
                    formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                    
                    
                    formSheet.present(animated: true, completionHandler: nil)
                }
            }
            
            
        }
    }
    
    func showObjectDetailsPage(_ objectType: String, objectKey: String) {
        
        self.delegate?.showObjectDetailsPage(objectType, objectKey: objectKey)
    }
}
