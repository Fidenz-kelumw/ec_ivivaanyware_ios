//
//  MemberTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MemberTableViewCell: UITableViewCell {

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var lblMemberSubtitle: UILabel!
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var btnCall: UIButton!
    
    private var member: Member?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues(_ member: Member) {
        
        self.member = member
        
        self.viewOnline.isHidden = true
        
        self.imgMember.image = UIImage(named: "icon_member_placeHolder")
        
        if let name = member.User?.UserName {
            self.lblMemberName.text = name
        } else {
            self.lblMemberName.text = "N/A"
        }
        
        if let role = member.RoleID {
            self.lblMemberSubtitle.text = role
        } else {
            self.lblMemberSubtitle.text = "N/A"
        }
        
        if let path = member.User?.ImagePath, let usr = member.User {
            if let acc = UserManager().getAccount() {
                
                ImageHandler().asyncImageLoadWithCache("\(Protocol)://\(acc)\(path)", key: usr.UserKey != nil ? usr.UserKey! : "aa") { (image) in
                    
                    if let img = image {
                        self.imgMember.image = img
                    } else {
                        self.imgMember.image = UIImage(named: "icon_member_placeHolder")
                    }
                }
            }
        }
        
        self.indicatorView.backgroundColor = UIColor.white
        if let oType = member.ObjectType, let roleId = member.RoleID {
            if let roles = Role.__where("ObjectType='\(oType)' and RoleID='\(roleId)'", sortBy: "Id", accending: true) as? [Role] {
                
                if let role = roles.first, let color = role.Color {
                    self.indicatorView.backgroundColor = UIColor.colorWithHexString(String(color.characters.dropFirst()) as NSString)
                }
            }
        }
        
        if let number = member.User?.Phone, number != "" {
            self.btnCall.isHidden = false
        } else {
            self.btnCall.isHidden = true
        }
    }
    
    @IBAction func callPressed(_ sender: Any) {
        
        if let number = self.member?.User?.Phone, number != "" {
            
            if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
                
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
