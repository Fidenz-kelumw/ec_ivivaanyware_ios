//
//  MembersViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MembersViewController: UIViewController {

    
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var objectKey: String?
    var objectType: String?
    var tabName: String?
    var forceSync:Bool?
    
    
    fileprivate var members: [Member] = []
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var isError = false
    fileprivate var errorMessage:String = "Failed to fetch data"
    
    ///////////////////////////////////////////
    //MARK: - View Controller Methods
    ///////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        if let sync = self.forceSync{
            if sync{
                self.showLoading()
                self.fetchDataWithoutTimer{
                    self.hideLoading()
                }
            } else{
                self.fetchData(nil)
            }
        }else{
            self.fetchData(nil)
        }
        
        
        self.tableView.dataSource = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(MembersViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        self.tableView.addSubview(refreshControl!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        
    }
    
    func refresh(_ sender: UIRefreshControl) {
        
        self.fetchDataWithoutTimer { 
            self.refreshControl?.endRefreshing()
        }
    }
    
    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    
    fileprivate func fetchData(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            
            self.showLoading()
            if SyncTimeManager().hasTimerExpired(EntityKey.Members) {
                
                SyncManager().syncMembers(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                    
                    self.hideLoading()
                    if success {
                        if let members = Member.all() as? [Member] {
                            
                            self.members = members
                            self.extractMembers()
                            
                            self.isError = false
                            self.tableView.reloadData()
                            completion?()
                            
                        } else {
                            
                            self.members = []
                            self.isError = false
                            self.tableView.reloadData()
                            completion?()
                        }
                    } else {
                        
                        self.members = []
                        self.isError = true
                        
                        if let er = error {
                            self.errorMessage = er
                        }
                        
                        self.tableView.reloadData()
                        completion?()
                    }
                }
                
            } else {
                
                self.hideLoading()
                if let members = Member.all() as? [Member] {
                    
                    self.members = members
                    self.extractMembers()
                    
                    self.isError = false
                    self.tableView.reloadData()
                    
                } else {
                    self.members = []
                    self.isError = true
                    
                    self.errorMessage = "Failed to fetch data"
                    
                    self.tableView.reloadData()
                }
                completion?()
            }
            
            
        }
        
    }
    
    fileprivate func fetchDataWithoutTimer(_ completion: (()->Void)?) {
        
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            
            SyncManager().syncMembers(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                
                self.hideLoading()
                if success {
                    if let members = Member.all() as? [Member] {
                        
                        self.members = members
                        self.extractMembers()
                        
                        self.isError = false
                        self.tableView.reloadData()
                        completion?()
                        
                    } else {
                        self.members = []
                        self.isError = false
                        self.tableView.reloadData()
                        completion?()
                    }
                } else {
                    self.members = []
                    self.isError = true
                    
                    if let er = error {
                        self.errorMessage = er
                    }
                    
                    self.tableView.reloadData()
                    completion?()
                }
            }
        }
    }
    
    fileprivate func showLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    
    fileprivate func extractMembers() {
        
        for member in self.members {
            
            if let memberId = member.Id {
                
                if let memberUsers = MemberUser.__where("memberId=\(memberId)", sortBy: "Id", accending: true) as? [MemberUser], let memberUser = memberUsers.first {
                    
                    member.User = memberUser
                }
                
            }
        }
    }

}


extension MembersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.members.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues(self.errorMessage, bgColor: ThemeBackgroundColor)
            } else {
                cell.setValues("No items found", bgColor: ThemeBackgroundColor)
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as! MemberTableViewCell
        
        cell.setValues(self.members[(indexPath as NSIndexPath).row])
        
        return cell
    }
}
