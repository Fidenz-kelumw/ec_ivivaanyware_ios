
//
//  MessageTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tempIndicator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setValues(_ message: Message) {
        
        if let usr = message.CreatedUser?.UserName {
            self.lblName.text = usr
        } else {
            self.lblName.text = "N/A"
        }
        
        if let msg = message.MessageText {
            self.lblMessage.text = msg
        } else {
            self.lblMessage.text = "N/A"
        }
        
        if message.isTemp {
            self.tempIndicator.isHidden = false
        } else {
            self.tempIndicator.isHidden = true
        }
        
        if let createdAt = message.CreatedAt, let date = DateTimeManager().stringToDate(createdAt, format: ISODateFormat) {
            
            self.lblTime.text = (date as NSDate).timeAgoSinceNow()
        } else {
            self.lblTime.text = "N/A"
        }
        
    }
}
