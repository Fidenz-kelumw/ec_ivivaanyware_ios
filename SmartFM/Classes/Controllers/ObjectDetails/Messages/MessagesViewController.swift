//
//  MessagesViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, AdvanceMessageDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var textBoxContainer: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var brnText: UIButton!
    @IBOutlet weak var txtContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var objectKey: String?
    var objectType: String?
    var tabName: String?
    var delegate: GenericFormDelegate?
    var forceSync:Bool?
    
    fileprivate var messageMain: MessageMain?
    
    fileprivate var messages:[Message] = []
//    private var messagesOrg:[Message] = []
    fileprivate var refreshControl: UIRefreshControl?
    //private var pullUpRefreshControl: UIRefreshControl = UIRefreshControl()
    fileprivate var kbHeight: CGFloat! = 0.0
    fileprivate var initialTablePosition: CGFloat = 0.0
    fileprivate var isSearchBarEditing: Bool = false
    fileprivate var alertError: LIHAlert?
    fileprivate var isKeyboardVisibale: Bool = false
    fileprivate var isError = false
    fileprivate var defaultRowHeight: CGFloat = 70.0
    fileprivate var selectedMessageType: MessageType?
    fileprivate var errorMessage = "Failed to fetch data"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initAlerts()
        
        if let sync = self.forceSync{
            if sync{
                self.showLoading()
                self.fetchDataWithoutTimer{
                    self.hideLoading()
                }
            } else{
                self.fetchData(nil)
            }
        }else{
            self.fetchData(nil)
        }
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(MessagesViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl!)
        
//        self.pullUpRefreshControl.triggerVerticalOffset = 50
//        self.pullUpRefreshControl.addTarget(self, action: #selector(MessagesViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
//        self.tableView.bottomRefreshControl = pullUpRefreshControl
//        self.pullUpRefreshControl.tag = 10
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let paddingView = UIView(frame: CGRect(x: 0,y: 0,width: 5,height: 40))
        self.txtMessage.leftView = paddingView
        self.txtMessage.leftViewMode = UITextFieldViewMode.always
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.initialTablePosition = self.tableView.frame.origin.y
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(_ sender: UIRefreshControl) {
        
        self.fetchDataWithoutTimer {
            self.refreshControl?.endRefreshing()
//            self.pullUpRefreshControl.endRefreshing()
            self.searchBar.text = nil
            if sender.tag == 10 {
                //self.scrollToBottom()
            }
        }
    }
    
    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    
    fileprivate func fetchData(_ completion: (()->Void)?) {
        
        self.showLoading()
        if SyncTimeManager().hasTimerExpired(EntityKey.Messages) {
            
            self.fetchDataWithoutTimer({
                completion?()
            })
            
        } else {
            
            self.hideLoading()
            if let messageMain = MessageMain.all().first as? MessageMain {
                
                self.messageMain = messageMain
                self.extractMessages()
                self.messages = messageMain.Messages
                self.selectedMessageType = messageMain.MessageTypes.first
                self.tableView.reloadData()
                self.scrollToBottom()
                self.configMessageButton()
                self.isError = false
                completion?()
                
            } else {
                self.messageMain = nil
                self.messages = []
                self.isError = false
                self.tableView.reloadData()
                completion?()
            }
        }
        
    }
    
    fileprivate func fetchDataWithoutTimer(_ completion:(()->Void)?) {
        if let objectType = self.objectType, let objectKey = self.objectKey, let tabName = self.tabName {
            SyncManager().syncMessages(objectType, objectKey: objectKey, tabName: tabName) { (success, error) in
                
                self.hideLoading()
                if success {
                    if let messageMain = MessageMain.all().first as? MessageMain {
                        
                        self.messageMain = messageMain
                        self.extractMessages()
                        self.messages = messageMain.Messages
                        self.selectedMessageType = messageMain.MessageTypes.first
                        self.tableView.reloadData()
                        self.scrollToBottom()
                        self.configMessageButton()
                        self.isError = false
                        completion?()
                        
                    } else {
                        self.messageMain = nil
                        self.messages = []
                        self.isError = false
                        self.tableView.reloadData()
                        completion?()
                    }
                } else {
                    self.messageMain = nil
                    self.messages = []
                    self.isError = true
                    
                    self.errorMessage = "Failed to fetch data"
                    if let er = error {
                        self.errorMessage = er
                    }
                    
                    self.tableView.reloadData()
                    completion?()
                }
            }
        }
    }
    
    fileprivate func configMessageButton() {
// this is removed due to new requriment, include in bug list. task 24
//        if let btnText = self.messageMain?.NewMessageAction {
//            self.brnText.setTitle(btnText, for: UIControlState())
//        }
        
        if self.messageMain?.AddNewMessage == "1" {
            self.setSendButtonStatus(enabled: true)
        } else {
            self.setSendButtonStatus(enabled: false)
        }
        
    }
    
    fileprivate func showLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.lblErrorMessage.isHidden = true
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
        
    }
    
    fileprivate func extractMessages() {
        
        if let messageMain = self.messageMain, let id = self.messageMain?.Id {
            
            if let messages = Message.__where("MessagesMainId=\(id)", sortBy: "Id", accending: true) as? [Message] {
                
                for message in messages {
                    
                    if let messageId = message.Id {
                        
                        if let messageUsers = MessageUser.__where("MessageId=\(messageId)", sortBy: "Id", accending: true) as? [MessageUser], let messageUser = messageUsers.first {
                            
                            message.CreatedUser = messageUser
                        }
                    }
                }
                
                messageMain.Messages = messages
                
            }
            
            
            if let messagetypes = MessageType.__where("MessagesMainId=\(id)", sortBy: "Id", accending: true) as? [MessageType] {
                
                messageMain.MessageTypes = messagetypes
            }
        }
        
        
    }
    
    fileprivate func scrollToBottom() {
        
        if self.messages.count > 0 {
            let indexPath = IndexPath(row: self.messages.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
        }
    }
    
    fileprivate func initAlerts() {
        
        self.alertError = LIHAlertManager.getErrorAlert("Failed to add message")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.initAlert(self.view)
    }
    
    fileprivate func setSendButtonStatus(enabled: Bool) {
        
        self.brnText.isEnabled = enabled
        if enabled {
            self.brnText.backgroundColor = ThemeRedColor
        } else {
            self.brnText.backgroundColor = DisabledButtonColor
        }
        
        self.txtMessage.backgroundColor = enabled ? textFieldEnabledColor : textFieldDisabledColor
        self.txtMessage.isEnabled = enabled
    }
    
    fileprivate func refineDate(_ dateString: String) -> String {
        
        if dateString.characters.last != "Z" {
            
            return dateString+"Z"
        }
        
        return dateString
    }
    
    fileprivate func heightForView(_ text:String) -> CGFloat {
        let width = UIScreen.main.bounds.width - 145.0
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: "Melbourne", size: 15.0)
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    fileprivate func addNewMessage(message: String, mTypeId: String) {
        
        if let ot = self.objectType, let ok = self.objectKey, let tab = self.tabName, message != "" {
            
            self.searchBar.text = nil
            self.txtMessage.text = ""
            let messageObj = Message()
            messageObj.MessageKey = "TempMessage"
            messageObj.MessageText = message
            messageObj.isTemp = true
            messageObj.CreatedAt = "\(DateTimeManager().dateToGMTString(Date(), format: ISODateFormat))Z"
            
            let userManager = UserManager()
            if let userName = userManager.getUserName(), let usrImage = userManager.getAccountImage(), let uk = userManager.getUserKey() {
                let msgUser = MessageUser()
                msgUser.ImagePath = usrImage
                msgUser.UserName = userName
                msgUser.UserKey = uk
                messageObj.CreatedUser = msgUser
            }
            self.messages.append(messageObj)
            self.messageMain?.Messages.append(messageObj)
            
            self.animateTable()
            //            self.tableView.reloadData()
            
            let indexPath = IndexPath(row: self.messages.count-1, section: 0)
            if self.messages.count == 1 {
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                self.tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            }
            self.scrollToBottom()
            
            APIClient().addMessage(ot, objectKey: ok, tabName: tab, message: message, indexPath: indexPath, messageTypeId: mTypeId, completion: { (success, messageKey, message, ip) in
                
                if success {
                    
                    SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Messages.rawValue)'")
                    
                    if let newMessage = self.messageMain?.Messages.filter({$0.MessageKey == "TempMessage"}).first {
                        newMessage.MessageKey = messageKey
                        newMessage.isTemp = false
                        //                        newMessage.CreatedAt = DateTimeManager().dateToString(NSDate(), format: ISODateFormat)
                        newMessage.MessagesMainId = self.messageMain?.Id
                        let mesId = newMessage.saveAndGetId()
                        
                        if let uName = userManager.getUserName(), let imgPath = userManager.getAccountImage(), let uk = userManager.getUserKey() {
                            
                            let mesUser = MessageUser()
                            mesUser.MessageId = mesId
                            mesUser.ImagePath = imgPath
                            mesUser.UserKey = uk
                            mesUser.UserName = uName
                            mesUser.save()
                        }
                        
                    }
                    if let iPath = ip {
                        self.tableView.reloadRows(at: [iPath], with: UITableViewRowAnimation.fade)
                    } else {
                        self.tableView.reloadData()
                    }
                    //                    self.tableView.reloadData()
                    
                } else {
                    
                    if let mg = message {
                        self.alertError?.contentText = mg
                    }
                    self.alertError?.show(nil, hidden: nil)
                    if let iPath = ip {
                        self.messages.remove(at: (iPath as NSIndexPath).row)
                        self.messageMain?.Messages.remove(at: (iPath as NSIndexPath).row)
                        if self.messages.count == 0 {
                            self.tableView.reloadData()
                        } else {
                            self.tableView.deleteRows(at: [iPath], with: UITableViewRowAnimation.fade)
                        }
                    } else {
                        self.tableView.reloadData()
                    }
                }
            })
        }
        
        
    }
    
    //MARK: - Keyboard animation
    func keyboardWillShow(_ notification: Notification) {
        self.isKeyboardVisibale = true
        if !self.isSearchBarEditing {
            
            
            if let userInfo = (notification as NSNotification).userInfo {
                if let keyboardSize =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    
                    
//                    kbHeight = keyboardSize.height - 64.0
                    self.kbHeight = keyboardSize.height
                    self.animateFields(true)
                }
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.isKeyboardVisibale = false

        self.animateFields(false)
    }
    
    func animateFields(_ up: Bool) {
        
        if up == false{
            
            self.animateResetViews()
            
        } else{
            self.animateTable()
            
            self.animateTextField()
            
            self.scrollToBottom()
        }
    }
    
    func animateTextField() {
        
        if self.isKeyboardVisibale {
            self.txtContainerBottomConstraint.constant = self.kbHeight
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
                
                }, completion: { (finished) -> Void in
                    
            })
        }
    }
    
    func animateTable() {
        
        if self.isKeyboardVisibale {
            
            let tableHeight = self.tableView.frame.size.height
            let contentHeight = self.tableView.contentSize.height
            
            
            if contentHeight > tableHeight {
                
                self.tableBottomConstraint.constant = self.textBoxContainer.frame.size.height + self.kbHeight
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                
            } else {
                let visibaleAreaSize = tableHeight - self.kbHeight
                
                if visibaleAreaSize < contentHeight {
                    
                    self.tableBottomConstraint.constant = self.textBoxContainer.frame.size.height + self.kbHeight
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.view.layoutIfNeeded()
                    })
                }
                
            }
        }
    }
    
    func animateResetViews() {
        
        if !self.isKeyboardVisibale {
            self.txtContainerBottomConstraint.constant = 0.0
            self.tableBottomConstraint.constant = self.textBoxContainer.frame.size.height
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
                
                
                }, completion: {finished in
                    
            })
        }
    }
    
    //MARK: - Events
    @IBAction func sendPressed(_ sender: AnyObject) {
        
        if let msg = self.txtMessage.text, let mType = self.selectedMessageType?.TypeID {
            if msg == "" {
                self.delegate?.showError?("Please Enter Comment")
            } else {
                self.addNewMessage(message: msg, mTypeId: mType)
            }
        } else {
            self.delegate?.showError?("Unknown error occured")
        }
    }
    
    @IBAction func advanceMessage(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        if let nvc = storyboard.instantiateViewController(withIdentifier: "AdvanceMessage") as? UINavigationController {
            
            if let vc = nvc.viewControllers.first as? AdvanceMessageViewController {
                
                vc.defaultText = self.txtMessage.text
                vc.messageTypes = self.messageMain?.MessageTypes
                vc.selectedMessageType = self.selectedMessageType
                vc.delegate = self
                
                let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                formSheet.shouldCenterVertically = false
                formSheet.shouldDismissOnBackgroundViewTap = true
                formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                
                formSheet.present(animated: true, completionHandler: nil)
            }
        }
        
        
    }
    
    
    //MARK: - Delegate Methods
    func addMessage(text: String, messageType: MessageType) {
        
        if let mType = messageType.TypeID {
            self.addNewMessage(message: text, mTypeId: mType)
        }
    }
}


extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.messages.count == 0 {
            return 1
        }
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.messages.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues("\(self.errorMessage)", bgColor: ThemeBackgroundColor)
            } else {
                if let tab = self.tabName {
                    cell.setValues("No \(tab) found", bgColor: ThemeBackgroundColor)
                }
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageTableViewCell
        let message = self.messages[(indexPath as NSIndexPath).row]
        cell.setValues(message)
        
        
        let defaultimage = UIImage(named: "icon_member_placeHolder")
        
        cell.imgUser.image = defaultimage
        cell.imgUser.tag = (indexPath as NSIndexPath).row
        
        if let image = message.CreatedUser?.ImagePath, let usr = message.CreatedUser {
            if let acc = UserManager().getAccount() {
                
                ImageHandler().asyncImageLoadWithCache("\(Protocol)://\(acc)\(image)", key: usr.UserKey != nil ? usr.UserKey! : "aa") { (image) in
                    
                    if let img = image {
                        if cell.imgUser.tag == (indexPath as NSIndexPath).row {
                            cell.imgUser.image = img
                        }
                    } else {
                        cell.imgUser.image = defaultimage
                    }
                }
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.messages.count > indexPath.row {
            
            if let messagetext = self.messages[indexPath.row].MessageText {
                
                let SingleRowSize = self.heightForView("a")
                let additionalHeight = self.heightForView(messagetext) - SingleRowSize
                
                
                return self.defaultRowHeight + additionalHeight
            }
        }
        
        return self.defaultRowHeight
    }
}


extension MessagesViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchBarEditing = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchBarEditing = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count > 0 {
            if let main = self.messageMain {
                self.messages = main.Messages.filter(){ $0.MessageText?.range(of: searchText,options: NSString.CompareOptions.caseInsensitive) != nil || $0.CreatedUser?.UserName?.range(of: searchText,options: NSString.CompareOptions.caseInsensitive) != nil }
            }
            
        } else {
            if let main = self.messageMain {
                self.messages = main.Messages
            } else {
                self.messages = []
            }
            
        }
        self.tableView.reloadData()
    }
}
