//
//  ObjectDetailsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import MBProgressHUD

enum DetailsVcStatus {
    case open, closed
}

enum DraggingDirection {
    case left, right
}

protocol ObjectDetailsDelegate {
    
    func updateChanges()
    func hasUnsavedData() -> Bool
    func resetUnsavedFlag()
}

class ObjectDetailsViewController: SmartFMParentViewController, UIGestureRecognizerDelegate, GenericFormDelegate, PageLinkNavigatorDelegate, ActionCellDelegate, ChecklistDelegate, LayoutDelegate, AttachmentsDelegate, ObjectDetailsInfoDelegate {

    //Outlets
    @IBOutlet weak var tabContainerView: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var placeHolderView: UIView!
    @IBOutlet var sideMenuConstraint: NSLayoutConstraint!
    //@IBOutlet weak var tabContainerHeight: NSLayoutConstraint!
    
    //Public variables
    var currentView: UIViewController!
    var objectType: String?
    var objectKey: String?
    var locationSpots: String?
    var isFromCreatePage: Bool = false
    var messageToDisplay: String?
    
    //Private variables
    fileprivate var buttons: [UIButton] = []
    fileprivate var objType: ObjType?
    fileprivate var performDrag: Bool = true
    fileprivate var detailsVc: ObjectTabNamesViewController?
    fileprivate var startLocation: CGPoint = CGPoint.zero
    fileprivate let detailsControllerWidth: CGFloat = 180
    fileprivate var detailsVcStatus: DetailsVcStatus = DetailsVcStatus.closed {
        didSet {
            if self.detailsVcStatus == DetailsVcStatus.closed {
                self.detailsVc?.view.isHidden = true
            }
        }
    }
    fileprivate var direction = DraggingDirection.right
    fileprivate var pressedTab: Tab?
    fileprivate var imageToShow: UIImage?
    fileprivate var objectDetailsDelegate: ObjectDetailsDelegate?
    fileprivate var currentTab: TabType?
    fileprivate var gpsLocationTab: UIButton?
    
    fileprivate var alertProcessing:LIHAlert?
    fileprivate var alertSuccess: LIHAlert?
    fileprivate var alertError: LIHAlert?
    fileprivate var progressHud: MBProgressHUD?
    
    fileprivate var duplicateTabTypes:[String] = []
    
    
    ///////////////////////////////////////////
    //MARK: - View Controller Methods
    ///////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deleteSynctimes()
        self.initAlerts()
        
        self.getObjectTypeDetails()
        
        let firstButton = self.addTabButtons()
        if let _ = locationSpots, let tab = gpsLocationTab {
            self.tabButtonPresses(tab)
        } else {
            self.tabButtonPresses(firstButton)
        }
        
        if isFromCreatePage == true {
            
            if let message = self.messageToDisplay {
                self.view.makeToast("\(message)", duration: 2.0, position: .bottom)
            } else {
                self.view.makeToast("Successfully Created", duration: 2.0, position: .bottom)
            }
            self.isFromCreatePage = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        deleteSynctimes()
    }
    
    fileprivate func deleteSynctimes() {
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectInformation.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Members.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Checklist.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Messages.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Attachments.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Layout.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.SubObjects.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.URLs.rawValue)'")
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ItemList.rawValue)'")
    }
    
    override func viewDidLayoutSubviews() {
        
//        self.tabContainerHeight.constant = CGFloat(self.buttons.count) * 70.0
        
        if self.detailsVc == nil {
            if let detailsVc = self.storyboard?.instantiateViewController(withIdentifier: "TabNames") as? ObjectTabNamesViewController {
                
                self.addChildViewController(detailsVc)
                self.view.addSubview(detailsVc.view)
                detailsVc.view.frame = CGRect(x: (-1*self.detailsControllerWidth + self.tabContainerView.frame.size.width), y: 0, width: self.detailsControllerWidth, height: self.tabContainerView.frame.size.height)
                if let tabs = self.objType?.tabs {
                    detailsVc.tabs = tabs
                }
                self.detailsVc = detailsVc
                self.view.bringSubview(toFront: self.tabContainerView)
                
                detailsVc.didMove(toParentViewController: self)
            
                self.detailsVc?.view.isHidden = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "InfoSegue" {
            
            if let vc = segue.destination as? ObjectDetailsInfoViewController {
                vc.objectType = self.objectType
                vc.objectKey = self.objectKey
                vc.formDelegate = self
                vc.navigatorDelegate = self
                vc.actionCellDelegate = self
                vc.delegate = self
            }
        } else if segue.identifier == "MembersSegue" {
            
            if let vc = segue.destination as? MembersViewController {
                
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
            }
            
        } else if segue.identifier == "ChecklistSegue" {
            
            if let vc = segue.destination as? ChecklistViewController {
                
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.checkCellMainDelegate = self
            }
        } else if segue.identifier == "Messages" {
            
            if let vc = segue.destination as? MessagesViewController {
                
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
                //                vc.checkCellMainDelegate = self
            }
        } else if segue.identifier == "LayoutSegue" {
            
            if let vc = segue.destination as? LayoutMainViewController {
                
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
            }
        } else if segue.identifier == "Attachments" {
            
            if let vc = segue.destination as? AttachmentsViewController {
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
            }
            
        } else if segue.identifier == "SubObjectsSegue" {
            
            if let vc = segue.destination as? SubObjectsViewController {
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
            }
        } else if segue.identifier == "URLsTab" {
            
            if let vc = segue.destination as? UrlsViewController {
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
            }
        } else if segue.identifier == "ItemListSegue" {
            
            if let vc = segue.destination as? ItemListViewController {
                vc.forceSync = self.setForceSyncStatus(self.currentTab?.rawValue)
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.delegate = self
            }
        } else if segue.identifier == "NearLocationSegue" {
            
            if let vc = segue.destination as? GPSNearLocationVewController {
                vc.objectKey = self.objectKey
                vc.objectType = self.objectType
                vc.tabName = self.pressedTab?.TabName
                vc.locationSpots = self.locationSpots
                vc.delegate = self
            }
        }
        
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    
    fileprivate func getObjectTypeDetails() {
        
        if let objType = self.objectType {
            
            if let objectTypes = ObjType.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [ObjType] {
                
                if objectTypes.count > 0 {
                    self.objType = objectTypes[0]
                    self.objType?.tabs = objectTypes[0].getTabs()
                    self.objType?.roles = objectTypes[0].getRoles()
                    self.objType?.stages = objectTypes[0].getStage()
                }
            }
            
            self.getDuplicateTabTypes()
        }
    }
    
    fileprivate func setForceSyncStatus(_ dTabType:String?) -> Bool{
        
        var status = false
        
        if let tabType = dTabType{
            if self.duplicateTabTypes.contains(tabType){
                status = true
            }
        }
        
        return status
    }
    
    fileprivate func getDuplicateTabTypes(){
        
        self.duplicateTabTypes.removeAll()
        
        if let tabs = self.objType?.tabs {
            var tempTabType:String
            for n in 0 ..< tabs.count{
                
                if let tabType = tabs[n].TabType{
                    tempTabType = tabType
                    for i in 0 ..< tabs.count{
                        if i != n && tempTabType==tabs[i].TabType{
                            self.duplicateTabTypes.append(tempTabType)
                        }
                    }
                }
                
            }
        }
    }
    
    fileprivate func addTabButtons() -> UIButton {
        var firstButton: UIButton = UIButton()
        
        if let tabs = self.objType?.tabs {
            
            var topView: UIView = self.tabContainerView
            
            for n in 0 ..< tabs.count {
                
                if let tabTypeString = tabs[n].TabType, let tabType = TabType(rawValue: tabTypeString) {
                    
                    let button = self.createTabButton(withType: tabType)
                    var attr = NSLayoutAttribute.bottom
                    if n==0 {
                        attr=NSLayoutAttribute.top
                        firstButton = button
                    }
                    
                    if tabTypeString == TabType.GPS.rawValue {
                        self.gpsLocationTab = button
                    }
                    
                    button.tag = (n+1)*10
                    button.addTarget(self, action: #selector(ObjectDetailsViewController.tabButtonPresses(_:)), for: UIControlEvents.touchUpInside)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    let top = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: topView, attribute: attr, multiplier: 1, constant: n==0 ? 10 : 20)
                    
                    let left = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.tabContainerView, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 10)
                    
                    let right = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.tabContainerView, attribute: NSLayoutAttribute.right, multiplier: 1, constant: -1*10)
                    
                    let height = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 40)
                    
                    self.tabContainerView.addSubview(button)
                    self.tabContainerView.addConstraints([top,left,right,height])
                    topView = button
                }
            }
            
            if tabs.count > 0 && tabs[0].TabHide == "1"{
                self.sideMenuConstraint.constant = 0.0
            }
        }
        return firstButton
    }
    
    fileprivate func createTabButton(withType type:TabType)->UIButton {
        
        let button: UIButton = UIButton()
        self.buttons.append(button)
        var normalImage = ""
        var selectedImage = ""
        button.contentMode = UIViewContentMode.scaleAspectFill
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
        button.contentVerticalAlignment = UIControlContentVerticalAlignment.fill
        
        if type == TabType.Info {
            normalImage = "vTabItem_info_unselected"
            selectedImage = "vTabItem_info_selected"
            
        } else if type == TabType.Messages {
            normalImage = "vTabItem_messages_unselected"
            selectedImage = "vTabItem_messages_selected"
            
        } else if type == TabType.Members {
            normalImage = "vTabItem_member_unselected"
            selectedImage = "vTabItem_member_selected"
            
        } else if type == TabType.Attachments {
            normalImage = "vTabItem_attachments_unselected"
            selectedImage = "vTabItem_attachment_selected"
            
        } else if type == TabType.Checklist {
            normalImage = "vTabItem_checklist_unselected"
            selectedImage = "vTabItem_checklist_selected"
            
        } else if type == TabType.Layout {
            normalImage = "vTabItem_layout_unselected"
            selectedImage = "vTabItem_layout_selected"
            
        } else if type == TabType.Settings {
            normalImage = "vTabItem_settings_unselected"
            selectedImage = "vTabItem_settings_selected"
            
        } else if type == TabType.SubObjects {
            normalImage = "vTabItem_subobjects_unselected"
            selectedImage = "vTabItem_subobjects_selected"
            
        } else if type == TabType.ItemList {
            normalImage = "vTabItem_itemlist_unselected"
            selectedImage = "vTabItem_itemlist_selected"
            
        } else if type == TabType.URLs {
            normalImage = "vTabItem_urls_unselected"
            selectedImage = "vTabItem_urls_selected"
            
        } else if type == TabType.GPS {
            normalImage = "vTabItem_gps_unselected"
            selectedImage = "vTabItem_gps_selected"
        }
        
        button.setImage(UIImage(named: normalImage)!, for: UIControlState())
        button.setImage(UIImage(named: selectedImage)!, for: UIControlState.selected)
        
        return button
    }
    
    fileprivate func setAllButtonsUnselected() {
        
        for button in self.buttons {
            button.isSelected = false
        }
    }
    
    fileprivate func initAlerts() {
        
        self.alertProcessing = LIHAlertManager.getProcessingAlert("Updating...")
        AlertManager().configProcessing(self.alertProcessing, hasNavBar: true)
        self.alertProcessing?.paddingTop = 0
        self.alertProcessing?.initAlert(self.view)
        
        self.alertError = LIHAlertManager.getErrorAlert("")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.paddingTop = 0
        self.alertError?.initAlert(self.view)
        
        self.alertSuccess = LIHAlertManager.getSuccessAlert("")
        AlertManager().configSuccessAlert(self.alertSuccess, hasNavBar: true)
        self.alertError?.paddingTop = 0
        self.alertSuccess?.initAlert(self.view)
        
    }
    
    fileprivate func showTab(_ pressedTab: Tab, sender: UIButton) {
        
        self.setAllButtonsUnselected()
        sender.isSelected = true
        
        if pressedTab.TabType == TabType.Info.rawValue {
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "InfoSegue", sender: nil)
            })
        } else if pressedTab.TabType == TabType.Messages.rawValue {
            performSegue(withIdentifier: "Messages", sender: nil)
            
        } else if pressedTab.TabType == TabType.Members.rawValue {
            
            performSegue(withIdentifier: "MembersSegue", sender: nil)
            
        } else if pressedTab.TabType == TabType.Checklist.rawValue {
            
            performSegue(withIdentifier: "ChecklistSegue", sender: nil)
            
        } else if pressedTab.TabType == TabType.Messages.rawValue {
            
            performSegue(withIdentifier: "Messages", sender: nil)
            
        } else if pressedTab.TabType == TabType.Layout.rawValue {
            
            performSegue(withIdentifier: "LayoutSegue", sender: nil)
            
        } else if pressedTab.TabType == TabType.Attachments.rawValue {
            
            performSegue(withIdentifier: "Attachments", sender: nil)
            
        } else if pressedTab.TabType == TabType.SubObjects.rawValue {
            
            performSegue(withIdentifier: "SubObjectsSegue", sender: nil)
            
        } else if pressedTab.TabType == TabType.URLs.rawValue {
            
            performSegue(withIdentifier: "URLsTab", sender: nil)
            
        } else if pressedTab.TabType == TabType.ItemList.rawValue {
            
            performSegue(withIdentifier: "ItemListSegue", sender: nil)
            
        } else if pressedTab.TabType == TabType.GPS.rawValue {
            
            performSegue(withIdentifier: "NearLocationSegue", sender: nil)
        }
        
        if let type = pressedTab.TabType {
            self.currentTab = TabType(rawValue: type)
        }
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    
    func tabButtonPresses(_ sender: UIButton) {
     
        if let tabs = self.objType?.tabs {
        
            let pressedTab = tabs[(sender.tag / 10)-1]
            self.pressedTab = pressedTab
            self.title = pressedTab.TabName
            
            if let delegate = self.objectDetailsDelegate , self.currentTab == TabType.Info && delegate.hasUnsavedData() {
                
                let alert = UIAlertController(title: "Warning", message: "Unsaved data will be lost. Do you want to proceed?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Proceed", style: UIAlertActionStyle.default, handler: { (_) in
                    
                    self.showTab(pressedTab, sender: sender)
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                self.showTab(pressedTab, sender: sender)
            }
        }
    }

    @IBAction func panGestureRecognized(_ sender: UIPanGestureRecognizer) {

        
        let velocity = sender.velocity(in: self.view)
        
        if sender.state == UIGestureRecognizerState.began {
            startLocation = sender.location(in: view)
            if startLocation.x < self.tabContainerView.frame.size.width + 50 && velocity.x > velocity.y && self.detailsVcStatus == DetailsVcStatus.closed {
                startLocation = sender.location(in: view)
                let trans = sender.translation(in: self.view)
                
                let newX = trans.x - self.detailsVc!.view.frame.width + startLocation.x
                if newX < self.tabContainerView.frame.size.width {
                    self.detailsVc!.view.frame.origin.x = newX
                } else {
                    self.detailsVc!.view.frame.origin.x = self.tabContainerView.frame.size.width
                }
                self.direction = DraggingDirection.right
                self.performDrag = true
                self.detailsVc?.view.isHidden = false
                
            } else if startLocation.x < self.tabContainerView.frame.size.width + self.detailsVc!.view.frame.size.width && velocity.x < velocity.y && self.detailsVcStatus == DetailsVcStatus.open {
                
                self.direction = DraggingDirection.left
                self.performDrag = true
                self.detailsVc?.view.isHidden = false
                
            } else {
                
                self.performDrag = false
            }
        }
        
        if self.performDrag {
            
            let trans = sender.translation(in: self.view)
            
            var newX = trans.x - self.detailsVc!.view.frame.width + startLocation.x
            
            if self.direction == DraggingDirection.right {
                newX = trans.x - self.detailsVc!.view.frame.width + startLocation.x
            } else {
                newX = trans.x + self.tabContainerView.frame.size.width
            }
            
            if newX < self.tabContainerView.frame.size.width {
                self.detailsVc!.view.frame.origin.x = newX
            } else {
                self.detailsVc!.view.frame.origin.x = self.tabContainerView.frame.size.width
            }
        }
        
        
        if sender.state == UIGestureRecognizerState.ended {
            
            let location = self.detailsVc!.view.frame.origin.x + self.detailsVc!.view.frame.size.width
            
            if self.direction == DraggingDirection.right {
                
                if location > 70 + self.tabContainerView.frame.size.width {
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                        
                        self.detailsVc!.view.frame.origin.x = self.tabContainerView.frame.size.width
                        
                        }, completion: {
                            finished in
                            
                            self.detailsVcStatus = DetailsVcStatus.open
                    })
                    
                } else {
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                        
                        self.detailsVc!.view.frame.origin.x = (-1*self.detailsVc!.view.frame.size.width) + self.tabContainerView.frame.size.width
                        
                        }, completion: {
                            finished in
                            
                            self.detailsVcStatus = DetailsVcStatus.closed
                    })
                }
            } else {
                
                if location < self.detailsVc!.view.frame.size.width  + self.tabContainerView.frame.size.width - 30 {
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                        
                        self.detailsVc!.view.frame.origin.x = (-1*self.detailsVc!.view.frame.size.width) + self.tabContainerView.frame.size.width
                        
                        }, completion: {
                            finished in
                            
                            self.detailsVcStatus = DetailsVcStatus.closed
                    })
                    
                } else {
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                        
                        self.detailsVc!.view.frame.origin.x = self.tabContainerView.frame.size.width
                        
                        }, completion: {
                            finished in
                            
                            self.detailsVcStatus = DetailsVcStatus.open
                    })
                }
            }
            
            
        }
    }
    
    @IBAction func forwardPressed(_ sender: AnyObject) {
        
        if let ok = self.objectKey, let ot = self.objectType {
            
            let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "ForwardVc") as! UINavigationController
            
            if vc.viewControllers.count>0 {
                if let fvc = vc.viewControllers[0] as? ForwardObjectViewController {
                    fvc.objectKey = ok
                    fvc.objectType = ot
                }
            }
            
            vc.navigationBar.barTintColor = popupHeaderColor
            vc.navigationBar.tintColor = UIColor.white
            let textAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
            ]
            vc.navigationBar.titleTextAttributes = textAttributes
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = false
            formSheet.shouldDismissOnBackgroundViewTap = false
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 350.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
            
        } else {
            self.showError("No selected object")
        }
    }
    
    
    
    ///////////////////////////////////////////
    //MARK: - Delegate Methods
    ///////////////////////////////////////////
    
    func showProcessingAlert(_ completion: @escaping ()->Void) {
        self.alertProcessing?.show({
            completion()
            }, hidden: nil)
    }
    
    func showProcessing(_ completion: @escaping ()->Void) {
        self.alertProcessing?.show({
            completion()
            }, hidden: nil)
    }
    
    func processing(_ completion: @escaping () -> Void) {
        self.alertProcessing?.show({
            completion()
            }, hidden: nil)
    }

    func showProc(completion finished: @escaping () -> Void) {
        
        self.alertProcessing?.show({
            finished()
            }, hidden: nil)
    }
    
    func hideProcessing() {
        self.alertProcessing?.hideAlert(nil)
    }
    
    func showSuccess(_ message: String) {
        self.alertSuccess?.contentText = message
        self.alertSuccess?.show(nil, hidden: nil)
    }
    
    func showError(_ message: String){
        self.alertError?.contentText = message
        self.alertError?.show(nil, hidden: nil)
    }
    
    func showObjects(_ fieldDataJson: String, objectType: String?) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectsVc") as? ObjectsViewController {
            vc.fromObject = true
            vc.filter = Filter()
            vc.filter?.ObjectType = objectType
            
            vc.customFilter = fieldDataJson
            vc.objectsType = ObjectsType.customFilter
            vc.backButtonOption = BackButtonOption.home
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showConfirmation(_ result: @escaping (Bool) -> Void) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to proceed?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Proceed", style: UIAlertActionStyle.default, handler: { (action) in
            
            result(true)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) in
            
            result(false)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToHome() {
        self.performSegue(withIdentifier: "HomeSegue", sender: nil)
    }
    
    func goToObjectsList() {
        
        DispatchQueue.main.async { 
            self.performSegue(withIdentifier: "ObjectListSegue", sender: nil)
        }
        
    }
    
    func showCreatePage(_ fieldDataJson: [NSDictionary], objectType: String?) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePageVc") as? CreatePageViewController {
            
            vc.fieldData = fieldDataJson
            vc.objectType = objectType
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setObjectDetailsDelegate(_ objectDetailsDelegate: ObjectDetailsDelegate?) {
        self.objectDetailsDelegate = objectDetailsDelegate
    }
    
    func showObjectDetailsPage(_ objectType: String, objectKey: String) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectDetailsVc") as? ObjectDetailsViewController {
            
            vc.objectType = objectType
            vc.objectKey = objectKey
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showObjectDetails(_ objectKey: String, objecttype: String) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ObjectDetailsVc") as? ObjectDetailsViewController {
            
            vc.objectType = objecttype
            vc.objectKey = objectKey
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showVc(_ vc: UIViewController) {
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func showpopup(_ vc: UIViewController) {
        
        let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
        formSheet.shouldCenterVertically = true
        formSheet.shouldDismissOnBackgroundViewTap = true
        let width = UIScreen.main.bounds.width - 40
        let height: CGFloat = 300
        formSheet.presentedFormSheetSize = CGSize(width: width, height: height)
        formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
        
        formSheet.present(animated: true, completionHandler: nil)
    }
    
    func showHUD(userInteraction: Bool) {
        
        self.progressHud = MBProgressHUD(view: self.view)
        progressHud?.isUserInteractionEnabled = !userInteraction
        self.view.addSubview(progressHud!)
        progressHud?.show(animated: true)
    }
    
    func hideHUD() {
        self.progressHud?.hide(animated: true)
    }
}
