//
//  ObjectTabNamesViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/9/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class ObjectTabNamesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var tabs: [Tab] = []
    var cellHeight: CGFloat = 60
    
    fileprivate var startLocation: CGPoint = CGPoint.zero
    fileprivate var performDrag = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tabs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = self.tabs[(indexPath as NSIndexPath).row].TabName
        cell.textLabel?.textColor = ThemeTextColor
        cell.textLabel?.font = UIFont(name: "Melbourne", size: 18)!
        cell.selectionStyle = .none
        cell.backgroundColor = TabBackgroundColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.cellHeight
    }

}
