//
//  SubObjectButtonTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/4/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SubObjectButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var sideLineView: UIView!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var btnAction2: UIButton!
    @IBOutlet weak var btnAction3: UIButton!
    
    var objectKey: String?
    var objectType: String?
    var delegate: GenericFormDelegate?
    
    fileprivate var actions: [SubObjectAction]?
    fileprivate var subObject: SubObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setSubObjectValues(_ object: SubObject) {
        
        self.subObject = object
        self.actions = object.action
        
        if let acts = self.actions {
            
            self.resetButtons()
            
            var count = 0;
            for act in acts {
                
                switch (count) {
                    case 0:
                        if let isCheck = act.IsChecked, isCheck == "1" {
                            self.setButtonMarked(button: btnAction)
                        } else {
                            self.enableActionButton(button: btnAction)
                        }
                        
                        if let txt = act.ButtonText {
                            self.btnAction.setTitle("  \(txt)  ", for: UIControlState.normal)
                        }
                        break
                    case 1:
                        if let isCheck = act.IsChecked, isCheck == "1" {
                            self.setButtonMarked(button: btnAction2)
                        } else {
                            self.enableActionButton(button: btnAction2)
                        }
                        
                        if let txt = act.ButtonText {
                            self.btnAction2.setTitle("  \(txt)  ", for: UIControlState.normal)
                        }
                        break
                    case 2:
                        if let isCheck = act.IsChecked, isCheck == "1" {
                            self.setButtonMarked(button: btnAction3)
                        } else {
                            self.enableActionButton(button: btnAction3)
                        }
                        
                        if let txt = act.ButtonText {
                            self.btnAction3.setTitle("  \(txt)  ", for: UIControlState.normal)
                        }
                        break
                    default:
                        break
                }
                count = count + 1
            }
            
        } else {
            self.resetButtons()
        }
        
        if let title = object.ObjectID {
            self.lblTitle.text = title
        } else {
            self.lblTitle.text = "N/A"
        }
        
        var setColor: Bool = false
        
        if let stageString = object.Stage, let ot = object.ObjectType {
            
            if let stages = Stage.__where("ObjectType='\(ot)' and Stage='\(stageString)'", sortBy: "Id", accending: true) as? [Stage] {
                
                if let stage = stages.first, let col = stage.Color {
                    
                    self.viewIndicator.backgroundColor = UIColor.colorWithHexString(String(col.characters.dropFirst()) as NSString)
                    setColor = true
                }
            }
            
        }
        
        if !setColor {
            self.viewIndicator.backgroundColor = UIColor.white
        }
        
    }
    
    fileprivate func resetButtons() {
        self.btnAction.isEnabled = false
        self.btnAction2.isEnabled = false
        self.btnAction3.isEnabled = false
        
        self.btnAction.setTitle("", for: UIControlState.normal)
        self.btnAction2.setTitle("", for: UIControlState.normal)
        self.btnAction3.setTitle("", for: UIControlState.normal)
        
        self.btnAction.backgroundColor = UIColor(rgb: 0x00FFFFFF)
        self.btnAction2.backgroundColor = UIColor(rgb: 0x00FFFFFF)
        self.btnAction3.backgroundColor = UIColor(rgb: 0x00FFFFFF)
    }
    
    fileprivate func enableActionButton(button: UIButton){
        button.isEnabled = true
        button.backgroundColor = UIColor(rgb: 0x00E8041E)
    }
    
    fileprivate func setButtonMarked(button: UIButton){
        button.isEnabled = false
        button.backgroundColor = UIColor(rgb: 0x00282F35)
    }
    
    fileprivate func authorizeActionPerform(subAction: SubObjectAction){
        
        if let isConfirmationNeeded = subAction.UserConfirmation, isConfirmationNeeded == "1" {
            let alertController = UIAlertController(title: "Confirmation", message: "Do you need to perform this action?", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Proceed", style: .default) { (action:UIAlertAction!) in
                self.performAction(action: subAction)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            }
            
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            
            if let pController = parentViewController {
                pController.present(alertController, animated: true, completion:nil)
            } else {
                self.performAction(action: subAction)
            }
        } else {
            self.performAction(action: subAction)
        }
    }
    
    fileprivate func performAction(action: SubObjectAction){
        
        if let ot = self.objectType, let ok = self.objectKey, let actId = action.ActionID {
            
            self.delegate?.processing? {
            }
            
            APIClient().subObjectAction(objectType: ot, objectKey: ok, actionId: actId, completion: { (success, message) in
                
                self.delegate?.hideProcessing?()
                if success {
                    self.delegate?.showSuccess?(message)
                    self.updateActionPerformedObject(self.subObject, action)
                    self.setSubObjectValues(self.subObject!)
                } else {
                    self.delegate?.showError?(message)
                }
            })
        }
        
    }
    
    fileprivate func updateActionPerformedObject(_ subObject: SubObject?, _ subObjectAction: SubObjectAction?){
        if let sObj = subObject, let acts = sObj.action, let sAct = subObjectAction {
            
            for ac in acts {
                if let id = ac.ActionID, let aId = sAct.ActionID, id == aId {
                    ac.IsChecked = "1"
                } else {
                    ac.IsChecked = "0"
                }
                
                ac.update()
            }
        }
    }
    
    @IBAction func actionPressed(_ sender: Any) {
        self.authorizeActionPerform(subAction: (self.actions?[0])!)
    }
    
    @IBAction func action2Pressed(_ sender: Any) {
        self.authorizeActionPerform(subAction: (self.actions?[1])!)
    }
    
    @IBAction func action3Pressed(_ sender: Any) {
        self.authorizeActionPerform(subAction: (self.actions?[2])!)
    }
    
    
}
