//
//  SubObjectsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/30/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SubObjectsViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var constFilterButtonHeight: NSLayoutConstraint!
    
    
    //private members
    fileprivate var objects: [SubObject] = []
    fileprivate var stages: [Stage] = []
    fileprivate var objectsOrg: [SubObject] = [] {
        didSet {
            self.objects = self.objectsOrg
        }
    }
    fileprivate var filters: SubObjectFilter? {
        didSet {
            self.configFilterButton()
        }
    }
    
    fileprivate var selectedFilter: SubObjectOptionsList? {
        didSet {
            
            self.configFilterButton()
        }
    }
    
    fileprivate var isError: Bool = false
    //for search
    fileprivate var isFromClear: Bool = false
    fileprivate var searchActive : Bool = false
    fileprivate var hasResults: Bool = false
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var isCommingBack: Bool = false
    
    
    //public members
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    var delegate: GenericFormDelegate?
    var forceSync:Bool?
    
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configFilterButton()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(SubObjectsViewController.refreshData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")

        self.loadingContainer.isHidden = false
        self.activityIndicator.startAnimating()
        
        self.showLoading()
        self.fetchObjects(completion: { (_) in
            self.hideLoading()
        }, useTimer: true)
        
        if let options = self.filters?.OptionList, options.count > 0 {
            
        } else {
            self.constFilterButtonHeight.constant = 0.0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowSubObjectDetails" {
            
            if let vc = segue.destination as? ObjectDetailsViewController {
                
                if let ip = self.tableView.indexPathForSelectedRow {
                    
                    vc.objectKey = self.objects[(ip as NSIndexPath).row].ObjectKey
                    vc.objectType = self.objects[(ip as NSIndexPath).row].ObjectType
                }
            }
        }
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Private Methods
    //////////////////////////////////////////////////////
    
    fileprivate func fetchObjects(_ showLoading: Bool = true, completion:((Bool)->Void)?, useTimer: Bool) {
        
        if self.forceSync == nil{
            self.forceSync = false
        }
        
        if !self.forceSync! {
            if (useTimer && SyncTimeManager().hasTimerExpired(EntityKey.SubObjects)) || !useTimer {
                
                if let ot = self.objectType, let ok = self.objectKey, let tn = self.tabName {
                    
                    if showLoading {
                        self.btnFilter.isEnabled = false
                        self.activityIndicator.startAnimating()
                        self.loadingContainer.isHidden = false
                    }
                    
                    SyncManager().syncSubObjects(ot, objectKey: ok, tabName: tn, filter: self.selectedFilter?.Value, success: { (success, error) in
                        
                        self.loadingContainer.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.btnFilter.isEnabled = true
                        
                        if success {
                            
                            self.setData(completion: completion)
                            
                        } else {
                            
                            var errorMessage = "Fetching data failed"
                            if let er = error {
                                errorMessage = er
                            }
                            
                            self.delegate?.showError?("\(errorMessage)")
                            completion?(false)
                        }
                    })
                    
                } else {
                    self.delegate?.showError?("Fetching data failed")
                    completion?(false)
                }
                
            } else {
                
                self.setData(completion: completion)
            }
        } else{
            if let ot = self.objectType, let ok = self.objectKey, let tn = self.tabName {
                
                if showLoading {
                    self.btnFilter.isEnabled = false
                    self.activityIndicator.startAnimating()
                    self.loadingContainer.isHidden = false
                }
                
                SyncManager().syncSubObjects(ot, objectKey: ok, tabName: tn, filter: self.selectedFilter?.Value, success: { (success, error) in
                    
                    self.loadingContainer.isHidden = true
                    self.activityIndicator.stopAnimating()
                    self.btnFilter.isEnabled = true
                    
                    if success {
                        
                        self.setData(completion: completion)
                        
                    } else {
                        
                        var errorMessage = "Fetching data failed"
                        if let er = error {
                            errorMessage = er
                        }
                        
                        self.delegate?.showError?("\(errorMessage)")
                        completion?(false)
                    }
                })
                
            } else {
                self.delegate?.showError?("Fetching data failed")
                completion?(false)
            }
        }
        
        
    }
    
    fileprivate func setData(completion:((Bool)->Void)?) {
        
        if let subobjmain = SubObjectsMain.all().first as? SubObjectsMain {
            if let subs = subobjmain.getSubObjects() {
                self.objects = subs
                self.objectsOrg = subs
            } else {
                self.objects = []
                self.objectsOrg = []
            }
            
            self.filters = subobjmain.getFilter()
            if let def = self.filters?.defaultData, let val = def.Value, val != "" {
                let dData = SubObjectOptionsList()
                dData.DisplayText = def.DisplayText
                dData.Value = val
                self.selectedFilter = dData
            }
            
            completion?(true)
            self.tableView.reloadData()
            
        } else {
            completion?(false)
        }
    }
    
    fileprivate func configSelectedFilter() {
        
        self.configFilterButton()
        self.fetchObjects(completion: { (success) in
            
            _ = SubObjectDefaultData.truncateTable()
            let def = SubObjectDefaultData()
            def.DisplayText = self.selectedFilter?.DisplayText
            def.Value = self.selectedFilter?.Value
            def.filterId = self.filters?.Id
            def.save()
        }, useTimer: false)
       
    }
    
    fileprivate func hideLoading() {
        self.loadingContainer.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    fileprivate func showLoading() {
        self.activityIndicator.startAnimating()
        self.loadingContainer.isHidden = false
    }
    
    fileprivate func configFilterButton() {
        
        if let ftr = self.selectedFilter {
            
            self.setFilterButtonText(text: ftr.DisplayText, secondary: ftr.Value)
            
        } else {
            
            if let filters = self.filters {
                
                if filters.NoDataText == "" {
                    self.setFilterButtonText(text: "Select", secondary: "Select")
                } else {
                    self.setFilterButtonText(text: filters.NoDataText, secondary: "Select")
                }
            } else {
                
                self.setFilterButtonText(text: "Loading filters...", secondary: "")
            }
        }
    }
    
    fileprivate func setFilterButtonText(text: String?, secondary: String?) {
        
        if let txt = text {
            self.btnFilter.setTitle(txt, for: UIControlState.normal)
        } else if let sec = secondary {
            self.btnFilter.setTitle(sec, for: UIControlState.normal)
        } else {
            self.btnFilter.setTitle("", for: UIControlState.normal)
        }
        
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Events
    //////////////////////////////////////////////////////
    
    func refreshData() {
        
        self.fetchObjects(false, completion: { (_) in
            self.refreshControl?.endRefreshing()
        }, useTimer: false)
        
    }
    
    @IBAction func filtersPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        if let nvc = storyboard.instantiateViewController(withIdentifier: "SelectPopupWithQR") as? UINavigationController {
            
            nvc.navigationBar.barTintColor = popupHeaderColor
            nvc.navigationBar.tintColor = UIColor.white
            let textAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
            ]
            nvc.navigationBar.titleTextAttributes = textAttributes
            
            if let vc = nvc.viewControllers.first as? SelectBuildingViewController {
                
                vc.hasHeader = false
                vc.hasQRScanner = false
                vc.delegate = self
                vc.titleString = "Select Filter"
                vc.selectType = SelectType.SubObjectFilter
                if let options = self.filters?.OptionList {
                    vc.items = options
                }
                vc.noDataValue = self.filters?.NoDataText
                
                vc.selectedAction = SelectedAction.dismiss
                let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                formSheet.shouldCenterVertically = true
                formSheet.shouldDismissOnBackgroundViewTap = true
                formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                
                formSheet.present(animated: true, completionHandler: nil)
            }
        }
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////
    
}


extension SubObjectsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.objects.count == 0 {
            return 1
        }
        return self.objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.objects.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues("Could not fetch data.", bgColor: ThemeBackgroundColor)
                return cell
            }
            
            cell.setValues("No items found.", bgColor: ThemeBackgroundColor)
            return cell
        }
        
        let object = self.objects[indexPath.row]
        
        if let actions = object.action, actions.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "objectButtonCell") as! SubObjectButtonTableViewCell
            cell.objectKey = object.ObjectKey
            cell.objectType = object.ObjectType
            cell.delegate = self.delegate
            cell.setSubObjectValues(object)
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "objectCell") as! ObjectTableViewCell
        cell.setSubObjectValues(object)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.objects.count - 1 >= indexPath.row {
            let obj = self.objects[indexPath.row]
            if let ot = obj.ObjectType, let ok = obj.ObjectKey {
                self.delegate?.showObjectDetails?(ok, objecttype: ot)
            }
        }
    }
}

extension SubObjectsViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if !isFromClear {
            searchActive = true;
        } else {
            self.isFromClear = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            searchActive = false;
        } else{
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.objects = self.objectsOrg.filter(){ $0.Description?.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil || $0.ObjectID?.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil }
        
        if searchText.characters.count <= 0 {
            searchActive = false;
            self.isFromClear = true
            self.objects = self.objectsOrg
        } else {
            searchActive = true
        }
        
        self.tableView.reloadData()
    }
    
    
}


extension SubObjectsViewController: GenericFormDelegate {
    
    func setSelectedItem(_ item: AnyObject) {
        
        self.selectedFilter = item as? SubObjectOptionsList
        self.configSelectedFilter()
    }
}
