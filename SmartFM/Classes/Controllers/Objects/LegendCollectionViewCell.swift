//
//  LegendCollectionViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 7/20/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class LegendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewLegendColor: UIView!
    @IBOutlet weak var lblLegendName: UILabel!
    
    
    func setValue(_ stage: Stage) {
        
        if let name = stage.Stage {
            self.lblLegendName.text = name
        }
        
        if let col = stage.Color {
            self.viewLegendColor.backgroundColor = UIColor.colorWithHexString(String(col.characters.dropFirst()) as NSString)
        } else {
            self.viewLegendColor.backgroundColor = UIColor.white
        }
    }
}
