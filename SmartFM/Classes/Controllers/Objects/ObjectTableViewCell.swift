//
//  ObjectTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class ObjectTableViewCell: UITableViewCell {
    
    @IBOutlet weak var objectDescription: UILabel!
    @IBOutlet weak var sideLineView: UIView!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(_ object: Object) {
        
        if let desc = object.Description {
            self.objectDescription.text = desc
        } else {
            self.objectDescription.text = "N/A"
        }
        
        if let title = object.ObjectID {
            self.lblTitle.text = title
        } else {
            self.lblTitle.text = "N/A"
        }
        
        var setColor: Bool = false
        
        if let stageString = object.Stage, let ot = object.ObjectType {
            
            if let stages = Stage.__where("ObjectType='\(ot)' and Stage='\(stageString)'", sortBy: "Id", accending: true) as? [Stage] {
                
                if let stage = stages.first, let col = stage.Color {
                    //self.viewIndicator.backgroundColor = self.colorWithHexString(col.substringFromIndex(col.startIndex.advancedBy(1)))
                    self.viewIndicator.backgroundColor = UIColor.colorWithHexString(String(col.characters.dropFirst()) as NSString)
                    setColor = true
                }
            }
            
        }
        
        if !setColor {
            self.viewIndicator.backgroundColor = UIColor.white
        }
        
    }
    
    func setSubObjectValues(_ object: SubObject) {
        
        if let desc = object.Description {
            self.objectDescription.text = desc
        } else {
            self.objectDescription.text = "N/A"
        }
        
        if let title = object.ObjectID {
            self.lblTitle.text = title
        } else {
            self.lblTitle.text = "N/A"
        }
        
        var setColor: Bool = false
        
        if let stageString = object.Stage, let ot = object.ObjectType {
            
            if let stages = Stage.__where("ObjectType='\(ot)' and Stage='\(stageString)'", sortBy: "Id", accending: true) as? [Stage] {
                
                if let stage = stages.first, let col = stage.Color {
                    //self.viewIndicator.backgroundColor = self.colorWithHexString(col.substringFromIndex(col.startIndex.advancedBy(1)))
                    self.viewIndicator.backgroundColor = UIColor.colorWithHexString(String(col.characters.dropFirst()) as NSString)
                    setColor = true
                }
            }
            
        }
        
        if !setColor {
            self.viewIndicator.backgroundColor = UIColor.white
        }
        
    }
    
}
