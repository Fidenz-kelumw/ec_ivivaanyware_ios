//
//  ObjectsMainViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ObjectsMainDelegate {
    
    func showCustomSearch()
}

class ObjectsMainViewController: SmartFMParentViewController, ObjectsDelegate {

    var building: Building?
    var filter: Filter?
    var objectType: String?
    
    
    var gotoObjectType: String?
    var gotoObjectKey: String?
    var menuNameFromHome: String?
    var objectsType: ObjectsType = ObjectsType.defaultFilter
    
    var delegate: ObjectsMainDelegate?
    var menuNameForNoFilterPage: String?
    
    fileprivate var filterButton: UIBarButtonItem?
    fileprivate var menuName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let objectType = self.objectType , let obj = ObjType.__where("ObjectType='\(objectType)'", sortBy: "Id", accending: true).first as? ObjType, let customFilter = obj.CustomFilterPage , customFilter == "1" {
            if let menu = obj.MenuName {
                var title = menu
                if let fltr = self.filter?.FilterName {
                    title = "\(menu) - \(fltr)"
                }
                self.menuName = menu
                self.title = title
            }
            
        } else {
            if let name = self.menuNameForNoFilterPage {
                self.title = name
            }
            self.navigationItem.rightBarButtonItem = nil
        }
        
        self.filterButton = self.navigationItem.rightBarButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EmbededObjects" {
            
            if let nvc = segue.destination as? UINavigationController, let vc = nvc.viewControllers.first as? ObjectsViewController {
                
                vc.building = self.building
                vc.filter = self.filter
                vc.objectType = self.objectType
                vc.delegate = self
                if let objectType = self.objectType , let obj = ObjType.__where("ObjectType='\(objectType)'", sortBy: "Id", accending: true).first as? ObjType, let menu = obj.MenuName {
                    vc.menuName = menu
                }
                self.delegate = vc.self
                vc.objectsType = self.objectsType
            }
        } else if segue.identifier == "objectDetails" {
            
            if let vc = segue.destination as? ObjectDetailsViewController {
                
                vc.objectType = self.gotoObjectType
                vc.objectKey = self.gotoObjectKey
            }
        }
    }
    
    @IBAction func customSearchClicked(_ sender: AnyObject) {
        self.delegate?.showCustomSearch()
    }
    
    
    func objectClicked(_ objectType: String?, objectKey: String?) {
        
        self.gotoObjectKey = objectKey
        self.gotoObjectType = objectType
        self.performSegue(withIdentifier: "objectDetails", sender: nil)
    }
    
    func objectListAppeared() {
        
    }
}
