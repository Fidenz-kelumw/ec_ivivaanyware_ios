//
//  ObjectsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol ObjectsDelegate {
    
    func objectClicked(_ objectType: String?, objectKey: String?)
    func objectListAppeared()
}

enum ObjectsType {
    case defaultFilter, customFilter, noFilter
}

enum BackButtonOption {
    case back, home
}

class ObjectsViewController: UIViewController, GenericFormDelegate, ObjectsMainDelegate, CustomSearchDelegate {

    //IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constCollectionViewContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var constCollectionViewTop: NSLayoutConstraint!
    @IBOutlet weak var constCollectionViewBottom: NSLayoutConstraint!
    
    
    //for search
    fileprivate var isFromClear: Bool = false
    fileprivate var searchActive : Bool = false
    fileprivate var hasResults: Bool = false
    fileprivate var refreshControl: UIRefreshControl?
    fileprivate var isCommingBack: Bool = false
    
    //Public variables
    var building: Building?
    var filter: Filter?
    var customFilter: String?
    var OriginalCustomFilter: String?
    var objectType: String?
    var fromObject = false
    var delegate: ObjectsDelegate?
    var objectsType: ObjectsType = ObjectsType.defaultFilter
    var backButtonOption: BackButtonOption = BackButtonOption.back
    var menuName: String?
    
    //Private variables
    fileprivate var objects: [Object] = []
    fileprivate var stages: [Stage] = []
    fileprivate var objectsOrg: [Object] = [] {
        didSet {
            self.objects = self.objectsOrg
        }
    }
    fileprivate var isError: Bool = false
    fileprivate var errorMessage: String = "Could not fetch data."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.OriginalCustomFilter = self.customFilter
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(ObjectsViewController.refreshData), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
        
        if let objectType = self.filter?.ObjectType , let obj = ObjType.__where("ObjectType='\(objectType)'", sortBy: "Id", accending: true).first as? ObjType, let customFilter = obj.CustomFilterPage , customFilter == "1" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "filter"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ObjectsViewController.showCustomSearch))
            
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        //Back button logic
//        if self.backButtonOption == BackButtonOption.home {
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_home"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ObjectsViewController.homePressed(_:)))
//        } else {
//            self.navigationItem.leftBarButtonItem = nil
//        }
        
        Configurations().configSelectionButton(forButton: self.btnLocation, withDisclosure: false)
       
//        if var title = self.filter?.ObjectType {
//            if title.characters.last == "y" {
//                title = "\(String(title.characters.dropLast()))ie"
//            }
//            self.title = "\(title)s"
//        }
        
        if self.building == nil {
            if let def = Defaults.all().first as? Defaults, let locKey = def.LocationKey {
                
                if let building = Building.__where("LocationKey='\(locKey)'", sortBy: "Id", accending: true).first as? Building {
                    self.building = building
                }
            }
        }
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.loadingContainer.isHidden = false
        self.activityIndicator.startAnimating()
        
        self.fetchObjects(completion: nil)
        
        self.setButtonTitle()
        
        if let objType = self.objectType {
            
            if let obj = ObjType.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true).first as? ObjType {
                
                if obj.CustomFilterPage != "1" {
                    self.navigationItem.rightBarButtonItem = nil
                }
            }
        }
        
        
        //legend config
        self.collectionView.layer.cornerRadius = 3
        self.collectionView.layer.borderColor = UIColor.gray.cgColor
        self.collectionView.layer.borderWidth = 1
        self.getStages()
        self.configCollectionViewHeight()
        self.collectionView.reloadData()
        
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ObjectsViewController.hideLegend), userInfo: nil, repeats: false)
    }
    
    deinit {
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isCommingBack {
            
            self.checkTimerAndReload()
            
        } else {
            
            self.isCommingBack = true
        }
        
        if !self.fromObject {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.delegate?.objectListAppeared()
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowObjectDetails" {
            
            if let vc = segue.destination as? ObjectDetailsViewController {
                
                if let ip = self.tableView.indexPathForSelectedRow {
                    
                    vc.objectKey = self.objects[(ip as NSIndexPath).row].ObjectKey
                    vc.objectType = self.objects[(ip as NSIndexPath).row].ObjectType
                }
            }
        } else if segue.identifier == "showCustomSearch" {
            
            if let vc = segue.destination as? CustomSearchViewController {
                
                vc.location = self.building
                vc.objectType = self.objectType
                vc.delegate = self
            }
        }
    }
    
    
    func hideLegend() {
        
        self.constCollectionViewContainerHeight.constant = 0
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.view.layoutIfNeeded()
        }) 
    }
    
    
    //MARK: - Private Methods
    
    fileprivate func getStages() {
        
        if self.objectsType == ObjectsType.customFilter || self.objectsType == ObjectsType.defaultFilter {
            
            if let ot = self.filter?.ObjectType {
                objectType = ot
            }
            
        } else {
            
            if let ot = self.objectType {
                objectType = ot
            }
        }
        
        if let ot = objectType {
            if let stages = Stage.__where("ObjectType='\(ot)'", sortBy: "Id", accending: true) as? [Stage] {
                
                self.stages = stages
            }
            
        }
    }
    
    fileprivate func configCollectionViewHeight() {
        
        var colHeight: CGFloat = 95
        
        let stagesCount = self.stages.count
        
        let rows = ceil(CGFloat(stagesCount) / 4)
        
        colHeight = (rows * 45) + 5
        
        self.constCollectionViewContainerHeight.constant = colHeight + self.constCollectionViewTop.constant + self.constCollectionViewBottom.constant
        self.view.layoutIfNeeded()
    }
    
    fileprivate func hideCustomFilter() {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    fileprivate func setButtonTitle() {
        if let location = self.building, let locName = location.LocationName {
            self.btnLocation.setTitle(locName, for: UIControlState())
        }
    }
    
    fileprivate func saveSyncTime() {
        
        let syncTime = SyncTime()
        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectList.rawValue)'")
        syncTime.EntityKey = EntityKey.ObjectList.rawValue
        syncTime.UpdatedAt = Date()
        syncTime.save()
    }
    
    fileprivate func fetchObjects(_ showLoading: Bool = true, completion:((Bool)->Void)?) {
        
        var defaultFilter: String? = nil
        var customFilter: String? = nil
        
        var oType: String? = nil
        
        if self.objectsType == ObjectsType.defaultFilter {
            
            if let filterId = self.filter?.FilterID {
                defaultFilter = filterId
            }
            oType = self.filter?.ObjectType
            
        } else if self.objectsType == ObjectsType.customFilter {
            if let filterId = self.filter?.FilterID {
                defaultFilter = filterId
            }
            if let aCustomFilter = self.customFilter {
                customFilter = aCustomFilter
            }
            oType = self.filter?.ObjectType
        } else {
            
            oType = self.objectType
        }
        
        if let ot = oType {
            
            if showLoading {
                self.showLoading()
            }
            
            APIClient().getObjects(ot, locationKey: self.building?.LocationKey, filterId: defaultFilter, customFilter: customFilter, more: nil, completionHandler: { (objects, error) in
                
                self.hideLoading()
                if let objs = objects {
                    
                    self.saveSyncTime()
                    
                    self.isError = false
                    self.objectsOrg = objs
                    completion?(true)
                    self.tableView.reloadData()
                    
                } else {
                    
                    if let er = error {
                        self.errorMessage = er
                    }
                    
                    self.isError = true
                    completion?(false)
                    self.tableView.reloadData()
                }
            })
            
        } else {
            
            completion?(false)
        }
        
    }
    
    fileprivate func hideLoading() {
        self.loadingContainer.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    fileprivate func showLoading() {
        self.activityIndicator.startAnimating()
        self.loadingContainer.isHidden = false
    }
    
    fileprivate func checkTimerAndReload() {
        
        if SyncTimeManager().hasTimerExpired(EntityKey.ObjectList) {
            
            NSLog("timer has expired. reloading...")
            self.fetchObjects(true, completion: nil)
            
        } else {
            NSLog("timer has not expired.")
        }
    }
    
    //MARK: - Events
    
    @IBAction func changeLocation(_ sender: AnyObject) {
        
        if let vc =  UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchAndSelect") as? SearchAndSelectViewController  {
            vc.delegate = self
            vc.objectType = self.objectType
            vc.hasHeader = true
            vc.titleString = "Select Location"
            vc.selectedAction = SelectedAction.dismiss
            if let buildings = Building.all() as? [Building] {
                
                var datas: [LData] = []
                for building in buildings {
                    let data = LData()
                    data.Id = building.Id
                    data.Value = building.LocationKey
                    data.DisplayText = building.LocationName
                    datas.append(data)
                }
                
                vc.items = datas
                let opt = LData()
                opt.DisplayText = "Any Location"
                opt.Value = ""
                vc.items.insert(opt, at: 0)
                vc.searchtype = SearchType.cachedData
            }
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
        }
    }
    
    func homePressed(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "goToHome", sender: nil)
    }
    
    @IBAction func backToObjectsList(_ sender: UIStoryboardSegue) {
        
        
    }
    
    func refreshData() {
        
        self.OriginalCustomFilter = nil
        self.customFilter = nil
        
        self.fetchObjects(false) { (success) in
            self.refreshControl?.endRefreshing()
        }
    }
    
    //MARK: - Delegate Methods
    func setSelectedItem(_ item: AnyObject) {
        
        if let data = item as? LData {
            
            let building = Building()
            building.Id = data.Id
            building.LocationName = data.DisplayText
            building.LocationKey = data.Value
            
            self.building = building
        }
        self.setButtonTitle()
        self.fetchObjects(completion: nil)
    }

    func showCustomSearch() {
        
        if let nvc = UIStoryboard(name: "Popups", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomSearch") as? UINavigationController {
            
            nvc.navigationBar.barTintColor = popupHeaderColor
            nvc.navigationBar.tintColor = UIColor.white
            let textAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
            ]
            nvc.navigationBar.titleTextAttributes = textAttributes
            
            if let vc = nvc.viewControllers.first as? CustomSearchPopupViewController {

                vc.delegate = self
                vc.objectType = self.objectType
                vc.defaultData = self.OriginalCustomFilter

                let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                formSheet.shouldCenterVertically = false
                formSheet.shouldDismissOnBackgroundViewTap = false
                formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 400.0)
                formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                
                formSheet.present(animated: true, completionHandler: nil)
            }
        }
        
    }
    
    func applyFilter(_ fieldData: String) {
        
        self.customFilter = fieldData
        self.objectsType = ObjectsType.customFilter
        self.fetchObjects(completion: nil)
    }
}

extension ObjectsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.objects.count == 0 {
            return 1
        }
        return self.objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.objects.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            
            if self.isError {
                cell.setValues(self.errorMessage, bgColor: ThemeBackgroundColor)
                return cell
            }
            
            if let title = self.filter?.ObjectType{
                var items = "items"
                if let menu = self.menuName {
                    items = menu
                }
                cell.setValues("No \(items) found.", bgColor: ThemeBackgroundColor)
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "objectCell") as! ObjectTableViewCell
        cell.setValues(self.objects[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.fromObject {
            self.performSegue(withIdentifier: "ShowObjectDetails", sender: nil)
        } else {
            
            self.delegate?.objectClicked(self.objects[(indexPath as NSIndexPath).row].ObjectType, objectKey: self.objects[(indexPath as NSIndexPath).row].ObjectKey)
        }
    }
}

extension ObjectsViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if !isFromClear {
            searchActive = true;
        } else {
            self.isFromClear = true
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" {
            searchActive = false;
        } else{
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.objects = self.objectsOrg.filter(){ $0.Description?.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil || $0.ObjectID?.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil }
        
        
        if searchText.characters.count <= 0 {
            searchActive = false;
            self.isFromClear = true
            self.objects = self.objectsOrg
        } else {
            searchActive = true
        }
        
        self.tableView.reloadData()
    }
    
    
}


extension ObjectsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.stages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LegendCell", for: indexPath) as! LegendCollectionViewCell
        
        cell.setValue(self.stages[(indexPath as NSIndexPath).item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let colSize = collectionView.frame.size
        var divider: CGFloat = 4
        
        if self.stages.count < 4 {
            divider = CGFloat(self.stages.count)
        }
        
        let width = (colSize.width - 25)/divider
        
        return CGSize(width: width, height: 40)
    }
}
