//
//  QRReaderViewController.swift
//  Facility Booking
//
//  Created by Fidenz on 6/22/15.
//  Copyright (c) 2015 fidenz. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

enum ScanType {
    
    case registration, openObject
}

protocol QRRegisterDelegate {
    
    func setValues(_ userId: String?, domainId: String?, regCode: String?)
    func setRegDetails(_ account: String, apiKey: String, ssl: String)
}

class QRReaderViewController: SmartFMParentViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var delegate: QRRegisterDelegate?

    @IBOutlet var videoLayerContainer: UIView!
    @IBOutlet var qrBorder: UIView!
    @IBOutlet var imgScanning: UIImageView!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var initialPosition: CGRect?
    var rotating = true
    var isInitialLoad: Bool = false
    var isVisible: Bool = true
    var goToObjectKey: String?
    var goToObjectType: String?
    
    var alertProcessing: LIHAlert?
    var alertError: LIHAlert?
    
    var scanType: ScanType = ScanType.registration
    
    //MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.isInitialLoad = true
        
        //self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Melbourne", size: 20)!]
        
        if self.scanType == ScanType.openObject {
            
            self.navigationItem.leftBarButtonItem = nil
        }
        

        rotateOnce()
        
        qrBorder?.layer.borderColor = ThemeRedColor.cgColor
        qrBorder?.layer.borderWidth = 2
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //self.title = "SCAN A QR CODE"
        if isInitialLoad {
            self.initialPosition = self.qrBorder?.frame
            self.isInitialLoad = false
        }
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error:NSError?
        let input: AnyObject!
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if (error != nil) {
            
            NSLog("\(error?.localizedDescription)")
            return
        }
        
        captureSession = AVCaptureSession()
        captureSession?.addInput(input as! AVCaptureInput)
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = self.videoLayerContainer.layer.bounds
        if let preLayer = videoPreviewLayer {
            self.videoLayerContainer.layer.addSublayer(preLayer)
        }
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async { () -> Void in
            
            self.captureSession?.startRunning()
        }
        
        self.videoLayerContainer.bringSubview(toFront: qrBorder!)
        self.qrBorder.isHidden = false
        
        self.initAlerts()
        self.alertProcessing?.hideAlert(){()->() in}
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.captureSession?.stopRunning()
        self.videoPreviewLayer?.removeFromSuperlayer()
        self.videoPreviewLayer = nil
        self.captureSession = nil
        self.alertProcessing?.hideAlert( {()->() in} )
        self.isVisible = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowObjectDetails" {
            
            if let vc = segue.destination as? ObjectDetailsViewController {
                
                vc.objectKey = self.goToObjectKey
                vc.objectType = self.goToObjectType
            }
        }
    }
    
    
    //MARK: - Private Methods
    
    func processQRCodeRegistration(_ QRResponse: String) {
        
        
        if let jsonData: Foundation.Data = QRResponse.data(using: String.Encoding.utf8, allowLossyConversion: true) {
            
            let jsonObject: Any?
            do {
                jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [])
            } catch _ as NSError {
                jsonObject = nil
            }
            
            if let json = jsonObject as? NSDictionary {
                
                if let _domainId = json["DomainID"] as? String, let _userId = json["UserID"] as? String, let _regCode = json["RegistrationCode"] as? String {
                    
                    
                    if let acc = json["Account"] as? String, let apikey = json["ApiKey"] as? String {
                        var ssl = "0"
                        if let s = json["SSL"] as? String {
                            ssl = s
                        }
                        if let s = json["ssl"] as? String {
                            ssl = s
                        }
                        
                        self.delegate?.setRegDetails(acc, apiKey: apikey, ssl: ssl)
                    }
                    
                    self.delegate?.setValues(_userId, domainId: _domainId, regCode: _regCode)
                    self.dismiss(animated: true, completion: nil)
                    return
                    
                } else {
                    self.startCaptureRunning()
                }
            } else {
                self.startCaptureRunning()
            }
        } else {
            self.startCaptureRunning()
        }
        
    }
    
    fileprivate func getObjectKeyFromQr(_ qrCode: String, pattern: String) -> String? {

        if let regX = try? NSRegularExpression(pattern: pattern, options: []) {
            let matches = regX.matches(in: qrCode, options: [], range: NSRange(location: 0, length: qrCode.characters.count))
            
            if matches.count > 0 {
                let match: String = (qrCode as NSString).substring(with: matches[0].rangeAt(1))
                
                return match
            }
        }
        
        return nil
    }
    
    func processQRCodeOpenObject(_ QRResponse: String) {
// new change according to eutech, remove reg checking
        
//        var foundPattern: Bool = false
//        if let objectTypes = ObjType.all() as? [ObjType] {
//
//            for objectType in objectTypes {
//
//                if let pattern = objectType.QRFormatiOS {
//
//                    if let ok = self.getObjectKeyFromQr(QRResponse, pattern: pattern) {
//                        foundPattern = true
//                        self.goToObjectKey = ok
//                        self.goToObjectType = objectType.ObjectType
//                        performSegue(withIdentifier: "ShowObjectDetails", sender: nil)
//                        break
//                    }
//                }
//            }
//
//        }
//
//        if !foundPattern {
//            APIClient().getObjectForQr(QRResponse) { (objectType, objectKey) in
//
//                self.alertProcessing?.hideAlert(nil)
//                if let ot = objectType, let ok = objectKey {
//
//                    self.goToObjectKey = ok
//                    self.goToObjectType = ot
//                    self.performSegue(withIdentifier: "ShowObjectDetails", sender: nil)
//
//                } else {
//                    self.alertError?.show(nil, hidden: nil)
//                    self.startCaptureRunning()
//                }
//            }
//        }
        
        APIClient().getObjectForQr(QRResponse) { (objectType, objectKey, message) in
            self.alertProcessing?.hideAlert(nil)
            if let ot = objectType, let ok = objectKey, let md = message {
                
                if let objectTypes = ObjType.all() as? [ObjType] {
                
                    for objectType in objectTypes {
                        if let type = objectType.ObjectType, type == ot {
                            self.goToObjectKey = ok
                            self.goToObjectType = ot
                            self.performSegue(withIdentifier: "ShowObjectDetails", sender: nil)
                            break
                        }
                    }
                    
                    self.alertError?.contentText = md
                    self.alertError?.show(nil, hidden: nil)
                    self.startCaptureRunning()
                    
                } else {
                    if let md = message {
                        self.alertError?.contentText = md
                    }
                    self.alertError?.show(nil, hidden: nil)
                    self.startCaptureRunning()
                }
            
            } else {
                if let md = message {
                    self.alertError?.contentText = md
                }
                self.alertError?.show(nil, hidden: nil)
                self.startCaptureRunning()
            }
        }
    }
    
    func processQRCodeForFormItem() {
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            //qrBorder?.frame = CGRectZero
            qrBorder?.frame = self.initialPosition!
            
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            qrBorder?.frame = barCodeObject.bounds;
            
            if metadataObj.stringValue != nil {
                self.stopCaptureRunning(metadataObj.stringValue)
            }
        }
        
        let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
        
        qrBorder?.frame = barCodeObject.bounds
    }
    
    func rotateOnce() {
        UIView.animate(withDuration: 0.5,
            delay: 0.0,
            options: .curveLinear,
            animations: {self.imgScanning.transform = self.imgScanning.transform.rotated(by: π)},
            completion: {finished in self.rotateAgain()})
    }
    
    func rotateAgain() {
        UIView.animate(withDuration: 0.5,
            delay: 0.0,
            options: .curveLinear,
            animations: {self.imgScanning.transform = self.imgScanning.transform.rotated(by: π)},
            completion: {finished in if self.rotating { self.rotateOnce() }})
    }
    
    func stopCaptureRunning(_ qrData: String) {
        self.captureSession?.stopRunning()
        self.alertProcessing?.show({ () -> () in
            
            if self.scanType == ScanType.registration {
                
                self.processQRCodeRegistration(qrData)
                
            } else {
                
                self.processQRCodeOpenObject(qrData)
                
            }
            
            
            }, hidden: nil)
    }
    
    func startCaptureRunning() {
        
        
        self.alertProcessing?.hideAlert({ () -> () in
            self.qrBorder?.frame = self.initialPosition!
            self.captureSession?.startRunning()
            //self.view.bringSubviewToFront(self.alertProcessing!.alertView!)
            //self.view.bringSubviewToFront(self.alertError!.alertView!)
        })
    }
    
    func initAlerts() {
        
        self.alertProcessing = LIHAlertManager.getProcessingAlert("Processing QR Code...")
        AlertManager().configProcessing(self.alertProcessing, hasNavBar: true)
        self.alertProcessing?.initAlert(self.view)
        
        self.alertError = LIHAlertManager.getErrorAlert("QR code not supported")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: true)
        self.alertError?.initAlert(self.view)
        
    }
    
    //MARK: - Events
    func backPressed(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closePressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
