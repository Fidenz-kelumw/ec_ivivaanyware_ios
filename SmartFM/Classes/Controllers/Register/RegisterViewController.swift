//
//  RegisterViewController.swift
//  Headcount
//
//  Created by Lasith Hettiarachchi on 8/20/15.
//  Copyright (c) 2015 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID

class RegisterViewController: UIViewController, UITextFieldDelegate, QRRegisterDelegate {

    
   // @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var txtUserId: UITextField!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var txtDomainId: UITextField!
    @IBOutlet weak var txtRegistrationCode: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    
    var alertProcessing:LIHAlert?
    var alertSuccess: LIHAlert?
    var alertError: LIHAlert?
    var alertSessionError: LIHAlert?
    
    
    var kbHeight: CGFloat! = 0.0
    
    
    
    //MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.lblVersion.text = "Version \(version))"
            if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                self.lblVersion.text = "Version \(version) (\(build))"
            }
        }
        
        UserManager().wipeUserData(completion: nil)
        
        self.initAlerts()
        
        //Setting textfield icons
        txtUserId.leftViewMode = UITextFieldViewMode.always
        txtUserId.leftView = UIImageView(image: UIImage(named: "icon_userId"))
        txtUserId.setValue(UIColor(red: 210.0/255.0, green: 212.0/255.0, blue: 213.0/255.0, alpha: 0.6), forKeyPath: "placeholderLabel.textColor")
        
        txtRegistrationCode.leftViewMode = UITextFieldViewMode.always
        txtRegistrationCode.leftView = UIImageView(image: UIImage(named: "icon_registrationCode"))
        txtRegistrationCode.setValue(UIColor(red: 210.0/255.0, green: 212.0/255.0, blue: 213.0/255.0, alpha: 0.6), forKeyPath: "placeholderLabel.textColor")
        
        txtDomainId.leftViewMode = UITextFieldViewMode.always
        txtDomainId.leftView = UIImageView(image: UIImage(named: "icon_domainId"))
        txtDomainId.setValue(UIColor(red: 210.0/255.0, green: 212.0/255.0, blue: 213.0/255.0, alpha: 0.6), forKeyPath: "placeholderLabel.textColor")
        
        //Keyboard notifications
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        
        if segue.identifier == "ShowQR" {
            
            if let navController: UINavigationController = segue.destination as? UINavigationController {
                
                let vcs = navController.viewControllers
                if vcs.count > 0 {
                    
                    if let vc = vcs[0] as? QRReaderViewController {
                        
                        vc.delegate = self
                    }
                }
            }
        }
    }
    
    
    //MARK: - Private Methods
    func initAlerts() {
        
        self.alertProcessing = LIHAlertManager.getProcessingAlert("Registering")
        AlertManager().configProcessing(self.alertProcessing, hasNavBar: false)
        self.alertProcessing?.paddingTop = 10
        self.alertProcessing?.initAlert(self.view)
        
        self.alertError = LIHAlertManager.getErrorAlert("Failed to Register")
        AlertManager().configErrorAlert(self.alertError, hasNavBar: false)
        self.alertError?.paddingTop = 5
        self.alertError?.initAlert(self.view)
        
        self.alertSessionError = LIHAlertManager.getErrorAlert("Failed to get session details")
        AlertManager().configSuccessAlert(self.alertSuccess, hasNavBar: false)
        self.alertSessionError?.paddingTop = 10
        self.alertSessionError?.initAlert(self.view)
        
    }
    
    fileprivate func showError(_ message: String?) {
        
        if let msg = message {
            self.alertError?.contentText = msg
        } else {
            self.alertError?.contentText = "Failed to Register"
        }
        
        self.alertError?.show(nil, hidden: nil)
    }
    
    fileprivate func getDomainData(domain: String, completion: @escaping (Bool)->Void) {
        
        APIClient().getDomainData(domain, completionHandler: { (domainData, error) in
            
            if let account = domainData?.Account, let apikey = domainData?.ApiKey {
                
                UserManager().saveDomainData(account: account, APIKey: apikey, SSL: domainData?.SSL == "1" ? "https" : "http")
                
                completion(true)
                
            } else {
                
                completion(false)
            }
            
        })
    }
    
    fileprivate func register(_ userId:String, registrationCode:String, domainId:String, account: String, apiKey: String, firebaseToken: String?) {
        
        APIClient().registerUser(userId, registrationCode: registrationCode, domainId: domainId, account: account, apiKey: apiKey, firebaseToken: firebaseToken, completionHandler: { (regUser, error) -> () in
            
            self.alertProcessing?.hideAlert(nil)
            
            if let user = regUser, let uk = user.UserKey {
                
                let userManager = UserManager()
                userManager.saveUserData(userKey: uk, userId: user.UserID, userName: user.defaults?.UserName, accountImage: user.defaults?.ImagePath, domain: user.UserDomain)
                
                if let defs = user.defaults {
                    defs.isUserLocationUpdateON = "0"
                    defs.UserLocationUpdateMins = "10"
                    defs.NearestSpotUpdateSeconds = "30"
                    defs.save()
                }
                
                UserManager().setFirstRun(true)
                
                if let accountInfo = user.accountInfo {
                    if let acc = accountInfo.Account {
                        UserManager().setAccount(acc)
                    }
                    if let apikey = accountInfo.ApiKey {
                        UserManager().setApiKey(apikey)
                    }
                }
                
                //SyncData
                SyncManager().syncMasterData(withTimer: false, completion: nil)
                
                //Parse
                if UIApplication.shared.isRegisteredForRemoteNotifications {
                    NSLog("registered")
                    
                } else {
                    
                    NSLog("not registered")
                }
                
                //Open Home
                if let nvc = self.storyboard?.instantiateViewController(withIdentifier: "MainNav") as? UINavigationController {
                    self.present(nvc, animated: true, completion: nil)
                }
                
            } else {
                
                self.showError(error)
                UserManager().wipeUserData(completion: nil)
            }
        })
    }
    
    
    //MARK: - Keyboard animation
    func keyboardWillShow(_ notification: Notification) {
        
        if let userInfo = (notification as NSNotification).userInfo {
            if let keyboardSize =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                kbHeight = keyboardSize.height
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.animateTextField(false)
    }
    
    func animateTextField(_ up: Bool) {
    
        if up == false{
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.frame.origin.y = 0.0
                
                }, completion: {finished in
            })
            
        } else {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.frame.origin.y = -1 * self.kbHeight
                
                }, completion: { (finished) -> Void in
                    
            })
        }
    }
    
    
    //MARK: - TextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTag: Int = textField.tag + 1
        // Try to find next responder
        let nextResponder: UIResponder? = self.view.viewWithTag(nextTag)
        if ((nextResponder) != nil) {
            // Found next responder, so set it.
            nextResponder!.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false
    }
    

    //MARK: - Events
    @IBAction func registerPressed(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if self.txtDomainId.text == "" || self.txtRegistrationCode.text == "" || self.txtUserId.text == "" {
            
            let alert = UIAlertController(title: "Error", message: "Please fill all fields to continue", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            present(alert, animated: true, completion: nil)
            
        } else {
        
            self.alertProcessing?.show({ () -> () in
                
                
                let uId = self.txtUserId.text!.trimmingCharacters(in: NSCharacterSet.whitespaces)
                let regiCode = self.txtRegistrationCode.text!.trimmingCharacters(in: NSCharacterSet.whitespaces)
                let DomaId = self.txtDomainId.text!.trimmingCharacters(in: NSCharacterSet.whitespaces)
                
                if let acc = UserManager().getAccount(), let ak = UserManager().getApiKey() {
                    
                    self.register(uId, registrationCode: regiCode, domainId: DomaId, account: acc, apiKey: ak, firebaseToken: InstanceID.instanceID().token())
                    
                } else {
                    
                    self.getDomainData(domain: DomaId, completion: { (success) in
                        
                        if success {
                            
                            if let acc = UserManager().getAccount(), let ak = UserManager().getApiKey() {
                                
                                self.register(uId, registrationCode: regiCode, domainId: DomaId, account: acc, apiKey: ak, firebaseToken: InstanceID.instanceID().token())
                                
                            } else {
                                
                                self.alertProcessing?.hideAlert(nil)
                                self.showError("Registration failed")
                            }
                            
                        } else {
                            
                            self.alertProcessing?.hideAlert(nil)
                            self.showError("Incorrect domain ID")
                        }
                    })
                }
                
                
                
            }, hidden: nil)
            
        }
    }
    
    //MARK: - Delegate Methods
    func setValues(_ userId: String?, domainId: String?, regCode: String?) {
        
        if let uid = userId {
            self.txtUserId.text = uid
        }
        if let dom = domainId {
            self.txtDomainId.text = dom
        }
        if let reg = regCode {
            self.txtRegistrationCode.text = reg
        }
        
        self.registerPressed(self.btnRegister)
    }
    
    func setRegDetails(_ account: String, apiKey: String, ssl: String) {
        
        UserManager().saveDomainData(account: account, APIKey: apiKey, SSL: ssl == "1" ? "https" : "http")
    }
}
