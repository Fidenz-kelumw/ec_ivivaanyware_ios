//
//  AdvanceSettingsTableViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import OLCOrm
import Firebase
import FirebaseInstanceID
import MBProgressHUD

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

class AdvanceSettingsTableViewController: SmartFMParentTableViewController, SettingsSwitchStatus {
    

    var progressHud: MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Advanced Settings"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        
//        UIApplication.shared.keyWindow?.rootViewController
    }
    
    
    //MARK: - private methods
    fileprivate func showProgress(userInteraction: Bool) {
        
        self.progressHud = MBProgressHUD(view: self.view)
        progressHud?.isUserInteractionEnabled = !userInteraction
        self.view.addSubview(progressHud!)
        progressHud?.show(animated: true)
    }
    
    fileprivate func hideProgress() {
        self.progressHud?.hide(animated: true)
    }
    
    ///////////////////////////////////////////
    //MARK: - Table view
    ///////////////////////////////////////////
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 7
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            
        case 0:
            return 2
        case 1:
            return 3
        case 2:
            return 1
        case 3:
            return 1
        case 4:
            return 1
        case 5:
            return 1
        case 6:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 30.0))
        
        switch section {
            
        case 0:
            label.text = "      REGISTRATION"
        case 1:
            label.text = "      SYNC SETTINGS"
        case 2:
            label.text = "      BACKGROUND TRACKING"
        case 3:
            label.text = "      INBOX SETTINGS"
        case 4:
            label.text = "      IMAGE UPLOAD SIZE"
        case 5:
            label.text = "      RESET"
        case 6:
            label.text = "      ERROR LOG"
        default:
            label.text = "      "
        }
        
        label.font = UIFont(name: "Melbourne", size: 16)!
        label.textColor = ThemeTextColor
        label.backgroundColor = ThemeBackgroundColor
        
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath as NSIndexPath).section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            if (indexPath as NSIndexPath).row == 0 {
                var accString="N/A"
                if let acc = UserManager().getAccount() {
                    accString = acc
                }
                cell.hideValue(false)
                cell.setCellFont("Default")
                cell.setValues("Account", value: accString)
                
            } else if (indexPath as NSIndexPath).row == 1 {
                var domainString = "N/A"
                if let domain = UserManager().getUserDomain() {
                    domainString = domain
                }
                cell.hideValue(false)
                cell.setCellFont("Default")
                cell.setValues("Domain", value: domainString)
            }
            
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if (indexPath as NSIndexPath).section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            if (indexPath as NSIndexPath).row == 0 {
                var masterString = "N/A"
                if let defaults = Defaults.all() as? [Defaults] {
                    
                    if defaults.count > 0 {
                        
                        let defaults1=defaults[0]
                        if let master = defaults1.MasterDataExpiryHours {
                            masterString = master
                        }
                    }
                }
                cell.setValues("Master Data Exp. Time(h)", value: masterString)
                
            } else if (indexPath as NSIndexPath).row == 1 {
                
                var transString = "N/A"
                if let defaults = Defaults.all() as? [Defaults] {
                    
                    if defaults.count > 0 {
                        
                        let defaults1=defaults[0]
                        if let transac = defaults1.TransactionExpirySeconds {
                            transString = transac
                        }
                    }
                }
                cell.setValues("Transaction Data Exp. Time(s)", value: transString)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                
            } else if (indexPath as NSIndexPath).row == 2 {
                
                var transString = "N/A"
                if let defaults = Defaults.all() as? [Defaults] {
                    
                    if defaults.count > 0 {
                        
                        let defaults1=defaults[0]
                        if let transac = defaults1.NearestSpotUpdateSeconds {
                            transString = transac
                        }
                    }
                }
                cell.setValues("Nearest Spot Update Duration (s)", value: transString)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                
            }
//            else if (indexPath as NSIndexPath).row == 3 {
//
//                var transString = "N/A"
//                if let defaults = Defaults.all() as? [Defaults] {
//
//                    if defaults.count > 0 {
//
//                        let defaults1=defaults[0]
//                        if let transac = defaults1.UserLocationUpdateMins {
//                            transString = transac
//                        }
//                    }
//                }
//                cell.setValues("Location Sync Duration (min)", value: transString)
//                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
//            }
            
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else if (indexPath as NSIndexPath).section == 2{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCellSwitch") as! SettingsTableViewCellSwitch
            cell.setValues("Track User Location", value: false)
            
            if let defaults = Defaults.all() as? [Defaults] {
                
                if defaults.count > 0 {
                    let defaults1=defaults[0]
                    if let status = defaults1.isUserLocationUpdateON {
                        cell.setValues("Track User Location", value: (status == "0" ? false:true))
                    }
                }
            }
            
            cell.delegate = self
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else if (indexPath as NSIndexPath).section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            if (indexPath as NSIndexPath).row == 0 {
                var size = "N/A"
                if let settings = Defaults.all() as? [Defaults] {
                    
                    if settings.count > 0 {
                        
                        let setting=settings[0]
                        if let inboxSize = setting.InboxSize {
                            size = inboxSize
                        }
                    }
                }
                cell.setValues("Inbox Size", value: size)
                
            }
            
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else if (indexPath as NSIndexPath).section == 4{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            if let uploadSize = UserManager().getUploadSize() {
                cell.setValues(uploadSize.rawValue, value: "")
            }
            cell.hideValue()
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else if (indexPath as NSIndexPath).section == 5{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            cell.setValues("Reset App", value: "")
            cell.hideValue()
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else if (indexPath as NSIndexPath).section == 6{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
            cell.setValues("Error Log", value: "")
            cell.hideValue()
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            return cell
            
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if (indexPath as NSIndexPath).section == 1 {
            
            if (indexPath as NSIndexPath).row == 0 {
                
                let alert: UIAlertController = UIAlertController(title: "Master Data Exp Time", message: "Set the master data expiration time in hours", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addTextField(configurationHandler: { (txtField) -> Void in
                    
                    txtField.keyboardType = UIKeyboardType.decimalPad
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                    
                    let txtTime = alert.textFields![0]
                    
                    if let text = txtTime.text {
                        
                        if let _ = text.toDouble() {
                            
                            if let defaults = Defaults.all() as? [Defaults] {
                                if defaults.count > 0 {
                                    let def = defaults[0]
                                    def.MasterDataExpiryHours = text
                                    def.update()
                                    tableView.reloadData()
                                }
                            }
                            
                        } else{
                            
                            
                        }
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else if (indexPath as NSIndexPath).row == 1 {
                
                let alert: UIAlertController = UIAlertController(title: "Transaction Data Exp Time", message: "Set the transaction data expiration time in seconds", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addTextField(configurationHandler: { (txtField) -> Void in
                    
                    txtField.keyboardType = UIKeyboardType.decimalPad
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                    
                    let txtTime = alert.textFields![0]
                    
                    if let text = txtTime.text {
                        
                        if let _ = text.toDouble() {
                            
                            if let defaults = Defaults.all() as? [Defaults] {
                                if defaults.count > 0 {
                                    let def = defaults[0]
                                    def.TransactionExpirySeconds = text
                                    def.update()
                                    tableView.reloadData()
                                }
                            }
                            
                        } else{
                            
                            
                        }
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }  else if (indexPath as NSIndexPath).row == 2 {
                
                let alert: UIAlertController = UIAlertController(title: "Nearest Spot Update Duration", message: "Set the nearest spot update duration in seconds", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addTextField(configurationHandler: { (txtField) -> Void in
                    
                    txtField.keyboardType = UIKeyboardType.decimalPad
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                    
                    let txtTime = alert.textFields![0]
                    
                    if let text = txtTime.text {
                        
                        if let _ = text.toDouble() {
                            
                            if let defaults = Defaults.all() as? [Defaults] {
                                if defaults.count > 0 {
                                    let def = defaults[0]
                                    def.NearestSpotUpdateSeconds = text
                                    def.update()
                                    tableView.reloadData()
                                }
                            }
                            
                        } else{
                            
                            
                        }
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else if (indexPath as NSIndexPath).row == 3 {
                
                let alert: UIAlertController = UIAlertController(title: "Location Sync Duration", message: "Set the location sync duration in minutes", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addTextField(configurationHandler: { (txtField) -> Void in
                    
                    txtField.keyboardType = UIKeyboardType.decimalPad
                })
                
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                    
                    let txtTime = alert.textFields![0]
                    
                    if let text = txtTime.text {
                        
                        if let _ = text.toDouble() {
                            
                            if let defaults = Defaults.all() as? [Defaults] {
                                if defaults.count > 0 {
                                    let def = defaults[0]
                                    def.UserLocationUpdateMins = text
                                    def.update()
                                    tableView.reloadData()
                                }
                            }
                            
                        } else{
                            
                            
                        }
                    }
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
            
        } else if (indexPath as NSIndexPath).section == 3 && (indexPath as NSIndexPath).row == 0 {
            
            
            let alert: UIAlertController = UIAlertController(title: "Inbox", message: "Set the maximum number of messages notification inbox can hold", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addTextField(configurationHandler: { (txtField) -> Void in
                
                txtField.keyboardType = UIKeyboardType.decimalPad
            })
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                
                let txtTime = alert.textFields![0]
                
                if let text = txtTime.text {
                    
                    if let _ = Int(text) {
                        
                        if let settings = Defaults.all() as? [Defaults] {
                            if settings.count > 0 {
                                let setting = settings[0]
                                setting.InboxSize = text
                                setting.update()
                                tableView.reloadData()
                            }
                        }
                        
                    } else{
                        
                        
                    }
                }
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }  else if (indexPath as NSIndexPath).section == 4 && (indexPath as NSIndexPath).row == 0 {
            
            let storyboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
            if let nvc = storyboard.instantiateViewController(withIdentifier: "SelectPopupWithQR") as? UINavigationController {
                
                nvc.navigationBar.barTintColor = popupHeaderColor
                nvc.navigationBar.tintColor = UIColor.white
                let textAttributes = [
                    NSForegroundColorAttributeName: UIColor.white,
                    NSFontAttributeName: UIFont(name: "Melbourne", size: 21)!
                ]
                nvc.navigationBar.titleTextAttributes = textAttributes
            
                if let vc = nvc.viewControllers.first as? SelectBuildingViewController {
                    vc.hasHeader = false
                    vc.hasQRScanner = false
                    vc.delegate = self
                    vc.selectedAction = SelectedAction.dismiss
                    vc.selectType = SelectType.ImageUploadSize
                    vc.titleString = "Image Upload Size"
                    vc.items = ["Small", "Medium","Large","Original"]
                    let formSheet: MZFormSheetController = MZFormSheetController(viewController: nvc)
                    formSheet.shouldCenterVertically = true
                    formSheet.shouldDismissOnBackgroundViewTap = true
                    formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 300.0)
                    formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
                    
                    formSheet.present(animated: true, completionHandler: nil)
                }
            }
            tableView.deselectRow(at: indexPath, animated: true)
            
        } else if (indexPath as NSIndexPath).section == 5 && (indexPath as NSIndexPath).row == 0 {
            
            let alert = UIAlertController(title: "Reset App?", message: "Are you sure you want to reset the app?", preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: "Reset", style: UIAlertActionStyle.destructive, handler: { (action) -> Void in
                
                self.showProgress(userInteraction: false)
                UserManager().wipeUserData(completion: { 
                    self.hideProgress()
                    InstanceID.instanceID().deleteID(handler: { (error) in
                        if let er = error {
                            NSLog("Failed to reset firebase instance. \(er)")
                        } else {
                            NSLog("Successfully unsubscribed from remote notifications")
                        }
                    })
                    
                    self.performSegue(withIdentifier: "resetApp", sender: nil)
                })
                
               
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        } else if (indexPath as NSIndexPath).section == 6 && (indexPath as NSIndexPath).row == 0 {
            self.performSegue(withIdentifier: "ShowErrors", sender: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = tableRowHighlightColor
        cell.selectedBackgroundView = bgColorView
    }
    
    func switchStatusChange(status: Bool) {
        if let defaults = Defaults.all() as? [Defaults], defaults.count > 0{
            defaults[0].isUserLocationUpdateON = (status ? "1":"0")
            defaults[0].update()
            
            if status {
                BackgroundLocationService.sharedInstance.startMonitoringSignificantLocationChanges()
            } else {
                BackgroundLocationService.sharedInstance.stopMonitoringSignificantLocationChanges()
            }
        }
    }
}


extension AdvanceSettingsTableViewController: GenericFormDelegate {
    
    func setSelectedItem(_ item: AnyObject) {
        
        let selected = item as! String
        if let new = ImageUploadSize(rawValue: selected) {
            UserManager().setUploadSize(new)
        } else {
            UserManager().setUploadSize(ImageUploadSize.Medium)
        }
        
        tableView.reloadRows(at: [IndexPath(row: 0, section: 3)], with: UITableViewRowAnimation.automatic)
    }
}
