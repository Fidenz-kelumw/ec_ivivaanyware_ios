//
//  ErrorLogTableViewCell.swift
//  Smart Office
//
//  Created by Lasith Hettiarachchi on 10/23/15.
//  Copyright © 2015 fidenz. All rights reserved.
//

import UIKit

class ErrorLogTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblErrorMessage: UILabel!
    @IBOutlet weak var lblApiNAme: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
