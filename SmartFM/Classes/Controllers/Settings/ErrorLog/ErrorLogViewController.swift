//
//  ErrorLogViewController.swift
//  Smart Office
//
//  Created by Lasith Hettiarachchi on 10/23/15.
//  Copyright © 2015 fidenz. All rights reserved.
//

import UIKit

class ErrorLogViewController: UIViewController {
    
    var logs: [LIHLog] = []
    
    
    @IBOutlet weak var tableErrorLog: UITableView!
    
    let refreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.refreshControl.addTarget(self, action: #selector(ErrorLogViewController.refreshTable(_:)), for: UIControlEvents.valueChanged)
        self.tableErrorLog.addSubview(self.refreshControl)
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK - Private Methods
    func loadData() {
        
        self.logs = LIHLogManager.getRecords(200)
        self.tableErrorLog.reloadData()
    }
    
    //MARK: - Events

    @IBAction func clear(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to clear the log?", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Clear", style: UIAlertActionStyle.destructive, handler: { (_) -> Void in
            
            LIHLogManager.clearLog()
            self.loadData()
            self.tableErrorLog.reloadData()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func backPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func refreshTable(_ sender: AnyObject) {
        
        self.logs = LIHLogManager.getRecords(nil)
        self.tableErrorLog.reloadData()
        self.refreshControl.endRefreshing()
    }
    
}


extension ErrorLogViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.logs.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "errorLogCell") as! ErrorLogTableViewCell
        
        cell.lblDateTime.text = "\(logs[(indexPath as NSIndexPath).row].date)  \(logs[(indexPath as NSIndexPath).row].time)"
        cell.lblApiNAme.text = logs[(indexPath as NSIndexPath).row].title
        cell.lblErrorMessage.text = logs[(indexPath as NSIndexPath).row].message
        
        return cell
    }
}
