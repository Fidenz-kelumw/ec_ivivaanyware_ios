//
//  ErrorLogger.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/12/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class ErrorLogger {
    
    static func logError(_ message: String, response: HTTPURLResponse?, apiName: String) {
        
        if let urlResponse = response {
            
            if let contentType = urlResponse.allHeaderFields["Content-Type"] as? String {
                
                let pattern = "text/plain"
                
                if let regX = try? NSRegularExpression(pattern: pattern, options: []) {
                    let matches = regX.matches(in: contentType, options: [], range: NSRange(location: 0, length: contentType.characters.count))
                    if matches.count > 0 {
                        
                        LIHLogManager.addToLog(message, title: apiName)
                    }
                }
            }
        } else {
            LIHLogManager.addToLog(message, title: apiName)
        }
    }
}
