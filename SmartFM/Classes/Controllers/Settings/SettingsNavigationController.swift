    //
//  SettingsNavigationController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SettingsNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsNavigationController.performGoToHome), name: NSNotification.Name(rawValue: "sessionExpired"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func performGoToHome() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
