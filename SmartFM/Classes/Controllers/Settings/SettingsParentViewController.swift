//
//  SettingsParentViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SettingsParentViewController: SmartFMParentViewController, SettingsDelegate {

    
    fileprivate var processingAlert: LIHAlert?
    fileprivate var successAlert: LIHAlert?
    fileprivate var failedAlert: LIHAlert?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initAlerts()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SettingsChild" {
            if let vc = segue.destination as? SettingsTableViewController {
                vc.delegate = self
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    ///////////////////////////////////////////
    //MARK: - Private Methods
    ///////////////////////////////////////////
    fileprivate func initAlerts() {
        
        self.processingAlert = LIHAlertManager.getProcessingAlert("Downloading...")
        AlertManager().configProcessing(self.processingAlert, hasNavBar: true)
        self.processingAlert?.initAlert(self.view)
        
        self.successAlert = LIHAlertManager.getSuccessAlert("Successfully downloaded")
        AlertManager().configSuccessAlert(self.successAlert, hasNavBar: true)
        self.successAlert?.initAlert(self.view)
        
        self.failedAlert = LIHAlertManager.getErrorAlert("Failed to download data")
        AlertManager().configErrorAlert(self.failedAlert, hasNavBar: true)
        self.failedAlert?.initAlert(self.view)
    }
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    
    @IBAction func close(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    ///////////////////////////////////////////
    //MARK: - Delegate Methods
    ///////////////////////////////////////////
    
    func showDownloading(_ completion:(()->Void)?) {
        
        self.processingAlert?.show({ () -> () in
            completion?()
            }, hidden: nil)
    }
    
    func hideDownloading() {
        
        self.processingAlert?.hideAlert(nil)
    }
    
    func showSuccess(_ completion:(()->Void)?) {
        
        self.successAlert?.show({ () -> () in
            completion?()
            }, hidden: nil)
    }
    
    func showFailed() {
        
        self.failedAlert?.show(nil, hidden: nil)
    }
    
    func goToAdvanceSettings() {
        
        performSegue(withIdentifier: "AdvanceSettings", sender: nil)
    }
}
