//
//  SettingsTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    var cellValueFont: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellFont(_ font: String) {
        self.cellValueFont = font
    }

    func setValues(_ key: String, value: String) {
        self.lblKey.text = key
        self.lblValue.text = value
        
        if self.cellValueFont != nil {
            self.lblValue.font = UIFont.systemFont(ofSize: 15.0)
        } else {
            self.lblValue.font = UIFont(name: "Melbourne", size: 16.0)
        }
    }
    
    func hideValue(_ status: Bool? = true) {
        self.lblValue.isHidden = status!
    }
}
