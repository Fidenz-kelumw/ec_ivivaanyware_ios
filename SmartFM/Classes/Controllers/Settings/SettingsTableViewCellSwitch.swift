//
//  SettingsTableViewCellSwitch.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 2/8/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol SettingsSwitchStatus {
    func switchStatusChange(status: Bool)
}

class SettingsTableViewCellSwitch: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    var delegate: SettingsSwitchStatus?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func onSwitchValueChange(_ sender: Any) {
        self.delegate?.switchStatusChange(status: self.switchButton.isOn)
    }
    
    func setValues(_ key: String, value: Bool) {
        self.lblMessage.text = key
        self.switchButton.isOn = value
    }
}
