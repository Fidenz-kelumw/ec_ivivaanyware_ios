//
//  SettingsTableViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

protocol SettingsDelegate {
    
    func showDownloading(_ completion:(()->Void)?)
    
    func hideDownloading()
    
    func showSuccess(_ completion:(()->Void)?)
    
    func showFailed()
    
    func goToAdvanceSettings()
}

class SettingsTableViewController: UITableViewController {

    var delegate: SettingsDelegate?
    
    ///////////////////////////////////////////
    //MARK: - View controller
    ///////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    ///////////////////////////////////////////
    //MARK: - Table view
    ///////////////////////////////////////////
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        switch section {
            
        case 0:
            return 1
        case 1:
            return 5
        case 2:
            return 1
        case 3:
            return 2
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 30.0))
        
        switch section {
            
        case 0:
            label.text = "      USER INFO"
        case 1:
            label.text = "      SYNC CONFIGURATIONS"
        case 2:
            label.text = "      ADVANCED SETTINGS"
        case 3:
            label.text = "      APP INFO"
        default:
            label.text = "      "
        }
        
        label.font = UIFont(name: "Melbourne", size: 16)!
//        label.font = label.font.fontWithSize(13)
        label.textColor = ThemeTextColor
        label.backgroundColor = ThemeBackgroundColor
        
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsTableViewCell
        
        switch (indexPath as NSIndexPath).section {
            
        case 0:
            var userId = "N/A"
            if let usr = UserManager().getUserId() {
                userId = usr
            }
            
            cell.setCellFont("Melbourne")
            cell.setValues("User ID", value: userId)
            
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            
        case 1:
            
            var buildingST = "N/A"
            var objectTypeST = "N/A"
            var filterST = "N/A"
            var defDataST = "N/A"
            if let allTimes = SyncTime.all() as? [SyncTime] {
                
                let buildingSTs = allTimes.filter({ $0.EntityKey == EntityKey.Building.rawValue })
                let objectTypeSTs = allTimes.filter({ $0.EntityKey == EntityKey.ObjectType.rawValue })
                let filterSTs = allTimes.filter({ $0.EntityKey == EntityKey.Filter.rawValue })
                let defDataSTs = allTimes.filter({ $0.EntityKey == EntityKey.DefaultData.rawValue })
                
                if buildingSTs.count > 0 {
                    if let buildingSTdt = buildingSTs[0].UpdatedAt {
                        let stringDate = DateTimeManager().dateToString(buildingSTdt, format: "dd-MM-yyyy HH:mm")
                        buildingST = stringDate
                    }
                }
                
                if objectTypeSTs.count > 0 {
                    if let objectTypeSTdt = objectTypeSTs[0].UpdatedAt {
                        let stringDate = DateTimeManager().dateToString(objectTypeSTdt, format: "dd-MM-yyyy HH:mm")
                        objectTypeST = stringDate
                    }
                }
                
                if filterSTs.count > 0 {
                    if let filterSTdt = filterSTs[0].UpdatedAt {
                        let stringDate = DateTimeManager().dateToString(filterSTdt, format: "dd-MM-yyyy HH:mm")
                        filterST = stringDate
                    }
                }
                
                if defDataSTs.count > 0 {
                    if let defDataSTdt = defDataSTs[0].UpdatedAt {
                        let stringDate = DateTimeManager().dateToString(defDataSTdt, format: "dd-MM-yyyy HH:mm")
                        defDataST = stringDate
                    }
                }
            }
            
            if (indexPath as NSIndexPath).row == 0 {
                
                cell.setValues("Download All Data", value: "")
                cell.hideValue()
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCellSelectionStyle.default
                
            } else if (indexPath as NSIndexPath).row == 1 {
                cell.setValues("Buildings [\(Building.all().count)]", value: buildingST)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCellSelectionStyle.default
            } else if (indexPath as NSIndexPath).row == 2 {
                cell.setValues("Object Types [\(ObjType.all().count)]", value: objectTypeST)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCellSelectionStyle.default
            } else if (indexPath as NSIndexPath).row == 3 {
                cell.setValues("Filters [\(Filter.all().count)]", value: filterST)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCellSelectionStyle.default
            } else {
                cell.setValues("Default Data", value: defDataST)
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCellSelectionStyle.default
            }
            break
            
            
            
        case 2:
            
            cell.setValues("Advanced Settings", value: "")
            cell.hideValue()
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            
            
            
        case 3:
            if (indexPath as NSIndexPath).row == 0 {
                var versionString = ""
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    versionString = version
                }
                cell.setValues("App Version", value: versionString)
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                
            } else {
                var buildString = ""
                if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                    buildString = build
                }
                cell.setValues("Build Version", value: buildString)
                cell.accessoryType = UITableViewCellAccessoryType.none
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
            }
            
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath as NSIndexPath).section == 1 {
            
            if (indexPath as NSIndexPath).row == 0 {
//                self.delegate?.showDownloading({ () -> Void in
//                    self.delegate?.hideDownloading()
//                })
                
                SyncManager().syncMasterData(withTimer: false, completion:  { (success) -> Void in
                    
                    if success {
                        self.delegate?.showSuccess({ () -> Void in
                            self.tableView.reloadData()
                        })
                        
                    } else {
                        self.delegate?.showFailed()
                    }
                })
                
            } else if (indexPath as NSIndexPath).row == 1 {
                self.delegate?.showDownloading({ () -> Void in
                    SyncManager().syncBuildings({ (success) -> Void in
                        self.delegate?.hideDownloading()
                        if success {
                            self.delegate?.showSuccess({ () -> Void in
                                self.tableView.reloadData()
                            })
                            
                        } else {
                            self.delegate?.showFailed()
                        }
                    })
                })
                
            } else if (indexPath as NSIndexPath).row == 2 {
                self.delegate?.showDownloading({ () -> Void in
                    SyncManager().syncObjectTypes({ (success) -> Void in
                        self.delegate?.hideDownloading()
                        if success {
                            self.delegate?.showSuccess({ () -> Void in
                                self.tableView.reloadData()
                            })
                            
                        } else {
                            self.delegate?.showFailed()
                        }
                    })
                })
                
            } else if (indexPath as NSIndexPath).row == 3 {
                self.delegate?.showDownloading({ () -> Void in
                    SyncManager().syncFilters({ (success) -> Void in
                        self.delegate?.hideDownloading()
                        if success {
                            self.delegate?.showSuccess({ () -> Void in
                                self.tableView.reloadData()
                            })
                            
                        } else {
                            self.delegate?.showFailed()
                        }
                    })
                })
                
            } else if (indexPath as NSIndexPath).row == 4 {
                self.delegate?.showDownloading({ () -> Void in
                    SyncManager().syncDefaultData({ (success) -> Void in
                        self.delegate?.hideDownloading()
                        if success {
                            self.delegate?.showSuccess({ () -> Void in
                                self.tableView.reloadData()
                            })
                            
                        } else {
                            self.delegate?.showFailed()
                        }
                    })
                })
                
            }
        } else if (indexPath as NSIndexPath).section == 2 {
            if (indexPath as NSIndexPath).row == 0 {
                self.delegate?.goToAdvanceSettings()
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = tableRowHighlightColor
        cell.selectedBackgroundView = bgColorView
    }
}
