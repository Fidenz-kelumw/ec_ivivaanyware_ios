//
//  SplashViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/5/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    var showObjectDetails: PushMessage?
    var locationMessage: LocationNotificationMessage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        let userManager = UserManager()
        
        if let _ = userManager.getAccount(), let _ = userManager.getUserKey(), let _ = userManager.getApiKey() {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainNav") as? UINavigationController {
                
                if let hvc = vc.viewControllers.first as? ViewController {
                    hvc.showObjectDetails = self.showObjectDetails
                    hvc.locationMessage = self.locationMessage
                }
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: true, completion: nil)
                return
            }
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "Register") {
            
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }

    
    
    ///////////////////////////////////////////
    //MARK: - Events
    ///////////////////////////////////////////
    
    @IBAction func reset(_ sender: UIStoryboardSegue) {
        
    }
}
