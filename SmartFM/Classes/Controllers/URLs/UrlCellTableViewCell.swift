//
//  UrlCellTableViewCell.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class UrlCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(url: ObjectURL) {
        
        self.lblTitle.text = url.URLName
        
        if let date = url.UpdatedAt {
            let dateManager = DateTimeManager()
            self.lblSubTitle.text = dateManager.getDateTimeFromISOString(date)
        }
    }
}
