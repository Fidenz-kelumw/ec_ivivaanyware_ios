//
//  UrlsViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class UrlsViewController: UIViewController, SetDateFilterDelegate {
    
    //IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet var btnSetFilter: UIButton!
    @IBOutlet var btnFilterClear: UIButton!
    
    //private members
    fileprivate var urls: [ObjectURL] = []
    fileprivate var originalUrls: [ObjectURL] = []
    fileprivate var refreshControl: UIRefreshControl?
    
    fileprivate let colorRed = UIColor(rgb: 0x00E8041E)
    fileprivate let colorGray = UIColor(rgb: 0x00282F34)
    
    //public members
    var objectType: String?
    var objectKey: String?
    var tabName: String?
    var delegate: GenericFormDelegate?
    var forceSync:Bool?
    
    
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.hideLoading()
        
        self.fetchData { (success, error) in
            if !success {
                var errorMessage = "Fetching URLs failed"
                if let er = error {
                    errorMessage = er
                }
                self.delegate?.showError?("\(errorMessage)")
            }
            self.tableView.reloadData()
        }
        
        self.tableView.register(UINib(nibName: "EmptyCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "emptyCell")
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(MessagesViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl!)
        self.buttonState(btnFilterClear, false)
    }
    
    
    
    
    //////////////////////////////////////////////////////
    //MARK: - Private Methods
    //////////////////////////////////////////////////////
    
    fileprivate func fetchData(completion: ((Bool, String?)->Void)?) {
        
        if self.forceSync == nil{
            self.forceSync = false
        }
        
        if !self.forceSync!{
            if SyncTimeManager().hasTimerExpired(EntityKey.URLs) {
                
                if let ot = self.objectType, let ok = self.objectKey, let tn  = self.tabName {
                    
                    
                    self.showLoading()
                    SyncManager().syncUrls(ot, objectKey: ok, tabName: tn, success: { (success, error) in
                        
                        self.hideLoading()
                        
                        if success {
                            completion?(self.loadUrls(), nil)
                        } else {
                            completion?(false, error)
                        }
                    })
                } else {
                    completion?(false, nil)
                }
                
            } else {
                
                self.hideLoading()
                completion?(self.loadUrls(), nil)
            }
        } else{
            
            if let ot = self.objectType, let ok = self.objectKey, let tn  = self.tabName {
                
                
                self.showLoading()
                SyncManager().syncUrls(ot, objectKey: ok, tabName: tn, success: { (success, error) in
                    
                    self.hideLoading()
                    
                    if success {
                        completion?(self.loadUrls(), nil)
                    } else {
                        completion?(false, error)
                    }
                })
            } else {
                completion?(false, nil)
            }
        }
        
        
    }
    
    fileprivate func loadUrls() -> Bool {
        
        if let urls = ObjectURL.all() as? [ObjectURL] {
            self.urls = urls
            self.originalUrls = urls
            return true
        }
        return false
    }
    
    fileprivate func showLoading() {
        
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.view.bringSubview(toFront: self.parentView)
    }
    
    fileprivate func hideLoading() {
        
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.parentView.isHidden = true
        
        self.view.sendSubview(toBack: self.parentView)
    }
    
    fileprivate func showDateFilterAlert() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Popups", bundle: Bundle.main)
        
        if let vc = storyboard.instantiateViewController(withIdentifier: "DateFilterPopup") as? SetDateFilterViewController {
            vc.delegate = self
            
            if (btnSetFilter.tag != 0){
                let data = btnSetFilter.titleLabel?.text?.components(separatedBy: " to ")
                
                if let arr = data {
                    vc.startDateFromController = arr[0]
                    vc.endDateFromController = arr[1]
                }
            }
            
            let formSheet: MZFormSheetController = MZFormSheetController(viewController: vc)
            formSheet.shouldCenterVertically = true
            formSheet.shouldDismissOnBackgroundViewTap = true
            formSheet.presentedFormSheetSize = CGSize(width: 300.0, height: 207.0)
            formSheet.transitionStyle = MZFormSheetTransitionStyle.fade
            
            formSheet.present(animated: true, completionHandler: nil)
            
        }
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Events
    //////////////////////////////////////////////////////
    
    func refresh(_ sender: UIRefreshControl) {
        
        self.btnSetFilter.tag = 0
        self.buttonState(btnFilterClear, false)
        self.btnSetFilter.setTitle("Set Date Filter", for: UIControlState.normal)
        
        if let ot = self.objectType, let ok = self.objectKey, let tn  = self.tabName {
            SyncManager().syncUrls(ot, objectKey: ok, tabName: tn, success: { (success, error) in
                
                self.refreshControl?.endRefreshing()
                
                if success {
                    _ = self.loadUrls()
                    self.tableView.reloadData()
                } else {
                    
                    var erMsg = "Fetching URLs failed"
                    if let er = error {
                        erMsg = er
                    }
                    self.delegate?.showError?("\(erMsg)")
                }
            })
        }
    }
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////
    
    func filterWithDate(startDate: String, endDate: String) {
        self.btnSetFilter.setTitle("\(startDate) to \(endDate)", for: UIControlState.normal)
        self.btnSetFilter.tag = 1
        
        self.buttonState(btnFilterClear, true)
        self.filterList(false, startDate, endDate)
    }
    
    @IBAction func setDateFilterPressed(_ sender: Any) {
        self.showDateFilterAlert()
    }
    
    @IBAction func clearPressed(_ sender: Any) {
        self.btnSetFilter.tag = 0
        self.buttonState(btnFilterClear, false)
        self.btnSetFilter.setTitle("Set Date Filter", for: UIControlState.normal)
        self.filterList(true, nil, nil)
    }
    
    private func buttonState(_ button: UIButton, _ state: Bool) {
        if (state) {
            button.backgroundColor = colorRed
            button.isEnabled = true
        } else {
            button.backgroundColor = colorGray
            button.isEnabled = false
        }
    }
    
    private func getDate(date: String) -> Date {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
    
        return dateFormatter.date(from: date)!
    }
    
    private func filterList(_ isReset: Bool, _ sDate: String?, _ eDate: String?) {
        
        if(isReset){
            self.urls = self.originalUrls
            self.tableView.reloadData()
        } else {
            if (btnSetFilter.tag != 0){
                
                if let sD = sDate, let eD = eDate {
                    let startDate = self.getDate(date: sD)
                    let endDate = self.getDate(date: eD)
                    
                    var tempURLCollection:[ObjectURL] = []
                    
                    for u in originalUrls {
                        
                        if let dateString = u.UpdatedAt {
                            let tempDate = dateString.components(separatedBy: "T")
                            if tempDate.count > 1 {
                                let date = self.getDate(date: tempDate[0])
                                if (date.isBetweeen(date: startDate, andDate: endDate)) {
                                    tempURLCollection.append(u)
                                }
                            }
                        }
                    }
                    
                    self.urls = tempURLCollection
                    self.tableView.reloadData()
                }
            }
        }
    }
}


extension UrlsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.urls.count == 0 {
            return 1
        }
        return self.urls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.urls.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell") as! EmptyCellTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "URLCell") as! UrlCellTableViewCell
        cell.setValues(url: self.urls[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selected = self.urls[indexPath.row]
        
        if let nvc = self.storyboard?.instantiateViewController(withIdentifier: "WebController") as? UINavigationController {
            
            if let vc = nvc.viewControllers.first as? WebViewController, let urlString = selected.URLPath, urlString != "" {
                
                if let url = URL(string: urlString) {
                    vc.webUrl = url
                    vc.delegate = self.delegate
                    self.delegate?.showVc?(nvc)
                }
            }
        }
    }
}
