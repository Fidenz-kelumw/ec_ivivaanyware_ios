//
//  WebViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    //IBOutlets
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    //private members
    
    
    
    //public members
    var webUrl: URL?
    var delegate: GenericFormDelegate?
    var loaded = false
    
    
    //////////////////////////////////////////////////////
    //MARK: - View Controller Methods
    //////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = self.webUrl {
            
            let req = URLRequest(url: url)
            self.webview.loadRequest(req)
            
            self.webview.delegate = self
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false //
    }
    
    
    //////////////////////////////////////////////////////
    //MARK: - Private Methods
    //////////////////////////////////////////////////////
    
    
    
    
    
    //////////////////////////////////////////////////////
    //MARK: - Events
    //////////////////////////////////////////////////////
    @IBAction func closePressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //////////////////////////////////////////////////////
    //MARK: - Delegate Methods
    //////////////////////////////////////////////////////

    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        
        self.webview.scrollView.minimumZoomScale = 1.0
        self.webview.scrollView.maximumZoomScale = 5.0
        self.webview.stringByEvaluatingJavaScript(from: "document.querySelector('meta[name=viewport]').setAttribute('content', 'user-scalable = 1;', false); ")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.delegate?.showError?("Failed to load URL")
    }
}
