//
//  VideoPlayerViewController.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/24/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import UIKit
import AVKit

protocol VideoPlayerDelegate {
    func donePressed()
}

class VideoPlayerViewController: AVPlayerViewController {

    var videoPlayerDelegate: VideoPlayerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.videoPlayerDelegate?.donePressed()
    }
}
