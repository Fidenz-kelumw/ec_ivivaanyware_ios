//
//  MyVisitorsViewController.swift
//  SmartFM
//
//  Created by Tharindu on 4/18/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MyVisitorsViewController: UIViewController, IndicatorInfoProvider {

    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar.layer.borderWidth = 1
        self.searchBar.layer.borderColor = ThemeBackgroundColor.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "My Visitors")
    }

}
