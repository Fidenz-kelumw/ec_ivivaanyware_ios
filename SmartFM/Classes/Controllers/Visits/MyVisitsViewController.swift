//
//  MyVisitsViewController.swift
//  SmartFM
//
//  Created by Tharindu on 4/17/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class MyVisitsViewController: SegmentedPagerTabStripViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // change segmented style
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let visitsView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VisitsView") as! VisitsViewController
        let myVisitorsView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyVisitorsView") as! MyVisitorsViewController
        return [visitsView, myVisitorsView]
        
    }

}
