//
//  VisitTableViewCell.swift
//  SmartFM
//
//  Created by Tharindu on 4/17/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class VisitTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.containerView.layer.cornerRadius = 5

        // Configure the view for the selected state
    }

}
