//
//  PointerView.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/14/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import UIKit

class PointerView: UIView {
    
    @IBInspectable var width: CGFloat = 40
    @IBInspectable var height: CGFloat = 40
//    @IBInspectable var color: CGFloat = 40
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.width/2, y: self.height))
        path.addLine(to: CGPoint(x: self.width, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        
        
        let mask = CAShapeLayer()
        mask.frame = self.bounds
        mask.path = path.cgPath
        
        self.layer.mask = mask
    }

}
