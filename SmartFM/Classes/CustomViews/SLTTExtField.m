//
//  SLTTExtField.m
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

#import "SLTTExtField.h"

@implementation SLTTExtField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) drawPlaceholderInRect:(CGRect)rect
{
    [[UIColor colorWithWhite:0.7 alpha:1.0] setFill];
    [[self placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:14] lineBreakMode:NSLineBreakByTruncatingTail];
}

@end
