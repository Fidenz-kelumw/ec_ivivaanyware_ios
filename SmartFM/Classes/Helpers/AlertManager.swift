//
//  AlertManager.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/28/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class AlertManager {
    
    func configProcessing(_ alert: LIHAlert?, hasNavBar: Bool) {
        alert?.hasNavigationBar = false
        alert?.contentTextFont = UIFont(name: "Melbourne", size: 18)
        alert?.alertHeight = hasNavBar ? 50 : 75
    }
    
    func configErrorAlert(_ alert: LIHAlert?, hasNavBar: Bool) {
        alert?.hasNavigationBar = false
        alert?.alertColor = ThemeRedColor
        alert?.contentTextFont = UIFont(name: "Melbourne", size: 18)
        alert?.alertHeight = hasNavBar ? 50 : 75
    }
    
    func configSuccessAlert(_ alert: LIHAlert?, hasNavBar: Bool) {
        alert?.hasNavigationBar = false
        alert?.contentTextFont = UIFont(name: "Melbourne", size: 18)
        alert?.alertHeight = hasNavBar ? 50 : 75
    }
}
