//
//  Configurations.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/9/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class Configurations {
    
    func configSelectionButton(forButton button: UIButton, withDisclosure: Bool) {
        
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        button.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 0.0)
        button.backgroundColor = SelectionButtonBackgroundColor
        
        if withDisclosure {
            let imageView: UIImageView = UIImageView(image: UIImage(named: "disclosureIndicator"), highlightedImage: nil)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            let top = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: button, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
            let bottom = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: button, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
            let right = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: button, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: -10)
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            button.superview?.addSubview(imageView)
            
            button.superview?.addConstraints([top,bottom,right])
        }
    }
}
