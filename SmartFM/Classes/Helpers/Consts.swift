//
//  Consts.swift
//  Headcount
//
//  Created by Lasith Hettiarachchi on 8/11/15.
//  Copyright (c) 2015 Lasith Hettiarachchi. All rights reserved.
//


import UIKit

// google map api key
let GOOGLE_MAP_API_KEY = "AIzaSyAypbZHJj3DBFGId0faCmxSLbhfyHZ8eTI"
// Key from Bandu : AIzaSyDCKMzheZCUgRoAC5szRyGN85_9aHfzbDk
// Key from Pasan : AIzaSyAnQ0roWNjWH1p4UKGvQYu2Jef78J_mEgM
// Development key: AIzaSyCmWUyKKKVEQ9kIk-gFmHmknOwaqqKOLXI

let π:CGFloat = CGFloat(M_PI)
//Colors
let ThemeBackgroundColor = UIColor(red: 24.0/255.0, green: 31.0/255.0, blue: 37.0/255.0, alpha: 1.0)
let TopHairLineColor = UIColor(red: 36.0/255.0, green: 45.0/255.0, blue: 52.0/255.0, alpha: 1.0)
let ThemeTextColor = UIColor(red: 207.0/255.0, green: 219.0/255.0, blue: 255.0/255.0, alpha: 1.0)
let ThemeRedColor = UIColor(red: 245.0/255.0, green: 57.0/255.0, blue: 58.0/255.0, alpha: 1.0)
let TableCellSelectionColor = UIColor(red: 34.0/255.0, green: 41.0/255.0, blue: 47.0/255.0, alpha: 1.0)

let TabSelectedColor = UIColor(red: 79.0/255.0, green: 88.0/255.0, blue: 95.0/255.0, alpha: 1.0)
let TabBackgroundColor = UIColor(red: 40.0/255.0, green: 47.0/255.0, blue: 52.0/255.0, alpha: 1.0)
let TabColor = UIColor(red: 36.0/255.0, green: 45.0/255.0, blue: 52.0/255.0, alpha: 1.0)

let textFieldDisabledColor = UIColor.colorWithRGB(red: 200, green: 200, blue: 200, alpha: 1)
let textFieldEnabledColor = UIColor.white
let DisabledButtonColor = UIColor.colorWithRGB(red: 64, green: 78, blue: 90, alpha: 0.30)
let DisabledButtonTextColor = UIColor.colorWithRGB(red: 255, green: 255, blue: 255, alpha: 0.45)
let EnabledButtonColor = UIColor.colorWithRGB(red: 64, green: 78, blue: 90, alpha: 1.0)
let EnabledButtonTextColor = UIColor.colorWithRGB(red: 255, green: 255, blue: 255, alpha: 1.0)

let ButtonsUnselectedColor: UIColor = UIColor.colorWithRGB(red: 19, green: 26, blue: 32, alpha: 1.0)

let alertErrorColor: UIColor = ThemeRedColor
//let alertErrorColor: UIColor = UIColor(red: 251.0/255.0, green: 117.0/255.0, blue: 127.0/255.0, alpha: 1.0)
let alertSuccessColor: UIColor = UIColor(red: 150.0/255.0, green: 202.0/255.0, blue: 78.0/255.0, alpha: 1.0)

let FrameBorderColor = UIColor.colorWithRGB(red: 92, green: 102, blue: 111, alpha: 1.0)
let textboxBorderColor = UIColor.colorWithRGB(red: 165, green: 165, blue: 165, alpha: 1.0)

let SelectionButtonBackgroundColor: UIColor = UIColor.colorWithRGB(red: 40, green: 47, blue: 53, alpha: 1.0)
let SelectionButtonDisabledBackgroundColor: UIColor = UIColor.colorWithRGB(red: 25, green: 32, blue: 38, alpha: 1.0)
let popupHeaderColor: UIColor = UIColor.colorWithRGB(red: 40, green: 47, blue: 53, alpha: 1.0)

let submitButtonColor = UIColor.colorWithRGB(red: 243, green: 58, blue: 56, alpha: 1.0)
let submitButtonDisabledColor = UIColor.colorWithRGB(red: 243, green: 58, blue: 56, alpha: 0.60)

let inboxReadCellColor = UIColor.colorWithRGB(red: 16, green: 24, blue: 30, alpha: 1.0)

let tableRowHighlightColor: UIColor = UIColor(red: 58.0/255.0, green: 65.0/255.0, blue: 78.0/255.0, alpha: 1.0)

let ISODateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let SyncDateFormat: String = "yyyy-MM-dd HH:mm:ss"

var Protocol = "http"

let sessionExpTime = 2.0 * 60.0 //seconds
let imagesKeepForDays = 7
let DefaultImagesLocation = "DefaultImages"

let CONNECTIVITY_ALERT_TITLE = "Connection Lost"
let CONNECTIVITY_ALERT_BODY = "Connection has been lost. Please check your internet connection.."
let NOTIFICATION_DATA_SPOTS = "NOTIFICATION_DATA_SPOTS"
