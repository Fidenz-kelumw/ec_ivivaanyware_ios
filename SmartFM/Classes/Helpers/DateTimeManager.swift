//
//  DateTimeManager.swift
//  Headcount
//
//  Created by Lasith Hettiarachchi on 8/26/15.
//  Copyright (c) 2015 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class DateTimeManager {
    
    
    func dateToString(_ date: Date, format: String) -> String {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = format
        let dateString: String = formatter.string(from: date)
        
        
        return dateString.components(separatedBy: "+")[0]
        
    }
    
    
    func dateToGMTString(_ date: Date, format: String) -> String {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        formatter.dateFormat = format
        let dateString: String = formatter.string(from: date)
        
        
        return dateString.components(separatedBy: "+")[0]
        
    }
    
    func stringToDate(_ dateString: String, format: String) -> Date? {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = format
        if let date: Date = formatter.date(from: dateString) {
            
            return date
        }
        
        return nil
    }
    
    func stringToGMTDate(_ dateString: String, format: String) -> Date? {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        formatter.dateFormat = format
        if let date: Date = formatter.date(from: dateString) {
            
            return date
        }
        
        return nil
    }
    
    func getDateFromDateTime(_ date: Date) -> String {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "yyyy-MM-dd"
        let dateOnly = formatter.string(from: date)
        
        return dateOnly
    }
    
    func getTimeFromDateTime(_ date: Date) -> String {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "HH:mm"
        let timeOnly = formatter.string(from: date)
        
        return timeOnly
    }
    

    
    func setTimeForNSDate(_ date: Date, hours: Int, minutes: Int, seconds: Int) -> Date? {
        
        let calendar: Calendar! = Calendar(identifier: Calendar.Identifier.gregorian)
        
        if let newDate = calendar.date(bySettingHour: hours, minute: minutes, second: seconds, of: date, matchingPolicy: Calendar.MatchingPolicy.nextTime, repeatedTimePolicy: Calendar.RepeatedTimePolicy.first, direction: Calendar.SearchDirection.forward) {
            return newDate
        }

        
        return nil
    }
    
    func getDateTimeFromISOString(_ ISOString: String) -> String {
        var dateOnly = ""
        var timeOnly = ""
        let dateAndTime = ISOString.components(separatedBy: "T")
        if dateAndTime.count >= 2 {
            dateOnly = dateAndTime[0]
            let timewithmil = dateAndTime[1].components(separatedBy: ".")[0]
            let timeArray = timewithmil.components(separatedBy: ":")
            if timeArray.count >= 2 {
                timeOnly = "\(timeArray[0]):\(timeArray[1])"
            }
        }
        
        return "\(dateOnly) \(timeOnly)"
    }
    
    func getDateOnlyFromISOString(_ ISOString: String) -> String? {
        var dateOnly = ""
        
        let dateAndTime = ISOString.components(separatedBy: "T")
        if dateAndTime.count >= 2 {
            dateOnly = dateAndTime[0]
            return dateOnly
        }
        
        return nil
    }
}
