//
//  FileHandler.swift
//  SafetyBriefing
//
//  Created by Lasith Hettiarachchi on 11/2/16.
//  Copyright © 2016 Fidenz. All rights reserved.
//

import Foundation
import UIKit

class FileHandler {
    
    func rotateImage(_ image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    static func getDirectory(forType type: MediaType) -> URL? {
        
        if let doc = self.getDocumentsDirectory() {
            _ = self.createFolder(folderName: type.rawValue, inDirectory: doc)
            return self.getDocumentsDirectory()?.appendingPathComponent(type.rawValue)
        }
        
        return nil
    }
    
    static func createFolder(folderName name: String, inDirectory path: URL) -> Bool {
        
        let dataPath = path.appendingPathComponent(name)
        do {
            try Foundation.FileManager.default.createDirectory(at: dataPath, withIntermediateDirectories: false, attributes: nil)
            return true
        } catch let error as NSError {
            NSLog("[createFolder] \(error.localizedDescription)")
            return false
        }
    }
    
    func getContentsOfAPath(path: URL) -> [URL] {
        
        do {
            return try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
        } catch _ as NSError {
            return []
        }
    }
    
    
    static func getDocumentsDirectory() -> URL? {
        
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: .userDomainMask).first
    }
    
    static func getContentsOfAPath(path: URL) -> [URL] {
        
        do {
            return try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles)
        } catch _ as NSError {
            
            return []
        }
    }
    
    static func isFileAvailable(forType directory: MediaType, fileName: String) -> Bool {
        
        if let filePath = self.getDirectory(forType: directory)?.appendingPathComponent(fileName).path {
            return FileManager.default.fileExists(atPath: filePath)
        }
        
        return false
    }
    
    static func isFileAvailable(atURL url: URL?) -> Bool {
        
        if let filePath = url {
            return FileManager.default.fileExists(atPath: filePath.absoluteString)
        }
        
        return false
    }
    
    static func renameFile(atURL url: URL, to name: String) -> URL? {
        
        let toURL = url.deletingLastPathComponent().appendingPathComponent(name)
        
        do {
            try FileManager.default.moveItem(at: url, to: toURL)
            return toURL
        } catch let e {
            NSLog("Failed to rename file for \(url.absoluteString) to \(toURL.absoluteString) | \(e.localizedDescription)")
        }
        
        return nil
    }
//
//    static func urlForFile(fileName: String, pageType: PageType) -> URL? {
//        
//        return self.getDirectory(forType: pageType)?.appendingPathComponent(fileName)
//    }
    
    static func deleteContentsAtURL(path: URL) {
        
        let contents = self.getContentsOfAPath(path: path)
        
        for item in contents {
            FileHandler.deleteItemAtURL(path: item)
        }
    }
    
    static func deleteItemAtURL(path: URL) {
        
        do {
            try Foundation.FileManager.default.removeItem(at: path)
        } catch let error as NSError {
            NSLog("[deleteItemAtURL] \(error.localizedDescription)")
        }
    }
    
//    static func getModifiedDate(forFileAtURL file: URL) -> String? {
//        
//        do {
//            let val = try file.resourceValues(forKeys: [URLResourceKey.contentModificationDateKey])
//            if let date = val.contentModificationDate {
//                return DateTimeManager.dateToString(date, format: "yyyy-MM-dd HH:mm")
//            }
//            return nil
//        } catch {
//            return nil
//        }
//    }
    
    
}
