//
//  ImageHandler.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/5/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ImageHandler {
    
    func asyncImageLoadWithCache(_ urlString: String, key: String, location: String = "images", completion: @escaping (UIImage?)->()) {
        
        let slashArray = urlString.characters.split{$0 == "/"}.map { String($0) }
        
        if slashArray.count > 0 {
            let dotArray = slashArray[slashArray.count - 1].characters.split{$0 == "."}.map { String($0) }
            var identifier: String?
            if dotArray.count > 1 {
                identifier = dotArray[dotArray.count - 2]
            } else if dotArray.count > 0 {
                identifier = dotArray[dotArray.count - 1]
            }
            
            if let id = identifier {
                
                let imageName = "\(id)_,_\(key)"
                
                let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
                let dirPath = (paths as NSString).appendingPathComponent(location)
                let imagePath = (paths as NSString).appendingPathComponent("\(location)/\(imageName).png" )
                let defaultFileManager = FileManager.default
                
                if (defaultFileManager.fileExists(atPath: imagePath)) {
                    
                    let getImage = UIImage(contentsOfFile: imagePath)
                    completion(getImage)
                    return
                    
                } else {
                    
                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
                        
                        do {
                            try defaultFileManager.createDirectory(atPath: dirPath, withIntermediateDirectories: true, attributes: nil)
                        } catch _ {
                        }
                        
                        
                        if let url = URL(string: urlString) {
                            if let data = try? Foundation.Data(contentsOf: url) {
                                
                                if var getImage =  UIImage(data: data) {
                                    getImage = self.rotateImage(getImage)
                                    if let jpegImage = UIImagePNGRepresentation(getImage){
                                        try? jpegImage.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
                                        
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            completion(getImage)
                                        })
                                        
                                        return
                                        
                                    } else {
                                        DispatchQueue.main.async(execute: { () -> Void in
                                            completion(nil)
                                        })
                                        return
                                    }
                                } else {
                                    DispatchQueue.main.async(execute: { () -> Void in
                                        completion(nil)
                                    })
                                    return
                                }
                                
                            } else {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    completion(nil)
                                })
                                return
                            }
                            
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(nil)
                            })
                            return
                        }
                        
                    }
                }
                
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(nil)
                })
                return
            }
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                completion(nil)
            })
            return
        }
    }
    
    func asyncImageDownload(_ urlString: String, name: String, completion: @escaping (UIImage?, String?)->()) {
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            
            var imageName = name
            if let name = self.constructName(fromURL: urlString) {
                imageName = name
            }
            
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
            let dirPath = (paths as NSString).appendingPathComponent("images")
            let imagePath = (paths as NSString).appendingPathComponent("images/\(imageName).png" )
            let defaultFileManager = FileManager.default
            
            if (defaultFileManager.fileExists(atPath: imagePath)) {
                
                let getImage = UIImage(contentsOfFile: imagePath)
                let fullPath = URL(string: imagePath)
                let path = fullPath?.deletingLastPathComponent()
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(getImage,path?.absoluteString)
                    return
                })
                
            } else {
                
                do {
                    try defaultFileManager.createDirectory(atPath: dirPath, withIntermediateDirectories: true, attributes: nil)
                } catch _ {
                }
                
                
                if let url = URL(string: urlString) {
                    if let data = try? Foundation.Data(contentsOf: url) {
                        
                        if let getImage =  UIImage(data: data) {
                            if let jpegImage = UIImagePNGRepresentation(getImage){
                                try? jpegImage.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
                                
                                DispatchQueue.main.async(execute: { () -> Void in
                                    let fullPath = URL(string: imagePath)
                                    let path = fullPath?.deletingLastPathComponent()
                                    completion(getImage,path?.absoluteString)
                                })
                                
                                return
                                
                            } else {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    completion(nil,nil)
                                })
                                return
                            }
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(nil,nil)
                            })
                            return
                        }
                        
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion(nil,nil)
                        })
                        return
                    }
                    
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(nil,nil)
                    })
                    return
                }
            }
        }
        
    }
    
    func rotateImage(_ image: UIImage) -> UIImage {
        
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        
        UIGraphicsBeginImageContext(image.size)
        
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return copy!
    }
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        let size = oldImage.size
        
        UIGraphicsBeginImageContext(size)
        
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)
        
        bitmap.draw(oldImage.cgImage!, in: CGRect(origin: origin, size: size))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    fileprivate func constructName(fromURL urlString: String) -> String? {
        
        if let url = URL(string: urlString) {
            let ex = url.pathExtension
            if ex != "" {
                
                let urlwithoutPath = url.deletingPathExtension()
                return urlwithoutPath.lastPathComponent
            }
        
        } else {
            
            if let last = urlString.characters.split(separator: "/").last {
                
                return String(last)
            }
        }
        
        return nil
    }
    
    fileprivate func resizeTo100(_ image: UIImage?) -> UIImage? {
        if let cgImage = image?.cgImage {
            let widthOrg = cgImage.width
            let heightOrg = cgImage.height
            
            if widthOrg > 500 {
                
                let width = 500
                let scale: CGFloat = CGFloat(width) / CGFloat(widthOrg)
                let height = Int(CGFloat(heightOrg) * scale)
                let bitsPerComponent = cgImage.bitsPerComponent
                let bytesPerRow = cgImage.bytesPerRow
                let colorSpace = cgImage.colorSpace
                let bitmapInfo = cgImage.bitmapInfo
                
                
                let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: cgImage.colorSpace!, bitmapInfo: bitmapInfo.rawValue)
                
                context!.interpolationQuality = CGInterpolationQuality.high
                
                context?.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: CGSize(width: CGFloat(width), height: CGFloat(height))))
                
                if let scaledImage = context?.makeImage().flatMap({ UIImage(cgImage: $0) })?.cgImage {
                    
                    let result = UIImage(cgImage: scaledImage,
                                         scale: 1,
                                         orientation: image!.imageOrientation)
                    
                    return result
                }
                
            } else {
                return image
            }
            
            
        }
        
        return nil
    }
    
    func resizeShorterSize(of image: UIImage, to size: Int) -> UIImage {
        
        let width = Int(image.size.width)
        let height = Int(image.size.height)
        
        var newWidth = width
        var newHeight = height
        
        if height > width {
            
            if width > size {
                newWidth = Int(size)
                newHeight = Int(height * newWidth / width)
                
            } else {
                return image
            }
            
        } else {
            
            if height > size {
                newHeight = Int(size)
                newWidth = Int(width * newHeight / height)
                
            } else {
                return image
            }
        }
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return newImage == nil ? image : newImage!
        
    }
    
    func asyncImageDownloadWithCache(_ urlString: String, key: String, completion: @escaping (UIImage?, UIImage?, String?)->()) {
        
        let slashArray = urlString.characters.split{$0 == "/"}.map { String($0) }
        
        if slashArray.count > 0 {
            let dotArray = slashArray[slashArray.count - 1].characters.split{$0 == "."}.map { String($0) }
            var identifier: String?
            if dotArray.count > 1 {
                identifier = dotArray[dotArray.count - 2]
            } else if dotArray.count > 0 {
                identifier = dotArray[dotArray.count - 1]
            }
            
            if let id = identifier {
                
                let imageName = "\(id)_,_\(key)"
                
                self.asyncImageDownload(urlString, name: imageName, completion: completion)
                
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(nil,nil,nil)
                })
                return
            }
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                completion(nil,nil,nil)
            })
            return
        }
    }
    
    func asyncExtractVideoThumbnail(url: URL, name: String, completion: @escaping (UIImage?)->()) {
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            
            let thumbnailName = "\(name)_thumb"
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
            
            let thumbnailPath = (paths as NSString).appendingPathComponent("images/\(thumbnailName).jpg" )
            
            let defaultFileManager = FileManager.default
            
            var thumbnail: UIImage? = nil
            
            if defaultFileManager.fileExists(atPath: thumbnailPath) {
                
                thumbnail = UIImage(contentsOfFile: thumbnailPath)
                
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    completion(thumbnail)
                    return
                })
                
            } else {
                
                if let newThumb = Thumbnailer().thumbnailFromVideo(url: url) {
                    thumbnail = self.resizeTo100(newThumb)

                    if let thumb = thumbnail {
                        self.saveImageToFolder(thumb, withName: thumbnailName, format: "jpg", completion: nil)
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        completion(thumbnail)
                        return
                    })
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        completion(nil)
                        return
                    })
                }
            }
            
            
            
        }
    }
    
    func asyncImageDownload(_ urlString: String, name: String, completion: @escaping (UIImage?, UIImage?, String?)->()) {
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            var imageName = name
            
            if let name = self.constructName(fromURL: urlString) {
                imageName = name
            }
            let thumbnailName = "\(name)_thumb"
            
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
            let dirPath = (paths as NSString).appendingPathComponent("images")
            
            let imagePath = (paths as NSString).appendingPathComponent("images/\(imageName).jpg" )
            let thumbnailPath = (paths as NSString).appendingPathComponent("images/\(thumbnailName).jpg" )
            
            let defaultFileManager = FileManager.default
            
            if (defaultFileManager.fileExists(atPath: imagePath)) {
                
                let getImage = UIImage(contentsOfFile: imagePath)
                let fullPath = URL(string: imagePath)
                let path = fullPath?.deletingLastPathComponent()
                
                var thumbnail: UIImage? = nil
                
                if defaultFileManager.fileExists(atPath: thumbnailPath) {
                    
                    thumbnail = UIImage(contentsOfFile: thumbnailPath)
                    
                } else {
                    
                    thumbnail = self.resizeTo100(getImage)
                    if let thumb = thumbnail {
                        self.saveImageToFolder(thumb, withName: thumbnailName, format: "jpg", completion: nil)
                    }
                }
                
                
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    completion(getImage,thumbnail,path?.absoluteString)
                    return
                })
                
            } else {
                
                do {
                    try defaultFileManager.createDirectory(atPath: dirPath, withIntermediateDirectories: true, attributes: nil)
                } catch _ {
                }
                
                
                if let url = URL(string: urlString) {
                    if let data = try? Foundation.Data(contentsOf: url) {
                        
                        if let getImage =  UIImage(data: data) {
                            if let jpegImage = UIImageJPEGRepresentation(getImage, 1.0){
                                try? jpegImage.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
                                
                                DispatchQueue.main.async(execute: { () -> Void in
                                    let fullPath = URL(string: imagePath)
                                    let path = fullPath?.deletingLastPathComponent()
                                    
                                    var thumbnail: UIImage? = nil
                                    
                                    if defaultFileManager.fileExists(atPath: thumbnailPath) {
                                        
                                        thumbnail = UIImage(contentsOfFile: thumbnailPath)
                                        
                                    } else {
                                        
                                        thumbnail = self.resizeTo100(getImage)
                                        if let thumb = thumbnail {
                                            self.saveImageToFolder(thumb, withName: thumbnailName, format: "jpg", completion: nil)
                                        }
                                    }
                                    
                                    completion(getImage, thumbnail, path?.absoluteString)
                                })
                                
                                return
                                
                            } else {
                                DispatchQueue.main.async(execute: { () -> Void in
                                    completion(nil,nil,nil)
                                })
                                return
                            }
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(nil,nil,nil)
                            })
                            return
                        }
                        
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion(nil,nil,nil)
                        })
                        return
                    }
                    
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(nil,nil,nil)
                    })
                    return
                }
            }
        }
        
    }
    
    func checkAndCompressImage(_ image: UIImage) -> UIImage {
        
        var compressionRatio: CGFloat = 1.0
        var pass:Int = 1
        var img: UIImage = image
        
        if var imageData = UIImageJPEGRepresentation(img, compressionRatio) {
            
            if imageData.count > 1 * 1000 * 1000 {
                
                if let imgdata0 = UIImageJPEGRepresentation(img, 0), let img0 = UIImage(data: imgdata0) {
                    
                    img = img0
                }
                
            } else {
                
                while imageData.count > 100000 && pass < 5 {
                    compressionRatio = compressionRatio * 0.5
                    if let newImageData = UIImageJPEGRepresentation(img, compressionRatio) {
                        imageData = newImageData
                    }
                    pass = pass + 1
                }
                
                if let im = UIImage(data: imageData) {
                    img = im
                }
            }
        }
        
        return img
    }
    
    func saveImageToFolder(_ aImage: UIImage, withName name: String, format: String, completion: ((UIImage?, String?)->())?) {
        
        var image = self.rotateImage(aImage)
        
        image = self.checkAndCompressImage(image)
        
        let imageName = name
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let dirPath = (paths as NSString).appendingPathComponent("images")
        let imagePath = (paths as NSString).appendingPathComponent("images/\(imageName).\(format)" )
        let defaultFileManager = FileManager.default
        
        if (defaultFileManager.fileExists(atPath: imagePath)) {
            
            let getImage = UIImage(contentsOfFile: imagePath)
            completion?(getImage, dirPath)
            return
            
        } else {
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
                
                do {
                    try defaultFileManager.createDirectory(atPath: dirPath, withIntermediateDirectories: true, attributes: nil)
                } catch _ {
                }
                
                
                if let jpegImage = UIImageJPEGRepresentation(image, 1.0){
                    try? jpegImage.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion?(image, dirPath)
                    })
                    
                    return
                    
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion?(nil, nil)
                    })
                    return
                }
                
            }
        }
    }
    
    func getURLforFile(_ name: String) -> URL? {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let filePath = (paths as NSString).appendingPathComponent("images/\(name)" )
        
        
        return URL(string: filePath)
    }
    
    func deleteItemAtPath(_ path: String) {
        
        do {
            try FileManager.default.removeItem(atPath: path)
        } catch let error as NSError {
            NSLog("[deleteItemAtURL] \(error.localizedDescription)")
        }
    }
    
    func deleteImagesFolder() {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let imagesPath = (paths as NSString).appendingPathComponent("images")
        
        self.deleteItemAtPath(imagesPath)
    }
    
    func clearImageCache(_ olderThanDays:Int, completion: ((Int)->Void)?) {
        
        let queue = DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background)
        
        queue.async { 
            
            let deleted = self.deleteImages(olderThanDays)
            
            DispatchQueue.main.async(execute: { 
                
                completion?(deleted)
            })
        }
    }
    
    fileprivate func deleteImages(_ olderThanDays: Int) -> Int {
        var deletedCount = 0
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        let imagesPath = (paths as NSString).appendingPathComponent("images")
        
        let imageNames = self.getContentsOfAPath(imagesPath)
        
        for imagename in imageNames {
            let imagepath = (imagesPath as NSString).appendingPathComponent(imagename)
            if let createdAt = self.createdDate(imagepath) {
                
                let interval = olderThanDays * 24 * 60 * 60
                
                if interval <= Int(-1 * createdAt.timeIntervalSinceNow) {
                    
                    self.deleteItemAtPath(imagepath)
                    deletedCount += 1
                }
            }
        }
        
        return deletedCount
    }
    
    fileprivate func getContentsOfAPath(_ path: String) -> [String] {
        
        do {
            return try FileManager.default.contentsOfDirectory(atPath: path)
        } catch let error as NSError {
            NSLog("[getContentsOfAPath] \(error.localizedDescription)")
            return []
        }
    }
    
    fileprivate func createdDate(_ file: String) -> Date? {
        
        let fileManager = FileManager.default
        do {
            let attributes = try fileManager.attributesOfItem(atPath: file)
            return attributes[FileAttributeKey.creationDate] as? Date
            
        } catch let e {
            print("Error while fetching file attributes \(e)")
        }
        
        return nil
    }
    
}

extension UIImageView {
    
    func loadImage(withUrl url: String, key: String) {
        
        ImageHandler().asyncImageLoadWithCache(url, key: key, location: DefaultImagesLocation) { (image) -> () in
            if image==nil {
            }
            self.image = image
        }
    }
}
