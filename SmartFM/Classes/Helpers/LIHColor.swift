//
//  LIHColor.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/27/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

extension UIColor {
    
    static func colorWithRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    static func colorWithHexString(_ hex: NSString) -> UIColor {
        
        var cString: NSString = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased() as NSString
        
        if cString.length < 6 { return UIColor.gray }
        
        if cString.hasPrefix("0X") { cString = cString.substring(from: 2) as NSString}
        
        if cString.length != 6 { return UIColor.gray }
        
        var range: NSRange = NSRange(location: 0, length: 2)
        let rString: NSString = cString.substring(with: range) as NSString
        
        range.location = 2
        let gString: NSString = cString.substring(with: range) as NSString
        
        range.location = 4
        let bString: NSString = cString.substring(with: range) as NSString
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        
        Scanner(string: rString as String).scanHexInt32(&r)
        Scanner(string: gString as String).scanHexInt32(&g)
        Scanner(string: bString as String).scanHexInt32(&b)
        
        let color: UIColor = UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
        return color
    }
}
