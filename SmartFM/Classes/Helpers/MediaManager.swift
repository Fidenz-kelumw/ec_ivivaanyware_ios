//
//  MediaManager.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/25/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class MediaManager {
    
    static func getMediaType(forURL url : URL) -> MediaType {
        
        let ext = url.pathExtension
        
        if ext == "mov" || ext == "mov" {
            return MediaType.VID
        } else if ext == "jpg" || ext == "png" {
            return MediaType.IMG
        } else if ext == "m4a" {
            return MediaType.AUD
        } else {
            return MediaType.VID
        }
    }
}
