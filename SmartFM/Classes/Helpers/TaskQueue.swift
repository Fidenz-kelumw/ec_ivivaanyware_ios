//
//  TaskQueue.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 7/28/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

protocol TaskQueueDelegate {
    
    func allTasksFinished()
}

class TaskQueue {
    
    fileprivate var inProgressCount: Int = 0 {
        didSet {
            if inProgressCount == 0 {
                self.delegate?.allTasksFinished()
            }
        }
    }
    fileprivate var failedTasksCount: Int = 0
    
    var delegate: TaskQueueDelegate?
    
    func addTaskToQueue() {
        
        inProgressCount = inProgressCount + 1
    }
    
    func taskFinished(_ success: Bool) {
        
        if !success {
            self.failedTasksCount = self.failedTasksCount + 1
        }
        if inProgressCount > 0 {
            inProgressCount = inProgressCount - 1
        }
    }
    
    func hasProcessingTasks() -> Bool {
        
        return self.inProgressCount > 0
    }
    
    func hasFailedTasks() -> Bool {
        
        return self.failedTasksCount > 0
    }
    
    func removeFailedTask() {
        
        self.failedTasksCount = self.failedTasksCount - 1
        
        if self.failedTasksCount < 0 {
            self.failedTasksCount = 0
        }
    }
    
    func reset() {
        
        self.failedTasksCount = 0
    }
}
