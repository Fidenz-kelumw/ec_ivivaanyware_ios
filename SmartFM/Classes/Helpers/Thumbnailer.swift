//
//  Thumbnailer.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 12/30/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import AVFoundation

class Thumbnailer {
    
    func thumbnailFromVideo(url: URL, completion:@escaping (UIImage?)->Void) {
        
        DispatchQueue.global().async {
            
            let asset = AVURLAsset(url: url)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            do {
                var thumbnailTime: CMTime = asset.duration
                thumbnailTime.value = 0;
                let cgImage = try imgGenerator.copyCGImage(at: thumbnailTime, actualTime: nil)
                let uiImage = UIImage(cgImage: cgImage)
                DispatchQueue.main.async {
                    completion(uiImage)
                }
            } catch let e {
                NSLog("Failed to extract image - \(e.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
            
        }
    }
    
    func thumbnailFromVideo(url: URL) -> UIImage? {
        let asset = AVURLAsset(url: url)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        do {
            var thumbnailTime: CMTime = asset.duration
            thumbnailTime.value = 0;
            let cgImage = try imgGenerator.copyCGImage(at: thumbnailTime, actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            return uiImage
        } catch let e {
            NSLog("Failed to extract image - \(e.localizedDescription)")
            return nil
        }
    }
}
