//
//  UserManager.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 6/1/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class UserManager {
    
    func isLogged() -> Bool{
        
        if let _ = self.getAccount(), let _ = self.getApiKey(), let _ = self.getUserKey() {
            return true
        }
        
        return false
    }
    
    func wipeUserData(completion: (()->Void)?) {
        
        self.setAccount(nil)
        self.setApiKey(nil)
        self.setUserKey(nil)
        self.setSSL(nil)
        self.setUserId(nil)
        self.setUserName(nil)
        self.setAccountImage(nil)
        self.setUserDomain(nil)
        self.setUploadSize(ImageUploadSize.Medium)
        self.setFirebaseTokenStatus(false)
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            
            Defaults.truncateTable()
            Building.truncateTable()
            ObjType.truncateTable()
            Stage.truncateTable()
            Tab.truncateTable()
            Role.truncateTable()
            SyncTime.truncateTable()
            Filter.truncateTable()
            Object.truncateTable()
            ObjectInformation.truncateTable()
            ObjectInfo.truncateTable()
            DefaultData.truncateTable()
            OptionsList.truncateTable()
            Action.truncateTable()
            PageLink.truncateTable()
            FieldData.truncateTable()
            Member.truncateTable()
            MemberUser.truncateTable()
            CheckItem.truncateTable()
            MessageMain.truncateTable()
            Message.truncateTable()
            MessageUser.truncateTable()
            Layout.truncateTable()
            LayoutPin.truncateTable()
            DroppedPin.truncateTable()
            Attachment.truncateTable()
            UploadedUser.truncateTable()
            AttachmentsMain.truncateTable()
            AttachmentType.truncateTable()
            PushMessage.truncateTable()
            HomeDefaultData.truncateTable()
            DefDataValue.truncateTable()
            SubObject.truncateTable()
            MessageType.truncateTable()
            ObjectURL.truncateTable()
            ObjectItem.truncateTable()
            ValueTypeData.truncateTable()
            SwitchTypeData.truncateTable()
            ButtonsTypeData.truncateTable()
            
            
            ImageHandler().deleteImagesFolder()
            
            DispatchQueue.main.async(execute: { 
                
                completion?()
            })
        }
        
        
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func saveUserData(userKey: String, userId: String?, userName: String?, accountImage: String?, domain: String?) {
        
        self.setUserKey(userKey)
        self.setUserId(userId)
        self.setUserName(userName)
        self.setAccountImage(accountImage)
        self.setUserDomain(domain)
    }
    
    func saveDomainData(account: String, APIKey: String, SSL: String) {
        
        self.setAccount(account)
        self.setApiKey(APIKey)
        self.setSSL(SSL)
    }
    
    
    
    let AccountKey: String = "account"
    let ApiKeyKey: String = "apiKey"
    let SSLKey: String = "sslKey"
    let UserKeyKey: String = "userkey"
    
    let UserIdKey: String = "userid"
    let UserNameKey: String = "userName"
    let AccountImageKey: String = "accountImage"
    let UserDomainKey: String = "userDomain"
    let AppNameKey: String = "appNAme"
    
    let UploadSizeKey: String = "imageuploadsize"
    let firebaseSyncKey: String = "firebasetokensent"
    let firstTime: String = "firsttime"
    
    
    /////////////////////////////////////////////////////
    //MARK: - GETTERS
    /////////////////////////////////////////////////////
    func getAccount() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.AccountKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let account = preferences.string(forKey: key) as String!
            return account
        }
    }
    
    func getApiKey() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.ApiKeyKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let account = preferences.string(forKey: key) as String!
            return account
        }
    }
    
    func getSSL() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.SSLKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let ssl = preferences.string(forKey: key) as String!
            
            if ssl == "http" || ssl == "https" {
                return ssl
            } else {
                return nil
            }
        }
    }
    
    func getUserKey() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.UserKeyKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let userKey = preferences.string(forKey: key) as String!
            return userKey
        }
    }
    
    func getUserId() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.UserIdKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let userId = preferences.string(forKey: key) as String!
            return userId
        }
    }
    
    func getUserName() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.UserNameKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let userName = preferences.string(forKey: key) as String!
            return userName
        }
    }
    
    func getAccountImage() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.AccountImageKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let accountImage = preferences.string(forKey: key) as String!
            return accountImage
        }
    }
    
    func getAppName() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.AppNameKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let accountImage = preferences.string(forKey: key) as String!
            return accountImage
        }
    }
    
    func getUserDomain() -> String? {
        let preferences = UserDefaults.standard
        
        let key = self.UserDomainKey
        
        if preferences.object(forKey: key) == nil {
            return nil
        } else {
            let userDomain = preferences.string(forKey: key) as String!
            return userDomain
        }
    }
    
    func getUploadSize() -> ImageUploadSize? {
        let preferences = UserDefaults.standard
        
        let key = self.UploadSizeKey
        
        if preferences.object(forKey: key) == nil {
            
        } else {
            if let uploadsize = preferences.string(forKey: key) as String! {
                return ImageUploadSize(rawValue: uploadsize)
            }
        }
        
        return ImageUploadSize.Medium
    }
    
    func isFirebaseTokenSent() -> Bool {
        let preferences = UserDefaults.standard
        
        let key = self.firebaseSyncKey
        
        if preferences.object(forKey: key) == nil {
            return false
        } else {
            if let hasSent = preferences.bool(forKey: key) as Bool! {
                return hasSent
            }
        }
        
        return false
    }
    
    func isFirstTime() -> Bool {
        let preferences = UserDefaults.standard
        
        let key = self.firstTime
        
        if preferences.object(forKey: key) == nil {
            return false
        } else {
            if let result = preferences.bool(forKey: key) as Bool! {
                return result
            }
        }
        
        return false
    }
    
    /////////////////////////////////////////////////////
    //MARK: - SETTERS
    /////////////////////////////////////////////////////
    func setAccount(_ account: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.AccountKey
        
        preferences.set(account, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setApiKey(_ apiKey: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.ApiKeyKey
        
        preferences.set(apiKey, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setSSL(_ ssl: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.SSLKey
        
        preferences.set(ssl, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setUserKey(_ userKey: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.UserKeyKey
        
        preferences.set(userKey, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setUserId(_ userId: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.UserIdKey
        
        preferences.set(userId, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
    }
    
    func setUserName(_ userName: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.UserNameKey
        
        preferences.set(userName, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setAppName(_ appName: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.AppNameKey
        
        preferences.set(appName, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setAccountImage(_ accountImage: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.AccountImageKey
        
        preferences.set(accountImage, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setUserDomain(_ userDomain: String?){
        
        let preferences = UserDefaults.standard
        
        let key = self.UserDomainKey
        
        preferences.set(userDomain, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setUploadSize(_ uploadsize: ImageUploadSize){
        
        let preferences = UserDefaults.standard
        
        let key = self.UploadSizeKey
        
        preferences.set(uploadsize.rawValue, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
        
    }
    
    func setFirebaseTokenStatus(_ status: Bool){
        
        let preferences = UserDefaults.standard
        
        let key = self.firebaseSyncKey
        
        preferences.set(status, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
    }
    
    func setFirstRun(_ status: Bool){
        
        let preferences = UserDefaults.standard
        
        let key = self.firstTime
        
        preferences.set(status, forKey: key)
        
        let didSave = preferences.synchronize()
        
        if !didSave {
            
        }
    }
}
