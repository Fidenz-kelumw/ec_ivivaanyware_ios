//
//  AttachedAsset.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/20/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class AttachedAsset {
    
    var attachedImage: UIImage?
    var attachedMediaURL: URL?
    var mediaType: MediaType?
}
