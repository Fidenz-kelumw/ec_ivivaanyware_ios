//
//  Attachment.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/10/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class AttachmentsMain: OLCModel {
    
    var attachments: [Attachment] = []
    var AttachmentTypes: [AttachmentType] = []
    var Id: NSNumber?
    var AddNewAttachment: String?
    var InputModes: [String]?
    
    class override func ignoredProperties()->[Any] {
        
        return ["attachments","AttachmentTypes"]
    }
}

class Attachment: OLCModel {
    
    var User: UploadedUser?
    var Id: NSNumber?
    var AttachmentKey: String?
    var AttachmentType: String?
    var AttachmentName: String?
    var FilePath: String?
    var UploadedAt: String?
    var ATTType: String?
    var attMainId: NSNumber?
    
    class override func ignoredProperties()->[Any] {
        
        return ["User"]
    }
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.attMainId  = aDecoder.decodeObject(forKey: "attMainId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.attMainId {
            aCoder.encode(obj, forKey: "attMainId")
        }
    }
}

class UploadedUser: OLCModel {
    
    var Id: NSNumber?
    var UserKey: String?
    var UserName: String?
    var ImagePath: String?
    var AttachmentId: NSNumber?
    
}

class AttachmentType: OLCModel {
    
    var Id: NSNumber?
    var TypeName: String?
    var Signature: String?
    var attMainId: NSNumber?
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.attMainId  = aDecoder.decodeObject(forKey: "attMainId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.attMainId {
            aCoder.encode(obj, forKey: "attMainId")
        }
    }
}
