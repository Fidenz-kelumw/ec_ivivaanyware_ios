//
//  Building.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/6/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Building: OLCModel {
    
    var Id: NSNumber?
    var LocationName: String?
    var LocationKey: String?
}