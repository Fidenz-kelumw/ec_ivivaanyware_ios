//
//  CellImage.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 7/26/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class CellAttachment {
    
    var folder: String? {
        didSet {
            if let f = self.folder {
                if f.hasSuffix("/") {
                    self.folder = f.substring(to: f.index(before: f.endIndex))
                }
            }
        }
    }
    var name: String?
    var image: UIImage?
    var isLocal: Bool = false
    var isUploading: Bool = false
    var isFailedUploading: Bool = false
    var mediaType: MediaType?
}
