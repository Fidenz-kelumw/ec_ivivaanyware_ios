//
//  CheckItem.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class CheckItem: OLCModel {
    
    var Id: NSNumber?
    var ItemKey: String?
    var ItemName: String?
    var Description: String?
    var Deadline: String?
    var Status: String?
    var StatusEdit: String?
    var CalendarEdit: String?
    var DescriptionEdit: String?
    var InfoCode: String?
}
