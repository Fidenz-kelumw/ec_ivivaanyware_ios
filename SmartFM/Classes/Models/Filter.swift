//
//  Filter.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Filter: OLCModel {
    
    var Id: NSNumber?
    var ObjectType: String?
    var FilterID: String?
    var FilterName: String?
    var Description: String?
    
}