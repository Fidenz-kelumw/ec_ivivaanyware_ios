//
//  Layout.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Layout: OLCModel {
    
    var pins: [LayoutPin]?
    var droppedPin: DroppedPin?
    var pinLabel: PinLabel?
    var Id: NSNumber?
    var Path: String?
    var ZoomLevels: String?
    
    
    class override func ignoredProperties()->[Any] {
        
        return ["pins","droppedPin","pinLabel"]
    }
    
}

class LayoutPin: OLCModel {
    
    var Id: NSNumber?
    var Name: String?
    var Description: String?
    var X: String?
    var Y: String?
    var Color: String?
    var ObjectType: String?
    var ObjectKey: String?
    var layoutId: NSNumber?
    
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.layoutId  = aDecoder.decodeObject(forKey: "layoutId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.layoutId {
            aCoder.encode(obj, forKey: "layoutId")
        }
    }
}

class DroppedPin: OLCModel {
    
    var Id: NSNumber?
    var X: String?
    var Y: String?
    var Editable: String?
    var layoutId: NSNumber?
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.layoutId  = aDecoder.decodeObject(forKey: "layoutId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.layoutId {
            aCoder.encode(obj, forKey: "layoutId")
        }
    }
}

class PinLabel: OLCModel {
    
    var Id: NSNumber?
    var Name: String?
    var Description: String?
    var layoutId: NSNumber?
    
}
