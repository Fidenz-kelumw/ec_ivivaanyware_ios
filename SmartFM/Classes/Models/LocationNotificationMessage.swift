//
//  LocationNotificationMessage.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/14/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class LocationNotificationMessage {
    
    var spotsContent: String?
    var objectKey: String?
    var objectType: String?
}
