//
//  Member.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Member: OLCModel {
    
    var User: MemberUser?
    
    var Id: NSNumber?
    var RoleID: String?
    var ObjectType: String?

    class override func ignoredProperties()->[Any] {
        
        return ["User"]
    }
    
    
}

class MemberUser: OLCModel {
    
    var Id: NSNumber?
    var UserKey: String?
    var UserName: String?
    var ImagePath: String?
    var Phone: String?
    
    var memberId: NSNumber?
}
