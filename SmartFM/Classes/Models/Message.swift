//
//  Message.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class MessageMain: OLCModel {
    
    var Messages: [Message] = []
    var MessageTypes: [MessageType] = []
    var Id: NSNumber?
    var AddNewMessage: String?
    var NewMessageAction: String?
    
    class override func ignoredProperties()->[Any] {
        
        return ["Messages", "MessageTypes"]
    }
}

class Message: OLCModel {
    
    var CreatedUser: MessageUser?
    var isTemp: Bool = false
    var Id: NSNumber?
    var MessageKey: String?
    var MessageText: String?
    var CreatedAt: String?
    var MessagesMainId: NSNumber?
    
    class override func ignoredProperties()->[Any] {
        
        return ["CreatedUser","isTemp"]
    }
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.MessagesMainId  = aDecoder.decodeObject(forKey: "MessagesMainId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.MessagesMainId {
            aCoder.encode(obj, forKey: "MessagesMainId")
        }
    }
}

class MessageUser: OLCModel {
    
    var Id: NSNumber?
    var UserKey: String?
    var UserName: String?
    var ImagePath: String?
    var MessageId: NSNumber?
    
}


class MessageType: OLCModel {
    
    var Id: NSNumber?
    var TypeID: String?
    var TypeName: String?
    var MessagesMainId: NSNumber?
}
