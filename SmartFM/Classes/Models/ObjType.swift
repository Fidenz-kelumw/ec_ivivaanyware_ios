//
//  ObjectType.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class ObjType: OLCModel {
    
    var stages: [Stage]? = nil
    var tabs: [Tab]? = nil
    var roles: [Role]? = nil
    var CreateFromMenu: [HomeDefaultData]? = nil
    var Id: NSNumber?
    var ObjectType: String?
    var hasCreateFromMenu: String = "0"
    var MenuName: String?
    var ObjectIconPath: String?
    var CustomFilterPage: String?
    var FilterListPage: String?
    var CreatePage: String?
    var QRFormatiOS: String?
    var QRFormatAndroid: String?
    var MovetoDetailPage: String?
    var MenuType: String?
    var MenuLink: String?
    var URL: String?
    
    class override func ignoredProperties()->[Any] {
        
        return ["stages","tabs","roles","CreateFromMenu"]
    }
    
    func getCreateFromMenu() -> [HomeDefaultData]? {
        
        if let objType = self.ObjectType {
            
            if let HomeDefaultData = HomeDefaultData.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [HomeDefaultData] {
                
                return HomeDefaultData
            }
        }
        
        return nil
    }
    
    func getStage() -> [Stage]? {
        
        if let objType = self.ObjectType {
            
            if let stages = Stage.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [Stage] {
                
                return stages
            }
        }
        
        return nil
    }
    
    func getTabs() -> [Tab]? {
        
        if let objType = self.ObjectType {
            
            if let tabs = Tab.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [Tab] {
                
                return tabs
            }
        }
        
        return nil
    }
    
    func getRoles() -> [Role]? {
        
        if let objType = self.ObjectType {
            
            if let roles = Role.__where("ObjectType='\(objType)'", sortBy: "Id", accending: true) as? [Role] {
                
                return roles
            }
        }
        
        return nil
    }
}


class Stage: OLCModel {
    
    var Id: NSNumber?
    var ObjectType: String?
    var Stage: String?
    var Color: String?
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectType  = aDecoder.decodeObject(forKey: "ObjectType") as? String
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectType {
            aCoder.encode(obj, forKey: "ObjectType")
        }
    }
}


class Tab: OLCModel {
    
    var Id: NSNumber?
    var ObjectType: String?
    var TabType: String?
    var TabName: String?
    var TabHide: String?
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectType  = aDecoder.decodeObject(forKey: "ObjectType") as? String
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectType {
            aCoder.encode(obj, forKey: "ObjectType")
        }
    }
}


class Role: OLCModel {
    
    var Id: NSNumber?
    var ObjectType: String?
    var RoleID: String?
    var Color: String?
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectType  = aDecoder.decodeObject(forKey: "ObjectType") as? String
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectType {
            aCoder.encode(obj, forKey: "ObjectType")
        }
    }
}


class HomeDefaultData: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    var FieldID: String?
    
    var ObjectType: String?
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectType  = aDecoder.decodeObject(forKey: "ObjectType") as? String
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectType {
            aCoder.encode(obj, forKey: "ObjectType")
        }
    }
}
