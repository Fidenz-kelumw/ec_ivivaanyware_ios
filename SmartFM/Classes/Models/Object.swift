//
//  Object.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/8/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Object: OLCModel {
    
    var Id: NSNumber?
    var ObjectType: String?
    var ObjectKey: String?
    var ObjectID: String?
    var Description: String?
    var Stage: String?
}
