//
//  ObjectInformation.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class ObjectInformation: OLCModel {
    
    var objectInfo: [ObjectInfo]?
    var pageLinks: [PageLink]?
    var actions: [Action]?
    var UpdateConfirmation: String?
    var Id: NSNumber?
    var UpdateObjectInfo: String?
    var HideUpdateButton: String?
    var UpdateButtonText: String?
    
    class override func ignoredProperties()->[Any] {
        
        return ["objectInfo","pageLinks","actions"]
    }
}

class ObjectInfo: OLCModel {
    
    var defaultData: DefaultData?
    var OptionList: [OptionsList]?
    
    var Id: NSNumber?
    var FieldID: String?
    var FieldName: String?
    var ValueType: String?
    var Editable: String?
    var LookUpService: String?
    var Mandatory: String?
    var Extended: String?
    var NoDataValue: String?
    var Style: String?
    var QRCode: String?
    var DownloadFolder: String?
    var UploadFolder: String?
    var InputModes: [String]?
    var DisableDelete: String?
    
    
    var ObjectInformationId: NSNumber?
    
    class override func ignoredProperties()->[Any] {
        
        return ["defaultData","OptionList"]
    }
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectInformationId  = aDecoder.decodeObject(forKey: "ObjectInformationId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectInformationId {
            aCoder.encode(obj, forKey: "ObjectInformationId")
        }
    }
}

class DefaultData: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    var ValuesArray: [String]?
    var Values: [DefDataValue]?
    
    var cellImages: [CellAttachment] = []
    
    var ObjectInfoId: NSNumber?
    
    class override func ignoredProperties()->[Any] {
        return ["cellImages", "Values"]
    }
}

class DefDataValue: OLCModel {
    
    var Id: NSNumber?
    var ATTName: String?
    var ATTType: String?
    
    var defaultDataId: NSNumber?
}

class OptionsList: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    
    var ObjectInfoId: NSNumber?
    
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectInfoId  = aDecoder.decodeObject(forKey: "ObjectInfoId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectInfoId {
            aCoder.encode(obj, forKey: "ObjectInfoId")
        }
    }
}

class PageLink: OLCModel {
    
    var fieldData: [FieldData]?
    
    var Id: NSNumber?
    var LinkType: String?
    var LinkName: String?
    var ObjectType: String?
    var ObjectKey: String?
    
    var ObjectInformationId: NSNumber?
    
    
    class override func ignoredProperties()->[Any] {
        
        return ["fieldData" as AnyObject]
    }
    
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectInformationId  = aDecoder.decodeObject(forKey: "ObjectInformationId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectInformationId {
            aCoder.encode(obj, forKey: "ObjectInformationId")
        }
    }
}

class FieldData: OLCModel {
    
    var Id: NSNumber?
    var FieldID: String?
    var Value: String?
    var DisplayText: String?
    
    var PageLinkId: NSNumber?
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.PageLinkId  = aDecoder.decodeObject(forKey: "PageLinkId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.PageLinkId {
            aCoder.encode(obj, forKey: "PageLinkId")
        }
    }
}

class Action: OLCModel {
    
    var Id: NSNumber?
    var ActionID: String?
    var ActionName: String?
    var Enabled: String?
    var Confirmation: String?
    var Cargo: String?
    
    var ObjectInformationId: NSNumber?
    
    
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.ObjectInformationId  = aDecoder.decodeObject(forKey: "ObjectInformationId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.ObjectInformationId {
            aCoder.encode(obj, forKey: "ObjectInformationId")
        }
    }
}

class LData: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    var isSelected: Bool = false
    
    var ObjectInfoId: NSNumber?
}
