//
//  ObjectItem.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 2/1/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class ObjectItem: OLCModel {
    
    var valueTypeData: ValueTypeData?
    var switchtypeData: SwitchTypeData?
    var buttonsTypeData: [ButtonsTypeData]?
    
    var Id: NSNumber?
    var ItemName: String?
    var ItemKey: String?
    var ItemData: String?
    var DataColor: String?
    var ItemEdit: String?
    var TypeID: String?
    var PageEditEnabled: String?
    
    class override func ignoredProperties()->[Any] {
        
        return ["valueTypeData","switchtypeData","buttonsTypeData"]
    }
}

class ValueTypeData: OLCModel {
    
    var Id: NSNumber?
    var MIN: String?
    var MAX: String?
    var INC: String?
    var objectItemId: NSNumber?
}

class SwitchTypeData: OLCModel {
    
    var Id: NSNumber?
    var isON: String?
    var isOFF: String?
    var objectItemId: NSNumber?
    
}

class ButtonsTypeData: OLCModel {
    
    var Id: NSNumber?
    var Name: String?
    var Value: String?
    var objectItemId: NSNumber?
    
}
