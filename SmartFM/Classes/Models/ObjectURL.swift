//
//  ObjectURL.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/31/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class ObjectURL: OLCModel {
    
    var Id: NSNumber?
    var URLName: String?
    var URLPath: String?
    var UpdatedAt: String?
}
