//
//  PushMessage.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/17/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class PushMessage: OLCModel {
    
    var Id: NSNumber?
    var Message: String?
    var MsgFrom: String?
    var ObjectType: String?
    var ObjectKey: String?
    var ParsePushId: String?
    var isRead: String = "0"
    var ReceivedAt: Date?
    var locationNotificationContent: LocationNotificationMessage?
    var GetLocation: String?
}
