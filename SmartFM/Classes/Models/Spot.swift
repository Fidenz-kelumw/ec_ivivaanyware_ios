//
//  Spot.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/8/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class Spot: OLCModel {
    
    var Id: NSNumber?
    var SpotID: String?
    var LocationKey: String?
    var ObjectKey: String?
    var ObjectType: String?
    var NotifyNearestSpot: String?
    var Lan: String?
    var Lat: String?
    var Distance: String?
    var isNotified: Bool = false
}
