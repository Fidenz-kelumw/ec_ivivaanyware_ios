//
//  Spot.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/11/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class SpotDetails {
    
    var SpotPopupHeading: String?
    var SpotPopupDescription: String?
    var ButtonText: String?
    var FocusTab: String?
    var SpotPopupImageURL: String?
    var NextpageObjectType: String?
    var NextPageObjectKey: String?
    var EnableActionButton: String?
}

