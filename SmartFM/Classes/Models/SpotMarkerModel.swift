//
//  Spot.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/12/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import GoogleMaps

class SpotMarkerModel {
    
    var marker: GMSMarker?
    var spotItem: Spot?
    
    init(marker: GMSMarker, spotItem: Spot) {
        self.marker = marker
        self.spotItem = spotItem
    }
}


