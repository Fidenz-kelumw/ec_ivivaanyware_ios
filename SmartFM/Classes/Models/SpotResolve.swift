//
//  SpotResolve.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 12/13/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class SpotResolve: NSObject {
    
    var spot: Spot?
    var isInside: Bool = false
    var isNotified: Bool = false
    var distenceToLocation: Double?
    
    init(spot: Spot, isInside: Bool, isNotified: Bool, distenceToLocation: Double) {
        self.spot = spot
        self.isInside = isInside
        self.isNotified = isNotified
        self.distenceToLocation = distenceToLocation
    }
}
