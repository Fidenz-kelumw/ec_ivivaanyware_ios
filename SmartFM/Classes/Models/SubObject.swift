//
//  SubObject.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/30/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class SubObjectsMain: OLCModel {
    
    var sub: [SubObject] = []
    var subFilter: SubObjectFilter?
    var Id: NSNumber?
    var temp = "aa"
    
    class override func ignoredProperties()->[Any] {
        return ["sub", "subFilter"]
    }
    
    override func save() -> Bool {
        
        if let objId = self.saveAndGetId() {
            
            for subobj in self.sub {
                subobj.mainobjectId = objId
                _ = subobj.save()
            }
            
            self.subFilter?.objectId = objId
            _ = self.subFilter?.save()
            
        } else {
            return false
        }
        return true
    }
    
    func getSubObjects() -> [SubObject]? {
        
        if let id = self.Id {
            if let subs = SubObject.__where("mainobjectId = \(id)", sortBy: "Id", accending: true) as? [SubObject] {
                for sub in subs {
                    if let id = sub.ObjectKey {
                        sub.action = SubObjectAction.getActions(forSubobject: id)
                    }
                }
                
                return subs
            }
        }
        
        return nil
    }
    
    func getFilter() -> SubObjectFilter? {
        
        if let id = self.Id {
            
            return SubObjectFilter.getSubObjectFilter(forSubobject: id)
        }
        
        return nil
    }
    
    class override func truncateTable() -> Bool {
        super.truncateTable()
        
        _ = SubObject.truncateTable()
        _ = SubObjectFilter.truncateTable()
        
        return true
    }
}

class SubObject: OLCModel {
    
    var action: [SubObjectAction]?
    var Id: NSNumber?
    var ObjectType: String?
    var ObjectKey: String?
    var ObjectID: String?
    var Description: String?
    var Stage: String?
    var mainobjectId: NSNumber?
    
    override func save() -> Bool {
        
        if let id = self.saveAndGetId() {
            
            if let actionList = action {
                for act in actionList {
                    act.objectId = id
                    act.save()
                }
            }
            
            return true
        } else {
            return false
        }
            
    }
    
    class override func ignoredProperties()->[Any] {
        return ["Action"]
    }
    
    class override func truncateTable() -> Bool {
        
        _ = SubObjectAction.truncateTable()
        return super.truncateTable()
    }
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.mainobjectId  = aDecoder.decodeObject(forKey: "mainobjectId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.mainobjectId {
            aCoder.encode(obj, forKey: "mainobjectId")
        }
    }
    
}

class SubObjectAction: OLCModel {
    
    var Id: NSNumber?
    var ButtonText: String?
    var ActionID: String?
    var UserConfirmation: String?
    var objectId: NSNumber?
    var objectKey: String?
    var IsChecked: String?
    
    static func getActions(forSubobject id: String) -> [SubObjectAction]? {
        let data = SubObjectAction.__where("objectKey='\(id)'", sortBy: "Id", accending: true) as? [SubObjectAction]
        return data
    }
}

class SubObjectFilter: OLCModel {
    
    var defaultData: SubObjectDefaultData?
    var OptionList: [SubObjectOptionsList]?
    var Id: NSNumber?
    var NoDataText: String?
    var objectId: NSNumber?
    
    override func save() -> Bool {
        
        if let id = self.saveAndGetId() {
            self.defaultData?.filterId = id
            self.defaultData?.save()
            
            if let opts = self.OptionList {
                
                for opt in opts {
                    opt.filterId = id
                    opt.save()
                }
            }
            return true
            
        } else {
            return false
        }
    }
    
    static func getSubObjectFilter(forSubobject id: NSNumber) -> SubObjectFilter? {
        
        if let filter = SubObjectFilter.__where("objectId=\(id)", sortBy: "Id", accending: true).first as? SubObjectFilter {
            
            if let id = filter.Id {
                filter.defaultData = SubObjectDefaultData.getDefaultData(forFilter: id)
                filter.OptionList = SubObjectOptionsList.getOptionsList(forFilter: id)
            }
            
            return filter
        }
        
        return nil
    }
    
    class override func truncateTable() -> Bool {
        
        _ = SubObjectDefaultData.truncateTable()
        _ = SubObjectOptionsList.truncateTable()
        
        return super.truncateTable()
    }
    
    class override func ignoredProperties()->[Any] {
        return ["defaultData", "OptionList"]
    }
}


class SubObjectDefaultData: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    
    var filterId: NSNumber?
    
    static func getDefaultData(forFilter id: NSNumber) -> SubObjectDefaultData? {
        
        return SubObjectDefaultData.__where("filterId=\(id)", sortBy: "Id", accending: true).first as? SubObjectDefaultData
    }
}


class SubObjectOptionsList: OLCModel {
    
    var Id: NSNumber?
    var DisplayText: String?
    var Value: String?
    
    var filterId: NSNumber?
    
    static func getOptionsList(forFilter id: NSNumber) -> [SubObjectOptionsList]? {
        
        return SubObjectOptionsList.__where("filterId=\(id)", sortBy: "Id", accending: true) as? [SubObjectOptionsList]
    }
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.filterId  = aDecoder.decodeObject(forKey: "filterId") as? NSNumber
    }
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        if let obj = self.filterId {
            aCoder.encode(obj, forKey: "filterId")
        }
    }
}
