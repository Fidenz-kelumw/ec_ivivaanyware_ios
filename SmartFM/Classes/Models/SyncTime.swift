//
//  SyncTime.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class SyncTime: OLCModel {
    
    var Id: NSNumber?
    var EntityKey: String?
    var UpdatedAt: Date?
}

enum EntityKey: String {
    case Building="building", ObjectType = "objecttype", Filter="filter", DefaultData="DefaultData", Master="Master", ObjectInformation="ObjectInformation", Members="Members", Messages="Messages", Attachments="Attachments", SubObjects="SubObjects", Layout="Layout", Checklist="Checklist", CreateField="CreateField",CustomSearch="CustomSearch",BackgroundEnterTime="BackgroundEnterTime", ObjectList="ObjectList", ItemList="ItemList", URLs="URLs", Spots="Spots"
}
