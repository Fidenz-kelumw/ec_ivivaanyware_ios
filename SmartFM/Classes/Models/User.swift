//
//  User.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/5/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class User {
    
    var Id: NSNumber?
    var UserID: String?
    var accountInfo: AccountInfo?
    var defaults: Defaults?
    var UserKey: String?
    var UserDomain: String?
    
}

class AccountInfo {
    
    var Id: NSNumber?
    var Account: String?
    var ApiKey: String?
    var SSL: String?
    var iVivaStatus: String?
}

class Defaults: OLCModel {
    
    var Id: NSNumber?
    var AttachmentName: String?
    var MasterDataExpiryHours: String?
    var TransactionExpirySeconds: String?
    var NearestSpotUpdateSeconds: String?
    var UserLocationUpdateMins: String?
    var InboxSize: String?
    var LocationKey: String?
    var ImagePath: String?
    var UserName: String?
    var GetObjectForQR: String?
    var isUserLocationUpdateON: String?
    var AppName: String?
    
    class override func ignoredProperties()-> [Any] {
        
        return ["ImagePath","UserName"]
    }
}
