//
//  APIClient.swift
//  Headcount
//
//  Created by Lasith Hettiarachchi on 8/24/15.
//  Copyright (c) 2015 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import Alamofire

class APIClient: NSObject {
    
    var apiKey: String?
    var account: String?
    var userKey: String?
    static let API_VERSION = "2"
    
    override init() {
        super.init()
        
        let userManager = UserManager()
        self.apiKey = userManager.getApiKey()
        self.userKey = userManager.getUserKey()
        self.account = userManager.getAccount()
        
        if let ssl = userManager.getSSL() {
            Protocol = ssl
        }
        
    }
    
    func getDomainData(_ domain: String, completionHandler:@escaping (AccountInfo?, NSError?)->Void) {
        if let url = URL(string: "https://domains.fxelle.com/v1/domaindata") {
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let values = ["domain":"\(domain)"]
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: values, options: [])
                
                Alamofire.request(request as URLRequestConvertible).responseJSON { (response) in
                    
                    let error = response.result.error
                    let value = response.result.value
                    
                    if error != nil {
                        NSLog("[APIClient] | [getDomainData] | Error = \(error!.localizedDescription)")
                        completionHandler(nil, error as NSError?)
                        return
                        
                    } else if let json = value as? NSDictionary {
                        
                        if response.response?.statusCode != 200 {
                            
                            let er = NSError(domain: "Failed to register", code: response.response!.statusCode, userInfo: nil)
                            completionHandler(nil,er)
                        } else {
                            
                            let domainData = AccountInfo()
                            domainData.SSL = json.value(forKey: "ssl") as? String
                            domainData.Account = json.value(forKey: "account") as? String
                            domainData.ApiKey = json.value(forKey: "apikey") as? String
                            domainData.iVivaStatus = json.value(forKey: "iviva-status") as? String
                            
                            completionHandler(domainData, error as NSError?)
                        }
                        
                        
                    } else {
                        NSLog("[APIClient] | [getDomainData] | response.value is nil")
                        completionHandler(nil,error as NSError?)
                        return
                    }
                }
                
                
            } catch let error as NSError {
                NSLog("[APIClient] - [getDomainData] - \(error.localizedDescription)")
                completionHandler(nil,nil)
            } catch {
                NSLog("[APIClient] - [getDomainData] - An error occured while converting to JSON")
                completionHandler(nil,nil)
            }
            
        } else {
            completionHandler(nil,nil)
        }
    }
    
    func callAPI(_ apiCall: String, parameters:[String:String], account:String, completionHandler: @escaping (NSDictionary?) -> Void) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let reqURL = "\(Protocol)://\(account)/OI/SmartFMAPI-\(APIClient.API_VERSION)/\(apiCall)"
        Alamofire.request(reqURL, method: .post, parameters: parameters, headers: headers)
            
            .responseString(){ response in
                
                let string = response.result.value
                
                let res: HTTPURLResponse? = response.response
                
                if let errorMessage = string {
                    
                    ErrorLogger.logError(errorMessage, response: res, apiName: apiCall)
                }
            }
            .responseJSON(){ response in
                
                let error = response.result.error
                let value = response.result.value
                
                if error != nil {
                    NSLog("[APIClient] | [\(apiCall)] | Error = \(error?.localizedDescription)")
                    completionHandler(nil)
                    return
                    
                } else if let json = value as? NSDictionary {
                   
                    completionHandler(json)
                    
                } else {
                    NSLog("[APIClient] | [\(apiCall)] | response.value is nil")
                    completionHandler(nil)
                    return
                }
            }
    }
    
    func callAPIWithErrorMessageFromString(_ apiCall: String, parameters:[String:String], account:String, completionHandler: @escaping (NSDictionary?, String?) -> Void) {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var stringErrorMessage: String?
        
        let reqURL = "\(Protocol)://\(account)/OI/SmartFMAPI-\(APIClient.API_VERSION)/\(apiCall)"
        Alamofire.request(reqURL, method: .post, parameters: parameters, headers: headers)
            
            .responseString(){ response in
                
                let string = response.result.value
                
                let res: HTTPURLResponse? = response.response
                
                if let errorMessage = string {
                    stringErrorMessage = errorMessage
                    ErrorLogger.logError(errorMessage, response: res, apiName: apiCall)
                }
            }
            .responseJSON(){ response in
                
                let error = response.result.error
                let value = response.result.value
                
                if error != nil {
                    NSLog("[APIClient] | [\(apiCall)] | Error = \(String(describing: error?.localizedDescription))")
                    completionHandler(nil, stringErrorMessage)
                    return
                    
                } else if let json = value as? NSDictionary {
                    
                    completionHandler(json, nil)
                    
                } else {
                    NSLog("[APIClient] | [\(apiCall)] | response.value is nil")
                    completionHandler(nil, stringErrorMessage)
                    return
                }
        }
    }
    
    func fetchLookupServices(_ name: String, fieldInfo: String?, objectType: String, completion: @escaping ([LData]?, String?)->Void) {
        
        var locKey: String = ""
        if let def = Defaults.all().first as? Defaults, let key = def.LocationKey {
            locKey = key
        }
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "LocationKey": locKey
            ]
            
            if let info = fieldInfo {
            
                params["FieldInfo"] = info
            }
            
            self.callAPIWithErrorMessageFromString(name, parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var errorMessage = "No Items Found"
                    if let er = error {
                        errorMessage = er
                    }
                    
                    if let data = json.value(forKey: "Data") as? [NSDictionary] {
                
                        var dataObjs: [LData] = []
                        for item in data {
                            let dataItem: LData = LData()
                            dataItem.DisplayText = item.value(forKey: "DisplayText") as? String
                            dataItem.Value = item.value(forKey: "Value") as? String
                            dataObjs.append(dataItem)
                        }
                        
                        if dataObjs.count == 1 {
                            if let val = dataObjs[0].Value, val == "0", let msg = dataObjs[0].DisplayText {
                                completion(nil, msg)
                            } else {
                                completion(dataObjs, nil)
                            }
                        } else {
                            completion(dataObjs, nil)
                        }
                        
                    } else {
                        completion(nil, errorMessage)
                    }
                    
                } else {
                    completion(nil, "No Items Found")
                }
            })
        }
    }
    
    func registerUser(_ userId:String, registrationCode:String, domainId:String, account: String, apiKey: String, firebaseToken: String?, completionHandler: @escaping (User?, String?) -> Void) -> (){
        
        var params = [
            "apikey" : apiKey,
            "UserID": userId,
            "RegistrationCode": registrationCode,
            "DomainID": domainId
        ]
        
        if let token = firebaseToken {
            params["FirebaseToken"] = token
        } else {
            NSLog("No token found")
        }
        
        self.callAPIWithErrorMessageFromString("RegisterUser", parameters: params, account: account) { (response, error) -> Void in
            
            var errorMessage = "Failed to Register"
            if let er = error {
                errorMessage = er
            }
            
            if let json = response {
                
                if let user = self.extractUser(json) {
                    user.UserID = userId
                    user.UserDomain = domainId
                    user.defaults = self.extractDefaults(json)
                    user.accountInfo = self.extractAccountInfo(json)
                    
                    completionHandler(user, nil)
                    return
                    
                } else {
                    completionHandler(nil, errorMessage)
                }
                
            } else {
                completionHandler(nil, errorMessage)
            }
        }
        
    }
    
    func getBuildings(_ completionHandler: @escaping ([Building]?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            
            self.callAPI("GetBuildings", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    
                    if let allBuildings = json.value(forKey: "Buildings") as? [NSDictionary] {
                        
                        var buildings: [Building] = []
                        for building in allBuildings {
                            let build: Building = Building()
                            if let locName = building.value(forKey: "LocationName") as? String {
                                build.LocationName = locName
                            }
                            if let LocationKey = building.value(forKey: "LocationKey") as? String {
                                build.LocationKey = LocationKey
                            }
                            
                            buildings.append(build)
                        }
                        
                        completionHandler(buildings)
                        return
                    } else {
                        completionHandler(nil)
                    }
                    
                } else {
                    
                    completionHandler(nil)
                }
            })
            
        }
        
    }
    
    func getObjectTypes(_ completionHandler: @escaping ([ObjType]?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            
            self.callAPI("GetObjectTypes", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    
                    if let objectTypes = json.value(forKey: "ObjectTypes") as? [NSDictionary] {
                        
                        var objectTypesObjs: [ObjType] = []
                        
                        for objectType in objectTypes {
                            
                            let objectTypeObj: ObjType = ObjType()
                            
                            //ObjectType
                            if let ot = objectType.value(forKey: "ObjectType") as? String {
                                objectTypeObj.ObjectType = ot
                            }
                            objectTypeObj.MenuName = objectType.value(forKey: "MenuName") as? String
                            objectTypeObj.ObjectIconPath = objectType.value(forKey: "ObjectIconPath") as? String
                            if let CustomFilterPage = objectType.value(forKey: "CustomFilterPage") as? String {
                                
                                objectTypeObj.CustomFilterPage  = CustomFilterPage
                            }
                            if let FilterListPage = objectType.value(forKey: "FilterListPage") as? String {
                                
                                objectTypeObj.FilterListPage  = FilterListPage
                            }
                            if let CreatePage = objectType.value(forKey: "CreatePage") as? String {
                                
                                objectTypeObj.CreatePage  = CreatePage
                            }
                            if let QRFormatiOS = objectType.value(forKey: "QRFormatiOS") as? String {
                                objectTypeObj.QRFormatiOS = QRFormatiOS
                            }
                            if let QRFormatAndroid = objectType.value(forKey: "QRFormatAndroid") as? String {
                                objectTypeObj.QRFormatAndroid = QRFormatAndroid
                            }
                            if let MovetoDetailPage = objectType.value(forKey: "MovetoDetailPage") as? String {
                                objectTypeObj.MovetoDetailPage = MovetoDetailPage
                            }
                            if let MenuType = objectType.value(forKey: "MenuType") as? String {
                                objectTypeObj.MenuType = MenuType
                            }
                            if let MenuLink = objectType.value(forKey: "MenuLink") as? String {
                                objectTypeObj.MenuLink = MenuLink
                            }
                            if let URL = objectType.value(forKey: "URL") as? String {
                                objectTypeObj.URL = URL
                            }
                            
                            //CreateFromMenu
                            if let CreateFromMenu = objectType.value(forKey: "CreateFromMenu") as? [NSDictionary] {
                                objectTypeObj.hasCreateFromMenu = "1"
                                var defDatas: [HomeDefaultData] = []
                                
                                for fromItem in CreateFromMenu {
                                    
                                    let defdata = HomeDefaultData()
                                    defdata.FieldID = fromItem.value(forKey: "FieldID") as? String
                                    defdata.Value = fromItem.value(forKey: "Value") as? String
                                    defdata.DisplayText = fromItem.value(forKey: "DisplayText") as? String
                                    defdata.ObjectType = objectTypeObj.ObjectType
                                    defDatas.append(defdata)
                                }
                                
                                objectTypeObj.CreateFromMenu = defDatas
                            } else {
                                objectTypeObj.hasCreateFromMenu = "0"
                            }
                            
                            //Stages
                            if let stages = objectType.value(forKey: "Stages") as? [NSDictionary] {
                                var stageObjs: [Stage] = []
                                for stage in stages {
                                    let stageObj: Stage = Stage()
                                    stageObj.ObjectType = objectTypeObj.ObjectType
                                    if let Stage = stage.value(forKey: "Stage") as? String {
                                        stageObj.Stage = Stage
                                    }
                                    if let Color = stage.value(forKey: "Color") as? String {
                                        stageObj.Color = Color
                                    }
                                    stageObjs.append(stageObj)
                                }
                                objectTypeObj.stages = stageObjs
                            }
                            
                            //Roles
                            if let roles = objectType.value(forKey: "Roles") as? [NSDictionary] {
                                var roleObjs: [Role] = []
                                for role in roles {
                                    let roleObj: Role = Role()
                                    roleObj.ObjectType = objectTypeObj.ObjectType
                                    if let RoleID = role.value(forKey: "RoleID") as? String {
                                        roleObj.RoleID = RoleID
                                    }
                                    if let Color = role.value(forKey: "Color") as? String {
                                        roleObj.Color = Color
                                    }
                                    roleObjs.append(roleObj)
                                }
                                objectTypeObj.roles = roleObjs
                            }
                            
                            //Tabs
                            if let tabs = objectType.value(forKey: "Tabs") as? [NSDictionary] {
                                var tabObjs: [Tab] = []
                                for tab in tabs {
                                    let tabObj: Tab = Tab()
                                    tabObj.ObjectType = objectTypeObj.ObjectType
                                    if let TabType = tab.value(forKey: "TabType") as? String {
                                        tabObj.TabType = TabType
                                    }
                                    if let TabName = tab.value(forKey: "TabName") as? String {
                                        tabObj.TabName = TabName
                                    }
                                    if let TabHide = tab.value(forKey: "TabHide") as? String {
                                        tabObj.TabHide = TabHide
                                    }
                                    tabObjs.append(tabObj)
                                }
                                objectTypeObj.tabs = tabObjs
                            }
                            
                            objectTypesObjs.append(objectTypeObj)
                        }
                        
                        completionHandler(objectTypesObjs)
                        
                    } else {
                        completionHandler(nil)
                        return
                    }
                    
                } else {
                    
                    completionHandler(nil)
                }
            })
            
        }
        
    }
    
    func getFilters(_ completionHandler: @escaping ([Filter]?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            self.callAPI("GetFilters", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    
                    if let filters = json.value(forKey: "Filters") as? [NSDictionary] {
                        
                        var filtersObjs: [Filter] = []
                        
                        for filter in filters {
                            
                            let filterObj = Filter()
                            
                            if let objType = filter.value(forKey: "ObjectType") as? String {
                                filterObj.ObjectType = objType
                            }
                            if let FilterID = filter.value(forKey: "FilterID") as? String {
                                filterObj.FilterID = FilterID
                            }
                            if let FilterName = filter.value(forKey: "FilterName") as? String {
                                filterObj.FilterName = FilterName
                            }
                            if let Description = filter.value(forKey: "Description") as? String {
                                filterObj.Description = Description
                            }
                            
                            filtersObjs.append(filterObj)
                        }
                        
                        completionHandler(filtersObjs)
                        
                    } else {
                        completionHandler(nil)
                    }
                    
                } else {
                    
                    completionHandler(nil)
                }
                
            })
            
        }
        
    }
    
    func getDefaultData(_ completionHandler: @escaping (Defaults?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            self.callAPI("GetDefaultData", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    
                    if let defaultdatas = json.value(forKey: "DefaultData") as? NSDictionary {
                        
                        let defObj = Defaults()
                        defObj.LocationKey = defaultdatas.value(forKey: "LocationKey") as? String
                        defObj.UserName = defaultdatas.value(forKey: "UserName") as? String
                        defObj.ImagePath = defaultdatas.value(forKey: "ImagePath") as? String
                        
                        
                        defObj.MasterDataExpiryHours = defaultdatas.value(forKey: "MasterDataExpiryHours") as? String
                        defObj.TransactionExpirySeconds = defaultdatas.value(forKey: "TransactionExpirySeconds") as? String
                        defObj.GetObjectForQR = defaultdatas.value(forKey: "GetObjectForQR") as? String
                        defObj.InboxSize = defaultdatas.value(forKey: "InboxSize") as? String
                        defObj.AppName = defaultdatas.value(forKey: "AppName") as? String
                        completionHandler(defObj)
                        
                    } else {
                        completionHandler(nil)
                    }
                    
                } else {
                    
                    completionHandler(nil)
                }
                
            })
            
        }
        
    }
    
    func getObjects(_ objectType: String, locationKey: String?, filterId: String?, customFilter: String?, more: String?, completionHandler: @escaping ([Object]?, String?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType
            ]
            
            if let locKey = locationKey {
                params["LocationKey"] = locKey
            }
            
            if let filter = filterId {
                params["FilterID"] = filter
            }
            
            if let custfilter = customFilter {
                params["CustomFilter"] = custfilter
            }
            
            if let mr = more {
                params["More"] = mr
            }
            
            self.callAPIWithErrorMessageFromString("GetObjects", parameters: params, account: acc, completionHandler: { (response, error) -> Void in
                
                if let json = response {
                    
                    if let objects = json.value(forKey: "Objects") as? [NSDictionary] {
                        
                        var objectsObjs: [Object] = []
                        
                        for object in objects {
                            
                            let objectObj = Object()
                            
                            if let ObjectType = object.value(forKey: "ObjectType") as? String {
                                objectObj.ObjectType = ObjectType
                            }
                            if let ObjectKey = object.value(forKey: "ObjectKey") as? String {
                                objectObj.ObjectKey = ObjectKey
                            }
                            if let ObjectID = object.value(forKey: "ObjectID") as? String {
                                objectObj.ObjectID = ObjectID
                            }
                            if let Descrition = object.value(forKey: "Descrition") as? String {
                                objectObj.Description = Descrition
                            }
                            if let Description = object.value(forKey: "Description") as? String {
                                objectObj.Description = Description
                            }
                            if let Stage = object.value(forKey: "Stage") as? String {
                                objectObj.Stage = Stage
                            }
                            
                            objectsObjs.append(objectObj)
                        }
                        
                        completionHandler(objectsObjs, nil)
                    } else {
                        completionHandler(nil, error)
                    }
                    
                } else {
                    
                    completionHandler(nil, nil)
                }
            })
            
        }
        
    }
    
    func getSubObjects(_ objectType: String, objectKey: String, tabName: String, filter: String?, completionHandler: @escaping (SubObjectsMain?, String?) -> Void) -> (){
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            if let fltr = filter {
                params["FilterID"] = fltr
            }
            
            self.callAPIWithErrorMessageFromString("GetSubObjects", parameters: params, account: acc, completionHandler: { (response, error) -> Void in
                
                if let json = response {
                    
                    let sub = SubObjectsMain()
                    
                    if let filter = json.value(forKey: "Filter") as? NSDictionary {
                        
                        sub.subFilter = self.extractFilter(from: filter)
                    }
                    
                    if let objects = json.value(forKey: "SubObjects") as? [NSDictionary] {
                        
                        sub.sub = self.extractSubObjects(from: objects)
                    }
                    
                    
                    completionHandler(sub, nil)
                    
                } else {
                    
                    completionHandler(nil, error)
                }
            })
            
        }
        
    }
    
    func subObjectAction(objectType: String, objectKey: String, actionId: String, completion: @escaping (Bool, String) -> Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "ActionID": actionId
            ]
            
            
            self.callAPIWithErrorMessageFromString("SubObjectAction", parameters: params, account: acc, completionHandler: { (response, error) -> Void in
                
                if let json = response {
                    
                    var status = false
                    if let success = json.value(forKey: "Success") as? String {
                        
                        status = success == "1"
                    }
                    
                    var msg = ""
                    if let message = json.value(forKey: "Message") as? String {
                        
                        msg = message
                    } else {
                        
                        if status {
                            msg = "Action successful"
                        } else {
                            msg = "Action failed"
                        }
                    }
                    
                    
                    completion(status, msg)
                    
                } else {
                    var errorMessage = "Failed. Please try again"
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, "\(errorMessage)")
                }
            })
            
        }
    }
    
    fileprivate func extractFilter(from json: NSDictionary) -> SubObjectFilter {
        
        let filter = SubObjectFilter()
        filter.NoDataText = json.value(forKey: "NoDataText") as? String
        
//        if let dd = json.value(forKey: "DefaultData") as? NSDictionary {
//            let def = SubObjectDefaultData()
//            def.DisplayText = dd.value(forKey: "DisplayText") as? String
//            def.Value = dd.value(forKey: "Value") as? String
//            filter.defaultData = def
//        }
        
        if let options = json.value(forKey: "OptionList") as? [NSDictionary] {
            
            var optionsList: [SubObjectOptionsList] = []
            
            for optList in options {
                let optionList: SubObjectOptionsList = SubObjectOptionsList()
                optionList.DisplayText = optList.value(forKey: "DisplayText") as? String
                optionList.Value = optList.value(forKey: "Value") as? String
                optionsList.append(optionList)
            }
            
            filter.OptionList = optionsList
        }
        
        return filter
    }
    
    fileprivate func extractSubObjects(from json: [NSDictionary]) -> [SubObject] {
        
        var objectsObjs: [SubObject] = []
        
        for object in json {
            
            let objectObj = SubObject()
            
            if let ObjectType = object.value(forKey: "ObjectType") as? String {
                objectObj.ObjectType = ObjectType
            }
            if let ObjectKey = object.value(forKey: "ObjectKey") as? String {
                objectObj.ObjectKey = ObjectKey
            }
            if let ObjectID = object.value(forKey: "ObjectID") as? String {
                objectObj.ObjectID = ObjectID
            }
            if let Descrition = object.value(forKey: "Descrition") as? String {
                objectObj.Description = Descrition
            }
            if let Description = object.value(forKey: "Description") as? String {
                objectObj.Description = Description
            }
            if let Stage = object.value(forKey: "Stage") as? String {
                objectObj.Stage = Stage
            }
            
            if let subActJson = object.value(forKey: "Action") as? [NSDictionary] {
                objectObj.action = self.extractSubObjectAction(from: subActJson, objectKey: objectObj.ObjectKey)
            }
            
            objectsObjs.append(objectObj)
        }
        
        return objectsObjs
    }
    
    fileprivate func extractSubObjectAction(from json: [NSDictionary], objectKey: String?) -> [SubObjectAction] {
        
        var subActions: [SubObjectAction] = []
        
        for object in json {
            
            let subAction = SubObjectAction()
            subAction.ButtonText = object.value(forKey: "ButtonText") as? String
            subAction.ActionID = object.value(forKey: "ActionID") as? String
            subAction.UserConfirmation = object.value(forKey: "UserConfirmation") as? String
            subAction.IsChecked = object.value(forKey: "IsChecked") as? String
            subAction.objectKey = objectKey
            
            subActions.append(subAction)
        }

        return subActions
    }

    func getObjectInfo(_ objectType: String, objectKey: String, completion: @escaping (ObjectInformation?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectInfo", parameters: params, account: acc, completionHandler: { (response, error) -> Void in
                
                if let json = response {
                    let oi = self.extractObjectInformations(json, arrayName: "ObjectInfo")
                    
                    completion(oi, nil)
                    
                } else {
                    completion(nil, error)
                }
            })
        }
    }
    
    func getCreateFields(_ objectType: String, completion: @escaping (ObjectInformation?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType
            ]
            
            self.callAPI("GetCreateFields", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    completion(self.extractObjectInformations(json, arrayName: "CreateFields"))
                    
                } else {
                    completion(nil)
                }
            })
        }
    }
    
    func getCustomSearchFields(_ objectType: String, locationKey: String?, completion: @escaping (ObjectInformation?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType
            ]
            
            if let loc = locationKey {
                params["LocationKey"] = loc
            }
            
            self.callAPI("GetFilterFields", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    completion(self.extractObjectInformations(json, arrayName: "FilterFields"))
                    
                } else {
                    completion(nil)
                }
            })
        }
    }
    
    func updateObjectInfo(_ objectType:String, objectKey:String, fieldData:String, completion: @escaping (Bool,String)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "FieldData": fieldData
            ]
            
            self.callAPIWithErrorMessageFromString("UpdateObjectInfo", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    var status = false
                    var label = "Failed to update info"
                    
                    if let success = json.value(forKey: "Success") as? String {
                        status = success=="1" ? true : false
                    }
                    if let message = json.value(forKey: "Message") as? String {
                        label = message
                    }
                    
                    completion(status,label)
                    
                } else {
                    var errorMessage = "Failed to update info"
                    if let er = error{
                        errorMessage = er
                    }
                    completion(false, errorMessage)
                }
            })
        }
    }
    
    func executeAction(_ action: Action, objectType: String, objectKey: String, completion: @escaping (Bool,String?,PageAction)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey, let actionId = action.ActionID {
            
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "ActionID": actionId
            ]
            
            if let cargo = action.Cargo {
                params["Cargo"] = cargo
            }
            
            self.callAPIWithErrorMessageFromString("ExecuteAction", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var status = false
                    let message: String? = json.value(forKey: "Message") as? String
                    var pageAction: PageAction = PageAction.None
                    
                    if let success = json.value(forKey: "Success") as? String {
                        status = success=="1" ? true : false
                    }
                    
                    if let pAction = json.value(forKey: "PageAction") as? String {
                        
                        if pAction == "" {
                            pageAction = PageAction.None
                        } else {
                            pageAction = PageAction(rawValue: pAction) != nil ? PageAction(rawValue: pAction)! : PageAction.None
                        }
                    }
                    
                    completion(status,message,pageAction)
                    
                } else {
                    var errorMessage = "Failed"
                    if let er = error{
                        errorMessage = er
                    }
                    
                    completion(false,errorMessage,PageAction.None)
                }
            })
        }
    }
    
    func getObjectMembers(_ objectType:String, objectKey:String, tabName: String, completion: @escaping ([Member]?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectMembers", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var memberObjs: [Member] = []
                    
                    if let members = json.value(forKey: "Members") as? [NSDictionary] {
                        
                        for memberJson in members {
                            
                            let member = Member()
                            
                            member.RoleID = memberJson.value(forKey: "RoleID") as? String
                            member.ObjectType = objectType
                            
                            let memberUser = MemberUser()
                            
                            if let memberUserJson = memberJson.value(forKey: "User") as? NSDictionary {
                                
                                memberUser.UserKey = memberUserJson.value(forKey: "UserKey") as? String
                                memberUser.UserName = memberUserJson.value(forKey: "UserName") as? String
                                memberUser.ImagePath = memberUserJson.value(forKey: "ImagePath") as? String
                                memberUser.Phone = memberUserJson.value(forKey: "Phone") as? String
                                
                                member.User = memberUser
                            }
                            
                            memberObjs.append(member)
                        }
                        
                        completion(memberObjs, nil)
                        
                    } else {
                        
                        completion(nil, error)
                    }
                } else {
                    
                    completion(nil, error)
                }
            })
        }
    }
    
    func getObjectURLs(_ objectType:String, objectKey:String, tabName: String, completion: @escaping ([ObjectURL]?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectURLs", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var urlobjs: [ObjectURL] = []
                    
                    if let urls = json.value(forKey: "ObjectURLs") as? [NSDictionary] {
                        
                        for urlJson in urls {
                            
                            let url = ObjectURL()
                            
                            url.URLName = urlJson.value(forKey: "URLName") as? String
                            url.URLPath = urlJson.value(forKey: "URLPath") as? String
                            url.UpdatedAt = urlJson.value(forKey: "UpdatedAt") as? String
                            
                            urlobjs.append(url)
                        }
                        
                        completion(urlobjs, nil)
                        
                    } else {
                        
                        completion(nil, error)
                    }
                } else {
                    
                    completion(nil, error)
                }
            })
        }
    }
    
    func getObjectItems(_ objectType:String, objectKey:String, tabName: String, completion: @escaping ([ObjectItem]?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectItems", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var itemobjs: [ObjectItem] = []
                    
                    if let items = json.value(forKey: "ObjectItems") as? [NSDictionary] {
                        
                        for itemJson in items {
                            
                            let objectItem = self.extractObjectItems(json: itemJson)
                            objectItem.PageEditEnabled = json.value(forKey: "PageEditEnabled") as? String
                            
                            itemobjs.append(objectItem)
                        }
                        
                        completion(itemobjs, nil)
                        
                    } else {
                        
                        completion(nil, error)
                    }
                } else {
                    
                    completion(nil, error)
                }
            })
        }
    }
    
    func getUpdateObjectItem(_ objectType:String, objectKey:String, tabName: String, itemKey: String, itemData: String, completion: @escaping (Bool, String)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "ItemKey":itemKey,
                "ItemData":itemData
            ]
            
            var errorMessage = "Update failed"
            
            self.callAPIWithErrorMessageFromString("UpdateObjectItem", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    if let success = json.value(forKey: "Success") as? String {
                        
                        if let message = json.value(forKey: "Message") as? String {
                            completion(success == "1", message)
                        } else {
                            completion(success == "1", "Updated successfully")
                        }
                        
                    } else {
                        
                        if let er = error{
                            errorMessage = er
                        }
                        completion(false, "\(errorMessage)")
                    }
                } else {
                    
                    if let er = error{
                        errorMessage = er
                    }
                    completion(false, "\(errorMessage)")
                }
            })
        }
    }
    
    func getObjectChecklist(_ objectType:String, objectKey:String, tabName: String, completion: @escaping ([CheckItem]?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectChecklist", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    if let checklistItems = json.value(forKey: "CheckItems") as? [NSDictionary] {
                        
                        var checklistObjs: [CheckItem] = []
                        
                        for checklistItem in checklistItems {
                            let item = CheckItem()
                            item.ItemKey = checklistItem.value(forKey: "ItemKey") as? String
                            item.ItemName = checklistItem.value(forKey: "ItemName") as? String
                            item.Description = checklistItem.value(forKey: "Description") as? String
                            item.Deadline = checklistItem.value(forKey: "Deadline") as? String
                            item.Status = checklistItem.value(forKey: "Status") as? String
                            item.StatusEdit = checklistItem.value(forKey: "StatusEdit") as? String
                            item.CalendarEdit = checklistItem.value(forKey: "CalendarEdit") as? String
                            item.DescriptionEdit = checklistItem.value(forKey: "DescriptionEdit") as? String
                            item.InfoCode = checklistItem.value(forKey: "InfoCode") as? String
                            
                            checklistObjs.append(item)
                        }
                        
                        completion(checklistObjs, nil)
                        
                    } else {
                        completion(nil, error)
                    }
                    
                } else {
                    
                    completion(nil, error)
                }
            })
        }
    }
    
    func getCheckItemInfo(objectType:String, objectKey:String, infoCode: String, completion: @escaping (String?) -> Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "InfoCode": infoCode
            ]
            
            self.callAPI("GetCheckItemInfo", parameters: params, account: acc, completionHandler: { (data) in
                
                if let json = data {
                    completion(json.value(forKey: "InfoText") as? String)
                } else {
                    completion(nil)
                }
            })
        } else {
            completion(nil)
        }
    }
    
    func createObject(_ objectType:String, fieldData:String, completion: @escaping (Bool, String, String?, String)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "FieldData": fieldData
            ]
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            var errorMessage: String = "Failed to create object"
            let apiCall = "CreateObject"
            
            let reqURL = "\(Protocol)://\(acc)/OI/SmartFMAPI-\(APIClient.API_VERSION)/\(apiCall)"
            Alamofire.request(reqURL, method: .post, parameters: params, headers: headers)
                
                .responseString(){ response in
                
                    let string = response.result.value
                    let res: HTTPURLResponse? = response.response
                    
                    if let errorMesg = string {
                        errorMessage = errorMesg
                        ErrorLogger.logError(errorMesg, response: res, apiName: "CreateObject")
                    }
                }
                .responseJSON(){ response in
                    
                    let error = response.result.error
                    let value = response.result.value
                    
                    if error != nil {
                        NSLog("[APIClient] | [\(apiCall)] | Error = \(String(describing: error?.localizedDescription))")
                        completion(false, "\(errorMessage)", nil, "0")
                        return
                        
                    } else if let json = value as? NSDictionary {
                        var status = false
                        var label = "Failed to create object"
                        let showDetails = json.value(forKey: "DetailsPage") as? String
                        
                        
                        if let success = json.value(forKey: "Success") as? String {
                            status = success=="1" ? true : false
                        }
                        if let message = json.value(forKey: "Message") as? String {
                            label = message
                        }
                        
                        completion(status,label,json.value(forKey: "ObjectKey") as? String, showDetails != nil ? showDetails! : "0")
                        
                    } else {
                        NSLog("[APIClient] | [\(apiCall)] | response.value is nil")
                        completion(false, "\(errorMessage)", nil, "0")
                        return
                    }
            }
        }
    }
    
    func getObjectForQr(_ qrCode: String, completion:@escaping (String?,String?,String?)-> Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "QRCode": qrCode,
                "LookUpService": ""
            ]
            
            
            print("\(params)\n\(acc)")
            self.callAPIWithErrorMessageFromString("GetObjectForQR", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let objectKey = json.value(forKey: "ObjectKey") as? String
                    let objectType = json.value(forKey: "ObjectType") as? String
                    let message = json.value(forKey: "Message") as? String
                    completion(objectType,objectKey, message)
                    
                } else {
                    
                    var errorMessage:String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    
                    completion(nil,nil,errorMessage)
                }
            })
        }
    }
    
    func getItemForQr(_ qrCode: String, lookUpService: String, completion:@escaping (LData?, String?)-> Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "QRCode": qrCode,
                "LookUpService": lookUpService
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectForQR", parameters: params, account: acc, completionHandler: { (response, error) in
                if let json = response {
                    let data = LData()
                    data.Value = json.value(forKey: "ObjectKey") as? String
                    data.DisplayText = json.value(forKey: "ObjectID") as? String
                    let message = json.value(forKey: "Message") as? String
                    completion(data,message)
                    
                } else {
                    
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(nil, errorMessage)
                }
            })
        }
    }
    
    func setItemStatus(_ objectType:String, objectKey:String, tabName: String, itemKey:String, status:String, completion: @escaping (Bool,String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "CheckItemKey": itemKey,
                "Status": status
            ]
            
            self.callAPIWithErrorMessageFromString("SetCheckItemStatus", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let status = json.value(forKey: "Success") as? String
                    let message = json.value(forKey: "Message") as? String
                    
                    completion(status == "1" ? true : false, message)
                    
                } else {
                    
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, errorMessage)
                }
            })
        }
    }
    
    func setItemDescription(_ objectType:String, objectKey:String, tabName: String, itemKey:String, description:String, completion: @escaping (Bool,String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "CheckItemKey": itemKey,
                "Description": description
            ]
            
            self.callAPIWithErrorMessageFromString("SetCheckItemDescription", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let status = json.value(forKey: "Success") as? String
                    let message = json.value(forKey: "Message") as? String
                    
                    completion(status == "1" ? true : false, message)
                    
                } else {
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, errorMessage)
                }
            })
        }
    }
    
    func setItemDeadline(_ objectType:String, objectKey:String, tabName: String, itemKey:String, deadline:String, completion: @escaping (Bool,String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "CheckItemKey": itemKey,
                "Deadline": deadline
            ]
            
            self.callAPIWithErrorMessageFromString("SetCheckItemDeadline", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let status = json.value(forKey: "Success") as? String
                    let message = json.value(forKey: "Message") as? String
                    
                    completion(status == "1" ? true : false, message)
                    
                } else {
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, errorMessage)
                }
            })
        }
    }

    func getObjectMessages(_ objectType:String, objectKey:String, tabName: String, completion: @escaping (MessageMain?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectMessages", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let messageMain = MessageMain()
                    if let messages = json.value(forKey: "Messages") as? [NSDictionary] {
                        
                        var messageObjs:[Message]  = []
                        
                        for message in messages {
                            
                            let messageObj: Message = Message()
                            
                            messageObj.MessageKey = message.value(forKey: "MessageKey") as? String
                            messageObj.MessageText = message.value(forKey: "MessageText") as? String
                            messageObj.CreatedAt = message.value(forKey: "CreatedAt") as? String
                            
                            if let createdUser = message.value(forKey: "CreatedUser") as? NSDictionary {
                                
                                let userObj = MessageUser()
                                userObj.UserKey = createdUser.value(forKey: "UserKey") as? String
                                userObj.UserName = createdUser.value(forKey: "UserName") as? String
                                userObj.ImagePath = createdUser.value(forKey: "ImagePath") as? String
                                messageObj.CreatedUser = userObj
                            }
                            
                            messageObjs.append(messageObj)
                        }
                        
                        messageMain.Messages = messageObjs
                    }
                    
                    
                    messageMain.AddNewMessage = json.value(forKey: "AddNewMessage") as? String
                    messageMain.NewMessageAction = json.value(forKey: "NewMessageAction") as? String
                    
                    if let messageTypesJson = json.value(forKey: "MessageTypes") as? [NSDictionary] {
                        
                        var messagetypes: [MessageType] = []
                        
                        for messagetypeJson in messageTypesJson {
                            let mstype: MessageType = MessageType()
                            mstype.TypeID = messagetypeJson.value(forKey: "TypeID") as? String
                            mstype.TypeName = messagetypeJson.value(forKey: "TypeName") as? String
                            messagetypes.append(mstype)
                        }
                        
                        messageMain.MessageTypes = messagetypes
                    }
                    
                    completion(messageMain, nil)
                    
                } else {
                    completion(nil, error)
                }
                
            })
            
        }
    }

    func addMessage(_ objectType:String, objectKey:String, tabName: String, message: String, indexPath:IndexPath?, messageTypeId: String, completion: @escaping (Bool, String?, String?, IndexPath?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "MessageText": message,
                "MessageType": messageTypeId
            ]
            
            self.callAPIWithErrorMessageFromString("AddNewMessage", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var message: String?
                    var messageKey: String?
                    
                    if let success = json.value(forKey: "Success") as? String {
                        
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        }
                        if let msgKey = json.value(forKey: "MessageKey") as? String {
                            messageKey = msgKey
                        }
                        
                        completion(success=="1" ? true : false, messageKey, message, indexPath)
                        
                    } else {
                        
                        completion(false, messageKey, message, indexPath)
                    }
                    
                } else {
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, nil, errorMessage, indexPath)
                }
                
            })
            
        }
    }
    
    func forwardObject(_ objectType:String, objectKey:String, memberKey: String, message: String, completion: @escaping (Bool, String)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "MemberKey": memberKey,
                "MessageText": message
            ]
            
            
            self.callAPIWithErrorMessageFromString("ForwardObject", parameters: params, account: acc, completionHandler: { (response, error) in
                
                var message: String = "Error occured. Please try again"
                
                if let json = response {
                    
                    
                    if let success = json.value(forKey: "Success") as? String {
                        
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        } else {
                            if success == "1" {
                                message = "Successfully updated"
                            }
                        }
                        
                        completion(success=="1" ? true : false, message)
                        
                    } else {
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        }
                        completion(false, message)
                    }
                    
                } else {
                    if let er = error {
                        message = er
                    }
                    completion(false, message)
                }
                
            })
        }
    }
    
    func getObjectLayout(_ objectType:String, objectKey:String, tabName: String, completion: @escaping (Layout?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPI("GetObjectLayout", parameters: params, account: acc, completionHandler: { (response) in
                
                if let json = response {
                    
                    let layoutObj = Layout()
                    
                    if let layout = json.value(forKey: "Layout") as? NSDictionary {
                        
                        layoutObj.Path = layout.value(forKey: "Path") as? String
                        layoutObj.ZoomLevels = layout.value(forKey: "ZoomLevels") as? String
                        if let pinlabel = layout.value(forKey: "PinLabel") as? NSDictionary {
                            let pinlbl = PinLabel()
                            pinlbl.Name = pinlabel.value(forKey: "Name") as? String
                            pinlbl.Description = pinlabel.value(forKey: "Description") as? String
                            layoutObj.pinLabel = pinlbl
                        }
                        
                    }
                    
                    if let pins = json.value(forKey: "Pins") as? [NSDictionary] {
                        
                        var layoutPinObjs:[LayoutPin]  = []
                        
                        for pin in pins {
                            
                            let layoutPinObj: LayoutPin = LayoutPin()
                            
                            layoutPinObj.Name = pin.value(forKey: "Name") as? String
                            layoutPinObj.Description = pin.value(forKey: "Description") as? String
                            layoutPinObj.X = pin.value(forKey: "X") as? String
                            layoutPinObj.Y = pin.value(forKey: "Y") as? String
                            layoutPinObj.Color = pin.value(forKey: "Color") as? String
                            layoutPinObj.ObjectType = pin.value(forKey: "ObjectType") as? String
                            layoutPinObj.ObjectKey = pin.value(forKey: "ObjectKey") as? String
                            
                            
                            
                            layoutPinObjs.append(layoutPinObj)
                        }
                        
                        layoutObj.pins = layoutPinObjs
                    }
                    
                    if let droppedPin = json.value(forKey: "DroppedPin") as? NSDictionary {
                        
                        let droppedPinObj: DroppedPin = DroppedPin()
                        droppedPinObj.X = droppedPin.value(forKey: "X") as? String
                        droppedPinObj.Y = droppedPin.value(forKey: "Y") as? String
                        droppedPinObj.Editable = droppedPin.value(forKey: "Editable") as? String
                        
                        layoutObj.droppedPin = droppedPinObj
                    }
                    
                    completion(layoutObj)
                    
                } else {
                    completion(nil)
                }
                
            })
            
        }
    }
    
    func updateDroppedPin(_ objectType:String, objectKey:String, tabName: String, X: String, Y: String, completion: @escaping (Bool, String)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "X": X,
                "Y": Y
            ]
            
            
            self.callAPIWithErrorMessageFromString("UpdateDroppedPin", parameters: params, account: acc, completionHandler: { (response, error) in
                
                var message: String = "Error occured. Please try again"
                
                if let json = response {
                    
                    
                    if let success = json.value(forKey: "Success") as? String {
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        } else {
                            if success == "1" {
                                message = "Successfully updated"
                            }
                        }
                        
                        completion(success=="1" ? true : false, message)
                        
                    } else {
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        }
                        completion(false, message)
                    }
                    
                } else {
                    
                    if let er = error {
                        message = er
                    }
                    completion(false, message)
                }
                
            })
        }
    }
    
    func getObjectAttachments(_ objectType:String, objectKey:String, tabName: String, completion: @escaping (AttachmentsMain?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName
            ]
            
            self.callAPIWithErrorMessageFromString("GetObjectAttachments", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    let attachmentsMain: AttachmentsMain = AttachmentsMain()
                    
                    
                    //Extract attachments
                    if let attachments = json.value(forKey: "Attachments") as? [NSDictionary] {
                        
                        var attachmentObjs:[Attachment]  = []
                        
                        for attachment in attachments {
                            
                            let attachmentObj: Attachment = Attachment()
                            
                            attachmentObj.AttachmentKey = attachment.value(forKey: "AttachmentKey") as? String
                            attachmentObj.AttachmentType = attachment.value(forKey: "AttachmentType") as? String
                            attachmentObj.AttachmentName = attachment.value(forKey: "AttachmentName") as? String
                            attachmentObj.FilePath = attachment.value(forKey: "FilePath") as? String
                            attachmentObj.UploadedAt = attachment.value(forKey: "UploadedAt") as? String
                            attachmentObj.ATTType = attachment.value(forKey: "ATTType") as? String
                            
                            if let uploadedUser = attachment.value(forKey: "UploadedUser") as? NSDictionary {
                                
                                let userObj = UploadedUser()
                                userObj.UserKey = uploadedUser.value(forKey: "UserKey") as? String
                                userObj.UserName = uploadedUser.value(forKey: "UserName") as? String
                                userObj.ImagePath = uploadedUser.value(forKey: "ImagePath") as? String
                                attachmentObj.User = userObj
                            }
                            
                            attachmentObjs.append(attachmentObj)
                        }
                        
                        attachmentsMain.attachments = attachmentObjs
                        
                    }
                    
                    
                    attachmentsMain.AddNewAttachment = json.value(forKey: "AddNewAttachment") as? String
                    
                    attachmentsMain.InputModes = json.value(forKey: "InputModes") as? [String]
                    
                    
                    //Extract attachment types
                    if let attachmentTypes = json.value(forKey: "AttachmentTypes") as? [NSDictionary] {
                        
                        var attachmentTypeObjs:[AttachmentType]  = []
                        
                        for attachmentType in attachmentTypes {
                            
                            let attachmentTypeObj: AttachmentType = AttachmentType()
                            
                            attachmentTypeObj.TypeName = attachmentType.value(forKey: "TypeName") as? String
                            attachmentTypeObj.Signature = attachmentType.value(forKey: "Signature") as? String
                            
                            attachmentTypeObjs.append(attachmentTypeObj)
                        }
                        
                        attachmentsMain.AttachmentTypes = attachmentTypeObjs
                        
                    }
                    
                    completion(attachmentsMain, nil)
                    
                } else {
                    completion(nil, error)
                }
                
            })
            
        }
    }
    
    func addNewAttachment(_ objectType:String, objectKey:String, tabName: String, attachmentType: String, attachmentName: String?, fileName: String, mediaType: MediaType, completion: @escaping (Bool, String?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey = self.apiKey {
            
            var params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "TabName": tabName,
                "AttachmentType": attachmentType,
                "FileName": fileName,
                "ATTType": mediaType.rawValue
            ]
            
            if let name = attachmentName , name != "" {
                params["AttachmentName"] = name
            }
            
            self.callAPIWithErrorMessageFromString("AddNewAttachment", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var message: String?
                    var messageKey: String?
                    
                    if let success = json.value(forKey: "Success") as? String {
                        
                        if let msg = json.value(forKey: "Message") as? String {
                            message = msg
                        }
                        if let msgKey = json.value(forKey: "AttachmentKey") as? String {
                            messageKey = msgKey
                        }
                        
                        completion(success=="1" ? true : false, messageKey, message)
                        
                    } else {
                        
                        var errorMessage: String? = nil
                        if let er = error {
                            errorMessage = er
                        }
                        completion(false, messageKey, errorMessage)
                    }
                    
                } else {
                    var errorMessage: String? = nil
                    if let er = error {
                        errorMessage = er
                    }
                    completion(false, nil, errorMessage)
                }
                
            })
            
        }
    }
    
    func validateUser(_ completion:@escaping (Bool)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            
            self.callAPI("ValidateRegistration", parameters: params, account: acc, completionHandler: { (response) -> Void in
                
                if let json = response {
                    
                    if let status = json.value(forKey: "Success") as? String {
                        
                        if status == "0" {
                            completion(false)
                        } else {
                            completion(true)
                        }
                        
                    } else {
                        completion(true)
                    }
                    
                } else {
                    
                    completion(true)
                }
                
            })
            
        } else {
            completion(true)
        }
    }
    
    func uploadVideo(url: URL, name: String,uploadFolder: String, completion: @escaping (Bool)->Void) {
        
        if let account = self.account {
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    //multipartFormData.append(attData as Data, withName: "file", fileName: "image.mov", mimeType: "video/quicktime")
                    multipartFormData.append(url, withName: "videoFile", fileName: "video.mov", mimeType: "video/quicktime")
                    multipartFormData.append(name.data(using: String.Encoding.utf8)!, withName: "filename")
            },
                
                to: "\(Protocol)://\(account)\(uploadFolder)",
                
                encodingCompletion: { encodingResult in
                    
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            let error = response.result.error
                            let res = response.response
                            
                            if error != nil {
                                NSLog("[APIClient] | [uploadVideo] | Error = \(error?.localizedDescription)")
                                completion(false)
                                return
                                
                            } else if res?.statusCode == 200 {
                                completion(true)
                            } else {
                                completion(false)
                            }
                        }
                        upload.uploadProgress(closure: { (prog) in
                            print("progress = \(prog.fractionCompleted)")
                        })
                        
                        
                    case .failure(let encodingError):
                        
                        NSLog("encodingError = \(encodingError)")
                        completion(false)
                    }
                    
                    
            }
            )
            
        }
    }
    
    func uploadAudio(url: URL, name: String,uploadFolder: String, completion: @escaping (Bool)->Void) {
        
        if let account = self.account {
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(url, withName: "audioFile", fileName: "audio.m4a", mimeType: "audio/mp4a-latm")
                    multipartFormData.append(name.data(using: String.Encoding.utf8)!, withName: "filename")
            },
                
                to: "\(Protocol)://\(account)\(uploadFolder)",
                
                encodingCompletion: { encodingResult in
                    
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            let error = response.result.error
                            let res = response.response
                            
                            if error != nil {
                                NSLog("[APIClient] | [uploadAudio] | Error = \(error?.localizedDescription)")
                                completion(false)
                                return
                                
                            } else if res?.statusCode == 200 {
                                completion(true)
                            } else {
                                completion(false)
                            }
                        }
                        upload.uploadProgress(closure: { (prog) in
                            print("progress = \(prog.fractionCompleted)")
                        })
                        
                        
                    case .failure(let encodingError):
                        
                        NSLog("encodingError = \(encodingError)")
                        completion(false)
                    }
                    
                    
            }
            )
            
        }
    }
    
    func uploadImage(_ image:UIImage, name: String, uploadFolder: String, completion: @escaping (Bool)->Void) {
        
        var compressedImage: UIImage = image
        if let size = UserManager().getUploadSize() {
            
            if size == ImageUploadSize.Small {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 400)
                
            } else if size == ImageUploadSize.Large {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 1200)
                
            } else if size == ImageUploadSize.Original {
                
            } else {
                compressedImage = ImageHandler().resizeShorterSize(of: image, to: 800)
            }
        }
        
        if let account = self.account {
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    if let imageData: Data = UIImageJPEGRepresentation(compressedImage, 1) {
                        multipartFormData.append(imageData, withName: "file", fileName: "image.jpg", mimeType: "image/jpg")
                        multipartFormData.append(name.data(using: String.Encoding.utf8)!, withName: "filename")
                    } else{
                        NSLog("[uploadImage] - failed to convert to nsdata")
                    }
                },
                
                to: "\(Protocol)://\(account)\(uploadFolder)",
                
                encodingCompletion: { encodingResult in
                    
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            let error = response.result.error
                            let res = response.response
                            
                            if error != nil {
                                NSLog("[APIClient] | [uploadImages] | Error = \(String(describing: error?.localizedDescription))")
                                completion(false)
                                return
                                
                            } else if res?.statusCode == 200 {
                                completion(true)
                            } else {
                                completion(false)
                            }
                        }
                        
                    case .failure(let encodingError):
                        
                        NSLog("encodingError = \(encodingError)")
                        completion(false)
                    }
                    
                    
                }
            )
        
        }
    }
    
    func getSpots(completion: @escaping ([Spot]?, String?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk
            ]
            
            self.callAPIWithErrorMessageFromString("GetSpots", parameters: params, account: acc, completionHandler: { (response, error) in
                
                if let json = response {
                    
                    var spotObjs: [Spot] = []
                    
                    if let spots = json.value(forKey: "Spots") as? [NSDictionary] {
                        
                        for spotJson in spots {
                            
                            let spot = Spot()
                            
                            spot.SpotID = spotJson.value(forKey: "SpotID") as? String
                            spot.LocationKey = spotJson.value(forKey: "LocationKey") as? String
                            spot.ObjectKey = spotJson.value(forKey: "ObjectKey") as? String
                            spot.ObjectType = spotJson.value(forKey: "ObjectType") as? String
                            spot.NotifyNearestSpot = spotJson.value(forKey: "NotifyNearestSpot") as? String
                            spot.Lan = spotJson.value(forKey: "Lan") as? String
                            spot.Lat = spotJson.value(forKey: "Lat") as? String
                            spot.Distance = spotJson.value(forKey: "Distance") as? String
                            
                            spotObjs.append(spot)
                        }
                        
                        completion(spotObjs, nil)
                        
                    } else {
                        
                        completion(nil, error)
                    }
                } else {
                    
                    completion(nil, error)
                }
            })
        }
    }
    
    func getSpotDetails(objectType: String, objectKey: String, locationKey: String, distence: String, completion: @escaping (SpotDetails?)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "ObjectType": objectType,
                "ObjectKey": objectKey,
                "LocationKey": locationKey,
                "Distance": distence
            ]
        
            self.callAPI("GetSpotDetails", parameters: params, account: acc, completionHandler: { (response) in
               
                if let json = response {
                    
                
                    let result = SpotDetails()
                    
                    if let heading = json.value(forKey: "SpotPopupHeading") as? String {
                        result.SpotPopupHeading = heading
                    }
                    if let description = json.value(forKey: "SpotPopupDescription") as? String {
                        result.SpotPopupDescription = description
                    }
                    if let text = json.value(forKey: "ButtonText") as? String {
                        result.ButtonText = text
                    }
                    if let tab = json.value(forKey: "FocusTab") as? String {
                        result.FocusTab = tab
                    }
                    if let imgURL = json.value(forKey: "SpotPopupImageURL") as? String {
                        result.SpotPopupImageURL = imgURL
                    }
                    if let nxtObjectType = json.value(forKey: "NextpageObjectType") as? String {
                        result.NextpageObjectType = nxtObjectType
                    }
                    if let nxtObjectKey = json.value(forKey: "NextPageObjectKey") as? String {
                        result.NextPageObjectKey = nxtObjectKey
                    }
                    if let btnStatus = json.value(forKey: "EnableActionButton") as? String {
                        result.EnableActionButton = btnStatus
                    }
                    
                    completion(result)
                } else {
                    
                    completion(nil)
                }
            })
        }
    }
    
    func notifyNearestSpot(spotId: String, proximity: String, distence: String, completion: @escaping (Bool)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "SpotID": spotId,
                "Proximity": proximity,
                "Distance": distence
            ]
            
            self.callAPI("NearestSpot", parameters: params, account: acc, completionHandler: { (response) in
                
                if let json = response {
                    
                    var status: Bool = false;
                    
                    if let state = json.value(forKey: "Status") as? String {
                        status = (state=="1" ? true : false)
                    }
                    
                    completion(status)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    func updateUserGEOLocation(lat: String, lon: String, completion: @escaping (Bool)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "lat": lat,
                "lang": lon
            ]
            
            self.callAPI("UpdateUserGPSCoordinates", parameters: params, account: acc, completionHandler: { (response) in
                
                if let json = response {
                    
                    var status: Bool = false;
                    
                    if let state = json.value(forKey: "Success") as? String {
                        status = (state=="1" ? true : false)
                    }
                    
                    completion(status)
                } else {
                    completion(false)
                }
            })
        }
    }
    
    //Firebase
    func updateFirebaseToken(_ token: String, completion: @escaping (Bool)->Void) {
        
        if let uk = self.userKey, let acc = self.account, let apikey=self.apiKey {
            
            let params = [
                "apikey" : apikey,
                "UserKey": uk,
                "FirebaseToken" : token
            ]
            
            self.callAPI("UpdateFirebaseToken", parameters: params, account: acc, completionHandler: { (response) in
                
                if let _ = response {
                    
                    completion(true)
                    
                } else {
                    completion(false)
                }
                
            })
            
        } else {
            completion(false)
        }
    }
    
    /////////////////////////////
    //MARK: - Helpers
    /////////////////////////////
    
    //object items
    private func extractObjectItems(json: NSDictionary) -> ObjectItem {
        
        let objectItem = ObjectItem()
        objectItem.ItemName = json.value(forKey: "ItemName") as? String
        objectItem.ItemKey = json.value(forKey: "ItemKey") as? String
        objectItem.ItemData = json.value(forKey: "ItemData") as? String
        objectItem.DataColor = json.value(forKey: "DataColor") as? String
        objectItem.ItemEdit = json.value(forKey: "ItemEdit") as? String
        objectItem.TypeID = json.value(forKey: "TypeID") as? String
        
        if objectItem.TypeID == ObjectItemType.VALUE.rawValue, let typeData = json.value(forKey: "TypeData") as? NSDictionary {
            objectItem.valueTypeData = self.extractValueTypeData(json: typeData)
            
        } else if objectItem.TypeID == ObjectItemType.SWITCH.rawValue, let typeData = json.value(forKey: "TypeData") as? NSDictionary {
            objectItem.switchtypeData = self.extractSwitchTypeData(json: typeData)
            
        } else if objectItem.TypeID == ObjectItemType.BUTTONS.rawValue, let typeData = json.value(forKey: "TypeData") as? [NSDictionary] {
            objectItem.buttonsTypeData = self.extractButtonsTypeData(json: typeData)
        }
        
        return objectItem
    }
    
    fileprivate func extractValueTypeData(json: NSDictionary) -> ValueTypeData {
        
        let valueTypeData = ValueTypeData()
        if let min = json.value(forKey: "MIN") as? Float {
            valueTypeData.MIN = "\(min)"
        }
        if let max = json.value(forKey: "MAX") as? Float {
            valueTypeData.MAX = "\(max)"
        }
        if let inc = json.value(forKey: "INC") as? Float {
            valueTypeData.INC = "\(inc)"
        }
        
        return valueTypeData
    }
    
    fileprivate func extractSwitchTypeData(json: NSDictionary) -> SwitchTypeData {
        
        let switchTypeData = SwitchTypeData()
        switchTypeData.isON = json.value(forKey: "ON") as? String
        switchTypeData.isOFF = json.value(forKey: "OFF") as? String
        
        return switchTypeData
    }
    
    fileprivate func extractButtonsTypeData(json: [NSDictionary]) -> [ButtonsTypeData] {
        
        var datas: [ButtonsTypeData] = []
        
        for jsonItem in json {
            let data = ButtonsTypeData()
            data.Name = jsonItem.value(forKey: "Name") as? String
            data.Value = jsonItem.value(forKey: "Value") as? String
            datas.append(data)
        }
        
        return datas
    }

    //Registration
    fileprivate func extractDefaults(_ json: NSDictionary) -> Defaults? {
        
        if let defs = json.value(forKey: "Defaults") as? NSDictionary, let usr = json.value(forKey: "User") as? NSDictionary {
            let defaults = Defaults()
            
            if let MasterDataExpiryHours = defs.value(forKey: "MasterDataExpiryHours") as? String {
                defaults.MasterDataExpiryHours = MasterDataExpiryHours
            }
            if let TransactionExpirySeconds = defs.value(forKey: "TransactionExpirySeconds") as? String {
                defaults.TransactionExpirySeconds = TransactionExpirySeconds
            }
            if let LocationKey = defs.value(forKey: "LocationKey") as? String {
                defaults.LocationKey = LocationKey
            }
            if let AttachmentName = defs.value(forKey: "AttachmentName") as? String {
                defaults.AttachmentName = AttachmentName
            }
            
            defaults.InboxSize = defs.value(forKey: "InboxSize") as? String
            
            if let UserName = usr.value(forKey: "UserName") as? String {
                defaults.UserName = UserName
            }
            
            if let ImagePath = usr.value(forKey: "ImagePath") as? String {
                defaults.ImagePath = ImagePath
            }
            
            if let AppName = usr.value(forKey: "AppName") as? String {
                defaults.AppName = AppName
//                let userManager = UserManager()
//                userManager.setAppName(AppName)
                
            }
            
            defaults.NearestSpotUpdateSeconds = "30"
            defaults.UserLocationUpdateMins = "10"
            
            return defaults
        }
        
        return nil
    }
    
    fileprivate func extractUser(_ json: NSDictionary) -> User? {
        
        if let usr = json.value(forKey: "User") as? NSDictionary {
            let user = User()
            
            if let UserKey = usr.value(forKey: "UserKey") as? String , UserKey != "" {
                user.UserKey = UserKey
                
            } else {
                return nil
            }
            
            return user
        }
        
        return nil
    }
    
    fileprivate func extractAccountInfo(_ json: NSDictionary) -> AccountInfo? {
        
        if let defs = json.value(forKey: "AccountInfo") as? NSDictionary {
            let ai = AccountInfo()
            ai.Account = defs.value(forKey: "Account") as? String
            ai.ApiKey = defs.value(forKey: "ApiKey") as? String
            return ai
        } else {
            
            return nil
        }
        
    }
    
    fileprivate func shouldMakeSecondAttempt(_ json: NSDictionary) -> Bool {
        
        if let redirect = json.value(forKey: "Redirect") as? String {
            if redirect == "1" {
                return true
            }
        }
        if let redirect = json.value(forKey: "Redirect") as? Int {
            if redirect == 1 {
                return true
            }
        }
        
        return false
    }
    
    
    //Object Information
    fileprivate func extractObjectInformations(_ json: NSDictionary, arrayName:String) -> ObjectInformation? {
        let objectInformation = ObjectInformation()
        
        if let objectInfos = json.value(forKey: arrayName) as? [NSDictionary] {
            
            var objectInfoObjs: [ObjectInfo] = []
            
            for objectInfo in objectInfos {
                
                objectInfoObjs.append(self.extractObjectInfo(objectInfo))
            }
            
            objectInformation.objectInfo = objectInfoObjs
        }
        
        if let UpdateObjectInfo = json.value(forKey: "UpdateObjectInfo") as? String {
            objectInformation.UpdateObjectInfo = UpdateObjectInfo
        }
        
        if let UpdateConfirmation = json.value(forKey: "UpdateConfirmation") as? String {
            objectInformation.UpdateConfirmation = UpdateConfirmation
        }
        
        objectInformation.UpdateButtonText = json.value(forKey: "UpdateButtonText") as? String
        objectInformation.HideUpdateButton = json.value(forKey: "HideUpdateButton") as? String
        
        if let PageLinks = json.value(forKey: "PageLinks") as? [NSDictionary] {
            objectInformation.pageLinks = self.extractPageLinks(PageLinks)
        }
        
        if let Actions = json.value(forKey: "Actions") as? [NSDictionary] {
            objectInformation.actions = self.extractActions(Actions)
        }
        
        return objectInformation
    }
    
    fileprivate func extractPageLinks(_ pLinks: [NSDictionary]) -> [PageLink] {
        
        var pagelinks: [PageLink] = []
        
        for plink in pLinks {
            
            let link = PageLink()
            if let LinkType = plink.value(forKey: "LinkType") as? String {
                link.LinkType = LinkType
            }
            if let LinkName = plink.value(forKey: "LinkName") as? String {
                link.LinkName = LinkName
            }
            if let ObjectType = plink.value(forKey: "ObjectType") as? String {
                link.ObjectType = ObjectType
            }
            if let ObjectKey = plink.value(forKey: "ObjectKey") as? String {
                link.ObjectKey = ObjectKey
            }
            if let FieldData = plink.value(forKey: "FieldData") as? [NSDictionary] {
                link.fieldData = self.extractFieldData(FieldData)
            }
            pagelinks.append(link)
        }
        
        return pagelinks
    }
    
    fileprivate func extractActions(_ acts: [NSDictionary]) -> [Action] {
        
        var actions: [Action] = []
        
        for act in acts {
            
            let action = Action()
            if let ActionID = act.value(forKey: "ActionID") as? String {
                action.ActionID = ActionID
            }
            if let ActionName = act.value(forKey: "ActionName") as? String {
                action.ActionName = ActionName
            }
            if let Enabled = act.value(forKey: "Enabled") as? String {
                action.Enabled = Enabled
            }
            if let Confirmation = act.value(forKey: "Confirmation") as? String {
                action.Confirmation = Confirmation
            }
            if let Cargo = act.value(forKey: "Cargo") as? String {
                action.Cargo = Cargo
            }
            actions.append(action)
        }
        
        return actions
    }
    
    fileprivate func extractFieldData(_ fDatas: [NSDictionary]) -> [FieldData] {
        var fielddatas: [FieldData] = []
        
        for fdata in fDatas {
            let fielddata = FieldData()
            if let FieldID = fdata.value(forKey: "FieldID") as? String {
                fielddata.FieldID = FieldID
            }
            if let Value = fdata.value(forKey: "Value") as? String {
                fielddata.Value = Value
            }
            if let DisplayText = fdata.value(forKey: "DisplayText") as? String {
                fielddata.DisplayText = DisplayText
            }
            
            fielddatas.append(fielddata)
        }
        
        return fielddatas
    }
    
    fileprivate func extractObjectInfo(_ objectInfoJson: NSDictionary) -> ObjectInfo {
        
        let objectInfo: ObjectInfo = ObjectInfo()
        if let FieldID = objectInfoJson.value(forKey: "FieldID") as? String {
            objectInfo.FieldID = FieldID
        }
        if let FieldName = objectInfoJson.value(forKey: "FieldName") as? String {
            objectInfo.FieldName = FieldName
        }
        if let ValueType = objectInfoJson.value(forKey: "ValueType") as? String {
            objectInfo.ValueType = ValueType
        }
        if let DefaultData = objectInfoJson.value(forKey: "DefaultData") as? NSDictionary {
            objectInfo.defaultData = self.extractDefaultData(DefaultData)
        }
        if let OptionList = objectInfoJson.value(forKey: "OptionList") as? [NSDictionary] {
            objectInfo.OptionList = self.extractOptionsList(OptionList)
        }
        if let Editable = objectInfoJson.value(forKey: "Editable") as? String {
            objectInfo.Editable = Editable
        }
        
        objectInfo.DisableDelete = objectInfoJson.value(forKey: "DisableDelete") as? String
        
        objectInfo.Extended = objectInfoJson.value(forKey: "Extended") as? String
        
        objectInfo.NoDataValue = objectInfoJson.value(forKey: "NoDataValue") as? String
        
        if let LookUpService = objectInfoJson.value(forKey: "LookUpService") as? String {
            objectInfo.LookUpService = LookUpService
        }
        if let Mandatory = objectInfoJson.value(forKey: "Mandatory") as? String {
            
            objectInfo.Mandatory = Mandatory
        }
        objectInfo.QRCode = objectInfoJson.value(forKey: "QRCode") as? String
        
        objectInfo.Style = objectInfoJson.value(forKey: "Style") as? String
        
        objectInfo.UploadFolder = objectInfoJson.value(forKey: "UploadFolder") as? String
        
        objectInfo.DownloadFolder = objectInfoJson.value(forKey: "DownloadFolder") as? String
        
        objectInfo.InputModes = objectInfoJson.value(forKey: "InputModes") as? [String]
        
        return objectInfo
    }
    
    fileprivate func extractDefaultData(_ defData: NSDictionary) -> DefaultData {
        
        let defaultdata: DefaultData = DefaultData()
        
        if let DisplayText = defData.value(forKey: "DisplayText") as? String {
            defaultdata.DisplayText = DisplayText
        }
        if let Value = defData.value(forKey: "Value") as? String {
            defaultdata.Value = Value
        }
        
        if let Values = defData.value(forKey: "Value") as? [NSDictionary] {
            defaultdata.Values = self.extractDefaultDataValues(values: Values)
        }
        
        if let Values = defData.value(forKey: "Value") as? [String] {
            defaultdata.ValuesArray = Values
        }
        
        return defaultdata
    }
    
    fileprivate func extractDefaultDataValues(values: [NSDictionary]) -> [DefDataValue] {
        
        var defaultDataValues: [DefDataValue] = []
        
        for value in values {
            
            let defaultdataValue = DefDataValue()
            defaultdataValue.ATTName = value.value(forKey: "ATTName") as? String
            defaultdataValue.ATTType = value.value(forKey: "ATTType") as? String
            
            defaultDataValues.append(defaultdataValue)
        }
        
        return defaultDataValues
    }
    
    fileprivate func extractOptionsList(_ options: [NSDictionary]) -> [OptionsList] {
        
        var optionsList: [OptionsList] = []
        
        for optList in options {
            let optionList: OptionsList = OptionsList()
            if let DisplayText = optList.value(forKey: "DisplayText") as? String {
                optionList.DisplayText = DisplayText
            }
            if let Value = optList.value(forKey: "Value") as? String {
                optionList.Value = Value
            }
            optionsList.append(optionList)
        }
        
        return optionsList
    }

}
