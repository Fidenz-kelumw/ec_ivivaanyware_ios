//
//  BackgroundLocationService.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 2/8/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocaitonUpdates {
    func locationUpdates(locaiton: CLLocation)
}

class BackgroundLocationService: NSObject, CLLocationManagerDelegate {
    
    public static let sharedInstance = BackgroundLocationService()
    
    let locationManager: CLLocationManager
    var delegate: LocaitonUpdates?
    
    override init() {
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 100
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        
        super.init()
        locationManager.delegate = self
    }
    
    // found location update
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let newLocation = locations.last {
            //self.delegate?.locationUpdates(locaiton: newLocation)
            
            let lat = "\(newLocation.coordinate.latitude)"
            let lon = "\(newLocation.coordinate.longitude)"
            
            APIClient().updateUserGEOLocation(lat: lat, lon: lon, completion: { (status) in
                print("Location data sent to server : \(status)")
            })
        }
    }
    
    // location manager with distence filter
    func startUpdatingLocation(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.stopUpdatingLocation()
            locationManager.startUpdatingLocation()
        }else{
            showTurnOnLocationServiceAlert()
        }
    }
    
    // stop getting updates - normal
    func stopUpdatingLocation(){
        locationManager.stopUpdatingLocation()
    }
    
    // stop getting updates - SignificantLocationChanges
    func stopMonitoringSignificantLocationChanges(){
        print("stopMonitoringSignificantLocationChanges")
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    // go low power location updates using
    func startMonitoringSignificantLocationChanges(){
        print("startMonitoringSignificantLocationChanges")
        locationManager.stopUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func showTurnOnLocationServiceAlert(){
        NotificationCenter.default.post(name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
}
