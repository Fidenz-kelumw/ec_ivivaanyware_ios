//
//  SingleLocationUpdate.swift
//  SmartFM
//
//  Created by Chathura Hettiarachchi on 2/8/18.
//  Copyright © 2018 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import CoreLocation

enum LocationManagerResponse: String {
    case success, denied = "Please turn on location services for SmartFM", disabled = "Please enable location services"
}

class SingleLocationUpdate:NSObject, CLLocationManagerDelegate {
    
    static let shared = SingleLocationUpdate()
    fileprivate let locationManager = CLLocationManager()
    fileprivate static var currentLocation: CLLocation?
    
    private override init() {
        super.init()
        self.locationManager.delegate = self
    }
    
    fileprivate func requestAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    fileprivate func startUpdatingLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    func start() {
        if CLLocationManager.locationServicesEnabled() {
            print("enabled")
            if CLLocationManager.authorizationStatus() == .denied {
                // Location Services are denied
                print("denied")
            } else {
                if CLLocationManager.authorizationStatus() == .notDetermined {
                    //request authorization
                    print("requesting")
                    self.requestAuthorization()
                } else {
                    //start updating
                    print("starting update")
                    self.startUpdatingLocation()
                }
            }
        } else {
            print("Disabled")
            //enable location services
        }
    }
    
    func stop() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func getCurrentLocation() -> (Double, Double)? {
        
        if let loc = SingleLocationUpdate.currentLocation {
            print("current location | lat = \(loc.coordinate.latitude), long = \(loc.coordinate.longitude)")
            return (loc.coordinate.latitude, loc.coordinate.longitude)
        }
        return nil
    }
    
    
    //Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        SingleLocationUpdate.currentLocation = locations.last
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            self.startUpdatingLocation()
        }
    }
}

