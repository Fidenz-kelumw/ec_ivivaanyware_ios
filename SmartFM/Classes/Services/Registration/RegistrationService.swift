//
//  RegistrationService.swift
//  Smart Office
//
//  Created by Lasith Hettiarachchi on 3/8/17.
//  Copyright © 2017 fidenz. All rights reserved.
//

import Foundation

class RegistrationService {
    
    fileprivate static var regServiceInstance: RegistrationService?
    fileprivate var isRunning: Bool = false
//    fileprivate var isMasterDataDownloading = false
//    fileprivate var regData: RegistrationData?
    
    fileprivate init() {
        
    }
    
    static func shared() -> RegistrationService {
        
        if let ins = RegistrationService.regServiceInstance {
            return ins
        }
        
        RegistrationService.regServiceInstance = RegistrationService()
        return RegistrationService.regServiceInstance!
    }
    
    //registration
    func registrationStarted() {
        self.isRunning = true
    }
    
    func registrationEnded() {
        self.isRunning = false
    }
    
    func isServiceRunning() -> Bool {
        return self.isRunning
    }
    
//    //master data
//    func masterDataDownloadStarted() {
//        self.isMasterDataDownloading = true
//    }
//    
//    func masterDataDownloadEnded() {
//        self.isMasterDataDownloading = false
//    }
//    
//    func isMasterDataDownloadRunning() -> Bool {
//        return self.isMasterDataDownloading
//    }
//    
//    //reg data
//    func setRegData(regData: RegistrationData?) {
//        
//        self.regData = regData
//    }
//    
//    func getRegData() -> RegistrationData? {
//        
//        return self.regData
//    }
}
