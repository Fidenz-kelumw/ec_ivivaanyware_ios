//
//  SyncManager.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/7/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import OLCOrm

class SyncManager {
    
    fileprivate func syncMasterData(_ completion:((Bool)->Void)?) {
        
        self.notifyMasterDataDownloadStarted()
        
        ImageHandler().clearImageCache(imagesKeepForDays) { (deletedCount) in
            NSLog("CacheCleaner - \(deletedCount) image(s) deleted")
        }
        
        var isBuildings = true, isObjectTypes = true, isFilters = true, isDefaultData = true
        
        self.syncObjectTypes({ (successOt) -> Void in
            
            isObjectTypes = successOt
            
            self.syncBuildings({ (success) in
                
                isBuildings = success
                
                self.syncFilters({ (successFilters) -> Void in
                    
                    isFilters = successFilters
                    
                    self.syncDefaultData({ (successDefData) in
                        
                        isDefaultData = successDefData
                        
                        if isBuildings && isObjectTypes && isFilters && isDefaultData {
                            let syncTime = SyncTime()
                            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Master.rawValue)'")
                            syncTime.EntityKey = EntityKey.Master.rawValue
                            syncTime.UpdatedAt = Date()
                            syncTime.save()
                            
                            completion?(true)
                            self.notifyMasterDataDownloaded()
                        } else {
                            completion?(false)
                            self.notifyMasterDataDownloaded()
                        }
                    })
                    
                })
            })
            
        })
        
    }
    
    
    ///////////////////////////////////
    //MARK: - Master Data
    ///////////////////////////////////
    
    func syncBuildings(_ success:@escaping (Bool)->Void) {
        
        APIClient().getBuildings { (buildings) -> Void in
            
            if let builds = buildings {
                
                if Building.truncateTable() {
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        for newBuilding in builds {
                            if newBuilding.save() {
                                
                                let syncTime = SyncTime()
                                SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Building.rawValue)'")
                                syncTime.EntityKey = EntityKey.Building.rawValue
                                syncTime.UpdatedAt = Date()
                                syncTime.save()
                            }
                        }
                        DispatchQueue.main.async {
                            success(true)
                        }
                    }
                    
                } else {
                    NSLog("[SyncManager] syncBuildings failed to truncate")
                    success(false)
                }
                
                
            } else {
                
                NSLog("[SyncManager] syncBuildings Failed")
                success(false)
            }
        }
    }
    
    func syncObjectTypes(_ success:@escaping (Bool)->Void) {
        
        APIClient().getObjectTypes { (objecttypes) -> Void in
            
            if let objectTypes = objecttypes {
                
                if ObjType.truncateTable() {
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for objecttype in objectTypes {
                            
                            if objecttype.save() {
                                
                                let syncTime = SyncTime()
                                SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectType.rawValue)'")
                                syncTime.EntityKey = EntityKey.ObjectType.rawValue
                                syncTime.UpdatedAt = Date()
                                syncTime.save()
                                
                                if let defDatas = objecttype.CreateFromMenu {
                                    if let type = objecttype.ObjectType {
                                        HomeDefaultData.query("delete from 'Smart_FM.HomeDefaultData' where ObjectType='\(type)'")
                                        for defData in defDatas {
                                            defData.save()
                                        }
                                    }
                                }
                                
                                if let roles = objecttype.roles {
                                    if let type = objecttype.ObjectType {
                                        Role.query("delete from 'Smart_FM.Role' where ObjectType='\(type)'")
                                        for role in roles {
                                            role.save()
                                        }
                                    }
                                }
                                
                                if let stages = objecttype.stages {
                                    if let type = objecttype.ObjectType {
                                        Stage.query("delete from 'Smart_FM.Stage' where ObjectType='\(type)'")
                                        for stage in stages {
                                            stage.save()
                                        }
                                    }
                                }
                                
                                if let tabs = objecttype.tabs {
                                    if let type = objecttype.ObjectType {
                                        Tab.query("delete from 'Smart_FM.Tab' where ObjectType='\(type)'")
                                        for tab in tabs {
                                            tab.save()
                                        }
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "objectTypesDownloaded"), object: self)
                            success(true)
                            return
                        }
                    }
                    
                } else {
                    success(false)
                }
                
                
                
            } else {
                
                NSLog("[SyncManager] syncObjectTypes Failed")
                success(false)
                return
            }
        }
    }
    
    func syncFilters(_ success:@escaping (Bool)->Void) {
        
        APIClient().getFilters { (filts) -> Void in
            
            if let filters = filts {
                
                if Filter.truncateTable() {
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for newFilter in filters {
                            if newFilter.save() {
                                
                                let syncTime = SyncTime()
                                SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Filter.rawValue)'")
                                syncTime.EntityKey = EntityKey.Filter.rawValue
                                syncTime.UpdatedAt = Date()
                                syncTime.save()
                            }
                        }
                        
                        DispatchQueue.main.async {
                            success(true)
                        }
                    }
                    
                } else {
                    NSLog("[SyncManager] syncBuildings failed to truncate")
                    success(false)
                }
                
                
            } else {
                NSLog("[SyncManager] syncFilters Failed")
                success(false)
            }
        }
    }
    
//    func syncSpots(_ success:@escaping (Bool)->Void) {
//
//        APIClient().getSpots { (spots) -> Void in
//
//            if let foundSpots = spots {
//
//                if Spot.truncateTable() {
//
//                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
//
//                        for spot in foundSpots {
//                            if spot.save() {
//
//                                let syncTime = SyncTime()
//                                SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Spots.rawValue)'")
//                                syncTime.EntityKey = EntityKey.Spots.rawValue
//                                syncTime.UpdatedAt = Date()
//                                syncTime.save()
//                            }
//                        }
//
//                        DispatchQueue.main.async {
//                            success(true)
//                        }
//                    }
//                } else {
//                    NSLog("[SyncManager] syncSpots failed to truncate")
//                    success(false)
//                }
//
//            } else {
//                NSLog("[SyncManager] syncSpots Failed")
//                success(false)
//            }
//        }
//    }
    
    func syncDefaultData(_ success:@escaping (Bool)->Void) {
        
        APIClient().getDefaultData { (defs) in
            
            if let defaults = defs {
                
                 
                DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                    if let oldDef = Defaults.all().first as? Defaults {
                        
                        oldDef.LocationKey = defaults.LocationKey
                        oldDef.MasterDataExpiryHours = defaults.MasterDataExpiryHours
                        oldDef.TransactionExpirySeconds = defaults.TransactionExpirySeconds
                        oldDef.GetObjectForQR = defaults.GetObjectForQR
                        oldDef.InboxSize = defaults.InboxSize
                        
                        let userManager = UserManager()
                        userManager.setUserName(defaults.UserName)
                        userManager.setAccountImage(defaults.ImagePath)
                        userManager.setAppName(defaults.AppName)
                        
                        
                        if oldDef.update() {
                            let syncTime = SyncTime()
                            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.DefaultData.rawValue)'")
                            syncTime.EntityKey = EntityKey.DefaultData.rawValue
                            syncTime.UpdatedAt = Date()
                            syncTime.save()
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "defaultDataDownloaded"), object: self)
                                success(true)
                            }
                            
                        } else {
                            DispatchQueue.main.async {
                                success(false)
                            }
                        }
                        
                    } else {
                        
                        NSLog("[SyncManager] Old Defaults objects received through registration not found.")
                        
                        let userManager = UserManager()
                        userManager.setUserName(defaults.UserName)
                        userManager.setAccountImage(defaults.ImagePath)
                        userManager.setAppName(defaults.AppName)
                        
                        defaults.save()
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.DefaultData.rawValue)'")
                        syncTime.EntityKey = EntityKey.DefaultData.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "defaultDataDownloaded"), object: self)
                            success(true)
                        }
                    }
                    
                }
                
            } else {
                NSLog("[SyncManager] syncDefaultData Failed")
                success(false)
            }
        }
    }
    
    ///////////////////////////////////
    //MARK: - Transaction Data
    ///////////////////////////////////
    
    func syncObjectInformation(_ objectType:String, objectKey: String, success:((Bool, String?)->Void)?) {
        
        ObjectInformation.truncateTable()
        ObjectInfo.truncateTable()
        DefaultData.truncateTable()
        DefDataValue.truncateTable()
        OptionsList.truncateTable()
        PageLink.truncateTable()
        FieldData.truncateTable()
        Action.truncateTable()
        
        
        APIClient().getObjectInfo(objectType, objectKey: objectKey) { (objectInfo, error) -> Void in
            
            if let objectInformation = objectInfo {
                
                if ObjectInformation.truncateTable() && ObjectInfo.truncateTable() && DefaultData.truncateTable() && OptionsList.truncateTable() && PageLink.truncateTable() && FieldData.truncateTable() && Action.truncateTable() && DefDataValue.truncateTable() {
                    
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        let objectInformationId = objectInformation.saveAndGetId()
                        
                        if let ois = objectInformation.objectInfo {
                            for oi in ois {
                                oi.ObjectInformationId = objectInformationId
                                let oiId = oi.saveAndGetId()
                                
                                if let dd = oi.defaultData {
                                    
                                    dd.ObjectInfoId = oiId
                                    let ddID = dd.saveAndGetId()
                                    
                                    if let defdatavals = dd.Values {
                                        for attValue in defdatavals {
                                            attValue.defaultDataId = ddID
                                            attValue.save()
                                        }
                                    }
                                }
                                
                                if let optionsLists = oi.OptionList {
                                    
                                    for optionsList in optionsLists {
                                        
                                        optionsList.ObjectInfoId = oiId
                                        optionsList.save()
                                    }
                                }
                            }
                        }
                        
                        if let pageLinks = objectInformation.pageLinks {
                            
                            for pageLink in pageLinks {
                                
                                pageLink.ObjectInformationId = objectInformationId
                                let pageLinkId = pageLink.saveAndGetId()
                                
                                if let fieldDatas = pageLink.fieldData {
                                    
                                    for fieldData in fieldDatas {
                                        fieldData.PageLinkId = pageLinkId
                                        fieldData.save()
                                    }
                                }
                            }
                        }
                        
                        if let actions = objectInformation.actions {
                            
                            for action in actions {
                                
                                action.ObjectInformationId = objectInformationId
                                action.save()
                            }
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectInformation.rawValue)'")
                        syncTime.EntityKey = EntityKey.ObjectInformation.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                    }
                    
                } else {
                    
                    success?(false, error)
                }
                
                
            } else {
                NSLog("[SyncManager] syncFilters Failed")
                success?(false, error)
            }
        }
    }
    
    func syncMembers(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectMembers(objectType, objectKey: objectKey, tabName: tabName) { (objects, error) in
            
            if let members = objects {
                
                if Member.truncateTable() && MemberUser.truncateTable() {
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for member in members {
                            
                            let memberId = member.saveAndGetId()
                            if let user = member.User {
                                user.memberId = memberId
                                user.save()
                            }
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Members.rawValue)'")
                        syncTime.EntityKey = EntityKey.Members.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                        
                    }
                    
                } else {
                    success?(false, error)
                }
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncSubObjects(_ objectType:String, objectKey: String, tabName: String, filter: String?, success:((Bool, String?)->Void)?) {
        
        APIClient().getSubObjects(objectType, objectKey: objectKey, tabName: tabName, filter: filter) { (object, error) in
            
            if let subObject = object {
                
                if SubObjectsMain.truncateTable() {
                    
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        _ = subObject.save()
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.SubObjects.rawValue)'")
                        syncTime.EntityKey = EntityKey.SubObjects.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                        
                    }
                    
                } else {
                    success?(false, error)
                }
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncChecklist(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectChecklist(objectType, objectKey: objectKey, tabName: tabName) { (objects, error) in
            
            if let checkItems = objects {
                
                if CheckItem.truncateTable() {
                    
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for item in checkItems {
                            
                            item.save()
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Checklist.rawValue)'")
                        syncTime.EntityKey = EntityKey.Checklist.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                    }
                    
                } else {
                    success?(false, error)
                }
                
                
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncMessages(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectMessages(objectType, objectKey: objectKey, tabName: tabName) { (object, error) in
            
            if let messageMain = object {
                
                if Message.truncateTable() && MessageUser.truncateTable()  && MessageType.truncateTable() && MessageMain.truncateTable() {
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        let messageMainId = messageMain.saveAndGetId()
                        
                        for item in messageMain.Messages {
                            item.MessagesMainId = messageMainId
                            let msgId = item.saveAndGetId()
                            if let usr = item.CreatedUser {
                                usr.MessageId = msgId
                                usr.save()
                            }
                        }
                        
                        for messagetype in messageMain.MessageTypes {
                            
                            messagetype.MessagesMainId = messageMainId
                            messagetype.save()
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Messages.rawValue)'")
                        syncTime.EntityKey = EntityKey.Messages.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                    }
                    
                } else {
                    success?(false, error)
                }
                
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncLayout(_ objectType:String, objectKey: String, tabName: String, success:((Bool)->Void)?) {
        
        APIClient().getObjectLayout(objectType, objectKey: objectKey, tabName: tabName) { (object) in
            
            if let layout = object {
                
                if Layout.truncateTable() && LayoutPin.truncateTable() && DroppedPin.truncateTable() && PinLabel.truncateTable() {
                    
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        let layoutId = layout.saveAndGetId()
                        
                        layout.pinLabel?.layoutId = layoutId
                        layout.pinLabel?.save()
                        
                        if let pins = layout.pins {
                            for pin in pins {
                                pin.layoutId = layoutId
                                pin.save()
                            }
                        }
                        
                        if let droppedPin = layout.droppedPin {
                            droppedPin.layoutId = layoutId
                            droppedPin.save()
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Layout.rawValue)'")
                        syncTime.EntityKey = EntityKey.Layout.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true)
                        }
                    }
                    
                } else {
                    success?(false)
                }
                
            } else {
                
                success?(false)
            }
        }
    }
    
    func syncAttachments(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectAttachments(objectType, objectKey: objectKey, tabName: tabName) { (object, error) in
            
            if let attachmentMain = object {
                
                 
                DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                    
                    if Attachment.truncateTable() && UploadedUser.truncateTable() && AttachmentType.truncateTable() && AttachmentsMain.truncateTable() {
                        
                        let attrId = attachmentMain.saveAndGetId()
                        
                        for attachment in attachmentMain.attachments {
                            attachment.attMainId = attrId
                            let attachmentId = attachment.saveAndGetId()
                            attachment.User?.AttachmentId = attachmentId
                            attachment.User?.save()
                        }
                        for attachmentType in attachmentMain.AttachmentTypes {
                            attachmentType.attMainId = attrId
                            attachmentType.save()
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.Attachments.rawValue)'")
                        syncTime.EntityKey = EntityKey.Attachments.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                    }
                    
                    DispatchQueue.main.async {
                        success?(true, nil)
                    }
                }
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncUrls(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectURLs(objectType, objectKey: objectKey, tabName: tabName) { (urlObjs, error) in
            
            if let urls = urlObjs {
                
                if ObjectURL.truncateTable() {
                    
                     
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for url in urls {
                            
                            url.save()
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.URLs.rawValue)'")
                        syncTime.EntityKey = EntityKey.URLs.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true, nil)
                        }
                        
                    }
                    
                } else {
                    success?(false, error)
                }
                
            } else {
                
                success?(false, error)
            }
        }
    }
    
    func syncObjectItems(_ objectType:String, objectKey: String, tabName: String, success:((Bool, String?)->Void)?) {
        
        APIClient().getObjectItems(objectType, objectKey: objectKey, tabName: tabName) { (items, error) in
            
            if let objectItems = items {
                
                if ObjectItem.truncateTable() && ValueTypeData.truncateTable() && SwitchTypeData.truncateTable() && ButtonsTypeData.truncateTable() {
                    
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        for objectItem in objectItems {
                            
                            if let oiId = objectItem.saveAndGetId() {
                                if let val = objectItem.valueTypeData {
                                    val.objectItemId = oiId
                                    val.save()
                                } else if let sw = objectItem.switchtypeData {
                                    sw.objectItemId = oiId
                                    sw.save()
                                } else if let btns = objectItem.buttonsTypeData {
                                    for btn in btns {
                                        btn.objectItemId = oiId
                                        btn.save()
                                    }
                                }
                            }
                            
                        }
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ItemList.rawValue)'")
                        syncTime.EntityKey = EntityKey.ItemList.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            
                            success?(true, nil)
                        }
                    }
                    
                } else {
                    success?(false, error)
                }
                
            } else {
                success?(false, error)
            }
        }
    }
    
    fileprivate func truncateFieldsData(_ completion: (()-> Void)?) {
        
         
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            
            ObjectInformation.truncateTable()
            ObjectInfo.truncateTable()
            DefaultData.truncateTable()
            OptionsList.truncateTable()
            PageLink.truncateTable()
            FieldData.truncateTable()
            Action.truncateTable()
            
            DispatchQueue.main.async {
                
                completion?()
            }
        }
    }
    
    func notifyMasterDataDownloaded() {
        
        NotificationCenter.default.post(name: NSNotification.Name.masterDataDownloadingEnded, object: nil)
        ProgressHUD.dismiss()
    }
    
    func notifyMasterDataDownloadStarted() {
        
        NotificationCenter.default.post(name: NSNotification.Name.masterDataDownloadingStarted, object: nil)
        
        ProgressHUD.show("Receiving configuration data", interaction: false)
    }
    
    ///////////////////////////////////
    //MARK: - Temp Data
    ///////////////////////////////////
    
    func syncCreateFields(_ objectType:String, success:((Bool)->Void)?) {
        
        
        self.truncateFieldsData { 
            
            APIClient().getCreateFields(objectType) { (objectInfo) -> Void in
                
                if let objectInformation = objectInfo {
                    
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                        
                        
                        let objectInformationId = objectInformation.saveAndGetId()
                        
                        if let ois = objectInformation.objectInfo {
                            
                            for oi in ois {
                                oi.ObjectInformationId = objectInformationId
                                let oiId = oi.saveAndGetId()
                                
                                if let dd = oi.defaultData {
                                    
                                    dd.ObjectInfoId = oiId
                                    dd.save()
                                }
                                
                                if let optionsLists = oi.OptionList {
                                    
                                    for optionsList in optionsLists {
                                        
                                        optionsList.ObjectInfoId = oiId
                                        optionsList.save()
                                    }
                                }
                            }
                        }
                    
                        
                        if let pageLinks = objectInformation.pageLinks {
                            
                            for pageLink in pageLinks {
                                
                                pageLink.ObjectInformationId = objectInformationId
                                let pageLinkId = pageLink.saveAndGetId()
                                
                                if let fieldDatas = pageLink.fieldData {
                                    
                                    for fieldData in fieldDatas {
                                        fieldData.PageLinkId = pageLinkId
                                        fieldData.save()
                                    }
                                }
                            }
                        }
                        
                        if let actions = objectInformation.actions {
                            
                            for action in actions {
                                
                                action.ObjectInformationId = objectInformationId
                                action.save()
                            }
                        }
                        
                        let syncTime = SyncTime()
                        SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.CreateField.rawValue)'")
                        syncTime.EntityKey = EntityKey.CreateField.rawValue
                        syncTime.UpdatedAt = Date()
                        syncTime.save()
                        
                        DispatchQueue.main.async {
                            success?(true)
                        }
                    }
                    
                } else {
                    NSLog("[SyncManager] syncFilters Failed")
                    success?(false)
                }
            }
        }
        
    }
    
    
    func syncCustomSearchFields(_ objectType:String, location: Building?, success:((Bool)->Void)?) {
        
        self.truncateFieldsData {
            APIClient().getCustomSearchFields(objectType, locationKey: location?.LocationKey) { (objectInfo) -> Void in
                
                if let objectInformation = objectInfo {
                    
                    let objectInformationId = objectInformation.saveAndGetId()
                    
                    if let ois = objectInformation.objectInfo {
                        for oi in ois {
                            oi.ObjectInformationId = objectInformationId
                            let oiId = oi.saveAndGetId()
                            
                            if let dd = oi.defaultData {
                                
                                dd.ObjectInfoId = oiId
                                dd.save()
                            }
                            
                            if let optionsLists = oi.OptionList {
                                
                                for optionsList in optionsLists {
                                    
                                    optionsList.ObjectInfoId = oiId
                                    optionsList.save()
                                }
                            }
                        }
                    }
                    
                    if let pageLinks = objectInformation.pageLinks {
                        
                        for pageLink in pageLinks {
                            
                            pageLink.ObjectInformationId = objectInformationId
                            let pageLinkId = pageLink.saveAndGetId()
                            
                            if let fieldDatas = pageLink.fieldData {
                                
                                for fieldData in fieldDatas {
                                    fieldData.PageLinkId = pageLinkId
                                    fieldData.save()
                                }
                            }
                        }
                    }
                    
                    if let actions = objectInformation.actions {
                        
                        for action in actions {
                            
                            action.ObjectInformationId = objectInformationId
                            action.save()
                        }
                    }
                    
                    let syncTime = SyncTime()
                    SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.CustomSearch.rawValue)'")
                    syncTime.EntityKey = EntityKey.CustomSearch.rawValue
                    syncTime.UpdatedAt = Date()
                    syncTime.save()
                    
                    success?(true)
                    
                    
                } else {
                    NSLog("[SyncManager] syncFilters Failed")
                    success?(false)
                }
            }
        }
    }
    
    func pullObjects(_ objectType: String, locationKey: String?, filterId: String?, customFilter: String?, more: String?, completionHandler: @escaping ([Object]?, String?) -> Void) {
        
        if Object.truncateTable() {
            
            APIClient().getObjects(objectType, locationKey: locationKey, filterId: filterId, customFilter: customFilter, more: more) { (objects, error) -> Void in
                
                if let newObjects = objects {
                    
                    completionHandler(newObjects, error)
                    
                    
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
                        if Object.truncateTable() {
                            
                            for newObject in newObjects {
                                newObject.save()
                            }
                            
                            let syncTime = SyncTime()
                            SyncTime.query("delete from 'Smart_FM.SyncTime' where EntityKey='\(EntityKey.ObjectList.rawValue)'")
                            syncTime.EntityKey = EntityKey.ObjectList.rawValue
                            syncTime.UpdatedAt = Date()
                            syncTime.save()
                            
                        } else {
                            NSLog("Failed to truncate table 'Object'")
                        }
                    })
                    
                } else {
                    
                    completionHandler(nil, error)
                }
            }
            
        } else {
            
            completionHandler(nil, nil)
        }
        
        
    }
    
    
    ///////////////////////////////////
    //MARK: - Background Sync
    ///////////////////////////////////
    
    func syncMasterData(withTimer hasTimer:Bool, completion:((Bool)->Void)?) {
        
        if hasTimer {
            
            if let defaults = Defaults.all() as? [Defaults] {
                if let def = defaults.first, let masterTime = def.MasterDataExpiryHours {
                    
                    if let times = SyncTime.__where("EntityKey='\(EntityKey.Master.rawValue)'", sortBy: "Id", accending: true) as? [SyncTime], let masterLastSynced = times.first, let updatedAt = masterLastSynced.UpdatedAt {
                        
                        if let masterInterval = masterTime.toDouble() {
                            
                            let interval = masterInterval * 60 * 60
                            
                            if interval <= Double(-1 * updatedAt.timeIntervalSinceNow) {
                                
                                self.syncMasterData(completion)
                                return
                                
                            } else {
                                completion?(false)
                            }
                        } else {
                            completion?(false)
                        }
                        
                    } else {
                        completion?(false)
                    }
                    
                } else {
                    
                    self.syncMasterData(completion)
                    return
                }
            } else {
                self.syncMasterData(completion)
                return
            }
            
        } else {
            
            self.syncMasterData(completion)
            return
        }
        
        completion?(false)
    }
    
    func syncObjectInfoWithTimer(_ objectType:String, objectKey: String, success:((Bool)->Void)?) {
        
        if let defaults = Defaults.all() as? [Defaults] {
            if let def = defaults.first, let transTime = def.TransactionExpirySeconds {
                
                if let times = SyncTime.__where("EntityKey='\(EntityKey.ObjectInformation.rawValue)'", sortBy: "Id", accending: true) as? [SyncTime], let transLastSynced = times.first, let updatedAt = transLastSynced.UpdatedAt {
                    
                    if let transInterval = transTime.toDouble() {
                        
                        let interval = transInterval
                        
                        if interval <= Double(-1 * updatedAt.timeIntervalSinceNow) {
                            
                            self.syncObjectInfoWithTimer(objectType, objectKey: objectKey, success: { (stat) in
                                
                                success?(stat)
                            })
                            
                        }
                    }
                    
                }
                
            }
        }
    }
    
}
