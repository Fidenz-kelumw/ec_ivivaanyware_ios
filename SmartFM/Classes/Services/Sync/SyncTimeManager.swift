//
//  SyncTimeManager.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 5/2/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

class SyncTimeManager {
    
    func hasTimerExpired(_ entityKey: EntityKey) -> Bool {
        
        if let defaults = Defaults.all() as? [Defaults] {
            if let def = defaults.first, let transTime = def.TransactionExpirySeconds {
                
                if let times = SyncTime.__where("EntityKey='\(entityKey.rawValue)'", sortBy: "Id", accending: true) as? [SyncTime], let transLastSynced = times.first, let updatedAt = transLastSynced.UpdatedAt {
                    
                    if let transInterval = transTime.toDouble() {
                        
                        let interval = transInterval
                        
                        if interval <= Double(-1 * updatedAt.timeIntervalSinceNow) {
                            
                            return true
                            
                        } else {
                            
                            return false
                        }
                    }
                    
                }
                
            }
        }
        
        return true
    }
}
