//
//  Enums.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/29/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

enum LinkType: String {
    
    case Details="Details", List = "List", Create = "Create"
}

enum PageAction: String {
    
    case Refresh="Refresh",ObjectList="ObjectList",Home="Home", None="none"
}

enum ImageUploadSize: String {
    case Small = "Small", Medium = "Medium", Large = "Large", Original = "Original"
}

enum MediaType: String {
    case IMG = "IMG", VID = "VID", AUD = "AUD"
}

enum InputMode: String {
    case Photo = "PHOTO", Audio = "AUDIO", Signature = "SIG", Video = "VIDEO", File = "FILE"
}

enum ValueType: String {
    case MLT="MLT", SLT = "SLT", DS = "DS", SAS = "SAS", DT="DT", DAT="DAT", BUTTONS, SAAR, FEEDBACK, ATT, IMG, LABEL = "LABEL"
}

enum FeedbackStyle: String {
    case FACES, SMILEY, STARS, THUMBS
}

enum ObjectItemType: String {
    case VALUE, SWITCH, BUTTONS
}
