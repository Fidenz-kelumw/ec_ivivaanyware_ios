//
//  TabSegue.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import UIKit

class TabSegue: UIStoryboardSegue {
    
    override func perform() {
        
        let tabBarController = self.source as! ObjectDetailsViewController
        let destinationController: UIViewController? = self.destination
        
        
        for view in tabBarController.placeHolderView.subviews {
            view.removeFromSuperview()
        }
        
        // Add view to placeholder view
        tabBarController.currentView = destinationController
        tabBarController.placeHolderView.addSubview(destinationController!.view)
        tabBarController.addChildViewController(destinationController!)
        
        // Set autoresizing
        tabBarController.placeHolderView.translatesAutoresizingMaskIntoConstraints = false
        destinationController!.view.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v1]-0-|", options: .alignAllTop, metrics: nil, views: ["v1": destinationController!.view])
        
        tabBarController.placeHolderView.addConstraints(horizontalConstraint)
        
        let verticalConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v1]-0-|", options: .alignAllTop, metrics: nil, views: ["v1": destinationController!.view])
        
        tabBarController.placeHolderView.addConstraints(verticalConstraint)
        
        tabBarController.placeHolderView.layoutIfNeeded()
        destinationController!.didMove(toParentViewController: tabBarController)
        
    }
}
