//
//  TabTypes.swift
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 4/4/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation

enum TabType: String {
    case Info = "Info", Messages="Messages", Members="Members", Attachments="Attachments", Checklist="Checklist", Layout="Layout", Settings="Settings", ItemList="ItemList", URLs="URLs", SubObjects="SubObjects", GPS="gps"
}
