//
//  LIHLog.swift
//  Headcount
//
//  Created by Lasith Hettiarachchi on 10/9/15.
//  Copyright (c) 2015 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
open class LIHLog {
    open var id: Int = 0
    open var type: String = ""
    open var message: String = ""{
        didSet {
            let si = self.message.startIndex
            let ei = self.message.endIndex
            self.message = self.message.replacingOccurrences(of: "'", with: "''", options: NSString.CompareOptions.literal, range: si..<ei)
        }
    }
    open var date: String = ""
    open var time: String = ""
    open var dateTime: Date?
    
    //additional
    open var title: String = ""
    
}
