//
//  LIHLogDateTimeManager.swift
//  Smart Office
//
//  Created by Lasith Hettiarachchi on 10/23/15.
//  Copyright © 2015 fidenz. All rights reserved.
//

import Foundation

class LIHLogDateTimeManager {
    
    static func dateToString(_ date: Date, format: String) -> String {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = format
        let dateString: String = formatter.string(from: date)
        
        return dateString
        
    }
    
    static func stringToDate(_ dateString: String, format: String) -> Date? {
        
        let formatter: DateFormatter = DateFormatter()
        let locale: Locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = format
        if let date: Date = formatter.date(from: dateString) {
            return date
        }
        
        return nil
    }
}
