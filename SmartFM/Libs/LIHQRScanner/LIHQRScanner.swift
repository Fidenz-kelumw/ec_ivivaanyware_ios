//
//  LIHQRSCanner.swift
//  Perx
//
//  Created by Lasith Hettiarachchi on 1/11/16.
//  Copyright © 2016 Lasith Hettiarachchi. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

@objc public protocol LIHQRScannerDelegate {
    
    @objc optional func qrDetected(_ qrString: String?, error: NSError?)
}

open class LIHQRScanner: NSObject, AVCaptureMetadataOutputObjectsDelegate {
    
    fileprivate var captureSession:AVCaptureSession?
    fileprivate var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    open var delegate: LIHQRScannerDelegate?
    fileprivate var initialFramePosition: CGRect?
    
    open var showFrame: Bool = true
    open var frameSize: CGSize = CGSize(width: 150.0, height: 150.0)
    open var frameColor: UIColor = UIColor.white
    open var frameWidth: CGFloat = 2
    open var QRFrame: UIView?
    
    open func initialize(videoContainer avLayer: UIView) {
        
        self.QRFrame?.removeFromSuperview()
        self.QRFrame = UIView(frame: CGRect(origin: avLayer.frame.origin, size: self.frameSize))
        self.QRFrame?.center = avLayer.center
        self.QRFrame?.backgroundColor = UIColor.clear
        self.QRFrame?.layer.borderColor = self.frameColor.cgColor
        self.QRFrame?.layer.borderWidth = frameWidth
        self.QRFrame?.layer.cornerRadius = 3
        avLayer.addSubview(self.QRFrame!)
        self.initialFramePosition = self.QRFrame?.frame
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error:NSError?
        let input: AnyObject!
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        if (error != nil) {
            
            NSLog("\(error?.localizedDescription)")
            return
        }
        
        captureSession = AVCaptureSession()
        captureSession?.addInput(input as! AVCaptureInput)
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = avLayer.layer.bounds
        if let preLayer = videoPreviewLayer {
            avLayer.layer.addSublayer(preLayer)
        }
        
        avLayer.bringSubview(toFront: self.QRFrame!)
        if self.showFrame {
            self.QRFrame?.isHidden = false
        } else {
            self.QRFrame?.isHidden = true
        }
    }
    
    open func startSession(_ completion: (()->())?) {
        
        if let initpos = self.initialFramePosition {
            self.QRFrame?.frame = initpos
        }
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async {
            
            self.captureSession?.startRunning()
            DispatchQueue.main.async {
                completion?()
            }
        }
    }
    
    open func stopSession() {
        
        
        self.captureSession?.stopRunning()
        
    }
    
    open func deInit() {
        
        self.videoPreviewLayer?.removeFromSuperlayer()
    }
    
    //MARK: - MetadataObjectsDelegate
    
    open func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            
            self.QRFrame?.frame = self.initialFramePosition!
            self.delegate?.qrDetected?(nil, error: nil)
            return
        }
        
        // Get the metadata object.
        if let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
        
            if metadataObj.type == AVMetadataObjectTypeQRCode {
                // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
                self.QRFrame?.frame = barCodeObject.bounds
                
                if metadataObj.stringValue != nil {
                    
                    self.delegate?.qrDetected?(metadataObj.stringValue, error: nil)
                    
                } else {
                    
                    self.delegate?.qrDetected?(nil, error: nil)
                }
                
            } else {
                
                self.delegate?.qrDetected?(nil, error: nil)
            }
            
        } else {
            
            self.delegate?.qrDetected?(nil, error: nil)
        }
        
    }
}
