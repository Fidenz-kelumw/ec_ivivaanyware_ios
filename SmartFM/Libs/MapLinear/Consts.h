//
//  Consts.h
//  FXELLE Shopper
//
//  Created by Fidenz on 9/18/14.
//  Copyright (c) 2014 fidenz. All rights reserved.
//

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define appColorTheme 0x131A20
#define appColorTheme2 0xF73737
#define appColorFont 0xffffff
#define progressHudColor 0x000000
#define appColorRed 0xF5393A


#define colorYello 0xFFCC00
#define colorLiteGray 0xDBDDDE
#define colorGray 0x8E8E93
#define colorLiteBlack 0x4A4A4A
#define colorGreen 0x4CD964

#define SMAPIKey @"masterApiKey"
#define SMServiceUrl @"masterServiceUrl"
#define SServiceUrl @"serviceUrl"
#define SAPIKey @"apiKey"

#define SSettingsShown @"firstTimeSettings"
#define SEBody @"emailBody"
#define SETitle @"emailTitle"
#define SHitCount @"maxHitCount"
#define SBeaconLog @"enableBeaconLog"
#define SDLimit @"downloadLimit"

@interface Consts : NSObject

@end
