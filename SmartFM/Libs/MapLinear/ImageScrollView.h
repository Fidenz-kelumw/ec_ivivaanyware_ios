
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapMarker.h"

@protocol MapLinearDelegate <NSObject>

@optional
- (void) pinPressed:(NSString*) pinId;

@end

@interface ImageScrollView : UIScrollView
{
    id <MapLinearDelegate> _delegatePin;
}

@property (nonatomic,strong) id delegatePin;

@property (nonatomic) NSUInteger index;
@property (nonatomic) NSNumber* northAngle;

-(void)setMarkeX:(float)x setY:(float)y image:(UIImage*)image forId:(NSString*)facId;
-(void)moveMarker:(float)newX setY:(float)newY image:(UIImage*)image forId:(NSString*)facilityId;
- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)zScale animated:(BOOL)animated;
-(void)removeUserMarker;

+ (NSUInteger)imageCount;

+ (NSString*)getMapURL;
+ (void) setMapURL:(NSString*)url;
+ (float)getScale;
+ (void) setScale:(float)scale;

//user icon properties
@property (nonatomic, strong) UIView *pointRadius;
@property (nonatomic, strong) UIView *pointDot;
@property (nonatomic, strong) UIView *pointArrow;
@property (nonatomic, strong) NSMutableArray* markers;
//NSMutableArray* markers;


@end
