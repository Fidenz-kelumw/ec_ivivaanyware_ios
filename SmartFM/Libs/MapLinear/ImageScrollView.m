
#import <Foundation/Foundation.h>

#import "ImageScrollView.h"
#import "TilingView.h"
#import "Consts.h"

#define TILE_IMAGES 1  // turn on to use tiled images, if off, we use whole images

// forward declaration of our utility functions
static NSUInteger _ImageCount(void);

#if TILE_IMAGES
static CGSize _ImageSizeAtIndex(NSUInteger index);
static UIImage *_PlaceholderImageNamed(NSString *name);
#endif

#if !TILE_IMAGES
static UIImage *_ImageAtIndex(NSUInteger index);
#endif

static NSString *_ImageNameAtIndex(NSUInteger index);
static NSString* mapURL;
static float scaleFactor;

#pragma mark -
@interface ImageScrollView () <UIScrollViewDelegate,CLLocationManagerDelegate>
{
    UIImageView *_zoomView;  // if tiling, this contains a very low-res placeholder image,
                             // otherwise it contains the full image.
    CGSize _imageSize;

#if TILE_IMAGES
    TilingView *_tilingView;
#endif
        
    CGPoint _pointToCenterAfterResize;
    CGFloat _scaleToRestoreAfterResize;
    BOOL goReverse;
    BOOL isSetAnim;
    CLLocationManager* locManager;
    CLLocationDirection direction;
}
@end
@implementation ImageScrollView
{
    float scale;
//    UIView* marker;
    MapMarker* userArrow;
    MapMarker* userDot;
    MapMarker* userPointRadius;
    
    
//    float x;
//    float y;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;        
    }
    isSetAnim = NO;
    [self initLocationMonitor];
    locManager.delegate = self;
    return self;
}

- (void)setIndex:(NSUInteger)index
{
    _index = index;
    
#if TILE_IMAGES
    [self displayTiledImageNamed:_ImageNameAtIndex(index) size:_ImageSizeAtIndex(index)];
#else
    [self displayImage:_ImageAtIndex(index)];
#endif
}

+ (NSUInteger)imageCount
{
    return _ImageCount();
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    // center the zoom view as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = _zoomView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    _zoomView.frame = frameToCenter;
//    self.zoomScrollView = _zoo
    
    self.bounces = NO;
    [self setBouncesZoom:NO];
    
}

- (void)setFrame:(CGRect)frame
{
    BOOL sizeChanging = !CGSizeEqualToSize(frame.size, self.frame.size);
    
    if (sizeChanging) {
        [self prepareToResize];
    }
    
    [super setFrame:frame];
    
    if (sizeChanging) {
        [self recoverFromResizing];
    }
}


#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _zoomView;
}


#pragma mark - Configure scrollView to display new image (tiled or not)

#if TILE_IMAGES

- (void)displayTiledImageNamed:(NSString *)imageName size:(CGSize)imageSize
{
    // clear views for the previous image
    [_zoomView removeFromSuperview];
    _zoomView = nil;
    _tilingView = nil;
        
    // reset our zoomScale to 1.0 before doing any further calculations
    self.zoomScale = 1.0;
    
    // make views to display the new image
    _zoomView = [[UIImageView alloc] initWithFrame:(CGRect){ CGPointZero, imageSize }];
    [_zoomView setImage:_PlaceholderImageNamed(imageName)];
    [self addSubview:_zoomView];
    
    //mod
    [TilingView setMapURL:mapURL];
    [TilingView setScale:scaleFactor];
    //mod end
    
    _tilingView = [[TilingView alloc] initWithImageName:imageName size:imageSize];
    _tilingView.frame = _zoomView.bounds;
    [_zoomView addSubview:_tilingView];
    
    [self configureForImageSize:imageSize];
}

#endif // TILE_IMAGES

- (void)configureForImageSize:(CGSize)imageSize
{
    _imageSize = imageSize;
    self.contentSize = imageSize;
    [self setMaxMinZoomScalesForCurrentBounds];
    self.zoomScale = self.minimumZoomScale;
}

- (void)setMaxMinZoomScalesForCurrentBounds
{
    CGSize boundsSize = self.bounds.size;
                
    // calculate min/max zoomscale
    CGFloat xScale = boundsSize.width  / _imageSize.width;
    CGFloat yScale = boundsSize.height / _imageSize.height;
    BOOL imagePortrait = _imageSize.height > _imageSize.width;
    BOOL phonePortrait = boundsSize.height > boundsSize.width;
    CGFloat minScale = imagePortrait == phonePortrait ? xScale+100 : MIN(xScale, yScale);
    
    CGFloat maxScale = 1.0 / [[UIScreen mainScreen] scale];

    if (minScale > maxScale) {
        minScale = maxScale;
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    maxScale = (scaleFactor-2)/2;
    if(scaleFactor<=2)
        maxScale =1;
    minScale = screenWidth/(screenHeight*4.0f);
    self.maximumZoomScale = maxScale;
    self.minimumZoomScale = minScale;
}

#pragma mark -
#pragma mark Methods called during rotation to preserve the zoomScale and the visible portion of the image

#pragma mark - Rotation support

- (void)prepareToResize
{
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    _pointToCenterAfterResize = [self convertPoint:boundsCenter toView:_zoomView];

    _scaleToRestoreAfterResize = self.zoomScale;
    
    if (_scaleToRestoreAfterResize <= self.minimumZoomScale + FLT_EPSILON)
        _scaleToRestoreAfterResize = 0;
}

- (void)recoverFromResizing
{
    [self setMaxMinZoomScalesForCurrentBounds];
    CGFloat maxZoomScale = MAX(self.minimumZoomScale, _scaleToRestoreAfterResize);
    self.zoomScale = MIN(self.maximumZoomScale, maxZoomScale);
    CGPoint boundsCenter = [self convertPoint:_pointToCenterAfterResize fromView:_zoomView];

    CGPoint offset = CGPointMake(boundsCenter.x - self.bounds.size.width / 2.0,
                                 boundsCenter.y - self.bounds.size.height / 2.0);

    CGPoint maxOffset = [self maximumContentOffset];
    CGPoint minOffset = [self minimumContentOffset];
    
    CGFloat realMaxOffset = MIN(maxOffset.x, offset.x);
    offset.x = MAX(minOffset.x, realMaxOffset);
    
    realMaxOffset = MIN(maxOffset.y, offset.y);
    offset.y = MAX(minOffset.y, realMaxOffset);
    
    self.contentOffset = offset;
}

- (CGPoint)maximumContentOffset
{
    CGSize contentSize = self.contentSize;
    CGSize boundsSize = self.bounds.size;
    return CGPointMake(contentSize.width - boundsSize.width, contentSize.height - boundsSize.height);
}

- (CGPoint)minimumContentOffset
{
    return CGPointZero;
}

#pragma delegates
-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    scale = scrollView.zoomScale;
//    NSLog(@"%f",scale);
    for(MapMarker* mark in self.markers){
        
        [self setMarker:mark image:mark.image forId:mark.pinId forTag:mark.pinTag];
    }
    //[self setMarker:userArrow forType:2];
}

#pragma private functions
-(void)setMarkeX:(float)mX setY:(float)mY image:(UIImage*)image forId:(NSString*)facId
{
    if(self.markers==nil)
        self.markers = [[NSMutableArray alloc]init];
    
    MapMarker* mark = [[MapMarker alloc]init];
    mark.x = mX;
    mark.y = mY;
    mark.image = image;
    if ([facId  isEqual: @"droppedPin"]) {
        mark.pinTag = self.markers.count + 500;
    } else {
        mark.pinTag = self.markers.count;
    }
    

    mark.viewImage = [[UIButton alloc]init];
    mark.viewImage.clipsToBounds = true;
    //mark.viewImage.frame = CGRectMake(mark.x*scale-20,mark.y*scale-20, 40*1.3, 40*1.3);
    mark.viewImage.frame = CGRectMake(mark.x*scale-image.size.width/2,mark.y*scale-image.size.height, image.size.width, image.size.height);
    //mark.viewImage.frame = CGRectMake(mark.x*self.zoomScale,mark.y*self.zoomScale, image.size.width, image.size.height);
    mark.pinId = facId;
    mark.viewImage.tag = self.markers.count;
    [mark.viewImage setImage:image forState:UIControlStateNormal];
    [mark.viewImage addTarget:self action:@selector(pinTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.markers addObject:mark];
    [self addSubview:mark.viewImage];
    [self bringSubviewToFront:mark.viewImage];
    

}

-(void)setMarker:(MapMarker*)marker image:(UIImage*)image forId:(NSString*)facilityId forTag:(NSUInteger)tag
{
    
    [marker.viewImage removeFromSuperview];
    marker.viewImage = [[UIButton alloc]init];
    //marker.viewImage.frame = CGRectMake(marker.x*scale-20,marker.y*scale-20, 40*1.3, 40*1.3);
    marker.viewImage.frame = CGRectMake(marker.x*scale-image.size.width/2,marker.y*scale-image.size.height, image.size.width, image.size.height);
    //marker.viewImage.frame = CGRectMake(marker.x*self.zoomScale,marker.y*self.zoomScale, image.size.width, image.size.height);
    marker.pinId = facilityId;
    //marker.viewImage.tag = self.markers.count;
    marker.viewImage.tag = tag;
    [marker.viewImage setImage:image forState:UIControlStateNormal];
    [marker.viewImage addTarget:self action:@selector(pinTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:marker.viewImage];
    
}

-(void)moveMarker:(float)newX setY:(float)newY image:(UIImage*)image forId:(NSString*)facilityId
{
    MapMarker *marker = nil;
    for (MapMarker *m in self.markers) {
        if ([m.pinId isEqual:facilityId]) {
            marker = m;
        }
    }
    
    if (marker == nil) {
        [self setMarkeX:newX setY:newY image:image forId:@"droppedPin"];
        for (MapMarker *m in self.markers) {
            if ([m.pinId isEqual:facilityId]) {
                marker = m;
            }
        }
    }
    
    marker.x = newX;
    marker.y = newY;
    NSInteger olderTag = marker.viewImage.tag;
    [marker.viewImage removeFromSuperview];
    marker.viewImage = [[UIButton alloc]init];
    marker.viewImage.tag = olderTag;
    //marker.viewImage.frame = CGRectMake(marker.x*scale-20,marker.y*scale-20, 40*1.3, 40*1.3);
    marker.viewImage.frame = CGRectMake(marker.x*scale-image.size.width/2,marker.y*scale-image.size.height, image.size.width, image.size.height);
    marker.pinId = facilityId;
    //marker.viewImage.tag = self.markers.count;
    [marker.viewImage setImage:image forState:UIControlStateNormal];
    [marker.viewImage addTarget:self action:@selector(pinTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:marker.viewImage];
    
}

-(void)pinTapped:(UIButton*)sender
{
    for(MapMarker* mark in self.markers){
        
        if (mark.pinTag == sender.tag) {
            [[self delegatePin] pinPressed:[mark pinId] ];
        }
    }
    //[[self delegatePin] pinPressed:[self.markers[sender.tag] facilityId] ];
}

-(void)scrollToPoint:(CGFloat)x setY:(CGFloat)y
{
    [self scrollRectToVisible:CGRectMake(x,  y,120, 150) animated:YES];
}

- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)zScale animated:(BOOL)animated
{
    
    //Ensure scale is clamped to the scroll view's allowed zooming range
    zScale = MIN(zScale, self.maximumZoomScale);
    zScale = MAX(zScale, self.minimumZoomScale);
    
    //`zoomToRect` works on the assumption that the input frame is in relation
    //to the content view when zoomScale is 1.0
    
    //Work out in the current zoomScale, where on the contentView we are zooming
    CGPoint translatedZoomPoint = CGPointZero;
    translatedZoomPoint.x = zoomPoint.x + self.contentOffset.x;
    translatedZoomPoint.y = zoomPoint.y + self.contentOffset.y;
    
    //Figure out what zoom scale we need to get back to default 1.0f
    CGFloat zoomFactor = 1.0f / self.zoomScale;
    
    //By multiplying by the zoom factor, we get where we're zooming to, at scale 1.0f;
    translatedZoomPoint.x *= zoomFactor;
    translatedZoomPoint.y *= zoomFactor;
    
    //work out the size of the rect to zoom to, and place it with the zoom point in the middle
    CGRect destinationRect = CGRectZero;
    destinationRect.size.width = CGRectGetWidth(self.frame) / zScale;
    destinationRect.size.height = CGRectGetHeight(self.frame) / zScale;
    destinationRect.origin.x = translatedZoomPoint.x - (CGRectGetWidth(destinationRect) * 0.5f);
    destinationRect.origin.y = translatedZoomPoint.y - (CGRectGetHeight(destinationRect) * 0.5f);
    
    if (animated) {
        [UIView animateWithDuration:0.55f delay:0.0f usingSpringWithDamping:1.0f initialSpringVelocity:0.6f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            [self zoomToRect:destinationRect animated:NO];
        } completion:^(BOOL completed) {
            if ([self.delegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)]) {
                [self.delegate scrollViewDidEndZooming:self withView:[self.delegate viewForZoomingInScrollView:self] atScale:zScale];
            }
        }];
    }
    else {
        [self zoomToRect:destinationRect animated:NO];
    }

    
}

//user icon
- (void) configureLocationPoint
{
    // Setup the ball view.
    self.pointRadius = [[UIView alloc] initWithFrame:CGRectMake(userPointRadius.x*scale-25, userPointRadius.y*scale-25, 50.0, 50.0)];
    self.pointRadius.backgroundColor = UIColorFromRGB(0x1AD6FD);
    self.pointRadius.layer.cornerRadius = 25.0;
    self.pointRadius.layer.borderColor = UIColorFromRGB(0x1AD6FD).CGColor;
    self.pointRadius.layer.borderWidth = 1.0f;
    self.pointRadius.layer.opacity = 0.5f;
//    [self addSubview:self.pointRadius];
    userPointRadius.view = self.pointRadius;
    
    self.pointDot = [[UIView alloc] initWithFrame:CGRectMake(userDot.x*scale-25, userDot.y*scale-25, 20.0, 20.0)];
    self.pointDot.backgroundColor = UIColorFromRGB(0x1D62F0);
    self.pointDot.layer.cornerRadius = 10.0;
    self.pointDot.layer.borderColor = UIColorFromRGB(0x1D62F0).CGColor;
    self.pointDot.layer.borderWidth = 0.0;
    self.pointDot.layer.opacity = 0.8f;
//    [self addSubview:self.pointDot];
    userDot.view=self.pointDot;
    
    self.pointArrow = [[UIImageView alloc] initWithFrame:CGRectMake(userArrow.x*scale-25, userArrow.y*scale-25, 50.0, 50.0)];
    //self.pointArrow.backgroundColor = UIColorFromRGB(0x1AD6FD);
    self.pointArrow.layer.cornerRadius = 25.0;
    //self.pointRadius.layer.borderColor = UIColorFromRGB(0x1AD6FD).CGColor;
    UIImage* img =[UIImage imageNamed:@"pointer.png"];
    
    self.pointArrow.backgroundColor = [UIColor colorWithPatternImage:img];
    self.pointArrow.layer.opacity = 0.8f;
//    [self addSubview:self.pointArrow];
    userArrow.view=self.pointArrow;
    
    goReverse = NO;
    if(!isSetAnim)
    {
        [NSTimer scheduledTimerWithTimeInterval:0.05
                                         target:self
                                       selector:@selector(animatePointRadius:)
                                       userInfo:nil
                                        repeats:YES];
        isSetAnim = YES;
    }
    
    CGPoint pos =  self.pointRadius.layer.position;
    [self.pointRadius.layer setPosition:CGPointMake(pos.x + 0, pos.y + 0)];
    [self.pointDot.layer setPosition:CGPointMake(pos.x + 0, pos.y + 0)];
}

- (void) animatePointRadius:(NSTimer *) timer
{
    float opacity = self.pointRadius.layer.opacity;
    
    if(opacity > 0.9f)
    {
        goReverse = YES;
        
    }
    else if(opacity < 0.2f)
    {
        goReverse = NO;
        
    }
    
    if(goReverse)
    {
        self.pointRadius.layer.opacity -= 0.05f;
    }
    else
    {
        self.pointRadius.layer.opacity += 0.05f;
    }
    
    if(direction!=0)
        [self rotateView:self.pointArrow degrees:direction];
    
    //angle+=5.0f;
    // [self rotateView:self.pointArrow degrees:(angle)];
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    direction = newHeading.magneticHeading;
    [self rotateView:self.pointArrow degrees:direction];
}

#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )
-(void)rotateView:(UIView *)view degrees:(double) degrees
{
    double offset=90;
    CGAffineTransform transform = CGAffineTransformMakeRotation(degreesToRadians([self.northAngle doubleValue]+degrees+offset));
    view.transform = transform;
}

-(void)initLocationMonitor
{
    locManager =[[CLLocationManager alloc]init];
    [locManager setDelegate:self];
    
    double version = [[[UIDevice currentDevice] systemVersion] doubleValue];
    if(version>=8)
    {
        [locManager requestWhenInUseAuthorization];
        [locManager requestAlwaysAuthorization];
    }
    
    if (CLLocationManager.headingAvailable ) {
        [locManager startUpdatingHeading];
    }
}

-(void)removeUserMarker
{
        
    [userArrow.view removeFromSuperview];
    [userDot.view removeFromSuperview];
    [userPointRadius.view removeFromSuperview];
}
//end user icon

#pragma static functions

+ (NSString*)getMapURL {return mapURL;}
+(void)setMapURL:(NSString *)url { mapURL =url; }
+ (float)getScale {return scaleFactor; }
+ (void) setScale:(float)scale { scaleFactor = scale; }

@end


static NSUInteger _ImageCount(void)
{
    return 1;
}

static NSString *_ImageNameAtIndex(NSUInteger index)
{
    return @"Mall Map";
}

#if TILE_IMAGES
static CGSize _ImageSizeAtIndex(NSUInteger index)
{
    return CGSizeMake(256*4*4, 256*4*4);
}

static UIImage *_PlaceholderImageNamed(NSString *name)
{
    NSString* url = [NSString stringWithFormat:@"%@/0/0/0",mapURL];
    UIImage* img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    return img;
}
#endif

