//
//  MapMarker.h
//  FXELLE Shopper
//
//  Created by Lasitha Samarasinghe on 5/27/15.
//  Copyright (c) 2015 fidenz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MapMarker : NSObject

@property float x;
@property float y;
@property UIImage* image;
@property NSUInteger pinTag;
@property UIButton* viewImage;
@property UIView* view;
@property NSString* pinId;

@end