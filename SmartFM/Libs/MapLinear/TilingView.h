
#import <UIKit/UIKit.h>


@interface TilingView : UIView

@property (nonatomic) NSString* mapURL;

- (id)initWithImageName:(NSString *)name size:(CGSize)size;
- (UIImage *)tileForScale:(CGFloat)scale row:(int)row col:(int)col;

+ (NSString*)getMapURL;
+ (void) setMapURL:(NSString*)url;
+ (float)getScale;
+ (void) setScale:(float)scale;

@end
