
#import "TilingView.h"
#import <QuartzCore/CATiledLayer.h>

static NSString* mapURL;
static float scaleFactor;

@implementation TilingView
{
    NSString *_imageName;
    int scl;
}

+ (Class)layerClass
{
	return [CATiledLayer class];
}

- (id)initWithImageName:(NSString *)name size:(CGSize)size
{
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (self) {
        _imageName = name;

        CATiledLayer *tiledLayer = (CATiledLayer *)[self layer];
        tiledLayer.levelsOfDetail = 4.0f;
    }
    return self;
}

// to handle the interaction between CATiledLayer and high resolution screens, we need to
// always keep the tiling view's contentScaleFactor at 1.0. UIKit will try to set it back
// to 2.0 on retina displays, which is the right call in most cases, but since we're backed
// by a CATiledLayer it will actually cause us to load the wrong sized tiles.
//
- (void)setContentScaleFactor:(CGFloat)contentScaleFactor
{
    float val = pow(2, scaleFactor-4);
    [super setContentScaleFactor:val];
//    [super setContentScaleFactor:scaleFactor];
}

- (void)drawRect:(CGRect)rect
{
 	CGContextRef context = UIGraphicsGetCurrentContext();
    
    // get the scale from the context by getting the current transform matrix, then asking
    // for its "a" component, which is one of the two scale components. We could also ask
    // for "d". This assumes (safely) that the view is being scaled equally in both dimensions.
    CGFloat scale = CGContextGetCTM(context).a;
    
    CATiledLayer *tiledLayer = (CATiledLayer *)[self layer];
    CGSize tileSize = tiledLayer.tileSize;

    tileSize.width /= scale;
    tileSize.height /= scale;
    
    // calculate the rows and columns of tiles that intersect the rect we have been asked to draw
    int firstCol = floorf(CGRectGetMinX(rect) / tileSize.width);
    int lastCol = floorf((CGRectGetMaxX(rect)-1) / tileSize.width);
    int firstRow = floorf(CGRectGetMinY(rect) / tileSize.height);
    int lastRow = floorf((CGRectGetMaxY(rect)-1) / tileSize.height);

    for (int row = firstRow; row <= lastRow; row++) {
        for (int col = firstCol; col <= lastCol; col++) {
            UIImage *tile = [self tileForScale:scale row:row col:col];
            CGRect tileRect = CGRectMake(tileSize.width * col, tileSize.height * row,
                                         tileSize.width, tileSize.height);

            // if the tile would stick outside of our bounds, we need to truncate it so as
            // to avoid stretching out the partial tiles at the right and bottom edges
            tileRect = CGRectIntersection(self.bounds, tileRect);
            
            [tile drawInRect:tileRect];            
        }
    }
    
}

- (UIImage *)tileForScale:(CGFloat)scale row:(int)row col:(int)col
{
    UIImage* image;
    int s=scaleFactor;
    int count = 0;
    
    while(count<scaleFactor-3)
    {
        float val = powf(2, count);
        if(val==scale)
        {
            s = 4+count;
            break;
        }
        else
        {
            count++;
        }
    }
    
    if(scale==0.25)
        s = 2;
    else if(scale==0.5)
        s = 3;
    //    else if(scale==1)
    //        s = 4;
    //    else if(scale==2)
    //        s = 5;
    //    else if(scale==4)
    //        s = 6;
    
    NSString* url =[NSString stringWithFormat:@"%@/%d/%d/%d",mapURL, s,col,row];
//    NSLog(@"%d in %f",s,scale);
    image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    //mod end
    
    return image;
}

#pragma private functions

-(void)addMarker
{
    UIView* view = [[UIView alloc]init];
    view.frame = CGRectMake(50, 50, 10, 10);
    view.backgroundColor = [UIColor redColor];
    [self addSubview:view];
}

#pragma static functions
+ (NSString*)getMapURL {return mapURL;}
+ (void) setMapURL:(NSString*)url { mapURL = url; }
+ (float)getScale {return scaleFactor;}
+ (void) setScale:(float)scale { scaleFactor = scale; }

@end
