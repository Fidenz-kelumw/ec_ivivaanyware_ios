//
//  AVAsset+VideoOrientation.m
//  SmartFM
//
//  Created by Lasith Hettiarachchi on 1/12/17.
//  Copyright © 2017 Lasith Hettiarachchi. All rights reserved.
//

#import <Foundation/Foundation.h>

//
//  AVAsset+VideoOrientation.m
//
//  Created by Luca Bernardi on 19/09/12.
//  Copyright (c) 2012 Luca Bernardi. All rights reserved.
//

#import "AVAsset+VideoOrientation.h"


static inline CGFloat RadiansToDegrees(CGFloat radians) {
    return radians * 180 / M_PI;
};

@implementation AVAsset (VideoOrientation)
    @dynamic videoOrientation;
    
- (LBVideoOrientation)videoOrientation
    {
        NSArray *videoTracks = [self tracksWithMediaType:AVMediaTypeVideo];
        if ([videoTracks count] == 0) {
            return LBVideoOrientationNotFound;
        }
        
        AVAssetTrack* videoTrack    = [videoTracks objectAtIndex:0];
        CGAffineTransform txf       = [videoTrack preferredTransform];
        CGFloat videoAngleInDegree  = RadiansToDegrees(atan2(txf.b, txf.a));
        
        LBVideoOrientation orientation = 0;
        switch ((int)videoAngleInDegree) {
            case 0:
            orientation = LBVideoOrientationRight;
            break;
            case 90:
            orientation = LBVideoOrientationUp;
            break;
            case 180:
            orientation = LBVideoOrientationLeft;
            break;
            case -90:
            orientation	= LBVideoOrientationDown;
            break;
            default:
            orientation = LBVideoOrientationNotFound;
            break;
        }
        
        return orientation;
    }
    
    @end
